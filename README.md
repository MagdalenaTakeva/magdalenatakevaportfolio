# Java Workshops
JAVA Workshops done while attending Telerik Alpha QA Academy
# Functional testing using C# and Selenium WebDriver of e-commerce website
AUT available live at: https://ecommerce-playground.lambdatest.io/ 
# C# Workshops 
<ul>
<li>PersonAndChild</li>
<li>LybraryTypesOfBooks</li>
<li>MSTest Project</li>
</ul>

# RestAssured and WireMock Workshops
Excercises in Java using RestAssured and WireMock
# Selenium Tutorials
Excercises in Java using Selenium WebDriver
# SQL Tasks
<ul>
<li>Installed MariaDB</li>
<li>Created Company database (Company sql file provided)</li>
<li>Created ConnectToMariaDB project to test Company database</li>
</ul>

# TestNG_Project
Excercises in Java using TestNG 