# Company Database Tests
To run tests successfully you need to:
1. Install MariaDB
- Follow instructions at https://www.mariadbtutorial.com/getting-started/install-mariadb/
2. Log into MariaDB
3. Create Company Database from Company SQL file on Windows, Ubuntu and Linux
- Follow instructions at https://databasefaqs.com/mariadb-create-database-from-sql-file/
