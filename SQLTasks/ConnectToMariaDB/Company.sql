-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for company
CREATE DATABASE IF NOT EXISTS `company` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `company`;

-- Dumping structure for table company.branch
CREATE TABLE IF NOT EXISTS `branch` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(40) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `manager_start_date` date DEFAULT NULL,
  PRIMARY KEY (`branch_id`),
  KEY `manager_id` (`manager_id`),
  CONSTRAINT `branch_ibfk_1` FOREIGN KEY (`manager_id`) REFERENCES `employee` (`employee_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.branch: ~3 rows (approximately)
DELETE FROM `branch`;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` (`branch_id`, `branch_name`, `manager_id`, `manager_start_date`) VALUES
	(1, 'Corporate', 100, '2006-02-09'),
	(2, 'Scranton', 102, '1992-04-06'),
	(3, 'Stamford', 106, '1998-02-13');
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;

-- Dumping structure for table company.branch_supplier
CREATE TABLE IF NOT EXISTS `branch_supplier` (
  `branch_id` int(11) NOT NULL,
  `supplier_name` varchar(40) NOT NULL,
  `supplier_type` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`branch_id`,`supplier_name`),
  CONSTRAINT `branch_supplier_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.branch_supplier: ~7 rows (approximately)
DELETE FROM `branch_supplier`;
/*!40000 ALTER TABLE `branch_supplier` DISABLE KEYS */;
INSERT INTO `branch_supplier` (`branch_id`, `supplier_name`, `supplier_type`) VALUES
	(2, 'BBB', 'Wood'),
	(2, 'Hammer Mill', 'Paper'),
	(2, 'J.T. Forms & Labels', 'Custom Forms'),
	(2, 'Uni-ball', 'Writing Utensils'),
	(3, 'Patriot Paper', 'Paper'),
	(3, 'Stamford Lables', 'Custom Forms'),
	(3, 'Uni-ball', 'Writing Utensils');
/*!40000 ALTER TABLE `branch_supplier` ENABLE KEYS */;

-- Dumping structure for table company.client
CREATE TABLE IF NOT EXISTS `client` (
  `client_id` int(11) NOT NULL,
  `client_name` varchar(40) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`client_id`),
  KEY `branch_id` (`branch_id`),
  CONSTRAINT `client_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.client: ~7 rows (approximately)
DELETE FROM `client`;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` (`client_id`, `client_name`, `branch_id`) VALUES
	(400, 'Dunmore Highschool', 2),
	(401, 'Lackawana Country', 2),
	(402, 'FedEx', 3),
	(403, 'John Daly Law, LLC', 3),
	(404, 'Scranton Whitepages', 2),
	(405, 'Times Newspaper', 3),
	(406, 'FedEx', 2);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

-- Dumping structure for table company.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` int(11) NOT NULL,
  `first_name` varchar(40) DEFAULT NULL,
  `last_name` varchar(40) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `salary` int(11) DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `branch_id` (`branch_id`),
  KEY `supervisor_id` (`supervisor_id`),
  KEY `index1` (`first_name`,`last_name`,`salary`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE SET NULL,
  CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`supervisor_id`) REFERENCES `employee` (`employee_id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.employee: ~10 rows (approximately)
DELETE FROM `employee`;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`employee_id`, `first_name`, `last_name`, `birthday`, `sex`, `salary`, `supervisor_id`, `branch_id`) VALUES
	(100, 'David', 'Wallace', '1967-11-17', 'M', 250000, NULL, 1),
	(101, 'Jan', 'Levinson', '1961-05-11', 'F', 110000, 100, 1),
	(102, 'Michael', 'Scott', '1964-03-15', 'M', 75000, 100, 2),
	(103, 'Angela', 'Martin', '1971-06-25', 'F', 63000, 102, 2),
	(104, 'Kelly', 'Kapoor', '1980-02-05', 'F', 55000, 102, 2),
	(105, 'Stanley', 'Hudson', '1958-02-19', 'M', 69000, 102, 2),
	(106, 'Josh', 'Porter', '1969-09-05', 'M', 78000, 100, 3),
	(107, 'Andy', 'Bernard', '1973-07-22', 'M', 65000, 106, 3),
	(108, 'Jim', 'Halpert', '1978-10-01', 'M', 71000, 106, 3),
	(109, 'Oscar', 'Lilov', '2000-01-01', 'M', 69000, 106, 3);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;

-- Dumping structure for function company.EmployeeSalaryLevel
DELIMITER //
CREATE FUNCTION `EmployeeSalaryLevel`(`salary` INT(11)
) RETURNS varchar(40) CHARSET latin1
BEGIN
DECLARE employeeSalaryLevel VARCHAR(40);
IF (salary >= 100001) Then
SET employeeSalaryLevel = 'Large salary';
ELSEIF (salary >= 60001 AND salary <= 100000) THEN
SET employeeSalaryLevel = 'Medium salary';
ELSE
SET employeeSalaryLevel = 'Low salary';
END if;
return employeeSalaryLevel;

END//
DELIMITER ;

-- Dumping structure for table company.employee_audit
CREATE TABLE IF NOT EXISTS `employee_audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `employee_lastname` varchar(50) NOT NULL,
  `changedate` datetime DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table company.employee_audit: ~3 rows (approximately)
DELETE FROM `employee_audit`;
/*!40000 ALTER TABLE `employee_audit` DISABLE KEYS */;
INSERT INTO `employee_audit` (`id`, `employee_id`, `employee_lastname`, `changedate`, `action`) VALUES
	(1, 109, 'Martinez', '2023-10-12 17:52:28', 'update'),
	(3, 109, 'Phan', '2023-12-06 14:21:31', 'update'),
	(4, 109, 'Lilov', '2023-12-06 14:49:51', 'update');
/*!40000 ALTER TABLE `employee_audit` ENABLE KEYS */;

-- Dumping structure for procedure company.getCountOfEmployeesBySex
DELIMITER //
CREATE PROCEDURE `getCountOfEmployeesBySex`(
	OUT `Employees_count` int,
	IN `gender` varchar(1)
)
BEGIN
    select count(*) INTO Employees_count from employee where sex = gender ;
END//
DELIMITER ;

-- Dumping structure for procedure company.getCountOfMalesFemalesSupervisedBySupervisor
DELIMITER //
CREATE PROCEDURE `getCountOfMalesFemalesSupervisedBySupervisor`(
	IN supervisorId int(11),
	OUT supervised_employees INT,
	OUT supervised_males INT,
	OUT supervised_females INT)
BEGIN

	-- ---total Employee count
	SELECT COUNT(*) INTO supervised_employees FROM employee WHERE supervisor_id = supervisorId;
	
-- 	---males count
	SELECT COUNT(*) into supervised_males FROM employee WHERE sex = "M" AND supervisor_id = supervisorId;
	
	-- --females count
	SELECT COUNT(*) into supervised_females FROM employee WHERE sex = "F" AND supervisor_id = supervisorId;
	
END//
DELIMITER ;

-- Dumping structure for procedure company.getCountOfMalesFemalesSupervisedBySupervisorId
DELIMITER //
CREATE PROCEDURE `getCountOfMalesFemalesSupervisedBySupervisorId`(
	IN supervisorId int(11),
	OUT id INT(11),
	OUT supervisor_name VARCHAR(100),
	OUT supervised_employees INT,
	OUT supervised_males INT,
	OUT supervised_females INT)
BEGIN

	DECLARE supervisorNum INT(11);
	
	SELECT employee_id INTO id from employee WHERE employee_id = supervisorId;

	SELECT employee_id INTO supervisorNum from employee WHERE employee_id = supervisorId;
		case supervisorNum 
			when 100 then SET supervisor_name = "David Wallace";
			when 102 then SET supervisor_name = "Michael Scott";
			when 106 then SET supervisor_name = "Josh Porter";
			ELSE SET supervisor_name = "not a supervisor";
		END case;
	
	-- ---total Employee count
	SELECT COUNT(*) INTO supervised_employees FROM employee WHERE supervisor_id = supervisorId;


	
-- 	---males count
	SELECT COUNT(*) into supervised_males FROM employee WHERE sex = "M" AND supervisor_id = supervisorId;
	
	-- --females count
	SELECT COUNT(*) into supervised_females FROM employee WHERE sex = "F" AND supervisor_id = supervisorId;
	
END//
DELIMITER ;

-- Dumping structure for procedure company.GetEmployeeSalaryLevel
DELIMITER //
CREATE PROCEDURE `GetEmployeeSalaryLevel`(
	IN `employeeId` INT(11),
	OUT `employeeSalaryLevel` VARCHAR(40)
)
BEGIN

DECLARE employeeSalary INT(11);

-- get salary of an employee
SELECT salary
INTO employeeSalary
FROM employee
WHERE employee_id = employeeId;

-- call the function
SET employeeSalaryLevel = EmployeeSalaryLevel(employeeSalary);
END//
DELIMITER ;

-- Dumping structure for procedure company.GetEmployeeSalaryLevel_2
DELIMITER //
CREATE PROCEDURE `GetEmployeeSalaryLevel_2`(
	IN `employeeId` INT(11),
	OUT `firstName` VARCHAR(40),
	OUT `lastName` VARCHAR(40),
	OUT `employeeSalaryLevel` VARCHAR(40)
)
BEGIN

DECLARE employeeSalary INT(11);

-- get salary of an employee

SELECT first_name INTO firstName
FROM employee
WHERE employee_id = employeeId;

SELECT last_name INTO lastName
FROM employee
WHERE employee_id = employeeId;

SELECT salary INTO employeeSalary
FROM employee
WHERE employee_id = employeeId;

-- call the function
SET employeeSalaryLevel = EmployeeSalaryLevel(employeeSalary);
END//
DELIMITER ;

-- Dumping structure for procedure company.getSupervisorNameByEmployeeId
DELIMITER //
CREATE PROCEDURE `getSupervisorNameByEmployeeId`(
	IN employeeId int(11),
	OUT supervisorName VARCHAR(100)
)
BEGIN

	DECLARE supervisorId INT(11);

	SELECT employee_id INTO supervisorId from employee WHERE employee_id = employeeId;
		case supervisorId
			when 100 then SET supervisorName = "David Wallace";
			when 102 then SET supervisorName = "Michael Scott";
			when 106 then SET supervisorName = "Josh Porter";
			ELSE SET supervisorName = "Not a supervisor";
			END case;
	
END//
DELIMITER ;

-- Dumping structure for procedure company.GetTotalMalesCount
DELIMITER //
CREATE PROCEDURE `GetTotalMalesCount`(
	OUT `TotalMalesCount` int
)
BEGIN
	SELECT COUNT(sex) into TotalMalesCount FROM employee WHERE employee.sex = 'M';
END//
DELIMITER ;

-- Dumping structure for function company.hello2
DELIMITER //
CREATE FUNCTION `hello2`(s CHAR(20)) RETURNS char(50) CHARSET utf8mb3 COLLATE utf8mb3_bin
    DETERMINISTIC
RETURN CONCAT('Hello, ',s,'!')//
DELIMITER ;

-- Dumping structure for procedure company.InsertSupplierName
DELIMITER //
CREATE PROCEDURE `InsertSupplierName`(IN inBranchId INT, IN inSupplierName VARCHAR(40), IN inSupplierType VARCHAR(40))
BEGIN
	-- exit if the DUPLICATE KEY occurs
	DECLARE exit handler FOR 1602	SELECT 'Duplicate key error encountered' Message;
	DECLARE exit handler FOR SQLEXCEPTION SELECT 'SQLEXception encountered' Message;
	DECLARE exit handler FOR SQLSTATE '23000' SELECT 'SQLSTATE 23000' ErrorCode;
	
	-- insert a nnew row into the branch_supplier
	INSERT INTO branch_supplier(branch_id, supplier_name, supplier_type) VALUES(inBranchId, inSupplierName, inSupplierType);
	
	-- return the supplier_type supplied by supplier_name
	SELECT COUNT(*) FROM branch_supplier WHERE supplier_name = inSupplierName;
	
END//
DELIMITER ;

-- Dumping structure for view company.managers_names
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `managers_names` (
	`Employees that are managers` VARCHAR(81) NULL COLLATE 'latin1_swedish_ci',
	`manager_id` INT(11) NULL
) ENGINE=MyISAM;

-- Dumping structure for table company.members
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table company.members: ~2 rows (approximately)
DELETE FROM `members`;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` (`id`, `full_name`, `email`, `birthDate`) VALUES
	(1, 'John', 'john@example.com', NULL),
	(2, 'Kim', 'kim@example.com', '2012-05-03');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;

-- Dumping structure for table company.new_users
CREATE TABLE IF NOT EXISTS `new_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) DEFAULT NULL,
  `last_name` varchar(40) DEFAULT 'Takeva',
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`first_name`),
  UNIQUE KEY `name_2` (`first_name`),
  UNIQUE KEY `first_name` (`first_name`),
  UNIQUE KEY `first_name_2` (`first_name`),
  UNIQUE KEY `first_name_3` (`first_name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table company.new_users: ~4 rows (approximately)
DELETE FROM `new_users`;
/*!40000 ALTER TABLE `new_users` DISABLE KEYS */;
INSERT INTO `new_users` (`id`, `first_name`, `last_name`, `count`) VALUES
	(5, 'Joe', 'Takeva', 2),
	(7, 'Magdalena', 'Takeva', 7),
	(10, NULL, 'Takeva', 1),
	(13, 'Marina', 'Takeva', 2);
/*!40000 ALTER TABLE `new_users` ENABLE KEYS */;

-- Dumping structure for table company.reminders
CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`memberId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table company.reminders: ~1 rows (approximately)
DELETE FROM `reminders`;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
INSERT INTO `reminders` (`id`, `memberId`, `message`) VALUES
	(1, 1, 'Hi John, please update your date of birth.');
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;

-- Dumping structure for table company.salaries
CREATE TABLE IF NOT EXISTS `salaries` (
  `employeeNumber` int(11) NOT NULL,
  `validFrom` date NOT NULL,
  `salary` decimal(12,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`employeeNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.salaries: ~0 rows (approximately)
DELETE FROM `salaries`;
/*!40000 ALTER TABLE `salaries` DISABLE KEYS */;
/*!40000 ALTER TABLE `salaries` ENABLE KEYS */;

-- Dumping structure for table company.salaryarchives
CREATE TABLE IF NOT EXISTS `salaryarchives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeNumber` int(11) DEFAULT NULL,
  `validFrom` date NOT NULL,
  `salary` decimal(12,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table company.salaryarchives: ~3 rows (approximately)
DELETE FROM `salaryarchives`;
/*!40000 ALTER TABLE `salaryarchives` DISABLE KEYS */;
INSERT INTO `salaryarchives` (`id`, `employeeNumber`, `validFrom`, `salary`) VALUES
	(1, 1002, '2000-01-01', 50000.00),
	(2, 1056, '2000-01-01', 60000.00),
	(3, 1076, '2000-01-01', 70000.00);
/*!40000 ALTER TABLE `salaryarchives` ENABLE KEYS */;

-- Dumping structure for table company.salarybudgets
CREATE TABLE IF NOT EXISTS `salarybudgets` (
  `total` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.salarybudgets: ~1 rows (approximately)
DELETE FROM `salarybudgets`;
/*!40000 ALTER TABLE `salarybudgets` DISABLE KEYS */;
INSERT INTO `salarybudgets` (`total`) VALUES
	(0.00);
/*!40000 ALTER TABLE `salarybudgets` ENABLE KEYS */;

-- Dumping structure for table company.sales
CREATE TABLE IF NOT EXISTS `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `fiscalYear` smallint(6) NOT NULL,
  `fiscalMonth` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product` (`product`,`fiscalYear`,`fiscalMonth`),
  CONSTRAINT `CONSTRAINT_1` CHECK (`fiscalMonth` >= 1 and `fiscalMonth` <= 12),
  CONSTRAINT `CONSTRAINT_2` CHECK (`fiscalYear` between 2000 and 2050),
  CONSTRAINT `CONSTRAINT_3` CHECK (`quantity` >= 0)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table company.sales: ~3 rows (approximately)
DELETE FROM `sales`;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` (`id`, `product`, `quantity`, `fiscalYear`, `fiscalMonth`) VALUES
	(1, '2003 Harley_Davidson Eagle Drag Bike', 440, 2020, 1),
	(2, '1969 Corvair Monza', 165, 2020, 1),
	(3, '1970 Playmouth Hemi Cuda', 220, 2020, 1);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;

-- Dumping structure for table company.saleschanges
CREATE TABLE IF NOT EXISTS `saleschanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salesId` int(11) DEFAULT NULL,
  `beforeQuantity` int(11) DEFAULT NULL,
  `afterQuantity` int(11) DEFAULT NULL,
  `changedAt` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table company.saleschanges: ~4 rows (approximately)
DELETE FROM `saleschanges`;
/*!40000 ALTER TABLE `saleschanges` DISABLE KEYS */;
INSERT INTO `saleschanges` (`id`, `salesId`, `beforeQuantity`, `afterQuantity`, `changedAt`) VALUES
	(1, 1, 150, 400, '2023-12-06 23:40:16'),
	(2, 1, 400, 440, '2023-12-06 23:49:27'),
	(3, 2, 150, 165, '2023-12-06 23:49:27'),
	(4, 3, 200, 220, '2023-12-06 23:49:27');
/*!40000 ALTER TABLE `saleschanges` ENABLE KEYS */;

-- Dumping structure for procedure company.SelectAllEmployees
DELIMITER //
CREATE PROCEDURE `SelectAllEmployees`()
BEGIN
	SELECT * FROM employee;
END//
DELIMITER ;

-- Dumping structure for procedure company.SelectAllEmployeesByFirstAndLastName
DELIMITER //
CREATE PROCEDURE `SelectAllEmployeesByFirstAndLastName`(IN firstName VARCHAR(40), IN lastName VARCHAR(40) )
BEGIN
	SELECT * FROM employee WHERE first_name = firstName AND last_name = lastName;
END//
DELIMITER ;

-- Dumping structure for procedure company.SelectAllEmployeesByGender
DELIMITER //
CREATE PROCEDURE `SelectAllEmployeesByGender`(IN gender VARCHAR(1))
BEGIN

SELECT * FROM employee WHERE sex = Upper(gender);

END//
DELIMITER ;

-- Dumping structure for procedure company.SelectAllEmployeesById
DELIMITER //
CREATE PROCEDURE `SelectAllEmployeesById`(IN id int(11))
BEGIN
	SELECT * FROM employee WHERE employee_id = id;
END//
DELIMITER ;

-- Dumping structure for procedure company.SelectAllFromBranchSupplier
DELIMITER //
CREATE PROCEDURE `SelectAllFromBranchSupplier`()
BEGIN
	SELECT * FROM branch_supplier;
END//
DELIMITER ;

-- Dumping structure for table company.trigger_test
CREATE TABLE IF NOT EXISTS `trigger_test` (
  `trigger_id` int(11) DEFAULT NULL,
  `message` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.trigger_test: ~6 rows (approximately)
DELETE FROM `trigger_test`;
/*!40000 ALTER TABLE `trigger_test` DISABLE KEYS */;
INSERT INTO `trigger_test` (`trigger_id`, `message`) VALUES
	(109, 'Oscar Martinez MALE with employee_Id = 109 was added in the employee table'),
	(110, 'Oscar Martinez MALE with employee_Id = 110 was added in the employee table'),
	(110, 'Oscar Martinez MALE with employee_Id = 110 was added in the employee table'),
	(110, 'Oscar Martinez MALE with employee_Id = 110 was added in the employee table'),
	(110, 'Oscar Martinez MALE with employee_Id = 110 was added in the employee table'),
	(110, 'Oscar Martinez MALE with employee_Id = 110 was added in the employee table');
/*!40000 ALTER TABLE `trigger_test` ENABLE KEYS */;

-- Dumping structure for table company.workcenters
CREATE TABLE IF NOT EXISTS `workcenters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wc_name` varchar(100) NOT NULL,
  `wc_capacity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table company.workcenters: ~2 rows (approximately)
DELETE FROM `workcenters`;
/*!40000 ALTER TABLE `workcenters` DISABLE KEYS */;
INSERT INTO `workcenters` (`id`, `wc_name`, `wc_capacity`) VALUES
	(1, 'Mold Machine', 100),
	(2, 'Packing', 200);
/*!40000 ALTER TABLE `workcenters` ENABLE KEYS */;

-- Dumping structure for table company.workcenterstats
CREATE TABLE IF NOT EXISTS `workcenterstats` (
  `totalCapacity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.workcenterstats: ~1 rows (approximately)
DELETE FROM `workcenterstats`;
/*!40000 ALTER TABLE `workcenterstats` DISABLE KEYS */;
INSERT INTO `workcenterstats` (`totalCapacity`) VALUES
	(300);
/*!40000 ALTER TABLE `workcenterstats` ENABLE KEYS */;

-- Dumping structure for table company.works_with
CREATE TABLE IF NOT EXISTS `works_with` (
  `employee_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `total_sales` int(11) DEFAULT NULL,
  PRIMARY KEY (`employee_id`,`client_id`),
  KEY `client_id` (`client_id`),
  CONSTRAINT `works_with_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE,
  CONSTRAINT `works_with_ibfk_2` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table company.works_with: ~9 rows (approximately)
DELETE FROM `works_with`;
/*!40000 ALTER TABLE `works_with` DISABLE KEYS */;
INSERT INTO `works_with` (`employee_id`, `client_id`, `total_sales`) VALUES
	(102, 401, 267000),
	(102, 406, 15000),
	(105, 400, 55000),
	(105, 404, 33000),
	(105, 406, 130000),
	(107, 403, 5000),
	(107, 405, 26000),
	(108, 402, 22500),
	(108, 403, 12000);
/*!40000 ALTER TABLE `works_with` ENABLE KEYS */;

-- Dumping structure for trigger company.after_members_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER after_members_insert AFTER INSERT ON members FOR EACH ROW
BEGIN
if NEW.birthDate IS NULL then INSERT INTO reminders(memberId, message) VALUES(NEW.id, CONCAT('Hi ', NEW.full_name, ', please update your date of birth.'));
END if;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger company.after_salaries_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER after_salaries_delete AFTER DELETE ON salaries FOR EACH ROW
BEGIN
UPDATE salarybudgets SET total = total - OLD.salary;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger company.after_sales_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER after_sales_update AFTER UPDATE ON sales FOR EACH ROW
BEGIN
	if OLD.quantity <> NEW.quantity then
		INSERT INTO salesChanges(salesId, beforeQuantity, afterQuantity)
		VALUES(OLD.id, OLD.quantity, NEW.quantity);
	END if;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger company.before_employee_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER before_employee_update 
    BEFORE UPDATE ON employee
    FOR EACH ROW BEGIN 
 INSERT INTO employee_audit
 SET  ACTION = 'update',    
 employee_id = OLD.employee_id,
 employee_lastname = OLD.last_name,
 changedate = NOW();
 END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger company.before_sales_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER before_sales_update BEFORE UPDATE ON sales FOR EACH ROW
BEGIN
DECLARE errorMessage VARCHAR(255);
SET errorMessage = CONCAT('The new quantity ', NEW.quantity, ' cannot be 3 times greater than the current quantity ', OLD.quantity);
if NEW.quantity >= OLD.quantity * 3 Then 
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = errorMessage;
END if;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger company.before_workcenters_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER before_workcenters_insert BEFORE INSERT ON workcenters FOR EACH ROW
BEGIN
DECLARE rowcount INT;
SELECT COUNT(*) INTO rowcount FROM workcenters;
if rowcount > 0 then
	UPDATE workcenterstats SET totalCapacity = totalCapacity + NEW.wc_capacity;
ELSE
	INSERT INTO workcenterstats(totalCapacity) VALUES(NEW.wc_capacity);
END if;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger company.my_trigger
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER my_trigger BEFORE INSERT
    ON employee
    FOR EACH ROW BEGIN
    if NEW.sex = 'M' THEN
        INSERT INTO trigger_test VALUE
            (NEW.employee_id ,CONCAT(NEW.first_name, ' ',NEW.last_name,' ', 'MALE', ' with employee_Id = ',NEW.employee_id, ' was added in the employee table' ));
    ELSEIF NEW.sex = 'F' THEN
        INSERT INTO trigger_test VALUE
            (NEW.employee_id ,CONCAT(NEW.first_name, ' ',NEW.last_name,' ', 'FEMALE', ' with employee_Id = ',NEW.employee_id, ' was added in the employee table' ));
    ELSE INSERT INTO trigger_test VALUE
             (NEW.employee_id ,CONCAT(NEW.first_name, ' ',NEW.last_name,' ', ' with employee_Id = ',NEW.employee_id, ' was added in the employee table' ));
    END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for view company.managers_names
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `managers_names`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `managers_names` AS SELECT Concat(e.first_name,' ', e.last_name) AS "Employees that are managers", b.manager_id
FROM employee AS e
left JOIN branch AS b
ON e.employee_id = b.manager_id ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
