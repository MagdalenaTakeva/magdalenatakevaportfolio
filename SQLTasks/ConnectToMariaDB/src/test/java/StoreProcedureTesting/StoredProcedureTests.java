package StoreProcedureTesting;

import DBTests.BasePageTests;
import Database.Queries;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static Database.Queries.selectEmployeeInfoById;
import static Database.UtilityMethods.compareResultSets;
import static java.sql.Types.INTEGER;
import static java.sql.Types.VARCHAR;

/* Syntax
{ call procedure_name() }         Accept no parameters and return no value
{ call procedure_name(?, ?) }     Accept two parameters and return no value
{ ?= call procedure_name() }        Accept no parameter and return value
{ ?= call procedure_name(?) }       Accept one parameter and return value
* */

public class StoredProcedureTests extends BasePageTests {

    @Test(priority = 1)
    void test_storedProcedureExists() {
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectProcedureByName("SelectAllEmployees"));
            resultSet.next();//pointing to the current record

            Assert.assertEquals(resultSet.getString("Name"), "SelectAllEmployees");
            System.out.println(resultSet.getString(2));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 2)
    void test_storedProceduresExistInDatabase() {
        String[] storedProceduresNamesInDatabase = {
                "getCountOfEmployeesBySex",
                "getCountOfMalesFemalesSupervisedBySupervisor",
                "getCountOfMalesFemalesSupervisedBySupervisorId",
                "GetEmployeeSalaryLevel",
                "GetEmployeeSalaryLevel_2",
                "getSupervisorNameByEmployeeId",
                "GetTotalMalesCount",
                "InsertSupplierName",
                "SelectAllEmployees",
                "SelectAllEmployeesByFirstAndLastName",
                "SelectAllEmployeesByGender",
                "SelectAllEmployeesById",
                "SelectAllFromBranchSupplier"};
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectALLStoredProceduresInDatabase("company"));

            for (String s : storedProceduresNamesInDatabase) {
                resultSet.next();
                Assert.assertEquals(resultSet.getString("Name"), s);
                System.out.println(resultSet.getString(2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 3)
    void test_SelectAllEmployees() throws SQLException {
        //whenever we call a stored procedure we need to 1)prepare the call and 2) execute the callable Statement
        callableStatement = connection.prepareCall("{CALL SelectAllEmployees()}");//this will return a callable statement
        resultSet1 = callableStatement.executeQuery(); //the first resultSet is coming from SP call

        statement = connection.createStatement();
        resultSet2 = statement.executeQuery("SELECT * FROM employee");

        Assert.assertTrue(compareResultSets(resultSet1, resultSet2));
    }

    @Test(priority = 4)
    void test_SelectAllEmployeesById() throws SQLException {
        //here the store procedure is taking one input parameter
        callableStatement = connection.prepareCall("{CALL SelectAllEmployeesById(?)}");
        callableStatement.setString(1, "102");
        resultSet1 = callableStatement.executeQuery();
//        resultSet1.next();
//        System.out.println(resultSet1.getString(1));

        statement = connection.createStatement();
        resultSet2 = statement.executeQuery(selectEmployeeInfoById(102));
//        resultSet2.next();
//        System.out.println(resultSet2.getString(1));

        Assert.assertTrue(compareResultSets(resultSet1, resultSet2));
    }

    @Test(priority = 5)
    void test1_SelectAllEmployeesByGender() throws SQLException {
        callableStatement = connection.prepareCall("{CALL SelectAllEmployeesByGender(?)}");
        callableStatement.setString(1, "m");
        resultSet1 = callableStatement.executeQuery();

        statement = connection.createStatement();
        resultSet2 = statement.executeQuery("SELECT * FROM employee WHERE sex = Upper('m')");

        Assert.assertTrue(compareResultSets(resultSet1, resultSet2));
    }

    @Test(priority = 6)
    void test2_SelectAllEmployeesByGender() throws SQLException {
        callableStatement = connection.prepareCall("{CALL SelectAllEmployeesByGender(?)}");
        callableStatement.setString(1, "f");
        resultSet1 = callableStatement.executeQuery();

        statement = connection.createStatement();
        resultSet2 = statement.executeQuery("SELECT * FROM employee WHERE sex = Upper('f')");

        Assert.assertTrue(compareResultSets(resultSet1, resultSet2));
    }

    @Test(priority = 7)
    void test_SelectEmployeeByFirstAndLastName() throws SQLException {
        callableStatement = connection.prepareCall("{CALL SelectAllEmployeesByFirstAndLastName(?, ?)}");
        callableStatement.setString(1, "David");
        callableStatement.setString(2, "Wallace");
        resultSet1 = callableStatement.executeQuery();

        statement = connection.createStatement();
        resultSet2 = statement.executeQuery("SELECT * FROM employee WHERE first_name = 'David' AND last_name = 'Wallace'");
        resultSet2.next();
        System.out.println(resultSet2.getString(1));

        Assert.assertTrue(compareResultSets(resultSet1, resultSet2));
    }

    @Test(priority = 8)
    void test_getCountOfEmployeesBySex() throws SQLException {
        callableStatement = connection.prepareCall("{CALL getCountOfEmployeesBySex(?,?)}");
        //register the output variable
        callableStatement.registerOutParameter(1, INTEGER);
        callableStatement.setString(2, "F");
        //execute the statement
        callableStatement.executeQuery();
        //get the value of the output parameter which is in first position
        int employeesCount = callableStatement.getInt(1);
        System.out.println(employeesCount);

        statement = connection.createStatement();
        resultSet = statement.executeQuery("SELECT count(*) as Employees_count from employee where sex = 'F'");
        resultSet.next();
        int expected_employeesCount = callableStatement.getInt("Employees_count");
        System.out.println(resultSet.getInt(1));

        Assert.assertEquals(expected_employeesCount, employeesCount);
    }

    @Test(priority = 9)
    void test_getCountOfMalesFemalesSupervisedBySupervisor() throws SQLException {
        callableStatement = connection.prepareCall("{CALL getCountOfMalesFemalesSupervisedBySupervisor(?,?,?,?)}");
        callableStatement.setInt(1, 106);
        callableStatement.registerOutParameter(2, INTEGER);
        callableStatement.registerOutParameter(3, INTEGER);
        callableStatement.registerOutParameter(4, INTEGER);
        callableStatement.executeQuery();

        int supervisedEmployees = callableStatement.getInt(2);
        int supervisedMales = callableStatement.getInt(3);
        int supervisedFemales = callableStatement.getInt(4);

        statement = connection.createStatement();
        resultSet = statement.executeQuery("SELECT\n" +
                " \n" +
                " \t-- ---total Employee count\n" +
                "\t(SELECT COUNT(*) as supervised_employees FROM employee WHERE supervisor_id = 106) AS supervised_employee,\n" +
                "\t\n" +
                "-- \t---males count\n" +
                "\t(SELECT COUNT(*) as supervised_males FROM employee WHERE sex = \"M\" AND supervisor_id = 106) AS supervised_males,\n" +
                "\t\n" +
                "\t-- --females count\n" +
                "\t(SELECT COUNT(*) as supervised_females FROM employee WHERE sex = \"F\" AND supervisor_id = 106) AS supervised_females;");

        resultSet.next();
        int expected_supervisedEmployees = resultSet.getInt(1);
        int expected_supervisedMales = resultSet.getInt(2);
        int expected_supervisedFemales = resultSet.getInt(3);

        Assert.assertTrue(supervisedEmployees == expected_supervisedEmployees &&
                supervisedMales == expected_supervisedMales &&
                supervisedFemales == expected_supervisedFemales);
    }

    @Test(priority = 10)
    void test_getCountOfMalesFemalesSupervisedBySupervisorId() throws SQLException {
        //Preparing a CallableStatement to call a procedure
        callableStatement = connection.prepareCall("{CALL getCountOfMalesFemalesSupervisedBySupervisorId(?,?,?,?,?,?)}");
        callableStatement.setInt(1, 100);
        callableStatement.registerOutParameter(2, INTEGER);
        callableStatement.registerOutParameter(3, VARCHAR);
        callableStatement.registerOutParameter(4, INTEGER);
        callableStatement.registerOutParameter(5, INTEGER);
        callableStatement.registerOutParameter(6, INTEGER);
        callableStatement.executeQuery();

        int id = callableStatement.getInt(2);
        String supervisorName = callableStatement.getString(3);
        int supervisedEmployees = callableStatement.getInt(4);
        int supervisedMales = callableStatement.getInt(5);
        int supervisedFemales = callableStatement.getInt(6);

        statement = connection.createStatement();
        resultSet = statement.executeQuery("\tSELECT employee_id,\n" +
                "\t\tcase  \n" +
                "\t\t\twhen employee_id = 100 then 'David Wallace'\n" +
                "\t\t\twhen employee_id = 102 then 'Michael Scott'\n" +
                "\t\t\twhen employee_id = 106 then 'Josh Porter'\n" +
                "\t\t\tELSE 'not a supervisor'\n" +
                "\t\tEND AS supervisorName,\t\n" +
                "\n" +
                "\t(SELECT COUNT(*) as supervised_employees FROM employee WHERE supervisor_id = 100) AS supervised_employees,\n" +
                "\n" +
                "\t(SELECT COUNT(*) as supervised_males FROM employee WHERE sex = \"M\" AND supervisor_id = 100) AS supervised_males,\n" +
                "\n" +
                "\t(SELECT COUNT(*) as supervised_females FROM employee WHERE sex = \"F\" AND supervisor_id = 100) AS supervised_females\n" +
                "\tFROM employee WHERE employee_id = 100;");
        resultSet.next();

        int expected_id = resultSet.getInt(1);
        String expected_supervisorName = resultSet.getString(2);
        int expected_supervisedEmployees = resultSet.getInt(3);
        int expected_supervisedMales = resultSet.getInt(4);
        int expected_supervisedFemales = resultSet.getInt(5);

        if (id == expected_id &&
                supervisorName.equals(expected_supervisorName) &&
                supervisedEmployees == expected_supervisedEmployees &&
                supervisedMales == expected_supervisedMales &&
                supervisedFemales == expected_supervisedFemales) {
            Assert.assertTrue(true);
        } else {
            Assert.fail();
        }
    }

    @Test(priority = 11)
    void test_getSupervisorNameByEmployeeId() throws SQLException {
        callableStatement = connection.prepareCall("{CALL getSupervisorNameByEmployeeId(?,?)}");
        callableStatement.setInt(1, 102);
        callableStatement.registerOutParameter(2, VARCHAR);
        callableStatement.executeQuery();

        String supervisorName = callableStatement.getString(2);

        statement = connection.createStatement();
        resultSet = statement.executeQuery("SELECT  \n" +
                "\t\tcase \n" +
                "\t\t\twhen employee_id = 100 then 'David Wallace'\n" +
                "\t\t\twhen employee_id = 102 then 'Michael Scott'\n" +
                "\t\t\twhen employee_id = 106 then 'Josh Porter'\n" +
                "\t\t\tELSE 'Not a supervisor'\n" +
                "\t\t\tEND AS supervisorName\n" +
                "\t\t\tFROM employee WHERE employee_id = 102;");
        resultSet.next();

        String expected_supervisorName = resultSet.getString(1);

        Assert.assertEquals(expected_supervisorName, supervisorName);
    }
}
