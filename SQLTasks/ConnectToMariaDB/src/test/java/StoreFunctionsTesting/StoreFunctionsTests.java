package StoreFunctionsTesting;

import DBTests.BasePageTests;
import Database.Queries;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.sql.Types;

import static Database.UtilityMethods.compareResultSets;

public class StoreFunctionsTests extends BasePageTests {

    @Test(priority = 1)
    void test_storedFunctionExists() {
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectFunctionByName("EmployeeSalaryLevel"));
            resultSet.next();//pointing to the current record

            Assert.assertEquals(resultSet.getString("Name"), "EmployeeSalaryLevel");
            System.out.println(resultSet.getString(2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 2)
    void test_storedFunctionsExistInDatabase() {
        String[] functionNamesInDatabase = {"EmployeeSalaryLevel", "hello2"};
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectAllFunctionsInDatabase("company"));

            for (String s : functionNamesInDatabase) {
                resultSet.next();

                Assert.assertEquals(resultSet.getString("Name"), s);
                System.out.println(resultSet.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 3)
    void test_EmployeeSalaryLevel_with_SQLStatement() {

        try {
            statement = connection.createStatement();
            resultSet1 = statement.executeQuery("SELECT first_name, last_name, salary, EmployeeSalaryLevel(salary) FROM employee");

            resultSet2 = statement.executeQuery("SELECT first_name, last_name, salary,\n" +
                    "case\n" +
                    "when salary >= 100001 Then 'Large salary'\n" +
                    "when (salary >= 60001 AND salary <= 100000) THEN 'Medium salary'\n" +
                    "when salary <=60000 then 'Low salary'\n" +
                    "END AS employeeSalaryLevel\n" +
                    "FROM employee;");

            Assert.assertTrue(compareResultSets(resultSet1,resultSet2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 4)
    void test_EmployeeSalaryLevel_with_StoredProcedure() throws SQLException {
        callableStatement = connection.prepareCall("{CALL GetEmployeeSalaryLevel_2(?,?,?,?)}");
        callableStatement.setInt(1, 102);
        callableStatement.registerOutParameter(2, Types.VARCHAR);
        callableStatement.registerOutParameter(3, Types.VARCHAR);
        callableStatement.registerOutParameter(4, Types.VARCHAR);
        resultSet1 = callableStatement.executeQuery();  

        String employeeSalaryLevel = callableStatement.getString("employeeSalaryLevel");

        statement = connection.createStatement();
        resultSet2 = statement.executeQuery("SELECT first_name, last_name, salary,\n" +
                "case\n" +
                "when salary >= 100001 Then 'Large salary'\n" +
                "when (salary >= 60001 AND salary <= 100000) THEN 'Medium salary'\n" +
                "when salary <=60000 then 'Low salary'\n" +
                "END AS employeeSalaryLevel\n" +
                "FROM employee\n" +
                "WHERE employee_id = 102;");
        resultSet2.next();
        String expected_employeeSalaryLevel = resultSet2.getString(4);

        Assert.assertEquals(employeeSalaryLevel, expected_employeeSalaryLevel);
    }
}
