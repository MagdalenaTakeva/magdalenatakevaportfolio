package DBTests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.sql.*;

public class BasePageTests {

    public static Connection connection = null;
    protected static Statement statement = null;
    public ResultSet resultSet;
    public ResultSet resultSet1;
    public ResultSet resultSet2;
    public CallableStatement callableStatement;

    @BeforeClass   //this will execute only once before executing all the test methods
    public void setUp() {
        try {
            //Getting the connection
            connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/Company", "root", "root");
            System.out.println("Connection established......");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getRowCount(ResultSet set) throws SQLException {
        // Go to the last row
        set.last();
        int numRows = set.getRow();

        // Reset row before iterating to get data
        set.beforeFirst();
        return numRows;
    }

    public static int getRowCount2(ResultSet set) throws SQLException {
        int rowCount;
        int currentRow = set.getRow();            // Get current row
        rowCount = set.last() ? set.getRow() : 0; // Determine number of rows
        if (currentRow == 0)                      // If there was no current row
            set.beforeFirst();                     // We want next() to go to first row
        else                                      // If there WAS a current row
            set.absolute(currentRow);              // Restore it
        return rowCount;
    }

    @AfterClass
    public void tearDown() throws SQLException {
        if (statement != null) statement.close();
        if (connection != null) connection.close();
    }
}
