package DBTests;

import Database.Queries;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.*;

public class CompanyDBTests extends BasePageTests {

    @Test
    public void selectEmployeeInfoByFirstName() throws SQLException, ClassNotFoundException {
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectEmployeeInfoByFirstName("David"));
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1) +
                        " " + resultSet.getString(2) +
                        " " + resultSet.getString(3) +
                        " " + resultSet.getString(4) +
                        " " + resultSet.getString(5) +
                        " " + resultSet.getString(6) +
                        " " + resultSet.getString(7) +
                        " " + resultSet.getString(8));

                System.out.println(statement.getResultSet().getString(2));
                Assert.assertEquals(statement.getResultSet().getString(2), "David");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectEmployeeInfoById() throws SQLException, ClassNotFoundException {
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectEmployeeInfoById(100));
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1) +
                        " " + resultSet.getString(2) +
                        " " + resultSet.getString(3) +
                        " " + resultSet.getString(4) +
                        " " + resultSet.getString(5) +
                        " " + resultSet.getString(6) +
                        " " + resultSet.getString(7) +
                        " " + resultSet.getString(8));

                Assert.assertEquals(resultSet.getInt(1), 100);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectAllEmployeesThatAreMales() throws SQLException, ClassNotFoundException {
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectTotalEmployeeCountByGender("M"));
            resultSet.next();
            System.out.println(resultSet.getString("COUNT(*)"));

            Assert.assertEquals(resultSet.getInt("COUNT(*)"), 7);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectAllEmployeesThatAreFemales() throws SQLException, ClassNotFoundException {
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectTotalEmployeeCountByGender("F"));
            resultSet.next();
            System.out.println(resultSet.getString("COUNT(*)"));

            Assert.assertEquals(resultSet.getInt("COUNT(*)"), 3);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectTotalEmployeeCountByCountingRows() throws SQLException, ClassNotFoundException {
        int sum = 0;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectAllEmployees());
            while (resultSet.next()) {
                System.out.println(resultSet.getString(1));
                sum++;
            }
            System.out.println("Total number of employees is: " + sum);
            System.out.println("Total number of employees is: " + getRowCount(resultSet));

            Assert.assertEquals(getRowCount(resultSet), 10);
            Assert.assertEquals(sum, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void selectTotalEmployeeCount() throws SQLException, ClassNotFoundException {
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(Queries.selectTotalCountEmployees());
            resultSet.next();
            System.out.println(resultSet.getString(1));

            Assert.assertEquals(resultSet.getInt(1), 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
