package Database;

public class Queries {
    public static String selectEmployeeInfoByFirstName(String firstName) {
        return String.format("SELECT * FROM employee WHERE first_name = '%s'", firstName);
    }

    public static String selectEmployeeInfoById(int id) {
        return String.format("SELECT * FROM employee WHERE employee.employee_id = '%d'", id);
    }

    public static String selectTotalEmployeeCountByGender(String sex) {
        return String.format("SELECT COUNT(*) FROM employee WHERE sex= '%s'", sex);
    }

    public static String selectAllEmployees() {
        return "SELECT * FROM employee";
    }

    public static String selectTotalCountEmployees() {
        return "SELECT COUNT(*) FROM employee";
    }

    public static String selectALLStoredProceduresInDatabase(String storedProcedureName){
        return String.format("SHOW PROCEDURE STATUS WHERE db = '%s';", storedProcedureName);
    }

    public static String selectProcedureByName(String procedureName){
        return String.format("SHOW PROCEDURE STATUS WHERE NAME = '%s';",procedureName);
    }

    public static String selectFunctionByName(String functionName){
        return String.format("SHOW FUNCTION STATUS WHERE NAME = '%s';",functionName);
    }

    public static String selectAllFunctionsInDatabase(String databaseName){
        return String.format("SHOW FUNCTION STATUS WHERE db = '%s';",databaseName);
    }
}
