package Database;

import org.apache.commons.lang3.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UtilityMethods {

    public static boolean compareResultSets(ResultSet resultSet1, ResultSet resultSet2) throws SQLException {
        while(resultSet1.next()){

            resultSet2.next();
            int count = resultSet1.getMetaData().getColumnCount();//this will give us total number of columns available in the resultSet
            for (int i = 1; i <= count; i++) {
                if(!StringUtils.equals(resultSet1.getString(i), resultSet2.getString(i))){
                    return false;
                }
            }
        }
        return true;
    }

}
