# AUT - https://ecommerce-playground.lambdatest.io/ Functional testing in C#, using Selenium WebDriver

   1. Automated tests are created to assert the business logic  
      
   2. Test logic is organized in page objects using Page Object Design Pattern. All web elements are represented as private getters so that they cannot be accessed outside of the page object.
   The page object exposes only a few public methods through 2 helper methods:WaitAndFindElement(By locator) and WaitAndFindElements(By locator)  
         
   3. Explicit waits are added to the code that poll the application for a specific condition to evaluate as true before it continues to the next command in the code.

   4. Wait for AJAX method is added to code to address the following issue: Sometimes Selenium test executions fail some tests intermittently. But when we try to fix the issue and try to debug the same scenario, it works perfectly fine. Most of the time this happens because of the delay in element loading, which means Selenium tries to perform any action on the element but it is not yet rendered in the DOM. Thanks to AJAX, the element that we need to work with may be missing from the page after the page is loaded and show up after a few seconds, causing the check or interaction to fail. Depending on the environment where tests are executed this loading can take from a few milliseconds to a couple of seconds.

   5. Facade and Factory Design Patterns are used for organizing the tests
   Common problem in automated tests are test workflows, a series of actions and assertions that need to happen always in an almost identical order - facades are classes that ease the use of a large chunk of dependent code by providing a simplified interface with fewer methods
   Facade Class - the main class thet the user will use to access the simplified API. It usually contains public methods wich use the dependent classes' logic.
   Dependent Classes - They hold specific logic which is later used in the facade. Usually used as parameters in the facade