﻿namespace LambdatestEcommerce.Pages.OrderHistoryPage;
public partial class OrderHistoryPage
{
    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=account/order", Driver.Url);
    }

    public void AssertOrderId(string expectedOrderId)
    {
        Assert.AreEqual(expectedOrderId, Driver.Url);
    }

    public void AssertCustomerName(string expectedCustomerName)
    {
        Assert.AreEqual(expectedCustomerName, Customer.Text);
    }

    public void AssertOrderTotal(string expectedOrderTotal)
    {
        Assert.AreEqual(expectedOrderTotal, Total.Text);
    }

    public void AssertOrderViewIconDisplayed()
    {
        Assert.True(ViewIcon.Displayed, "Order view icon is not displayed");
    }
}
