﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.OrderHistoryPage;
public partial class OrderHistoryPage : WebPage
{
    public OrderHistoryPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=account/order";

    public void ProceedToOrderInfoPage()
    {
        ClickElement(ViewIcon);
    }

    // More methods need to be added

}
