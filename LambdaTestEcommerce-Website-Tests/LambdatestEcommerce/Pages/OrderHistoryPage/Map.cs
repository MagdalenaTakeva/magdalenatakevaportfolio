﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.OrderHistoryPage;
public partial class OrderHistoryPage
{
    private IWebElement OrderHistoryTitle => WaitAndFindElement(By.XPath("//h1"));
    private IReadOnlyCollection<IWebElement> ordersList => (IReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath("//div[@class='table-responsive']//tbody/tr"));
    private IWebElement OrderId => WaitAndFindElement(By.XPath("//div[@class='table-responsive']//tbody/tr/td[1]"));
    private IWebElement Customer => WaitAndFindElement(By.XPath("//div[@class='table-responsive']//tbody/tr/td[2]"));
    private IWebElement NumberOfProducts => WaitAndFindElement(By.XPath("//div[@class='table-responsive']//tbody/tr/td[3]"));
    private IWebElement Status => WaitAndFindElement(By.XPath("//div[@class='table-responsive']//tbody/tr/td[4]"));
    private IWebElement Total => WaitAndFindElement(By.XPath("//div[@class='table-responsive']//tbody/tr/td[5]"));
    private IWebElement Date => WaitAndFindElement(By.XPath("//div[@class='table-responsive']//tbody/tr/td[6]"));
    private IWebElement ViewIcon => WaitAndFindElement(By.XPath("//div[@class='table-responsive']//tbody/tr/td[7]/a"));
}
