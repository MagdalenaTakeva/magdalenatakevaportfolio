﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.VoucherSuccessPage
{
    public partial class VoucherSuccessPage
    {
        public IWebElement ContinueButton => WaitAndFindElement(By.CssSelector("#content a"));
    }
}
