﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.VoucherSuccessPage
{
    public partial class VoucherSuccessPage : Pages.WebPage
    {
        public VoucherSuccessPage(IWebDriver driver) : base(driver)
        {
        }

        protected override string Url => base.Url + "/index.php?route=account/voucher/success";

        public void ClickContinueButton()
        {
            ClickElement(ContinueButton);
        }
    }
}
