﻿namespace LambdatestEcommerce.Pages.VoucherSuccessPage
{
    public partial class VoucherSuccessPage
    {
        public void AssertPageUrl()
        {
            Assert.AreEqual(base.Url + "/index.php?route=account/voucher/success", Driver.Url);
        }
    }
}
