﻿namespace LambdatestEcommerce.Pages.Login;

public partial class LoginPage
{
    public void AssertEmailInputFieldIsDisplayed()
    {
        Assert.That(EmailAddressInputField.Displayed);
    }

    public void AssertLoginButtonIsDisplayed()
    {
        Assert.That(LoginButton.Displayed);
    }

    public void AssertPasswordInputFieldIsDisplayed()
    {
        Assert.That(PasswordInputField.Displayed);
    }

    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=account/login", Driver.Url);
    }

    public void AssertWarningMessage(string expectedMessage)
    {
        string actualMessage = LoginEmailFieldErrorMsg.Text;
        Assert.AreEqual(expectedMessage, actualMessage);
    }

    public void AssertSuccessMessage(string expectedMessage)
    {
        string actualMessage = EmailConfirmationMessage.Text;
        Assert.AreEqual(expectedMessage, actualMessage);
    }

    public void AssertRightColumnLinkByNameDisplayed(string name, string expectedLink)
    {
        Assert.True(GetRightColumnLinkByName(name).Displayed);
    }
}