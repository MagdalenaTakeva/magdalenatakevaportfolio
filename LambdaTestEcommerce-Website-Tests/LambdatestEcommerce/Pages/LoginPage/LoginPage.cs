﻿using LambdatestEcommerce.Models;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Login;

public partial class LoginPage : WebPage
{
    public LoginPage(IWebDriver driver)
      : base(driver)
    {
    }
    protected override string Url => base.Url + "/index.php?route=account/login";

    protected override void WaitForPageToLoad()
    {
        WaitForElementToBeVisible(By.Id("input-email"));
    }

    public void LoginUser(string email, string pass)
    {
        Driver.Manage().Cookies.DeleteAllCookies();
        GoTo();
        WaitForPageToLoad();
        ProvideLoginEmail(email);
        ProvideLoginPassword(pass);
        ClickElement(LoginButton);
    }

    public void LoginUser(UserData userInfo)
    {
        GoTo();
        WaitForPageToLoad();
        ProvideLoginEmail(userInfo.Email);
        ProvideLoginPassword(userInfo.Password);
        ClickElement(LoginButton);
    }

    public void ClickContinueButton()
    {
        ClickElement(ContinueButton);
    }

    public void ClickForgottenPasswordLink()
    {
        ClickElement(ForgottenPasswordLink);
    }

    public void ClickRightColumnLinkByName(string linkName)
    {
        ClickElement(GetRightColumnLinkByName(linkName));
    }

    public void ProvideLoginEmail(string email)
    {
        FillInInput(EmailAddressInputField, email);
    }

    public void ProvideLoginPassword(string password)
    {
        FillInInput(PasswordInputField, password);
    }

}