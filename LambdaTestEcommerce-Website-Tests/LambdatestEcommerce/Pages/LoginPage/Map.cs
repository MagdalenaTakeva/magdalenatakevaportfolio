﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Login;

public partial class LoginPage
{
    private IWebElement EmailAddressInputField => WaitAndFindElement(By.Id("input-email"));
    private IWebElement PasswordInputField => WaitAndFindElement(By.Id("input-password"));
    private IWebElement ForgottenPasswordLink => WaitAndFindElement(By.LinkText("Forgotten Password"));
    private IWebElement LoginButton => WaitAndFindElement(By.XPath("//input[@value='Login']"));
    private IWebElement ContinueButton => WaitAndFindElement(By.LinkText("Continue"));
    private IWebElement LoginEmailFieldErrorMsg => WaitAndFindElement(By.XPath("//div[contains(@class,'alert-danger')]"));
    private IWebElement EmailConfirmationMessage => WaitAndFindElement(By.XPath("//div[contains(@class,'alert-success')]"));

    private IWebElement GetRightColumnLinkByName(string name)
    {
        return WaitAndFindElement(By.XPath($"//aside[@id='column-right']//*[text()=' {name}']"));
    }
}