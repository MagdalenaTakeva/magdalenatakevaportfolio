﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Registration;

public partial class RegistrationPage
{
    private IWebElement LinkToLoginPage => WaitAndFindElement(By.LinkText("login page"));
    private IWebElement LinkToPrivacyPolicy => WaitAndFindElement(By.LinkText("Privacy Policy"));
    private IWebElement PrivacyPolicyHeader => WaitAndFindElement(By.XPath("//div[@class='modal-header']/h4[text()='Privacy Policy']"));
    private IWebElement FirstNameInputField => WaitAndFindElement(By.Id("input-firstname")); 
    private IWebElement LastNameInputField => WaitAndFindElement(By.Id("input-lastname")); 
    private IWebElement EmailInputField => WaitAndFindElement(By.Id("input-email")); 
    private IWebElement TelephoneInputField => WaitAndFindElement(By.Id("input-telephone"));
    private IWebElement PasswordInputField => WaitAndFindElement(By.Id("input-password")); 
    private IWebElement PasswordConfirmInputField => WaitAndFindElement(By.Id("input-confirm"));
    private IWebElement PrivacyPolicyCheckbox => WaitAndFindElement(By.XPath("//div[.//input[@id='input-agree']]/child::label"));
    private IWebElement ContinueButton => WaitAndFindElement(By.XPath("//input[@value='Continue']"));
    private IWebElement WarningMessage => WaitAndFindElement(By.XPath("//div[contains(@class, 'alert-danger')]"));
    private IWebElement TelephoneHelp => WaitAndFindElement(By.XPath("//label[contains(text(), 'Telephone')]/following-sibling::div/child::small[@id='input-telephone-help']"));

    private IWebElement GetErrorMessageByLabel(string inputLabel)
    {
        return WaitAndFindElement(By.XPath($"//label[contains(text(), '{inputLabel}')]/following-sibling::div/div"));
    }

    private IWebElement GetRightColumnLinkByName(string name)
    {
        return WaitAndFindElement(By.XPath($"//aside[@id='column-right']//*[text()=' {name}']"));
    }

    private IWebElement GetPersonalDetailsInputFieldByLabel(string label)
    {
        return WaitAndFindElement(By.XPath($"//label[.='{label}']/following-sibling::div/input"));
    }
}
