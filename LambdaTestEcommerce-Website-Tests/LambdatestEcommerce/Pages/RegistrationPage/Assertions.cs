﻿namespace LambdatestEcommerce.Pages.Registration;

public partial class RegistrationPage
{
    public void AssertPrivacyPolicyDialogBoxHeaderDisplayed()
    {
        Assert.True(PrivacyPolicyHeader.Displayed);
    }

    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=account/register", Driver.Url);
    }

    public void AssertErrorMessageByLabel(string label, string expectedMessage)
    {
        string actualMessage = GetErrorMessageByLabel(label).Text.Trim();
        Assert.AreEqual(expectedMessage, actualMessage);
    }

    public void AssertTelephoneHelperMessage(string expectedHelperMessage)
    {
        Assert.AreEqual(expectedHelperMessage, TelephoneHelp.Text.Trim());
    }

    public void AssertTelephoneWarningMessageDisplayedTogetherWithHelperMessage(string inputLabel, string expectedMessage, string expectedHelperMessage)
    {
        string actualMessageFirstLine = GetErrorMessageByLabel(inputLabel).Text.Trim();

        Assert.Multiple(() =>
        {
            Assert.AreEqual(expectedMessage, actualMessageFirstLine);
            AssertTelephoneHelperMessage(expectedHelperMessage);
        });
    }
    public void AssertEmailHasNoAtSign(string invalidInput)
    {
        string actualWarningMessage = EmailInputField.GetAttribute("validationMessage");
        Assert.AreEqual($"Please include an '@' in the email address. '{invalidInput}' is missing an '@'.", actualWarningMessage);
    }

    public void AssertEmailMissesPartBeforeAtSign(string invalidInput)
    {
        string actualWarningMessage = EmailInputField.GetAttribute("validationMessage");
        Assert.AreEqual($"Please enter a part followed by '@'. '{invalidInput}' is incomplete.", actualWarningMessage);
    }

    public void AssertEmailMissesPartAfterAtSign(string invalidInput)
    {
        string actualWarningMessage = EmailInputField.GetAttribute("validationMessage");
        Assert.AreEqual($"Please enter a part following '@'. '{invalidInput}' is incomplete.", actualWarningMessage);
    }

    public void AssertDotSignInEmailIsAtTheWrongPosition()
    {
        string actualWarningMessage = EmailInputField.GetAttribute("validationMessage");
        Assert.AreEqual($"'.' is used at a wrong position in '.com'.", actualWarningMessage);
    }
    public void AssertWarningMessage(string expectedMessage)
    {
        string actualMessage = WarningMessage.Text.Trim();
        Assert.AreEqual(expectedMessage, actualMessage);
    }

    public void AssertSuccessfulRegistrationUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=account/success", Driver.Url);
    }

    public void AssertInputFieldDisplayed(string label)
    {
        Assert.That(GetPersonalDetailsInputFieldByLabel(label).Displayed);
    }

    public void AssertLinkToLoginPageDisplayed()
    {
        Assert.True(LinkToLoginPage.Displayed);
    }
}
