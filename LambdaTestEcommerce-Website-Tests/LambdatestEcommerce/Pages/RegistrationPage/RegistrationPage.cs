﻿using LambdatestEcommerce.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Registration;

public partial class RegistrationPage : WebPage
{
    public RegistrationPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=account/register";

    public void RegisterNewUser(UserData userInfo)
    {

        GoTo();
        FillInRegistrationForm(userInfo);
    }

    public void CheckPrivacyPolicyCheckbox()
    {
        ClickElement(PrivacyPolicyCheckbox);
    }

    public void ClickRightColumnLinkByName(string linkName)
    {
        ClickElement(GetRightColumnLinkByName(linkName));
    }

    public void GoToLoginPage()
    {
        ClickElement(LinkToLoginPage);
    }

    public void GoToPrivacyPolicyPage()
    {
        ClickElement(LinkToPrivacyPolicy);
    }

    public void ClickContinueButton()
    {
        ClickElement(ContinueButton);
    }

    public void FillInRegistrationForm(UserData userInfo)
    {
        ProvideFirstName(userInfo.FirstName);
        ProvideLastName(userInfo.LastName);
        ProvideEmail(userInfo.Email);
        ProvideTelephone(userInfo.Telephone);
        ProvidePassword(userInfo.Password);
        ProvidePasswordConfirm(userInfo.PasswordConfirm);
    }

    public void ProvideFirstName(string firstName)
    {
        FillInInput(GetPersonalDetailsInputFieldByLabel("First Name"), firstName);
    }

    public void ProvideLastName(string lastName)
    {
        FillInInput(GetPersonalDetailsInputFieldByLabel("Last Name"), lastName);
    }

    public void ProvideEmail(string email)
    {
        FillInInput(GetPersonalDetailsInputFieldByLabel("E-Mail"), email);
    }

    public void ProvideTelephone(string tel)
    {
        FillInInput(GetPersonalDetailsInputFieldByLabel("Telephone"), tel);
    }

    public void ProvidePassword(string password)
    {
        FillInInput(GetPersonalDetailsInputFieldByLabel("Password"), password);
    }

    public void ProvidePasswordConfirm(string passwordConfirm)
    {
        FillInInput(GetPersonalDetailsInputFieldByLabel("Password Confirm"), passwordConfirm);
    }

}