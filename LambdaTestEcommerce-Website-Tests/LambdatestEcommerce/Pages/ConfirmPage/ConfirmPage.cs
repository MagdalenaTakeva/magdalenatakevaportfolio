﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.ConfirmPage;

public partial class ConfirmPage : WebPage
{
    public ConfirmPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=extension/maza/checkout/confirm";

    public void ConfirmOrder()
    {
        ClickElement(ConfirmOrderButton);
    }
    
    public void ClickEditButton()
    {
        ClickElement(EditButton);
    }
}