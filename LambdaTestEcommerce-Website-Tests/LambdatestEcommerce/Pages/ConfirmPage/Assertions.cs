﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Pages.ConfirmPage;

public partial class ConfirmPage
{
    public void AssertProductsTableDetails(List<Product> productList)
    {
        var rowsCount = ProductsList.Count();
        double expectedOrderSubTotal = 0;
        double expectedOrderGrandTotal = 0;

        IEnumerable<Product> sortedList = productList.OrderBy(product => product.ProductName);

        for (int i = 1; i <= rowsCount; i++)
        {
            var actualProductName = GetProductDetails(i, 1).Text;
            var actualProductCode = GetProductDetails(i, 2).Text;
            var actualProductQuantity = GetProductDetails(i, 3).Text;
            var actualProductPrice = double.Parse(GetProductDetails(i, 4).Text.Replace("$", ""));
            var actualProductTotal = double.Parse(GetProductDetails(i, 5).Text.Replace("$", ""));

            if (actualProductName.Contains("\r\n\r\nReward Points: 400"))
            {
                actualProductName = actualProductName.Replace("\r\n\r\nReward Points: 400", "");
            }

            var expectedProductName = sortedList.ElementAt(i - 1).ProductName;
            var expectedProductCode = sortedList.ElementAt(i - 1).ProductCode;
            var expectedProductQuantity = sortedList.ElementAt(i - 1).Quantity.ToString();
            var expectedProductPrice = sortedList.ElementAt(i - 1).ChangeUnitPriceWhenShippingMethodApplied();
            var expectedProductTotal = sortedList.ElementAt(i - 1).ChangeProductTotalPriceWhenShippingMethodApplied();
            expectedOrderSubTotal += expectedProductTotal;

            Assert.AreEqual(expectedProductName, actualProductName);
            Assert.AreEqual(expectedProductCode, actualProductCode);
            Assert.AreEqual(expectedProductQuantity, actualProductQuantity);
            Assert.AreEqual(expectedProductPrice, actualProductPrice);
            Assert.AreEqual(expectedProductTotal, actualProductTotal);

        }

        var flatShippingRate = 5;
        expectedOrderGrandTotal = expectedOrderSubTotal + flatShippingRate;
        AssertOrderTotal(expectedOrderGrandTotal.ToString());

    }

    public void AssertPaymentAddress(string expectedPaymentAddress)
    {
        Assert.AreEqual(expectedPaymentAddress, PaymentAddress.Text.Replace("\r\n", " "));
    }

    public void AssertPaymentAndShippingAddressesAreTheSame(BillingAndShippingAddresses billingAndShippingAddressesAreTheSame)
    {
        if (billingAndShippingAddressesAreTheSame == BillingAndShippingAddresses.TheSame)
        {
            Assert.AreEqual(PaymentAddress.Text.Replace("\r\n", " "), ShippingAddress.Text.Replace("\r\n", " "));
        }
        else if (billingAndShippingAddressesAreTheSame == BillingAndShippingAddresses.NotTheSame)
        {
            Assert.That(ShippingAddress.Text.Replace("\r\n", " "), Is.Not.EqualTo(PaymentAddress.Text.Replace("\r\n", " ")));
        }
    }

    public void AssertShippingMethodIsFlatShippingRate()
    {
        var expectedShippingMethod = "Flat Shipping Rate";
        Assert.AreEqual(expectedShippingMethod, ShippingMethod.Text.Trim());
    }

    public void AssertProductQuantity(string expectedProductQuantity)
    {
        Assert.AreEqual(expectedProductQuantity, Quantity.Text);
    }

    public void AssertOrderTotal(string expectedOrderTotal)
    {
        Assert.AreEqual(expectedOrderTotal, OrderTotalValue.Text.Replace("$", "").Replace(".00", ""));
    }

    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=extension/maza/checkout/confirm", Driver.Url);
    }
}
