﻿using OpenQA.Selenium;
using System.Collections.ObjectModel;

namespace LambdatestEcommerce.Pages.ConfirmPage;

public partial class ConfirmPage
{
    private IWebElement ConfirmOrderTitle => WaitAndFindElement(By.TagName("h1"));
    private ReadOnlyCollection<IWebElement> ProductsList => (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.CssSelector("div.table-responsive.mb-4 table tbody tr"));
    private ReadOnlyCollection<IWebElement> ColumnsList => (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath("//table[contains(@class, 'table-hover')]/descendant::tr/td"));
    private IWebElement ProductName => WaitAndFindElement(By.XPath("//table[contains(@class, 'table-hover')]//tbody//td[1]"));
    private IWebElement Model => WaitAndFindElement(By.XPath("//table[contains(@class, 'table-hover')]//tbody//td[2]"));
    private IWebElement Quantity => WaitAndFindElement(By.XPath("//table[contains(@class, 'table-hover')]//tbody//td[3]"));
    private IWebElement Price => WaitAndFindElement(By.XPath("//table[contains(@class, 'table-hover')]//tbody//td[4]"));
    private IWebElement ProductTotal => WaitAndFindElement(By.XPath("//table[contains(@class, 'table-hover')]//tbody//td[5]"));
    private IWebElement SubtotalValue => WaitAndFindElement(By.XPath("//table[contains(@class, 'table-hover')]//td[.='Sub-Total:']/following-sibling::td"));
    private IWebElement FlatShippingRateValue => WaitAndFindElement(By.XPath("//table[contains(@class, 'table-hover')]//td[.='Flat Shipping Rate:']/following-sibling::td"));
    private IWebElement OrderTotalValue => WaitAndFindElement(By.XPath("//table[contains(@class, 'table-hover')]//td[.='Total:']/following-sibling::td"));
    private IWebElement PaymentAddress => WaitAndFindElement(By.XPath("//div[h4[.='Payment Address']]//div[@class='card-body']"));
    private IWebElement ShippingAddress => WaitAndFindElement(By.XPath("//div[h4[.='Shipping Address']]//div[@class='card-body']"));
    private IWebElement ShippingMethod => WaitAndFindElement(By.XPath("//div[h4[.='Shipping Method:']]//div[text()='Flat Shipping Rate']"));
    private IWebElement ConfirmOrderButton => WaitAndFindElement(By.XPath("//button[@id='button-confirm']"));
    private IWebElement EditButton => WaitAndFindElement(By.XPath("//a[text()=' Edit']"));

    private ReadOnlyCollection<IWebElement> TableRowsList(int rowNumb)
    {
        return (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath($"//table[contains(@class, 'table-hover')]/descendant::tr[{rowNumb}]"));
    }

    private IWebElement GetProductDetails(int rowNumb, int colNumb)
    {
        return WaitAndFindElement(By.CssSelector($"div.table-responsive.mb-4 table tbody tr:nth-child({rowNumb}) > td:nth-child({colNumb})"));

    }
}
