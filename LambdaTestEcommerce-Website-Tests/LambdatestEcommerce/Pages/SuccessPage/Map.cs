﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.SuccessPage;
public partial class SuccessPage
{
    private IWebElement OrderHasBeenPlacedPageTitle => WaitAndFindElement(By.XPath("//h1[text()=' Your order has been placed!']"));
    private IWebElement MyAccountLink => WaitAndFindElement(By.LinkText("my account"));
    private IWebElement HistoryLink => WaitAndFindElement(By.LinkText("history"));
    private IWebElement DownloadsLink => WaitAndFindElement(By.LinkText("downloads"));
    private IWebElement StoreOwnerLink => WaitAndFindElement(By.LinkText("store owner"));
    private IWebElement ContinueButton => WaitAndFindElement(By.XPath("//div[contains(@class, 'buttons')]/a"));
    
    private IWebElement GetRightColumnLinkByName(string name)
    {
        return WaitAndFindElement(By.XPath($"//aside[@id='column-right']//*[text()=' {name}']"));
    }
}
