﻿namespace LambdatestEcommerce.Pages.SuccessPage;
public partial class SuccessPage
{
    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=checkout/success", Driver.Url);
    }

    public void AssertPageTitle()
    {
        Assert.AreEqual("Your order has been placed!", OrderHasBeenPlacedPageTitle.Text.Trim());
    }
}
