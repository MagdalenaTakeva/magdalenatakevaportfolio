﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.SuccessPage;
public partial class SuccessPage : WebPage
{
    public SuccessPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=checkout/success";

    public void ClickMyAccountLink()
    {
        ClickElement(MyAccountLink);
    }
    
    public void ClickOrderHistoryLink()
    {
        ClickElement(HistoryLink);
    }
    
    public void ClickStoreOwnerLink()
    {
        ClickElement(StoreOwnerLink);
    }
    
    public void ClickDownloadsLink()
    {
        ClickElement(DownloadsLink);
    }
    
    public void PressContinueButton()
    {
        ClickElement(ContinueButton);
    }
}
