﻿using LambdatestEcommerce.Models;
using LambdatestEcommerce.Sections.BreadcrumbSection;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.CompareProductsPage
{
    public partial class CompareProductsPage : WebPage
    {
        public CompareProductsPage(IWebDriver driver) : base(driver)
        {
            BreadcrumbSection = new BreadcrumbSection(driver);
        }
        public BreadcrumbSection BreadcrumbSection { get; }

        protected override string Url => base.Url + "/index.php?route=product/compare";

        public void AddMultipleProductsToCart(List<Product> productsList)
        {
            foreach (var product in productsList)
            {
                AddProductToCart(product.ProductId);
            }
        }

        public void AddAllProductsToCart()
        {
            foreach ( var product in AddToCartButtonsList)
            {
                ClickElement(product);
            }   
        }

        public void AddProductToCart(int productId)
        {
            ClickElement(GetProductAddButtonByProductId(productId));
        }

        public void RemoveMultipleProductsFromProductsComparePage(List<Product> productsList)
        {
            foreach (var product in productsList)
            {
                RemoveProduct(product.ProductId);
            }
        }

        public void RemoveAllProductsFromCompareProductsPage()
        {
            try
            {
                foreach (var product in RemoveButtonsList)
                {
                    ClickElement(product);
                }

            }
            catch (StaleElementReferenceException e)
            {
                foreach (var product in RemoveButtonsList)
                {
                    ClickElement(product);
                }
            }         
        }

        public void RemoveProduct(int productId)
        {
            ClickElement(GetProductRemoveButtonByProductId(productId));
        }

        public void ClickContinueButton()
        {
            ClickElement(ContinueButton);
        }

        public void ClickNotificationProductCompareButton()
        {
            ClickElement(NotificationProductCompareButton);
        }

        public void CloseCompareProductNotification()
        {
            ClickElement(CloseProductCompareNotification);
        }
    }
}
