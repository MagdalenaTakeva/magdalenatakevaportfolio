﻿using OpenQA.Selenium;
using System.Collections.ObjectModel;

namespace LambdatestEcommerce.Pages.CompareProductsPage
{
    public partial class CompareProductsPage
    {
        private IWebElement Title => WaitAndFindElement(By.CssSelector("#content h1"));
        private IWebElement NotChosenAnyProductsText => WaitAndFindElement(By.CssSelector("#content p"));
        private IWebElement ContinueButton => WaitAndFindElement(By.CssSelector("#content a"));
        private ReadOnlyCollection<IWebElement> AddToCartButtonsList => (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath("//input[@class = 'btn btn-primary btn-block']"));
        private ReadOnlyCollection<IWebElement> RemoveButtonsList => (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.LinkText("Remove"));
        private IReadOnlyCollection<IWebElement> Products => FindElements(By.XPath("//a[contains(@href, 'product/product')]"));
        private IWebElement SuccessMessage => WaitAndFindElement(By.XPath("//div[contains(@class, 'success')]"));
        private IWebElement NotificationProductCompareButton => WaitAndFindElement(By.XPath("//a[text()[contains(.,'Product Compare')]]"));
        private IWebElement CloseProductCompareNotification => WaitAndFindElement(By.XPath("//button[@aria-label = 'Close']/span"));
        private IWebElement GetProductAddButtonByProductId(int productId)
        {
            return WaitAndFindElement(By.XPath($"//input[contains(@onclick, \"cart.add('{productId}'\")]"));
        }

        private IWebElement GetProductRemoveButtonByProductId(int productId)
        {
            return WaitAndFindElement(By.XPath($"//a[contains(@href, 'remove={productId}')]"));
        }
    }
}
