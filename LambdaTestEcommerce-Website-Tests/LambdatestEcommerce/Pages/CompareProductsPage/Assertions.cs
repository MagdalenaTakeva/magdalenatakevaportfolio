﻿using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Pages.CompareProductsPage
{
    public partial class CompareProductsPage
    {
        public void AssertPageUrl()
        {
            string expectedUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/compare";
            Assert.AreEqual(expectedUrl, Driver.Url);
        }

        public void AssertNumberOfProductsAsExpected(int expectedNumber)
        {
            int actualNumber = Products.Count;
            if (actualNumber == 0)
            {
                actualNumber = 0;
            }

            Assert.AreEqual(expectedNumber, actualNumber);
        }

        public void AssertSuccessMessageDisplayed()
        {
            string expectedMessage = "Success: You have modified your product comparison!";
            string actualMessage = SuccessMessage.Text.Replace("\r\n×","");
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        public void AssertUserNotChosenAnyProducts()
        {
            string expectedText = "You have not chosen any products to compare.";
            Assert.AreEqual(expectedText, NotChosenAnyProductsText.Text);
        }

        public void AssertPageTitle()
        {
            Assert.AreEqual("Product Comparison", Title.Text);
        } 
    }
}
