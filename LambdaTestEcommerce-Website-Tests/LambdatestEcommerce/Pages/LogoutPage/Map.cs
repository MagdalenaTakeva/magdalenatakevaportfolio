﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.LogoutPage;
public partial class LogoutPage
{
    private IWebElement AccountLogoutTitle => WaitAndFindElement(By.XPath("//h1"));
    private IWebElement ContinueButton => WaitAndFindElement(By.XPath("//div[contains(@class, 'buttons')]/a"));
    private IReadOnlyCollection<IWebElement> LogoutPageText => (IReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath("//h1/following::p"));
}
