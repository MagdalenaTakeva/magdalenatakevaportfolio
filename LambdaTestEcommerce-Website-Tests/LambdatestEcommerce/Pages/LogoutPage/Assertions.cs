﻿using System.Text;

namespace LambdatestEcommerce.Pages.LogoutPage;
public partial class LogoutPage
{
    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=account/logout", Driver.Url);
    }

    public void AssertPageTitle()
    {
        Assert.AreEqual("Account Logout", AccountLogoutTitle.Text.Trim());
    }

    public void AssertLogoutPageText()
    {
        StringBuilder stringBuilder = new StringBuilder();

        foreach(var paragraph in LogoutPageText)
        {
            stringBuilder.Append(paragraph.Text);
        }

        string expectedText = "You have been logged off your account. It is now safe to leave the computer.Your shopping cart has been saved, the items inside it will be restored whenever you log back into your account.© LambdaTest - Powered by OpenCart";

        Assert.AreEqual(expectedText, stringBuilder.ToString());
    }
}
