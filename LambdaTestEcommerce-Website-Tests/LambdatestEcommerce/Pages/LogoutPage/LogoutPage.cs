﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.LogoutPage;
public partial class LogoutPage : WebPage
{
    public LogoutPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=account/logout";
}