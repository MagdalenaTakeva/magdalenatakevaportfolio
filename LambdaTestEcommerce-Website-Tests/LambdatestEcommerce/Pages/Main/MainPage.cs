﻿using LambdatestEcommerce.Models;
using LambdatestEcommerce.Sections.QuickViewSection;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace LambdatestEcommerce.Pages.Main;

public partial class MainPage : WebPage
{
    public MainPage(IWebDriver driver)
       : base(driver)
    {
        QuickViewSection = new QuickViewSection(driver);
    }

    public QuickViewSection QuickViewSection { get; }

    protected override void WaitForPageToLoad()
    {
        WaitForElementToBeVisible(By.Id("search"));
    }

    public void SearchCategoryById(int categoryId)
    {
        GoTo();
        WaitForPageToLoad();
        AllCategoriesButton.Click();
        GetSearchCategeoryById(categoryId).Click();
        SearchButton.Click();
        WaitForAjax();
    }

    public void ClickMyAccountLoginLink()
    {
        GoTo();
        WaitForElementToBeClickable(MyAccountDropdownLink);
        HoverOverElement(MyAccountDropdownLink);
        MyAccountLoginLink.Click();
        WaitForAjax();
    }

    public void ClickMyAccountRegisterLink()
    {
        GoTo();
        WaitForElementToBeClickable(MyAccountDropdownLink);
        HoverOverElement(MyAccountDropdownLink);
        MyAccountRegisterLink.Click();
        WaitForAjax();
    }

    public void ViewProductsToCompare()
    {
        ClickElement(ProductCompareButton);
    }

    public void CloseProductNotification()
    {
        ClickElement(CloseNotification);
    }

    public void ClickSwiperButtonNext()
    {
        HoverOverElement(TopProductsModuleSwiperButtonNext);
        TopProductsModuleSwiperButtonNext.Click();
        WaitForAjax();
    }

    public void ClickSwiperButtonPrevious()
    {
        HoverOverElement(TopProductsSwiperButtonPrevious);
        TopProductsSwiperButtonPrevious.Click();
        WaitForAjax();
    }

    public void AddItemsInTopProductsModuleToCompareList(List<CompareProductInfo> productsList, int productId1, int productId2)
    {
        for (int i = 0; i < productsList.Count; i++)
        {
            var productImage = GetImageLinksOfItemsInCompareProductsModule(productsList[i].ProductId);
            var compareButton = GeCompareProductButtonOfItemsInTopProductsModule(productsList[i].ProductId);

            if (productsList[i].ProductId == productId1 ||
            productsList[i].ProductId == productId2)
            {
                HoverOverElement(GetImageLinksOfItemsInCompareProductsModule(productsList[i].ProductId));
                compareButton.Click();
                CloseProductNotification();

                if (!productImage.Displayed)
                {
                    while (!productImage.Displayed)
                    {
                        ClickSwiperButtonNext();
                    }

                    if (!productImage.Displayed)
                    {
                        while (!productImage.Displayed)
                        {
                            ClickSwiperButtonPrevious();
                        }
                    }
                }
            }
        }
    }

    public void AddItemsInTopProductsModuleToWishList(List<CompareProductInfo> productsList, int productId1, int productId2)
    {
        for (int i = 0; i < productsList.Count; i++)
        {
            var productImage = GetImageLinksOfItemsInCompareProductsModule(productsList[i].ProductId);
            var wishListButton = GetAddToWishlistButtonOfItemsInTopProductsModule(productsList[i].ProductId);

            if (productsList[i].ProductId == productId1 ||
            productsList[i].ProductId == productId2)
            {
                HoverOverElement(GetImageLinksOfItemsInCompareProductsModule(productsList[i].ProductId));
                wishListButton.Click();
                CloseProductNotification();

                if (!productImage.Displayed)
                {
                    while (!productImage.Displayed)
                    {
                        ClickSwiperButtonNext();
                    }

                    if (!productImage.Displayed)
                    {
                        while (!productImage.Displayed)
                        {
                            ClickSwiperButtonPrevious();
                        }
                    }
                }
            }
        }
    }

    public void QuickViewItemInTopProductsModule(List<CompareProductInfo> productsList, int productId)
    {
        for (int i = 0; i < productsList.Count; i++)
        {
            var productImage = GetImageLinksOfItemsInCompareProductsModule(productsList[i].ProductId);
            var quickViewButton = GetQuickViewButtonOfItemsInTopProductsModule(productsList[i].ProductId);

            if (productsList[i].ProductId == productId)
            {
                HoverOverElement(productImage);
                quickViewButton.Click();

                if (!productImage.Displayed)
                {
                    while (!productImage.Displayed)
                    {
                        ClickSwiperButtonNext();
                    }

                    if (!productImage.Displayed)
                    {
                        while (!productImage.Displayed)
                        {
                            ClickSwiperButtonPrevious();
                        }
                    }
                }
            }
        }
    }
 }