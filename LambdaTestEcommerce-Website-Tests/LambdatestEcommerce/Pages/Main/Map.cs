﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Main;

public partial class MainPage
{
    private IWebElement MyAccountDropdownLink => WaitAndFindElement(By.XPath("//a[contains(@href,'account/account')][@role='button']"));
    private IWebElement MyAccountLoginLink => WaitAndFindElement(By.XPath("//li[1][.//a[contains(@href,'login')]]"));
    private IWebElement MyAccountRegisterLink => WaitAndFindElement(By.XPath("//li[2][.//a[contains(@href,'register')]]"));
    private IWebElement SearchButton => WaitAndFindElement(By.XPath("//button[@class='type-text']"));
    private IWebElement AllCategoriesButton => WaitAndFindElement(By.XPath("//div[@class='dropdown search-category']/button"));
    private IWebElement SearchDesktopsCategory => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//a[@data-category_id='20']"));
    private IWebElement SearchLaptopsCategory => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//a[@data-category_id='18']"));
    private IWebElement SearchComponentsCategory => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//a[@data-category_id='25']"));
    private IWebElement SearchTabletsCategory => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//a[@data-category_id='57']"));
    private IWebElement SearchSoftwareCategory => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//a[@data-category_id='17']]"));
    private IWebElement SearchPhonesAndPDAsCategory => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//a[@data-category_id='24']"));
    private IWebElement SearchCamerasCategory => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//a[@data-category_id='33']"));
    private IWebElement SearchMP3PlayersCategory => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//a[@data-category_id='34']"));

    private IWebElement ProductCompareButton => WaitAndFindElement(By.XPath("//a[text()[contains(.,'Product Compare')]]"));

    private IWebElement CloseNotification => WaitAndFindElement(By.XPath("//button[@aria-label = 'Close']"));

    private IWebElement TopProductsModuleSwiperButtonNext => WaitAndFindElement(By.XPath("//div[@id='mz-product-tab-37213259-0']/descendant::a[@class='swiper-button-next']/div/*[name()='svg']"));
    private IWebElement TopProductsSwiperButtonPrevious => WaitAndFindElement(By.XPath("//div[@id='mz-product-tab-37213259-0']/descendant::a[@class='swiper-button-prev']/div/*[name()='svg']")); 
    private IWebElement GetSearchCategeoryById(int categoryId)
    {
        return WaitAndFindElement(By.XPath($"//div[@id='entry_217822']//a[@data-category_id='{categoryId}']"));
    }

    private IWebElement GeCompareProductButtonOfItemsInTopProductsModule(int productId)
    {
        return WaitAndFindElement(By.XPath($"//div[contains(@id,'mz-product-tab-37213259-0')]/descendant::button[contains(@class, 'compare-{productId}')]"));
    }

    private IWebElement GetAddToWishlistButtonOfItemsInTopProductsModule(int productId)
    {
        return WaitAndFindElement(By.XPath($"//div[contains(@id,'mz-product-tab-37213259-0')]/descendant::button[contains(@class, 'wishlist-{productId}')]"));
    }

    private IWebElement GetQuickViewButtonOfItemsInTopProductsModule(int productId)
    {
        return WaitAndFindElement(By.XPath($"//div[contains(@id,'mz-product-tab-37213259-0')]/descendant::button[contains(@class, 'quick-view-{productId}')]"));
    }

    private IWebElement GetImageLinksOfItemsInCompareProductsModule(int productId)
    {
        return WaitAndFindElement(By.XPath($"//div[contains(@id,'mz-product-tab-37213259-0')]/descendant::a[contains(@href,'{productId}')]"));
    }

    //Quick View locators
    private IWebElement AddToCartButton => WaitAndFindElement(By.XPath("//div[@id='entry_212964']/button"));
    private IWebElement BuyNowButton => WaitAndFindElement(By.XPath("//div[@id='entry_212965']/button"));

    private IWebElement CompareThisProductButton => WaitAndFindElement(By.XPath("//div[@id='entry_212966']/button"));
    private IWebElement Quantity => WaitAndFindElement(By.XPath("//input[@name='quantity']"));
    private IWebElement IncreaseQuantity => WaitAndFindElement(By.XPath("//button[@aria-label='Increase quantity']"));
    private IWebElement DecreaseQuantity => WaitAndFindElement(By.XPath("//button[@aria-label='Decrease quantity']"));
    private IWebElement ProductName => WaitAndFindElement(By.TagName("h1"));
    private IWebElement Brand => WaitAndFindElement(By.XPath("//li[.//text()='Brand:']/a"));
    private IWebElement ProductCode => WaitAndFindElement(By.XPath("//span[.//text()='Product 14']"));
    private IWebElement Availability => WaitAndFindElement(By.XPath("//span[@class = 'badge badge-success']"));
    private IWebElement Price => WaitAndFindElement(By.XPath("//div[@class='price']/span"));
    private IWebElement AddToWishList => WaitAndFindElement(By.XPath("//div[@class='image-thumb d-flex']/button[@title='Add to Wish List']"));
    private IWebElement LargeImage => WaitAndFindElement(By.XPath("//div[@class='image-thumb d-flex']/a"));
    private IReadOnlyCollection<IWebElement> AdditionalImages => WaitAndFindElements(By.XPath("//div[@class='image-additional']/descendant::a"));
    private IWebElement VerticalPreviousSlide => WaitAndFindElement(By.XPath("//div[@aria-label='Previous slide'][1]"));
    private IWebElement HorizontalPreviousSlide => WaitAndFindElement(By.XPath("//div[@aria-label='Previous slide'][2]"));
    private IWebElement VerticalNextSlide => WaitAndFindElement(By.XPath("//div[@aria-label='Next slide'][1]"));
    private IWebElement HorizontalNextSlide => WaitAndFindElement(By.XPath("//div[@aria-label='Next slide'][2]"));
    private IWebElement Close => WaitAndFindElement(By.XPath("//button[@aria-label='close']"));
}