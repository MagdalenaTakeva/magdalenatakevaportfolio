﻿namespace LambdatestEcommerce.Pages.Main;

public partial class MainPage
{
    public void AssertPageUrl(string endPoint)
    {
        string expectedUrl = base.Url + endPoint;
        Assert.AreEqual(expectedUrl, Driver.Url);
    }

    // Quick View assertions
    public void AssertProductName(string productName)
    {
        Assert.AreEqual(productName, ProductName.Text);
    }

    public void AssertProductAvailability(string availability)
    {
        Assert.AreEqual(availability, Availability.Text);
    }
}