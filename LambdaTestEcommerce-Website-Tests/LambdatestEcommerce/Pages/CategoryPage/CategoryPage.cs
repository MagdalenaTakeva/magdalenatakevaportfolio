﻿using LambdatestEcommerce.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace LambdatestEcommerce.Pages.Category;

public partial class CategoryPage : WebPage
{
    public CategoryPage(IWebDriver driver) : base(driver)
    {
    }

    public void ClickProductName(Product product)
    {
        GetProductNameLinkByProductName(product.ProductName).Click();
    }

    public void ClickProductImage(Product product)
    {
        GetProductImageLinkByProductsId(product.ProductId).Click();
    }

    public void ClickListView()
    {
        ClickElement(ListView);
    }

    public void ClickGridView()
    {
        ClickElement(GridView);
    }

    public void AddProductToCartByProductId(int productId, int quantity)
    {
        for (var i = 1; i <= quantity; i++)
        {
            var imageLink = GetProductImageLinkByProductsId(productId);
            var addToCartButton = GetAddToCartButtonByProductId(productId);

            ScrollToElement(imageLink);
            HoverOverElement(imageLink);
            WaitForElementToBeClickable(addToCartButton);
            addToCartButton.Click();
        }
    }

    public void AddMultipleProductsToCart(List<Product> productsList)
    {
        foreach (var product in productsList)
        {
            AddProductToCartByProductId(product.ProductId, product.Quantity);
        }
    }

    public void AddProductToWishList(int productId)
    {
        HoverOverElement(GetProductImageLinkByProductsId(productId));
        GetAddToWishListButtonById(productId).Click();
    }

    public void AddProductToCompareList(int productId)
    {
        var imageLink = GetProductImageLinkByProductsId(productId);
        var compareButton = GetCompareThisProductButtonByProductId(productId);

        ScrollToElement(imageLink);
        HoverOverElement(imageLink);
        WaitForElementToBeClickable(compareButton);
        compareButton.Click();
        WaitForAjax();
    }

   public void AddMultipleProductsToCompare(List<Product> productsList)
    {
        foreach (var product in productsList)
        {
            AddProductToCompareList(product.ProductId);
        }
    }

    //Manufacturer Filter


    public void FilterByManufacturer(string manufacturerName)
    {
        foreach (var manufacturer in ManufacturersList)
        {
            if (manufacturer.Text == manufacturerName && (!manufacturer.Displayed))
            {
                ClickElement(SeeMore);
            }

            string borderColor = GetManufacturerName(manufacturerName).GetCssValue("border-color");
            if (borderColor.Equals("rgba(0, 0, 0, 0)"))
            {
                SelectManufacturer(manufacturerName);
            }
        }
    }

    public void SelectManufacturer(string manufacturer)
    {
        var selectedManufacturer = GetManufacturerName(manufacturer);
        
        ClickElement(selectedManufacturer);
    }

    public void DeselectManufacturer(string manufacturer)
    {
        string borderColor = GetManufacturerName(manufacturer).GetCssValue("border-color");
        if (borderColor.Equals("rgb(14, 186, 197)"))
        {
            SelectManufacturer(manufacturer);
        }
    }

    public void ClearSelectedManufacturerFilter()
    {
        ClickElement(ClearManufacturerFilter);
    }

    public void ClearAllFilters()
    {
        ClickElement(ClearAllFiltersX);
    }

    public void ClickSeeMore()
    {
        ClickElement(SeeMore);
    }

    public void ClickSeeLess()
    {
        ClickElement(SeeLess);
    }

    public void ShowProducts(string numberOfProductsToBeDisplayed)
    {
        SelectFromDropDownList(ShowDropDown, numberOfProductsToBeDisplayed);
    }

    public void ClickExpandCollapseFilterArrow()
    {
        ClickElement(ManufacturerUpArrow);
    }

    public void MovePriceSlider(int xOffset, int yOffset)
    {
        DragAndDropElement(PriceSlider, xOffset, yOffset);
    }

    public void UseSearchFilter(string productName)
    {
        FillInInput(SearchInputField, productName);
        PressEnterKey();
        WaitForAjax();
    }

    public int GetProductsCount()
    {
        var count = 0;

        if (Products.Count() > 0)
        {
            foreach (var product in Products)
            {
                count++;
            }
        }
        else
        {
            count = 0;
        }

        return count;

    }
}