﻿using OpenQA.Selenium;
using System.Collections.ObjectModel;

namespace LambdatestEcommerce.Pages.Category;

public partial class CategoryPage
{
    private ReadOnlyCollection<IWebElement> Products => (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.CssSelector(".product-thumb"));
    private IWebElement ListView => WaitAndFindElement(By.Id("list-view"));
    private IWebElement GridView => WaitAndFindElement(By.Id("grid-view"));
    private IWebElement ProductCompare => WaitAndFindElement(By.XPath("//a[contains(@class,'compare-total')]"));
    private IWebElement SortByDropDown => WaitAndFindElement(By.Id("input-sort-212464"));
    private ReadOnlyCollection<IWebElement> SearchProductsList => (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath("//h4/a[contains(@href,'product/product')]"));
    private IWebElement ClearAllFiltersX => WaitAndFindElement(By.XPath("//span[@data-mz-reset='all']"));

    // Search Criteria

    private IWebElement CategoryDropDown => WaitAndFindElement(By.XPath("//select[@name='category_id']"));
    private IWebElement ShowDropDown => WaitAndFindElement(By.Id("input-limit-212463"));

    // Price

    private IWebElement PriceSlider => WaitAndFindElement(By.XPath("//*[@id='mz-filter-panel-0-0']/descendant::span[1]"));
    private IWebElement MinimumPrice => WaitAndFindElement(By.XPath("//*[@id='mz-filter-panel-0-0']/descendant::input[1]"));

    // Manufacturer

    private IWebElement ClearManufacturerFilter => WaitAndFindElement(By.XPath("//div[@class='mz-filter-group manufacturer ']/descendant::*/i[@class='fas fa-times']"));
    private IWebElement ManufacturerUpArrow => WaitAndFindElement(By.XPath("//div[@class='mz-filter-group manufacturer ']/descendant::*/i[@class='fas fa-angle-up ml-auto']"));
    private IWebElement SeeMore => WaitAndFindElement(By.LinkText("See more"));
    private IWebElement SeeLess => WaitAndFindElement(By.LinkText("See less"));
    private ReadOnlyCollection<IWebElement> ManufacturersList => (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath("//*[@id='mz-filter-panel-0-1']/descendant::div[@class='mz-filter-value both ']"));
    private IWebElement GetManufacturerName(string manufacturerName)
    {
            return WaitAndFindElement(By.XPath($"//*[@id='mz-filter-panel-0-1']/descendant::label/img[@alt='{manufacturerName}']"));
    }


    // Search
    private IWebElement SearchInputField => WaitAndFindElement(By.CssSelector("#mz-filter-panel-0-2 input"));
    private IWebElement PaginationText => WaitAndFindElement(By.XPath("//*[contains(@class,'content-pagination')]/descendant::div[3]"));
    
    // Color


    // Size


    // Discount


    private IWebElement GetProductImageLinkByProductsId(int productId)
    {
        return WaitAndFindElement(By.XPath($"//a[contains(@id,'image-{productId}')]"));
    }

    private IWebElement GetProductNameLinkByProductName(string productName)
    {
        return WaitAndFindElement(By.XPath($"//h4[.//a[text()='{productName}']]"));
    }

    private ReadOnlyCollection<IWebElement> ProductsNamesList(string productName)
    {
        
        return (ReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath($"//*[@id='entry_212469']/descendant::h4/a[text()='{productName}']"));
    }

    private IWebElement GetAddToCartButtonByProductId(int productId)
    {
        return WaitAndFindElement(By.CssSelector($".product-thumb button.btn.btn-cart.cart-{productId}"));
    }

    private IWebElement GetCompareThisProductButtonByProductId(int productId)
    {
        return WaitAndFindElement(By.CssSelector($".product-thumb button.btn.btn-compare.compare-{productId}"));
    }

    private IWebElement GetAddToWishListButtonById(int productId)
    {
        return WaitAndFindElement(By.CssSelector($".product-thumb button.btn.btn-wishlist.wishlist-{productId}"));
    }

    private IWebElement GetQuickViewButtonByProductName(int productId)
    {
        return WaitAndFindElement(By.CssSelector($".product-thumb button.btn.btn-quick-view.quick-view-{productId}"));
    }
}