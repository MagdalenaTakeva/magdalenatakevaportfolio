﻿namespace LambdatestEcommerce.Pages.Category;
public partial class CategoryPage
{
    public void AssertCategoryPageUrlByCategoryId(int categoryId)
    {
        string expectedUrl = base.Url + $"/index.php?route=product%2Fsearch&category_id={categoryId}&search=";
        Assert.AreEqual(expectedUrl, Driver.Url);
    }

    public void AssertManufacturerImageBorderColorWhenElementSelected(string manufacturerName)
    {
        string borderColor = GetManufacturerName(manufacturerName).GetCssValue("border-color");
        Assert.AreEqual("rgb(14, 186, 197)", borderColor);
    }

    public void AssertManufacturerImageBorderColorWhenElementNotSelected(string manufacturerName)
    {
        string borderColor = GetManufacturerName(manufacturerName).GetCssValue("border-color");
        Assert.AreEqual("rgba(0, 0, 0, 0)", borderColor);
    }

    public void AssertPriceSliderOffset(string expectedSliderOffset)
    {
        string actualSliderOffset = PriceSlider.GetAttribute("style");
        Assert.AreEqual(expectedSliderOffset, actualSliderOffset);
    }

   public void AssertAllProductsDisplayedHaveTheSameName(string productName)
    {
        var productList = ProductsNamesList(productName);
        var count = productList.Count();

        if (count > 0)
        {
            foreach (var product in productList)
            {
                Assert.AreEqual(productName, product.Text);
            }
        }  
    }

    public void AssertProductsCount()
    {
        var count = GetProductsCount();
        if (count > 0)
        {
            string[] pagination = PaginationText.Text.Split(" ");
            int numberOfProducts = int.Parse(pagination[5]);
            Assert.AreEqual(count, numberOfProducts);
        }
    }
}