﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.DevTools.V111.Debugger;
using System.Security.Cryptography.X509Certificates;

namespace LambdatestEcommerce.Pages.Cart;

public partial class CartPage
{
    public void AssertProductsTableDetails(List<Product> productsInStock, ShippingMethod shippingMethod)
    {
        IEnumerable<Product> sortedList = productsInStock.OrderBy(product => product.ProductName);
        double expectedSubTotal = 0;
        double expectedGrandTotal = 0;

        for (var i = 2; i <= TableRowsList.Count; i++)
        {
            var actualProductName = GetProductDetails(i, 2).Text;
            var actualProductModel = GetProductDetails(i, 3).Text;
            var actualProductQuantity = GetProductQuantity(i).GetAttribute("value");
            var actualProductUnitPrice = GetProductDetails(i, 5).Text;
            var actualProductTotalPrice = GetProductDetails(i, 6).Text;

            if (actualProductName.Contains("\r\n\r\nReward Points: 400"))
            {
                actualProductName = actualProductName.Replace("\r\n\r\nReward Points: 400", "");
            }

            if (actualProductName.Contains("\r\n***"))
            {
                actualProductName = actualProductName.Replace("\r\n***", "");
            }

            var expectedProductName = sortedList.ElementAt(i - 2).ProductName;
            var expectedProductCode = sortedList.ElementAt(i - 2).ProductCode;
            var expectedProductQuantity = sortedList.ElementAt(i - 2).Quantity;

            Assert.AreEqual(expectedProductName, actualProductName);
            Assert.AreEqual(expectedProductCode, actualProductModel);
            Assert.AreEqual(expectedProductQuantity, int.Parse(actualProductQuantity));

            if (shippingMethod == ShippingMethod.NotApplied)
            {
                var expectedProductUnitPrice = sortedList.ElementAt(i - 2).UnitPrice;
                var expectedProductTotalPrice = sortedList.ElementAt(i - 2).ProductTotalPrice;
                expectedSubTotal += expectedProductTotalPrice;

                Assert.AreEqual(expectedProductUnitPrice, double.Parse(actualProductUnitPrice.Substring(1)));
                Assert.AreEqual(expectedProductTotalPrice, double.Parse(actualProductTotalPrice.Substring(1)));
            }
            else if (shippingMethod == ShippingMethod.Applied)
            {
                var expectedProductUnitPrice = sortedList.ElementAt(i - 2).ChangeUnitPriceWhenShippingMethodApplied();
                var expectedProductTotalPrice = sortedList.ElementAt(i - 2).ChangeProductTotalPriceWhenShippingMethodApplied();
                expectedSubTotal += expectedProductTotalPrice;

                Assert.AreEqual(expectedProductUnitPrice, double.Parse(actualProductUnitPrice.Substring(1)));
                Assert.AreEqual(expectedProductTotalPrice, double.Parse(actualProductTotalPrice.Substring(1)));
            }
        }

        if (shippingMethod == ShippingMethod.NotApplied)
        {
            expectedGrandTotal = expectedSubTotal;
            AssertGrandTotal(expectedGrandTotal);
        }
        else
        {
            var flatShippingRate = 5;
            expectedGrandTotal = expectedSubTotal + flatShippingRate;
            AssertGrandTotal(expectedGrandTotal);
        }
    }

    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=checkout/cart", Driver.Url);
    }

    public void AssertProductName(string expectedProductName)
    {
        Assert.AreEqual(expectedProductName, GetProductName(expectedProductName).Text);
    }

    public void AssertProductQuantity(string expectedQuantity)
    {
        Assert.AreEqual(expectedQuantity, QuantityBox.GetAttribute("value"));
    }

    public void AssertProductQuantity(int expectedQuantity)
    {
        Assert.AreEqual(expectedQuantity, int.Parse(QuantityBox.GetAttribute("value")));
    }

    public void AssertTotalPrice(double expectedTotalPrice)
    {
        Assert.AreEqual(expectedTotalPrice, double.Parse(ProductTotal.Text.Substring(1)));
    }

    public void AssertUnitPrice(double expectedUnitPrice)
    {
        Assert.AreEqual(expectedUnitPrice, Double.Parse(UnitPrice.Text.Replace("$", "")));
    }

    public void AssertShoppingCartEmptyTextDisplayed()
    {
        var expectedMessage = "Your shopping cart is empty!";
        Assert.AreEqual(expectedMessage, ShoppingCartIsEmptyText.Text);
    }

    public void AssertWarningMessage(string expectedWarningMessage)
    {
        var actualWarningMessage = WarningMessage().Text.Trim().Replace("\r\n×", "");
        Assert.AreEqual(expectedWarningMessage, actualWarningMessage);
    }

    public void AssertAlertSuccessMessage(string expectedSuccessMessage)
    {
        string actualSuccessMessage = AlertSuccessMessage().Text.Replace("\r\n×", string.Empty);
        Assert.AreEqual(expectedSuccessMessage, actualSuccessMessage);
    }

    public void AssertShippingEstimateHasBeenApplied()
    {
        Assert.AreEqual("Success: Your shipping estimate has been applied!",
                        AlertSuccessMessage().Text.Replace("\r\n×", ""));
    }

    public void AssertOrderSubtotal(string expectedSubTotal)
    {
        Assert.AreEqual(expectedSubTotal, SubTotal.Text);
    }

    public void AssertOrderEcoTax(string expectedEcoTax)
    {
        Assert.AreEqual(expectedEcoTax, EcoTax.Text);
    }

    public void AssertOrderVatTax(string expectedVatTax)
    {
        Assert.AreEqual(expectedVatTax, VatTax.Text);
    }

    public void AssertGrandTotal(double expectedTotal)
    {
        Assert.AreEqual(expectedTotal, Double.Parse(GrandTotal.Text.Replace("$", "")));
    }

    public void AssertFlatShippingRate(string expectedFlatShippingRate)
    {
        Assert.AreEqual(expectedFlatShippingRate, FlatShippingRate.Text);
    }

    public void AssertGiftCertificateAddedToCart()
    {
        Assert.True(GiftSertificateApplied.Displayed);
    }

    public void AssertProductOutOfStockWarningDisplayed()
    {
        var expectedMessage = " Products marked with *** are not available in the desired quantity or not in stock!";
        Assert.True(AlertMessageByTitle(expectedMessage).Displayed);
    }

    public void AssertNumberOfProductsAsExpected(int expectedNumber)
    {
        int actualNumber = Products.Count();
        if (actualNumber == 0)
        {
            actualNumber = 0;
        }

        Assert.AreEqual(expectedNumber, actualNumber);
    }
}