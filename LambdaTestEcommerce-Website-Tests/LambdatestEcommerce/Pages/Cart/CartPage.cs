﻿using LambdatestEcommerce.Models;
using LambdatestEcommerce.Sections.BreadcrumbSection;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Cart;

public partial class CartPage : WebPage
{
    public CartPage(IWebDriver driver) : base(driver)
    {
        BreadcrumbSection = new BreadcrumbSection(driver);
    }

    public BreadcrumbSection BreadcrumbSection { get; }
    
    protected override string Url => base.Url + "/index.php?route=checkout/cart";

    public void ApplyCoupon(string coupon)
    {
        ClickElement(UseCouponCodeTitle);
        ProvideCoupon(coupon);
        ClickElement(ApplyCouponButton);
    }

    public void ApplyGiftSertificate(string giftSertificate)
    {
        ClickElement(UseGiftCertificateTite);
        ProvideGiftCertificate(giftSertificate);
        ClickElement(ApplyGiftCertificateButton);
    }
    public void ApplyShippingRate(CartPageShippingInfo shippingInfo)
    {
        ClickElement(EstimateShippingAndTaxesTitle);
        SelectCountryOption(shippingInfo);
        SelectRegionOption(shippingInfo);
        ProvidePostCode(shippingInfo);
        ClickElement(GetQuotesButton);
        ClickElement(FlatShippingRateRadioButton);
        ClickElement(ApplyShippingButton);
    }

    public void IncreaseProductQuantity(string newQuantity)
    {
        FillInInput(QuantityBox, newQuantity);
        ClickElement(UpdateQuantity);
        WaitForAjax();
    }

    public void RemoveProduct()
    {
        ClickElement(Remove);
    }

    public void ProceedToCheckout()
    {
        ClickElement(CheckoutButton);
    }

    public void RemoveAllProductsFromCart()
    {
        GoTo();

        if (RemoveButtons.Count == 0) return;

        try
        {
            foreach (var removeButton in RemoveButtons)
            {
                ClickElement(removeButton);
            }
        }
        catch (StaleElementReferenceException e)
        {

            foreach (var removeButton in RemoveButtons)
            {
                ClickElement(removeButton);
            }
        }
    }

    public void PressContinueShoppingButton()
    {
        ClickElement(ContinueShoppingButton);
    }

    public void PressContinueButton()
    {
        ClickElement(ContinueButton);
    }

    public void SelectCountryOption(CartPageShippingInfo shippingInfo)
    {
        SelectFromDropDownList(SelectCountry, shippingInfo.Country);
    }

    public void SelectRegionOption(CartPageShippingInfo shippingInfo)
    {
        SelectFromDropDownList(SelectRegion, shippingInfo.Region);
    }

    public void ProvidePostCode(CartPageShippingInfo shippingInfo)
    {
        FillInInput(PostCodeInputField, shippingInfo.PostCode);
    }

    public void ProvideGiftCertificate(string giftSertificate)
    {
        FillInInput(GiftCertificateTextField, giftSertificate);
    }

    public void ProvideCoupon(string coupon)
    {
        FillInInput(CouponCodeTextField, coupon);
    }

    public void ProvideQuantity(string newQuantity)
    {
        FillInInput(QuantityBox, newQuantity);
    }
}