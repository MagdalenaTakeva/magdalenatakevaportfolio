﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Cart;

public partial class CartPage
{
    #region Use Coupon Code
    private IWebElement CouponCodeTextField => WaitAndFindElement(By.Id("input-coupon"));
    private IWebElement UseCouponCodeTitle => WaitAndFindElement(By.XPath("//h5[.='Use Coupon Code ']"));
    private IWebElement ApplyCouponButton => WaitAndFindElement(By.Id("button-coupon"));
    private IWebElement InvalidCouponWarningMessage => WaitAndFindElement(By.XPath("//div[text()=' Warning: Coupon is either invalid, expired or reached its usage limit! ']"));
    private IWebElement CouponWarningMessage => WaitAndFindElement(By.XPath("//div[text()=' Warning: Please enter a coupon code! ']"));

    #endregion

    #region Estimate Shipping & Taxes

    private IWebElement EstimateShippingAndTaxesTitle => WaitAndFindElement(By.XPath("//div[@class='card'][2]"));
    private IWebElement SelectCountry => WaitAndFindElement(By.Id("input-country"));
    private IWebElement SelectRegion => WaitAndFindElement(By.Id("input-zone"));
    private IWebElement PostCodeInputField => WaitAndFindElement(By.Id("input-postcode"));
    private IWebElement CountryWarningMessage => WaitAndFindElement(By.XPath("//div[text()='Please select a country!']"));//select[@id='input-country']/following-sibling::div[@class= 'invalid-feedback']
    private IWebElement RegionWarningMessage => WaitAndFindElement(By.XPath("//div[text()='Please select a region / state!']"));//select[@id='input-zone']/following-sibling::div[@class= 'invalid-feedback']
    private IWebElement PostCodeWarningMessage => WaitAndFindElement(By.XPath("//div[text()='Postcode must be between 2 and 10 characters!']"));//????
    private IWebElement GetQuotesButton => WaitAndFindElement(By.Id("button-quote"));
    private IWebElement FlatShippingRateRadioButton => WaitAndFindElement(By.XPath("//input[@name='shipping_method']"));
    private IWebElement ApplyShippingButton => WaitAndFindElement(By.Id("button-shipping"));
    private IWebElement CancelButton => WaitAndFindElement(By.XPath("//div[@class='modal-footer']/button[@data-dismiss]"));
    #endregion

    #region Use Gift Certificate
    private IWebElement GiftCertificateTextField => WaitAndFindElement(By.Id("input-voucher"));
    private IWebElement UseGiftCertificateTite => WaitAndFindElement(By.XPath("//div[@class='card'][3]"));
    private IWebElement GiftCertificateWarningMessage => WaitAndFindElement(By.XPath("//div[@class='card'][3]//div[contains(@class,'alert')]"));//*[text()=' Warning: Please enter a gift certificate code! ']
    private IWebElement ApplyGiftCertificateButton => WaitAndFindElement(By.Id("button-voucher"));
    #endregion

    #region Shopping Cart Table
    private IReadOnlyCollection<IWebElement> TableRowsList => (IReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath($"//*[@id='content']/form/descendant::tr"));
    private IWebElement ProductImage => WaitAndFindElement(By.XPath($"//*[@id='content']/form/descendant::img"));
    private IWebElement Model => WaitAndFindElement(By.XPath("//*[@id='content']/form/descendant::td[3]"));
    private IWebElement QuantityBox => WaitAndFindElement(By.XPath("//*[@id='content']/form/descendant::input"));
    private IWebElement UpdateQuantity => WaitAndFindElement(By.XPath("//*[@id='content']/form/descendant::button[@title='Update']"));
    private IWebElement Remove => WaitAndFindElement(By.XPath("//*[@id='content']/form/descendant::button[@title='Remove']"));
    private IWebElement UnitPrice => WaitAndFindElement(By.XPath("//*[@id='content']/form/descendant::td[5]"));
    private IWebElement ProductTotal => WaitAndFindElement(By.XPath("//*[@id='content']/form/descendant::td[6]"));

    #endregion

    #region Table with: Sub-Total; Eco Tax; VAT and Total
    private IWebElement SubTotal => WaitAndFindElement(By.XPath("//*[@id='content']//td[text()='Sub-Total:']/following-sibling::td"));
    private IWebElement EcoTax => WaitAndFindElement(By.XPath("//*[@id='content']//td[text()='Eco Tax (-2.00):']/following-sibling::td"));
    private IWebElement VatTax => WaitAndFindElement(By.XPath("//*[@id='content']//td[text()='VAT (20%):']/following-sibling::td"));
    private IWebElement FlatShippingRate => WaitAndFindElement(By.XPath("//*[@id='content']//td[text()='Flat Shipping Rate:']/following-sibling::td"));
    private IWebElement GrandTotal => WaitAndFindElement(By.XPath("//*[@id='content']//td[text()='Total:']/following-sibling::td"));
    private IWebElement GiftSertificateApplied => WaitAndFindElement(By.XPath("//td[contains(text(), 'Gift Certificate')]"));
    #endregion

    private IReadOnlyCollection<IWebElement> RemoveButtons => (IReadOnlyCollection<IWebElement>)FindElements(By.CssSelector("button.btn.btn-danger"));
    private IReadOnlyCollection<IWebElement> Products => FindElements(By.XPath("//td[@class= 'text-left']/a[contains(@href, 'product/product')]"));
    #region Buttons
    private IWebElement ContinueShoppingButton => WaitAndFindElement(By.CssSelector(" a.btn.btn-lg.btn-secondary.mr-auto"));
    private IWebElement ContinueButton => WaitAndFindElement(By.CssSelector("#content a"));
    private IWebElement CheckoutButton => WaitAndFindElement(By.XPath("//a[text()='Checkout']"));
    #endregion
    private IWebElement ShoppingCartIsEmptyText => WaitAndFindElement(By.XPath("//*[@id='content']/p"));

    private IWebElement GetProductName(string productName)
    {
        return WaitAndFindElement(By.XPath($"//*[@id='content']/descendant::a[2][text()='{productName}']"));
    }

    private IWebElement GetProductDetails(int row,int col)
    {
        return WaitAndFindElement(By.XPath($"//form//table/descendant::tr[{row}]/td[{col}]"));
    }

    private IWebElement GetProductQuantity(int row)
    {
        return WaitAndFindElement(By.XPath($"//form//table/descendant::tr[{row}]/td[4]/descendant::input"));
    }

    private IWebElement AlertMessageByTitle(string title)
    {
        return WaitAndFindElement(By.XPath($"//*[text()='{title}']/parent::div//descendant::div[contains(@class,'alert')]"));
    }

    private IWebElement AlertSuccessMessage()
    {
        return WaitAndFindElement(By.CssSelector($"div.alert.alert-success.alert-dismissible"));
    } 
    private IWebElement WarningMessage()
    {
        return WaitAndFindElement(By.CssSelector(" div.alert.alert-danger.alert-dismissible"));
    }
}
