﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.MyVoucherPage;
public partial class MyVoucherPage
{
    private IWebElement RecipientsNameInputField => WaitAndFindElement(By.Id("input-to-name"));
    private IWebElement RecipientsEmailInputField => WaitAndFindElement(By.Id("input-to-email"));
    private IWebElement YourNameInputField => WaitAndFindElement(By.Id("input-from-name"));
    private IWebElement YourEmailInputField => WaitAndFindElement(By.Id("input-from-email"));
    private IWebElement MessageInputField => WaitAndFindElement(By.Id("input-message"));
    private IWebElement AmountInputField => WaitAndFindElement(By.Id("input-amount"));
    private IWebElement AgreeCheckbox => WaitAndFindElement(By.XPath("//input[@name='agree']"));
    private IWebElement ContinueButton => WaitAndFindElement(By.XPath("//input[@type='submit']"));

    private IWebElement GetGiftCertificateThemeByName(string themeName)
    {
        return WaitAndFindElement(By.XPath($"//input[@name='voucher_theme_id']/parent::label[text()=' {themeName}']"));
    }
}
