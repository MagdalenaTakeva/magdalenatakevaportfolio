﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Models;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.MyVoucherPage;
public partial class MyVoucherPage : Pages.WebPage
{
    public MyVoucherPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=account/voucher";

    public void FillInGiftCertificate(GiftCertificateInfo giftCertificateInfo, GiftCertificateTheme giftCertificateTheme)
    {
        RecipientsNameInputField.SendKeys(giftCertificateInfo.RecipientsName);
        RecipientsEmailInputField.SendKeys(giftCertificateInfo.RecipientsEmail);
        YourNameInputField.SendKeys(giftCertificateInfo.YourName);
        YourEmailInputField.Clear();
        YourEmailInputField.SendKeys(giftCertificateInfo.YourEmail);
        
        switch (giftCertificateTheme)
        {
            case GiftCertificateTheme.Birthday:
                ClickElement(GetGiftCertificateThemeByName("Birthday"));
                break;
            case GiftCertificateTheme.Christmas:
                ClickElement(GetGiftCertificateThemeByName("Christmas"));
                break;
            case GiftCertificateTheme.General:
                ClickElement(GetGiftCertificateThemeByName("General"));
                break;
        }
        
        AmountInputField.Clear();
        AmountInputField.SendKeys(giftCertificateInfo.Amount);
        CheckGiftCertificateNonRefundableCheckbox();
        PressContinueButton();
    }
    
    public void CheckGiftCertificateNonRefundableCheckbox()
    {
        ClickElement(AgreeCheckbox);
    }

    public void PressContinueButton()
    {
        ClickElement(ContinueButton);
    }
}