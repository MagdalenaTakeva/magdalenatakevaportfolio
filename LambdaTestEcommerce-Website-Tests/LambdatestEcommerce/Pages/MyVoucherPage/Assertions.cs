﻿namespace LambdatestEcommerce.Pages.MyVoucherPage;
public partial class MyVoucherPage
{
    public void AssertGiftCertificateFormCompletedSuccessfully(string endPoint)
    {
        string expectedUrl = base.Url + endPoint;
        Assert.AreEqual(expectedUrl, Driver.Url);
    }
}
