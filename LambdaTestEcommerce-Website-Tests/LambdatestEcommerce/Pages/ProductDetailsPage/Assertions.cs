﻿namespace LambdatestEcommerce.Pages.ProductDetails;

public partial class ProductDetailsPage
{
    public void AssertProductPageUrl(string url)
    {
        string expectedUrl = url;
        Assert.AreEqual(expectedUrl, Driver.Url.Replace("&limit=100", ""));
    }

    public void AssertProductName(string expectedName)
    {
        Assert.AreEqual(expectedName, GetProductName(expectedName).Text);
    }

    public void AssertProductBrand(string expectedBrand)
    {
        Assert.AreEqual(expectedBrand, Brand.Text);
    }

    public void AssertProductIsInStock()
    {
        string backgoundColorInStock = ProductInStock.GetCssValue("background-color");
        Assert.AreEqual("In Stock", ProductInStock.Text);
        Assert.AreEqual("rgba(40, 167, 69, 1)", backgoundColorInStock);
    }

    public void AssertProductIsNotInStock()
    {
        string backgoundColorInStock = ProductNotInStock.GetCssValue("background-color");
        Assert.AreEqual("rgba(220, 53, 69, 1)", backgoundColorInStock);
    }

    public void AssertProductPrice(double expectedPrice)
    {
        Assert.AreEqual(expectedPrice, Double.Parse(Price.Text.Replace("$","")));
    }

    public void AssertAddToCartButtonDisabled()
    {
        Assert.AreEqual(false, AddToCartButton.Enabled);
    }

    public void AssertAddToCartButtonEnabled()
    {
        Assert.AreEqual(true, AddToCartButton.Enabled);
    }

    public void AssertbuyNowButtonDisabled()
    {
        Assert.AreEqual(false, BuyNowButton.Enabled);
    }

    public void AssertBuyNowButtonEnabled()
    {
        Assert.AreEqual(true, BuyNowButton.Enabled);
    }

    public void AssertProductDetails(string expectedName, string expectedBrand, double expectedPrice)
    {
        AssertProductName(expectedName);
        AssertProductBrand(expectedBrand);
        AssertProductPrice(expectedPrice);
    }

    public void AssertProductInStockAndAddToCartAndBuyNowButtonsEnabled()
    {
        AssertProductIsInStock();
        AssertAddToCartButtonEnabled();
        AssertBuyNowButtonEnabled();
    }

    public void AssertProductIsNotInStockAndAddTocartAndBuyNowButtonsDisabled()
    {
        AssertProductIsNotInStock();
        AssertAddToCartButtonDisabled();
        AssertbuyNowButtonDisabled();
    }
}
