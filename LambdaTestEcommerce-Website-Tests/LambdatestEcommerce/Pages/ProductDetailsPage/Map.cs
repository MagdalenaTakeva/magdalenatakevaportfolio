﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.ProductDetails;

public partial class ProductDetailsPage
{
    private IWebElement Brand => WaitAndFindElement(By.XPath("//ul//span[text()='Brand:']/following-sibling::a"));
    private IWebElement ProductInStock => WaitAndFindElement(By.XPath("//span[text()='Availability:']/following-sibling::span[contains(@class,'badge-success')]"));
    private IWebElement ProductNotInStock => WaitAndFindElement(By.XPath("//span[text()='Availability:']/following-sibling::span[contains(@class, 'badge-danger')]"));
    private IWebElement Price => WaitAndFindElement(By.XPath("//div[@class='price']/h3"));
    private IWebElement DecreaseQuantityButton => WaitAndFindElement(By.XPath("//*[@id='entry_216841']//button[@aria-label='Decrease quantity']"));
    private IWebElement IncreaseQuantityButton => WaitAndFindElement(By.XPath("//*[@id='entry_216841']//button[@aria-label='Increase quantity']"));
    private IWebElement QuantityInput => WaitAndFindElement(By.XPath("//*[@id='entry_216841']//input[@name='quantity']"));
    private IWebElement AddToCartButton => WaitAndFindElement(By.XPath("//*[@id='entry_216842']/button"));
    private IWebElement BuyNowButton => WaitAndFindElement(By.XPath("//*[@id='entry_216843']/button"));
    private IWebElement CompareThisProductButton => WaitAndFindElement(By.XPath("//*[@id='entry_216844']//button[@title='Compare this Product']"));
    private IWebElement HiddenInput => WaitAndFindElement(By.XPath("//input[@name='product_id']"));
    
    private IWebElement GetProductName(string productName)
    {
        return WaitAndFindElement(By.XPath($"//h1[text()='{productName}']"));
    }
}
