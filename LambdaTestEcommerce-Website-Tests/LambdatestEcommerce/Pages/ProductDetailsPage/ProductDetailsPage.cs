﻿using AngleSharp.Dom;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.ProductDetails;

public partial class ProductDetailsPage : WebPage
{
    public ProductDetailsPage(IWebDriver driver) : base(driver)
    {
    }
    protected override string Url => "";

    public void AddProductToCart()
    {
        ClickElement(AddToCartButton);
    }

    public void ClickAddToCartButtonThatShouldBeDisabled()
    {
        ScrollToElement(AddToCartButton);
        AddToCartButton.Click();
    }

    public void BuyNowProduct()
    {
        ClickElement(BuyNowButton);
    }

    public void ClickBuyNowButtonThatShouldBeDisabled()
    {
        ScrollToElement(BuyNowButton);
        BuyNowButton.Click();
    }

    public void IncreaseProductQuantity(int expectedQuantity)
    {
        for (var i = 1; i < expectedQuantity; i++)
        {
            ClickElement(IncreaseQuantityButton);
        }
    }

    public void DecreaseProductQuantity(int expectedStartQuantity, int expectedEndQuantity)
    {
        for (var i = expectedStartQuantity; i >= expectedEndQuantity; i--)
        {
            ClickElement(DecreaseQuantityButton);
        }
    }

    public string GetProductId()
    {
        string productId = HiddenInput.GetAttribute("value");
        return productId;
    }
}
