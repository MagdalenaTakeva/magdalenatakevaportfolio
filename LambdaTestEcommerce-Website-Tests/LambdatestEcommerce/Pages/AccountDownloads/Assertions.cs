﻿namespace LambdatestEcommerce.Pages.AccountDownloads
{
    public partial class AccountDownloadsPage
    {
        public void AssertPageUrl()
        {
            string expectedUrl = base.Url + "/index.php?route=account/download";
            Assert.AreEqual(expectedUrl, Driver.Url);
        }
    }
}
