﻿using LambdatestEcommerce.Sections.BreadcrumbSection;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.AccountDownloads
{
    public partial class AccountDownloadsPage : WebPage
    {
        public AccountDownloadsPage(IWebDriver driver) : base(driver)
        {
            BreadcrumbSection = new BreadcrumbSection(driver);
        }

        public BreadcrumbSection BreadcrumbSection { get; }
    }
}
