﻿using LambdatestEcommerce.Sections.CartInfoSection;
using LambdatestEcommerce.Sections.MainMenuSection;
using LambdatestEcommerce.Sections.SearchSection;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace LambdatestEcommerce.Pages;

public abstract class WebPage
{
    private const int WAIT_FOR_ELEMENT_TIMEOUT = 60;
    Actions actions;

    public WebPage(IWebDriver driver)
    {
        Driver = driver;
        WebDriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(WAIT_FOR_ELEMENT_TIMEOUT));
        MainMenuSection = new MainMenuSection(driver);
        SearchSection = new SearchSection(driver);
        CartInfoSection = new CartInfoSection(driver);
        actions = new Actions(driver);
    }

    protected virtual string Url => "https://ecommerce-playground.lambdatest.io";

    protected IWebDriver Driver { get; set; }
    protected WebDriverWait WebDriverWait { get; set; }
    public SearchSection SearchSection { get; }
    public MainMenuSection MainMenuSection { get; }
    public CartInfoSection CartInfoSection { get; }

    public void GoTo()
    {
        Driver.Manage().Window.Maximize();
        Driver.Navigate().GoToUrl(Url);
        WaitForPageToLoad();
    }

    public void Refresh()
    {
        Driver.Navigate().Refresh();
        WaitForPageToLoad();
    }

    protected virtual void WaitForPageToLoad()
    {
    }

    protected IWebElement WaitAndFindElement(By locator)
    {
        return WebDriverWait.Until(ExpectedConditions.ElementExists(locator));
    }

    protected IReadOnlyCollection<IWebElement> WaitAndFindElements(By locator)
    {

        return WebDriverWait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(locator));
    }

    protected IReadOnlyCollection<IWebElement> FindElements(By locator)
    {

        return Driver.FindElements(locator);
    }

    protected void WaitForAjax()
    {
        var js = (IJavaScriptExecutor)Driver;
        WebDriverWait.Until(wd => js.ExecuteScript("return jQuery.active").ToString() == "0");
    }

    protected void WaitUntilPageLoadsCompletely()
    {
        var js = (IJavaScriptExecutor)Driver;
        WebDriverWait.Until(wd => js.ExecuteScript("return document.readyState").ToString() == "complete");
    }

    public void HoverOverElement(IWebElement elementToBeHovered)
    {
        actions.MoveToElement(elementToBeHovered).Perform();
    }

    public void WaitForElementToBeClickable(IWebElement elementToClick)
    {
        WebDriverWait.Until(ExpectedConditions.ElementToBeClickable(elementToClick));
    }

    public void WaitForElementToBeVisible(By by)
    {
        WebDriverWait.Until(ExpectedConditions.ElementIsVisible(by));
    }

    public void ScrollToElement(IWebElement elementToScrollTo)
    {
        ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", elementToScrollTo);
    }

    public void FocusControlElement(IWebElement elementToFocusOn)
    {
        ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].focus();", elementToFocusOn);
    }

    public void WaitForElementToExist(By by)
    {
        WebDriverWait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
    }

    public void DragAndDropElement(IWebElement source, int xOffset, int yOffSet)
    {
        actions.DragAndDropToOffset(source, xOffset, yOffSet).Perform();
    }

    public void PressEnterKey()
    {
        actions.SendKeys(Keys.Enter).Build().Perform();
    }

    public void ClickElement(IWebElement element)
    {
        ScrollToElement(element);
        WaitForElementToBeClickable(element);
        element.Click();
        WaitForAjax();
    }

    public void FillInInput(IWebElement element, string text)
    {
        ScrollToElement(element);
        WaitForElementToBeClickable(element);
        element.Clear();
        element.SendKeys(text);
        WaitForAjax();

    }

    public void SelectFromDropDownList(IWebElement element, string text)
    {
        ScrollToElement(element);
        WaitForElementToBeClickable(element);
        SelectElement selectElement = new SelectElement(element);
        selectElement.SelectByText(text);
        WaitForAjax();
    }

}
