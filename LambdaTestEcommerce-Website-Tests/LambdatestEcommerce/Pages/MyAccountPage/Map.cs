﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.MyAccount;

public partial class MyAccountPage
{
    private IWebElement GetAccountLinksByLinkText(string text)
    {
        return WaitAndFindElement(By.LinkText(text));
    }

    private IWebElement GetRightColumnLinkByName(string name)
    {
        return WaitAndFindElement(By.XPath($"//aside[@id='column-right']//*[text()=' {name}']"));
    }
}