﻿namespace LambdatestEcommerce.Pages.MyAccount
{
    public partial class MyAccountPage
    {
        public void AssertRightColumnLinkByNameDisplayed(string name, string expectedLink)
        {
            Assert.True(GetRightColumnLinkByName(name).Displayed);
        }

        public void AssertPageUrl(string endPoint)
        {
            string expectedUrl = base.Url + endPoint;
            Assert.AreEqual(expectedUrl, Driver.Url);
        }

        public void AssertPageUrl()
        {
            string expectedUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=account/account";
            Assert.AreEqual(expectedUrl, Driver.Url);
        }

        public void AssertPageTitle(string expectedTitle)
        {
            Assert.AreEqual(expectedTitle, Driver.Title);
        }
    }
}