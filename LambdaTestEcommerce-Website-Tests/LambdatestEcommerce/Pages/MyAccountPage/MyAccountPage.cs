﻿using OpenQA.Selenium;
using LambdatestEcommerce.Sections.BreadcrumbSection;
using LambdatestEcommerce.Sections.WishListSection;
using LambdatestEcommerce.Sections.CompareProductsSection;

namespace LambdatestEcommerce.Pages.MyAccount;

public partial class MyAccountPage : WebPage
{
    public MyAccountPage(IWebDriver driver) : base(driver)
    {
        BreadcrumbSection = new BreadcrumbSection(driver);
        WishlistSecion = new WishlistSecion(driver);
        CompareProductsSection = new CompareProductsSection(driver);
    }

    public BreadcrumbSection BreadcrumbSection { get; }

    public WishlistSecion WishlistSecion { get; }

    public CompareProductsSection CompareProductsSection { get; }

    protected override string Url => base.Url + "/index.php?route=account/account";

    public void ClickOnLinkByLabel(string label)
    {
        ClickElement(GetAccountLinksByLinkText(label));
    }

    public void ClickRightColumnLinkByName(string linkName)
    {
        ClickElement(GetRightColumnLinkByName(linkName));
    }
}
