﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.ContactsPage;
public partial class ContactsPage
{
    private IWebElement ContactFormTitle => WaitAndFindElement(By.XPath("//h1[text()='Contact Form']"));
}
