﻿namespace LambdatestEcommerce.Pages.ContactsPage;
public partial class ContactsPage
{
    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=information/contact", Driver.Url);
    }
}
