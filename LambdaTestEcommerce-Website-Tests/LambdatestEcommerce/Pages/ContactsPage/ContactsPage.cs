﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.ContactsPage;
public partial class ContactsPage : WebPage
{
    public ContactsPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=information/contact";
}
