﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.ForgottenPasswordPage;
public partial class ForgottenPasswordPage
{
    private IWebElement EmailInputField => WaitAndFindElement(By.Id("input-email"));
    private IWebElement ContinueButton => WaitAndFindElement(By.XPath("//button[@type='submit' and text()='Continue']"));
    private IWebElement BackButton => WaitAndFindElement(By.XPath("//a[text()=' Back']"));
    private IWebElement WarningMessage => WaitAndFindElement(By.XPath("//div[contains(@class,'alert-danger')]"));
    private IWebElement SuccessMessage => WaitAndFindElement(By.XPath("//div[contains(@class,'alert-success')]"));
}
