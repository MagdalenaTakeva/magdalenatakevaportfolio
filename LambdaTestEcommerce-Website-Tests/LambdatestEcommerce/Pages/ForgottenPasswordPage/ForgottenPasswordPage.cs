﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.ForgottenPasswordPage;
public partial class ForgottenPasswordPage : WebPage
{
    public ForgottenPasswordPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=account/forgotten";

    public void SendEmail(string emailToBeSent)
    {
        FillInInput(EmailInputField, emailToBeSent);
        ClickElement(ContinueButton);
    }

    public void GoBack()
    {
        ClickElement(BackButton);
    }
}
