﻿namespace LambdatestEcommerce.Pages.ForgottenPasswordPage;
public partial class ForgottenPasswordPage
{
    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=account/forgotten", Driver.Url);
    }

    public void AssertWarningMessage()
    {
        Assert.AreEqual("Warning: The E-Mail Address was not found in our records, please try again!", WarningMessage.Text.Trim());
    }

    public void AssertSuccessMessage()
    {
        Assert.AreEqual("An email with a confirmation link has been sent your email address.", SuccessMessage.Text.Trim());
    }
}
