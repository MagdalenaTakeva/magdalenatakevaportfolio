﻿using Bogus;
using LambdatestEcommerce.Models;
using System.Diagnostics.Metrics;

namespace LambdatestEcommerce.Pages.Checkout;

public partial class CheckoutPage
{
    public void AssertProductsTableDetails(List<Product> productsInStock)
    {
        IEnumerable<Product> sortedList = productsInStock.OrderBy(product => product.ProductName);
        double expectedSubTotal = 0;

        for (var i = 1; i <= TableRowsList.Count; i++)
        {
            var actualProductName = GetProductName(i).GetAttribute("title");
            var actualProductQuantity = GetProductQuantity(i).GetAttribute("value");
            var actualProductUnitPrice = GetProductDetails(i, 4).Text;
            var actualProductTotalPrice = GetProductDetails(i, 5).Text;

            var expectedProductName = sortedList.ElementAt(i - 1).ProductName;
            var expectedProductQuantity = sortedList.ElementAt(i - 1).Quantity;
            var expectedProductUnitPrice = sortedList.ElementAt(i - 1).ChangeUnitPriceWhenShippingMethodApplied();
            var expectedProductTotalPrice = sortedList.ElementAt(i - 1).ChangeUnitPriceWhenShippingMethodApplied();

            Assert.AreEqual(expectedProductName, actualProductName);
            Assert.AreEqual(expectedProductQuantity, int.Parse(actualProductQuantity));
            Assert.AreEqual(expectedProductUnitPrice, double.Parse(actualProductUnitPrice.Substring(1)));
            Assert.AreEqual(expectedProductTotalPrice, double.Parse(actualProductTotalPrice.Substring(1)));
            expectedSubTotal += expectedProductTotalPrice;
        }

        double expectedShippingRate = 5.00;
        double expectedGrandTotal = expectedSubTotal + expectedShippingRate;

        AssertSubTotal(expectedSubTotal);
        AssertFlatShippingRate(expectedShippingRate);
        AssertGrandTotal(expectedGrandTotal);
    }

    public void AssertAlertWarningMessage(string expectedWarningMessage)
    {
        string actualWarningMessage = AlertWarningMessage().Text.Replace("\r\n×", String.Empty);
        Assert.AreEqual(expectedWarningMessage, actualWarningMessage);
    }

    public void AssertWarningMessageByLabel(string inputLabel, string expectedMessage)
    {
        string actualMessage = GetErrorMessageByLabel(inputLabel).Text;
        if (inputLabel.Equals("Telephone"))
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedMessage, actualMessage);
                AssertTelephoneHelperMessage();
            });
        }
        else
        {
            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }

    public void AssertTelephoneHelperMessage()
    {
        var expectedHelperMessage = "Enter valid phone number with country code!";
        Assert.AreEqual(expectedHelperMessage, TelephoneHelp.Text.Trim());
    }

    public void AssertPageUrl()
    {
        Assert.AreEqual(base.Url + "/index.php?route=checkout/checkout", Driver.Url);
    }

    public void AssertProductName(string expectedProductName)
    {
        Assert.AreEqual(expectedProductName, ProductName.Text);
    }

    public void AssertProductQuantity(string expectedQuantity)
    {
        Assert.AreEqual(expectedQuantity, QuantityBox.GetAttribute("value"));
    }

    public void AssertGrandTotal(double expectedGrandTotal)
    {
        Assert.AreEqual(expectedGrandTotal, double.Parse(GrandTotal.Text.Substring(1)));
    }

    public void AssertSubTotal(double expectedSubTotal)
    {
        Assert.AreEqual(expectedSubTotal, double.Parse(SubTotal.Text.Replace("$", "")));
    }
    public void AssertFlatShippingRate(double expectedShippingRate)
    {
        Assert.AreEqual(expectedShippingRate, double.Parse(FlatShippingRate.Text.Replace("$", "")));
    }
}
