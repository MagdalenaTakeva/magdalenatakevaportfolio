﻿using Bogus.DataSets;
using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Checkout;

public partial class CheckoutPage : WebPage
{
    public CheckoutPage(IWebDriver driver) : base(driver)
    {
    }

    protected override string Url => base.Url + "/index.php?route=checkout/checkout";


    public void FillInCheckoutForm(UserData userInfo,
                                                        UserTypes userType,
                                                        AddressTypes2 addressType,
                                                        BillingAddressInfo billingAddress,
                                                        ShippingAddressInfo shippingAddress,
                                                        BillingAndShippingAddresses billingAndShippingAddressesAreTheSame)
    {
        FillInClientInfo(userInfo, userType, addressType, billingAddress, shippingAddress,
            billingAndShippingAddressesAreTheSame);

        if (userType == UserTypes.NewUser)
        {
            CheckPrivacyPolicyCheckbox();
        }

        CheckTermsAndConditionsCheckbox();
        PressContinueButton();
    }

    public void FillInClientInfo(UserData userInfo,
                                                         UserTypes userType,
                                                         AddressTypes2 addressType,
                                                         BillingAddressInfo billingAddress,
                                                         ShippingAddressInfo shippingAddress,
                                                         BillingAndShippingAddresses billingAndShippingAddressesAreTheSame)
    {
        switch (userType)
        {
            case UserTypes.LoggedInUser:
                SwitchAddress(userType, addressType, billingAddress, shippingAddress);
                break;
            case UserTypes.NewUser:
                SelectRegisterAccountRadioButton();
                FillPersonalDetails(userType, userInfo);
                FillBillingAddress(userType, billingAddress, addressType);
                break;
            case UserTypes.LoggedOutUser:
                SelectLoginRadioButton();
                FillPersonalDetails(userType, userInfo);
                SwitchAddress(userType, addressType, billingAddress, shippingAddress);
                break;
            case UserTypes.GuestUser:
                SelectGuestUserCheckoutRadioButton();
                FillPersonalDetails(userType, userInfo);
                FillBillingAddress(userType, billingAddress, addressType);
                break;
        }

        if (billingAndShippingAddressesAreTheSame == BillingAndShippingAddresses.NotTheSame)
        {
            SelectDeliveryAndBillingAddressToBeTheSame();
            FillShippingAddress(shippingAddress);
        }
    }

    public void LoginUser(UserData userinfo)
    {
        ProvideLoginEmail(userinfo.Email);
        ProvideLogInPassword(userinfo.Password);
        ClickElement(LoginSubmitButton);
    }

    private void FillPersonalDetails(UserTypes userType, UserData userInfo)
    {
        switch (userType)
        {
            case UserTypes.NewUser:
                FillInputPaymentUser(userInfo, UserTypes.NewUser);
                break;
            case UserTypes.GuestUser:
                FillInputPaymentUser(userInfo, UserTypes.GuestUser);
                break;

            case UserTypes.LoggedOutUser:
                LoginUser(userInfo);
                break;
        }
    }

    public void SwitchAddress(UserTypes userType, AddressTypes2 addressType, BillingAddressInfo billingAddress, ShippingAddressInfo shippingAddress)
    {
        switch (addressType)
        {
            case AddressTypes2.NewBillingAddress:
                SelectPaymentNewAddress();
                FillBillingAddress(userType, billingAddress, addressType);
                break;
            case AddressTypes2.ExistingBillingAddress:
                SelectPaymentExistingAddress();
                break;
            case AddressTypes2.NewShippingAddress:
                SelectShippingExistingAddress();
                FillShippingAddress(shippingAddress);
                break;
            case AddressTypes2.ExistingShippingAddress:
                break;
        }
    }

    private void FillBillingAddress(UserTypes userType, BillingAddressInfo address, AddressTypes2 addressType)
    {
        if (userType == UserTypes.LoggedInUser && addressType == AddressTypes2.NewBillingAddress)
        {
            ProvidePaymentFirstName(address.FirstName);
            ProvidePaymentLastName(address.LastName);
        }

        ProvidePaymentCompany(address.Company);
        ProvidePaymentAddress1(address.Address1);
        ProvidePaymentAddress2(address.Address2);
        ProvidePaymentCity(address.City);
        ProvidePaymentPostCode(address.PostCode);
        SelectPaymentCountry(address.Country);
        SelectPaymentRegion(address.Region);

    }

    private void FillShippingAddress(ShippingAddressInfo shippingAddress)
    {
        ProvideShippingAddressFirstName(shippingAddress.FirstName);
        ProvideShippingAddressLastName(shippingAddress.LastName);
        ProvideShippingAddressCompany(shippingAddress.Company);
        ProvideShippingAddressAddress1(shippingAddress.Address1);
        ProvideShippingAddressAddress2(shippingAddress.Address2);
        ProvideShippingAddressCity(shippingAddress.City);
        ProvideShippingAddressPostCode(shippingAddress.PostCode);
        SelectShippingAddressCountry(shippingAddress.Country);
        SelectShippingAddressRegion(shippingAddress.Region);
    }

    public void SelectPaymentExistingAddress()
    {
        ClickElement(InputPaymentAddressExisting);
    }

    public void SelectPaymentNewAddress()
    {
        ClickElement(InputPaymentAddressNew);
    }

    public void SelectShippingExistingAddress()
    {
        ClickElement(ShippingAddressExisting);
    }

    public void FillInputPaymentUser(UserData userInfo, UserTypes userType)
    {
        if (userType == UserTypes.NewUser)
        {
            ProvidePaymentFirstName(userInfo.FirstName);
            ProvidePaymentLastName(userInfo.LastName);
            ProvidePaymentEmail(userInfo.Email);
            ProvidePaymentTelephone(userInfo.Telephone);
            ProvidePaymentPassword(userInfo.Password);
            ProvidePaymentConfirmPassword(userInfo.PasswordConfirm);
        } 
        else if (userType == UserTypes.GuestUser)
        {
            ProvidePaymentFirstName(userInfo.FirstName);
            ProvidePaymentLastName(userInfo.LastName);
            ProvidePaymentEmail(userInfo.Email);
            ProvidePaymentTelephone(userInfo.Telephone);
        }
    }

    public void CheckPrivacyPolicyCheckbox()
    {
        ClickElement(AgreeToPrivacyPolicy);
    }

    public void CheckTermsAndConditionsCheckbox()
    {
        ClickElement(AgreeToTermsAndConditions);
    }

    public void PressContinueButton()
    {
        ClickElement(ContinueButton);
    }

    // Radio Buttons related
    public void SelectRegisterAccountRadioButton()
    {
        ClickElement(RegisterAccountRadioButton);
    }

    public void SelectLoginRadioButton()
    {
        ClickElement(LoginRadioButton);
    }

    public void SelectGuestUserCheckoutRadioButton()
    {
        ClickElement(GuestCheckoutRadioButton);
    }

    public void GoBackToTop()
    {
        ClickElement(BackToTopIcon);
    }

    public void SelectDeliveryAndBillingAddressToBeTheSame()
    {
        ClickElement(DeliveryAndBillingAddressAreTheSameCheckbox);
    }

    //Login related
    public void ProvideLoginEmail(string email)
    {
        FillInInput(EmailLoginField, email);
    }

    public void ProvideLogInPassword(string password)
    {
        FillInInput(PasswordLoginField, password);
    }

    // PaymentInput related
    public void ProvidePaymentFirstName(string name)
    {
        FillInInput(InputPaymentFirstName, name);
    }

    public void ProvidePaymentLastName(string name)
    {
        FillInInput(InputPaymentLastName, name);
    }

    public void ProvidePaymentEmail(string email)
    {
        FillInInput(InputPaymentEmail, email);
    }

    public void ProvidePaymentTelephone(string telephone)
    {
        FillInInput(InputPaymentTelephone, telephone);
    }

    public void ProvidePaymentCompany(string company)
    {
        FillInInput(InputPaymentCompany, company);
    }

    public void ProvidePaymentAddress1(string address1)
    {
        FillInInput(InputPaymentAddress1, address1);
    }

    public void ProvidePaymentAddress2(string address2)
    {
        FillInInput(InputPaymentAddress2, address2);
    }

    public void ProvidePaymentCity(string city)
    {
        FillInInput(InputPaymentCity, city);
    }

    public void SelectPaymentCountry(string name)
    {
        ClickElement(InputPaymentCountry);
        ClickElement(GetCountryOptionByName(name));
    }

    public void SelectPaymentRegion(string name)
    {
        ClickElement(InputPaymentRegion);
        ClickElement(GetRegionOptionByName(name));
    }

    public void ProvidePaymentPostCode(string postCode)
    {
        FillInInput(InputPaymentPostCode, postCode);
    }

    public void ProvidePaymentPassword(string password)
    {
        FillInInput(InputPaymentPassword, password);

    }
    public void ProvidePaymentConfirmPassword(string confirmPassword)
    {
        FillInInput(InputPaymentPasswordConfirm, confirmPassword);
    }

    // Shipping Address related

    public void ProvideShippingAddressFirstName(string firstName)
    {
        FillInInput(ShippingAddressFirstName, firstName);
    }

    public void ProvideShippingAddressLastName(string lastName)
    {
        FillInInput(ShippingAddressLastName, lastName);
    }

    public void ProvideShippingAddressCompany(string company)
    {
        FillInInput(ShippingAddressCompany, company);
    }

    public void ProvideShippingAddressAddress1(string address1)
    {
        FillInInput(ShippingAddressAddress1, address1);
    }

    public void ProvideShippingAddressAddress2(string address2)
    {
        FillInInput(ShippingAddressAddress2, address2);
    }

    public void ProvideShippingAddressCity(string city)
    {
        FillInInput(ShippingAddressCity, city);
    }

    public void ProvideShippingAddressPostCode(string postCode)
    {
        FillInInput(ShippingAddressPostCode, postCode);
    }

    public void SelectShippingAddressCountry(string country)
    {
        ClickElement(ShippingAddressCountry);
        ClickElement(GetShippingAddressCountryOptionByName(country));
    }

    public void SelectShippingAddressRegion(string region)
    {
        ClickElement(ShippingAddressRegion);
        ClickElement(GetShippingAddressRegionOptionByName(region));
    }
}
