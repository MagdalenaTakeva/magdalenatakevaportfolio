﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Pages.Checkout;

public partial class CheckoutPage
{
    #region Buttons
    private IWebElement LoginRadioButton => WaitAndFindElement(By.XPath("//input[@id='input-account-login']/following-sibling::label"));
    private IWebElement RegisterAccountRadioButton => WaitAndFindElement(By.XPath("//input[@id='input-account-register']/following-sibling::label"));
    private IWebElement GuestCheckoutRadioButton => WaitAndFindElement(By.XPath("//input[@id='input-account-guest']/following-sibling::label"));
    #endregion

    #region Login
    private IWebElement EmailLoginField => WaitAndFindElement(By.XPath("//input[@id='input-login-email']"));
    private IWebElement PasswordLoginField => WaitAndFindElement(By.XPath("//input[@id='input-login-password']"));
    private IWebElement LoginSubmitButton => WaitAndFindElement(By.Id("button-login"));
    #endregion

    #region Personal Details and Billing Address
    private IWebElement InputPaymentAddressExisting => WaitAndFindElement(By.XPath("//input[@id='input-payment-address-existing']/parent::div/label"));
    private IWebElement InputPaymentAddressNew => WaitAndFindElement(By.XPath("//input[@id='input-payment-address-new']/parent::div/label"));
    private IWebElement InputPaymentFirstName => WaitAndFindElement(By.Id("input-payment-firstname"));
    private IWebElement InputPaymentLastName => WaitAndFindElement(By.Id("input-payment-lastname"));
    private IWebElement InputPaymentEmail => WaitAndFindElement(By.Id("input-payment-email"));
    private IWebElement InputPaymentTelephone => WaitAndFindElement(By.Id("input-payment-telephone"));
    private IWebElement InputPaymentPassword => WaitAndFindElement(By.Id("input-payment-password"));
    private IWebElement InputPaymentPasswordConfirm => WaitAndFindElement(By.Id("input-payment-confirm"));
    private IWebElement InputPaymentCompany => WaitAndFindElement(By.Id("input-payment-company"));
    private IWebElement InputPaymentAddress1 => WaitAndFindElement(By.Id("input-payment-address-1"));
    private IWebElement InputPaymentAddress2 => WaitAndFindElement(By.Id("input-payment-address-2"));
    private IWebElement InputPaymentCity => WaitAndFindElement(By.Id("input-payment-city"));
    private IWebElement InputPaymentPostCode => WaitAndFindElement(By.Id("input-payment-postcode"));
    private IWebElement InputPaymentCountry => WaitAndFindElement(By.Id("input-payment-country"));
    private IWebElement InputPaymentRegion => WaitAndFindElement(By.Id("input-payment-zone"));
    #endregion  

    #region Preferred payment and shipping methods
    private IWebElement DeliveryAndBillingAddressAreTheSameCheckbox => WaitAndFindElement(By.XPath("//input[@id='input-shipping-address-same']/parent::div/label"));
    private IWebElement ShippingAddressExisting => WaitAndFindElement(By.XPath("//input[@id='input-shipping-address-existing']/parent::div/label"));

    private IWebElement ShippingAddressFirstName => WaitAndFindElement(By.Id("input-shipping-firstname"));
    private IWebElement ShippingAddressLastName => WaitAndFindElement(By.Id("input-shipping-lastname"));
    private IWebElement ShippingAddressCompany => WaitAndFindElement(By.Id("input-shipping-company"));
    private IWebElement ShippingAddressAddress1 => WaitAndFindElement(By.Id("input-shipping-address-1"));
    private IWebElement ShippingAddressAddress2 => WaitAndFindElement(By.Id("input-shipping-address-2"));
    private IWebElement ShippingAddressCity => WaitAndFindElement(By.Id("input-shipping-city"));
    private IWebElement ShippingAddressPostCode => WaitAndFindElement(By.Id("input-shipping-postcode"));
    private IWebElement ShippingAddressCountry => WaitAndFindElement(By.Id("input-shipping-country"));
    private IWebElement ShippingAddressRegion => WaitAndFindElement(By.Id("input-shipping-zone"));

    private IWebElement CashOnDeliveryRadioButton => WaitAndFindElement(By.Id("input-payment-method-cod"));
    private IWebElement FlatRateRadioButton => WaitAndFindElement(By.Id("input-shipping-method-flat.flat"));
    #endregion

    #region Use Coupon Code
    private IWebElement UseCouponCodeLabel => WaitAndFindElement(By.XPath("//h5[.='Use Coupon Code ']"));
    private IWebElement UseCouponCodeInputField => WaitAndFindElement(By.Id("input-coupon"));
    private IWebElement ApplyCouponButton => WaitAndFindElement(By.Id("button-coupon"));
    #endregion

    #region Use Gift Certificate Code
    private IWebElement UseGiftcertificateLabel => WaitAndFindElement(By.Id("//h5[.='Use Gift Certificate ']"));
    private IWebElement UseGiftCertificateInputField => WaitAndFindElement(By.Id("input-voucher"));
    private IWebElement ApplyGiftCertificateButton => WaitAndFindElement(By.Id("button-voucher"));
    #endregion

    #region Subscribe/ Agree/ Continue
    private IWebElement SubscribeToStoreNewsletter => WaitAndFindElement(By.XPath("//input[@id='input-newsletter']/following-sibling::label"));
    private IWebElement AgreeToPrivacyPolicy => WaitAndFindElement(By.XPath("//input[@id='input-account-agree']/following-sibling::label"));
    private IWebElement AgreeToTermsAndConditions => WaitAndFindElement(By.XPath("//input[@id='input-agree']/following-sibling::label"));
    private IWebElement ContinueButton => WaitAndFindElement(By.Id("button-save"));
    #endregion

    #region Cart Products Details Table
    public IWebElement ProductImage => WaitAndFindElement(By.XPath("//div[@id='checkout-cart']//a[.//img]"));
    public IWebElement ProductName => WaitAndFindElement(By.XPath("//div[@id='checkout-cart']//td[2]/a"));
    public IWebElement Model => WaitAndFindElement(By.XPath("//div[@id='checkout-cart']//td[2]/small"));
    public IWebElement QuantityBox => WaitAndFindElement(By.XPath("//div[@id='checkout-cart']//input"));
    public IWebElement UpdateButton => WaitAndFindElement(By.XPath("//div[@id='checkout-cart']//button[1]"));
    public IWebElement RemoveButton => WaitAndFindElement(By.XPath("//div[@id='checkout-cart']//button[2]"));
    public IWebElement UnitPrice => WaitAndFindElement(By.XPath("//div[@id='checkout-cart']//td[4]"));
    public IWebElement Total => WaitAndFindElement(By.XPath("//div[@id='checkout-cart']//td[5]"));
    public IWebElement CheckoutTable => WaitAndFindElement(By.Id("checkout-total"));
    public IWebElement SubTotal => WaitAndFindElement(By.XPath("//table[@id='checkout-total']//td[text()='Sub-Total:']/following-sibling::td"));
    public IWebElement FlatShippingRate => WaitAndFindElement(By.XPath("//table[@id='checkout-total']//td[text()='Flat Shipping Rate:']/following-sibling::td"));
    public IWebElement EcoTax => WaitAndFindElement(By.XPath("//table[@id='checkout-total']//td[text()='Eco Tax (-2.00):']/following-sibling::td"));
    public IWebElement VatTax => WaitAndFindElement(By.XPath("//table[@id='checkout-total']//td[text()='VAT (20%):']/following-sibling::td"));
    public IWebElement GrandTotal => WaitAndFindElement(By.XPath("//table[@id='checkout-total']//tr[position()=last()]/td[2]"));
    public IReadOnlyCollection<IWebElement> RemoveButtons => (IReadOnlyCollection<IWebElement>)WaitAndFindElements(By.XPath("//table[@class='table']//button[2]"));
    public IWebElement BackToTopIcon => WaitAndFindElement(By.Id("back-to-top"));
    public IWebElement TelephoneHelp => WaitAndFindElement(By.XPath("//label[contains(text(), 'Telephone')]/following-sibling::div/child::small[@id='input-payment-telephone-help']"));
    #endregion

    private IReadOnlyCollection<IWebElement> TableRowsList => (IReadOnlyCollection<IWebElement>)WaitAndFindElements(By.CssSelector($"#checkout-cart table tbody tr"));
    private IWebElement GetProductDetails(int row, int col)
    {
        return WaitAndFindElement(By.CssSelector($"#checkout-cart table tbody tr:nth-child({row}) td:nth-child({col})"));
    }

    private IWebElement GetProductQuantity(int row)
    {
        return WaitAndFindElement(By.CssSelector($"#checkout-cart table tbody tr:nth-child({row}) td:nth-child(3) input"));
    }

    private IWebElement GetProductName(int row)
    {
        return WaitAndFindElement(By.CssSelector($"#checkout-cart table tbody tr:nth-child({row}) td:nth-child(2) a"));
    }
    private IWebElement GetCountryOptionByName(string countryName)
    {
        return WaitAndFindElement(By.XPath($"//select[@id='input-payment-country']/option[contains(text(),'{countryName}')]"));
    }

    public IWebElement GetShippingAddressCountryOptionByName(string countryName)
    {
        return WaitAndFindElement(By.XPath($"//select[@id='input-shipping-country']/option[contains(text(),'{countryName}')]"));
    }

    public IWebElement GetRegionOptionByName(string regionName)
    {
        return WaitAndFindElement(By.XPath($"//select[@id='input-payment-zone']/option[contains(text(),'{regionName}')]"));
    }

    private IWebElement GetShippingAddressRegionOptionByName(string regionName)
    {
        return WaitAndFindElement(By.XPath($"//select[@id='input-shipping-zone']/option[contains(text(),'{regionName}')]"));
    }

    private IWebElement GetErrorMessageByLabel(string inputLabel)
    {
        return WaitAndFindElement(By.XPath($"//label[contains(text(), '{inputLabel}')]/following-sibling::div/div"));
    }

    private IWebElement AlertWarningMessage()
    {
        return WaitAndFindElement(By.XPath($"//div[contains(@class, 'alert-warning')]"));
    }

    private IWebElement AlertSuccessMessage()
    {
        return WaitAndFindElement(By.XPath($"//div[contains(@class, 'alert-success')]"));
    }
}
