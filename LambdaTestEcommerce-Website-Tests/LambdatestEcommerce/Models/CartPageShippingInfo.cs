namespace LambdatestEcommerce.Models;

public class CartPageShippingInfo
{
    public string Country { get; set; }
    public string Region { get; set; }
    public string PostCode { get; set; }
    public string ShippingMethod { get; set; }
}