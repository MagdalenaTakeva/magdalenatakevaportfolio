﻿using LambdatestEcommerce.Enums;

namespace LambdatestEcommerce.Models;

public class Product
{
    public string ProductName { get; set; }
    public string ProductCode { get; set; }
    public string ProductBrand { get; set; }
    public double UnitPrice { get; set; }
    public double NewUnitPrice { get; set; }
    public double ProductTotalPrice { get; set; }
    public double NewProductTotalPrice { get; set; }
    public int Quantity { get; set; }
    public int ProductId { get; set; }
    public bool InStock { get; set; }
    public bool AddToCart { get; set; }
    public bool BuyNow { get; set; }
    public string ProductUrl { get; set;}
    public int ProductCategoryId{ get; set; }

    public double ChangeUnitPriceWhenShippingMethodApplied()
    {
        if (InStock)
        {
            UnitPrice = NewUnitPrice;
        }

        return UnitPrice;
    }

    public double ChangeProductTotalPriceWhenShippingMethodApplied()
    {
        if (InStock)
        {
            ProductTotalPrice = NewProductTotalPrice;
        }

        return NewProductTotalPrice;
    }
}
