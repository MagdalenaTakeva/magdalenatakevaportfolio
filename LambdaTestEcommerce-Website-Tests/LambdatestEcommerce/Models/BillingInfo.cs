﻿namespace LambdatestEcommerce.Models;

public class BillingInfo
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Telephone { get; set; }
    public string Password { get; set; }
    public string PasswordConfirm { get; set; }
    public string Company { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string PostCode { get; set; }
    public string Country { get; set; }
    public string Region { get; set; }
    public bool ShouldCreateAccount { get; set; } = false;
    public bool ShouldLogIn { get; set; } = false;
    public bool ShouldGuestCheckout { get; set; } = false;
}
