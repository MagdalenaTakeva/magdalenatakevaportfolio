using LambdatestEcommerce.Enums;

namespace LambdatestEcommerce.Models;

public class GiftCertificateInfo
{
    public string RecipientsName { get; set; }
    public string RecipientsEmail { get; set; }
    public string YourName { get; set; }
    public string YourEmail { get; set; }
    public string Amount { get; set; }
}