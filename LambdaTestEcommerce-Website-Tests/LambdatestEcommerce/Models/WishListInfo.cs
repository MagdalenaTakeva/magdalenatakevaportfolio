﻿namespace LambdatestEcommerce.Models
{
    public class WishListInfo
    {
        public string ImageLink { get; set; }
        public string ProductName { get; set; }
        public string Model { get; set; }
        public string Stock { get; set; }
        public string UnitPrice { get; set; }
    }
}
