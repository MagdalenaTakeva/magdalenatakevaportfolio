﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Models;
using LambdatestEcommerce.Pages.Cart;
using LambdatestEcommerce.Pages.Category;
using LambdatestEcommerce.Pages.Checkout;
using LambdatestEcommerce.Pages.ConfirmPage;
using LambdatestEcommerce.Pages.Login;
using LambdatestEcommerce.Pages.Main;
using LambdatestEcommerce.Pages.MyAccount;
using LambdatestEcommerce.Pages.ProductDetails;
using LambdatestEcommerce.Pages.Registration;
using LambdatestEcommerce.Pages.SuccessPage;
using LambdatestEcommerce.Sections.MainMenuSection;
using LambdatestEcommerce.Pages.MyVoucherPage;
using LambdatestEcommerce.Pages.OrderHistoryPage;
using OpenQA.Selenium;
using LambdatestEcommerce.Sections.CartInfoSection;
using LambdatestEcommerce.Sections.SearchSection;

namespace LambdatestEcommerce.Facades;
public partial class PurchaseFacade
{
    protected static IWebDriver _driver;
    protected static MainPage mainPage;
    protected static RegistrationPage registrationPage;
    protected static MyAccountPage myAccountPage;
    protected static LoginPage loginPage;
    protected static CategoryPage categoryPage;
    protected static CartPage cartPage;
    protected static CartInfoSection cartInfoSection;
    protected static CheckoutPage checkoutPage;
    protected static ConfirmPage confirmPage;
    protected static SuccessPage successPage;
    protected static SearchSection searchSection;
    protected static ProductDetailsPage productDetailsPage;
    protected static MainMenuSection mainMenuSection;
    protected static MyVoucherPage myVoucherPage;
    protected static OrderHistoryPage orderHistoryPage;

    public PurchaseFacade(IWebDriver _driver)
    {
        mainPage = new MainPage(_driver);
        registrationPage = new RegistrationPage(_driver);
        loginPage = new LoginPage(_driver);
        myAccountPage = new MyAccountPage(_driver);
        categoryPage = new CategoryPage(_driver);
        cartPage = new CartPage(_driver);
        cartInfoSection = new CartInfoSection(_driver);
        checkoutPage = new CheckoutPage(_driver);
        confirmPage = new ConfirmPage(_driver);
        productDetailsPage = new ProductDetailsPage(_driver);
        searchSection = new SearchSection(_driver);
        successPage = new SuccessPage(_driver);
        mainMenuSection = new MainMenuSection(_driver);
        myVoucherPage = new MyVoucherPage(_driver);
        orderHistoryPage = new OrderHistoryPage(_driver);
    }

    public static void AddProductToCartApplyShippingRateCheckoutAndConfirmOrder(List<Product> productList,
                                        UserData userInfo,
                                        UserTypes userType,
                                        AddressTypes2 addressType,
                                        BillingAddressInfo billingAddress,
                                        ShippingAddressInfo shippingAddress,
                                        BillingAndShippingAddresses billingAndShippingAddressesAreTheSame,
                                        CartPageShippingInfo shippingInfo)
    {
        cartPage.RemoveAllProductsFromCart();
        mainPage.SearchCategoryById(20);
        categoryPage.ClickListView();
        categoryPage.ShowProducts("100");
        categoryPage.AddMultipleProductsToCart(productList);
        cartInfoSection.ViewCart();
        cartPage.ApplyShippingRate(shippingInfo);
        cartPage.AssertAlertSuccessMessage("Success: Your shipping estimate has been applied!");
        cartPage.AssertProductsTableDetails(productList, ShippingMethod.Applied);
        cartPage.ProceedToCheckout();
        checkoutPage.FillInCheckoutForm(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);
        confirmPage.AssertProductsTableDetails(productList);
        confirmPage.ConfirmOrder();
        successPage.AssertPageTitle();
    }

    public static void SelectGridViewAddProductToCartApplyShippingRateCheckoutAndConfirmOrder(
                                        List<Product> productList,
                                        UserData userInfo,
                                        UserTypes userType,
                                        AddressTypes2 addressType,
                                        BillingAddressInfo billingAddress,
                                        ShippingAddressInfo shippingAddress,
                                        BillingAndShippingAddresses billingAndShippingAddressesAreTheSame,
                                        CartPageShippingInfo shippingInfo,
                                        ShippingMethod shippingMethod)
    {
        cartPage.RemoveAllProductsFromCart();
        mainPage.SearchCategoryById(20);
        categoryPage.ClickGridView();
        categoryPage.ShowProducts("100");
        categoryPage.AddMultipleProductsToCart(productList);
        cartInfoSection.ViewCart();
        cartPage.ApplyShippingRate(shippingInfo);
        cartPage.AssertAlertSuccessMessage("Success: Your shipping estimate has been applied!");
        cartPage.AssertProductsTableDetails(productList, shippingMethod);
        cartPage.ProceedToCheckout();
        checkoutPage.FillInCheckoutForm(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);
        confirmPage.AssertProductsTableDetails(productList);
        confirmPage.ConfirmOrder();
        successPage.AssertPageTitle();
    }

    public static void AddMultipleProductsToCartApplyShippingRateCheckoutAndConfirmOrder(UserTypes userType,
                                                                  UserData userInfo,
                                                                  AddressTypes2 addressType,
                                                                  List<Product> productList,
                                                                  BillingAddressInfo billingAddress,
                                                                  ShippingAddressInfo shippingAddress,
                                                                  BillingAndShippingAddresses billingAndShippingAddressesAreTheSame,
                                                                  CartPageShippingInfo shippingInfo)
    {
        mainPage.GoTo();
        cartInfoSection.OpenCart();
        cartInfoSection.EditCartFromCartIcon();
        cartPage.RemoveAllProductsFromCart();
        searchSection.SelectCategoryToSearch("Desktops");
        categoryPage.ShowProducts("100");
        categoryPage.AddMultipleProductsToCart(productList);
        cartInfoSection.ViewCart();
        cartPage.AssertProductsTableDetails(productList, ShippingMethod.NotApplied);
        cartPage.ApplyShippingRate(shippingInfo);
        cartPage.AssertAlertSuccessMessage("Success: Your shipping estimate has been applied!");
        cartPage.AssertProductsTableDetails(productList, ShippingMethod.Applied);
        cartPage.ProceedToCheckout();
        checkoutPage.AssertProductsTableDetails(productList);
        checkoutPage.FillInCheckoutForm(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);
        confirmPage.AssertProductsTableDetails(productList);
        confirmPage.ConfirmOrder();
        successPage.AssertPageTitle();
    }

    public static void BuyProductAsLoggedInUser(List<Product> productList,
                                               UserData userInfo,
                                               UserTypes userType,
                                               AddressTypes2 addressType,
                                               BillingAddressInfo billingAddress,
                                               ShippingAddressInfo shippingAddress,
                                               BillingAndShippingAddresses billingAndShippingAddressesAreTheSame,
                                               CartPageShippingInfo shippingInfo)
    {
        loginPage.GoTo();
        loginPage.LoginUser(userInfo.Email, userInfo.Password);
        cartInfoSection.OpenCart();
        cartInfoSection.EditCartFromCartIcon();
        cartPage.RemoveAllProductsFromCart();
        searchSection.SelectCategoryToSearch("Desktops");
        categoryPage.ShowProducts("100");
        categoryPage.AddMultipleProductsToCart(productList);
        cartInfoSection.ViewCart();
        cartPage.ApplyShippingRate(shippingInfo);
        cartPage.AssertAlertSuccessMessage("Success: Your shipping estimate has been applied!");
        cartPage.AssertProductsTableDetails(productList, ShippingMethod.Applied);
        cartPage.ProceedToCheckout();
        checkoutPage.FillInCheckoutForm(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);
        confirmPage.AssertProductsTableDetails(productList);
        confirmPage.ConfirmOrder();
        successPage.AssertPageTitle();
    }


    public static void BuyProductsByLoggingInAndAddingAdditionalItemsToAlreadyStoredItemsInCart(Product product,
                                                                                                 UserData userInfo,
                                                                                                 UserTypes userType,
                                                                                                 AddressTypes2 addressType,
                                                                                                 BillingAddressInfo billingAddress,
                                                                                                 ShippingAddressInfo shippingAddress,
                                                                                                 BillingAndShippingAddresses billingAndShippingAddressesAreTheSame,
                                                                                                 CartPageShippingInfo shippingInfo)

    {
        loginPage.LoginUser(userInfo.Email, userInfo.Password);
        cartInfoSection.OpenCart();
        cartInfoSection.EditCartFromCartIcon();
        cartPage.RemoveAllProductsFromCart();
        searchSection.SelectCategoryAndProductToSearch("Desktops", product.ProductName);
        categoryPage.ClickListView();
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        cartInfoSection.CloseProductNotification();
        cartInfoSection.OpenCart();
        cartInfoSection.AssertCartCurrentAmount("1");
        cartInfoSection.CloseCart();
        mainMenuSection.LogoutFromMyAccountMenu();
        mainMenuSection.LoginFromMyAccountMenu();
        loginPage.LoginUser(userInfo.Email, userInfo.Password);
        searchSection.SelectCategoryToSearch("Desktops");
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        cartInfoSection.ViewCart();
        cartInfoSection.AssertCartCurrentAmount("2");
        cartPage.ApplyShippingRate(shippingInfo);
        cartPage.AssertAlertSuccessMessage("Success: Your shipping estimate has been applied!");
        cartPage.ProceedToCheckout();
        checkoutPage.FillInCheckoutForm(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);
        confirmPage.AssertProductQuantity("2");
        confirmPage.ConfirmOrder();
        successPage.AssertPageTitle();
    }

    public static void AddStoredItemInCartToNewItemInCartWhenUserLogsInAtCheckout(Product product,
                                                                                    UserData userInfo,
                                                                                    UserTypes userType,
                                                                                    AddressTypes2 addressType,
                                                                                    BillingAddressInfo billingAddress,
                                                                                    ShippingAddressInfo shippingAddress,
                                                                                    BillingAndShippingAddresses billingAndShippingAddressesAreTheSame,
                                                                                    CartPageShippingInfo shippingInfo)

    {
        loginPage.LoginUser(userInfo.Email, userInfo.Password);
        cartInfoSection.OpenCart();
        cartInfoSection.EditCartFromCartIcon();
        cartPage.RemoveAllProductsFromCart();
        searchSection.SelectCategoryAndProductToSearch("Desktops", product.ProductName);
        categoryPage.ClickListView();
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        cartInfoSection.CloseProductNotification();
        cartInfoSection.OpenCart();
        cartInfoSection.AssertCartCurrentAmount("1");
        cartInfoSection.CloseCart();
        mainMenuSection.LogoutFromMyAccountMenu();
        mainMenuSection.LoginFromMyAccountMenu();
        searchSection.SelectCategoryToSearch("Desktops");
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        cartInfoSection.CloseProductNotification();
        cartInfoSection.OpenCart();
        cartInfoSection.AssertCartCurrentAmount("1");
        cartInfoSection.EditCartFromCartIcon();
        cartPage.ApplyShippingRate(shippingInfo);
        cartPage.AssertAlertSuccessMessage("Success: Your shipping estimate has been applied!");
        cartPage.ProceedToCheckout();
        checkoutPage.FillInCheckoutForm(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);
        confirmPage.AssertProductQuantity("2");
    }


    public static void LoggedInUserBuysGiftCertificateAndOneProduct(UserData userInfo,
                                                                     GiftCertificateInfo giftCertificateInfo,
                                                                     GiftCertificateTheme giftCertificateTheme,
                                                                     Product product,
                                                                     CartPageShippingInfo shippingInfo,
                                                                     BillingAndShippingAddresses billingAndShippingAddressesAreTheSame,
                                                                     string expectedSubTotal,
                                                                     string expectedShippingRate,
                                                                     double expectedGrandTotal)
    {
        loginPage.LoginUser(userInfo.Email, userInfo.Password);
        cartPage.RemoveAllProductsFromCart();
        mainMenuSection.OpenMyVoucherPage();
        myVoucherPage.FillInGiftCertificate(giftCertificateInfo, giftCertificateTheme);
        myVoucherPage.AssertGiftCertificateFormCompletedSuccessfully("/index.php?route=account/voucher/success");
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        cartInfoSection.ViewCart();
        cartPage.ApplyShippingRate(shippingInfo);
        cartPage.AssertAlertSuccessMessage("Success: Your shipping estimate has been applied!");
        cartPage.AssertOrderSubtotal(expectedSubTotal);
        cartPage.AssertFlatShippingRate(expectedShippingRate);
        cartPage.AssertGrandTotal(expectedGrandTotal);
        cartPage.ProceedToCheckout();
        checkoutPage.AssertGrandTotal(expectedGrandTotal);
        checkoutPage.CheckTermsAndConditionsCheckbox();
        checkoutPage.PressContinueButton();
        confirmPage.AssertPaymentAddress("Tony Marinov Planet fgfg Sofia 1000 Sofia - town,Bulgaria");
        confirmPage.AssertPaymentAndShippingAddressesAreTheSame(billingAndShippingAddressesAreTheSame);
        confirmPage.AssertShippingMethodIsFlatShippingRate();
        confirmPage.ConfirmOrder();
        successPage.AssertPageUrl();
        successPage.ClickMyAccountLink();
        myAccountPage.ClickOnLinkByLabel("View your order history");
        orderHistoryPage.AssertPageUrl();
        orderHistoryPage.AssertCustomerName("Tony Tony");
    }
}
