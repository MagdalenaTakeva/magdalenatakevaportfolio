namespace LambdatestEcommerce.Facades;
using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Models;
using LambdatestEcommerce.Pages.Cart;
using LambdatestEcommerce.Pages.Category;
using LambdatestEcommerce.Pages.Checkout;
using Pages.ConfirmPage;
using LambdatestEcommerce.Pages.Login;
using LambdatestEcommerce.Pages.Main;
using LambdatestEcommerce.Pages.MyAccount;
using LambdatestEcommerce.Pages.ProductDetails;
using LambdatestEcommerce.Pages.Registration;
using LambdatestEcommerce.Pages.SuccessPage;
using LambdatestEcommerce.Sections.MainMenuSection;
using LambdatestEcommerce.Pages.MyVoucherPage;
using LambdatestEcommerce.Pages.OrderHistoryPage;
using OpenQA.Selenium;

public class CheckoutFacade
{
    protected static IWebDriver _driver;
    protected static MainPage mainPage;
    protected static RegistrationPage registrationPage;
    protected static MyAccountPage myAccountPage;
    protected static LoginPage loginPage;
    protected static CategoryPage categoryPage;
    protected static CartPage cartPage;
    protected static CheckoutPage checkoutPage;
    protected static ConfirmPage confirmPage;
    protected static SuccessPage successPage;
    protected static ProductDetailsPage productDetailsPage;
    protected static MainMenuSection mainMenuSection;
    protected static MyVoucherPage myVoucherPage;
    protected static OrderHistoryPage orderHistoryPage;

    public CheckoutFacade(IWebDriver _driver)
    {
        mainPage = new MainPage(_driver);
        registrationPage = new RegistrationPage(_driver);
        loginPage = new LoginPage(_driver);
        myAccountPage = new MyAccountPage(_driver);
        categoryPage = new CategoryPage(_driver);
        cartPage = new CartPage(_driver);
        checkoutPage = new CheckoutPage(_driver);
        confirmPage = new ConfirmPage(_driver);
        productDetailsPage = new ProductDetailsPage(_driver);
        successPage = new SuccessPage(_driver);
        mainMenuSection = new MainMenuSection(_driver);
        myVoucherPage = new MyVoucherPage(_driver);
        orderHistoryPage = new OrderHistoryPage(_driver);
    }
    
}