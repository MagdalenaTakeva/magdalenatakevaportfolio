﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Models;
using LambdatestEcommerce.Pages.Cart;
using LambdatestEcommerce.Pages.Category;
using LambdatestEcommerce.Pages.Checkout;
using LambdatestEcommerce.Pages.ConfirmPage;
using LambdatestEcommerce.Pages.Login;
using LambdatestEcommerce.Pages.Main;
using LambdatestEcommerce.Pages.MyAccount;
using LambdatestEcommerce.Pages.MyVoucherPage;
using LambdatestEcommerce.Pages.ProductDetails;
using LambdatestEcommerce.Pages.Registration;
using LambdatestEcommerce.Pages.VoucherSuccessPage;
using LambdatestEcommerce.Sections.CartInfoSection;
using LambdatestEcommerce.Sections.MainMenuSection;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Facades;
public class CartFacade
{
    protected static IWebDriver _driver;
    protected static MainPage mainPage;
    protected static RegistrationPage registrationPage;
    protected static MyAccountPage myAccountPage;
    protected static LoginPage loginPage;
    protected static CategoryPage categoryPage;
    protected static CartPage cartPage;
    protected static ConfirmPage confirmPage;
    protected static CheckoutPage checkoutPage;
    protected static MyVoucherPage myVoucherPage;
    protected static VoucherSuccessPage voucherSuccessPage;
    protected static ProductDetailsPage productDetailsPage;
    protected static CartInfoSection cartInfoSection;
    protected static Product product;
    protected static MainMenuSection mainMenuSection;

    public CartFacade(IWebDriver _driver)
    {
        mainPage = new MainPage(_driver);
        registrationPage = new RegistrationPage(_driver);
        loginPage = new LoginPage(_driver);
        myAccountPage = new MyAccountPage(_driver);
        categoryPage = new CategoryPage(_driver);
        cartPage = new CartPage(_driver);
        checkoutPage = new CheckoutPage(_driver);
        confirmPage = new ConfirmPage(_driver);
        myVoucherPage = new MyVoucherPage(_driver);
        voucherSuccessPage = new VoucherSuccessPage(_driver);
        confirmPage = new ConfirmPage(_driver);
        productDetailsPage = new ProductDetailsPage(_driver);
        cartInfoSection = new CartInfoSection(_driver);
        mainMenuSection = new MainMenuSection(_driver);
    }

    public static void AddProductToCart(Product newProduct)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        
        categoryPage.AddProductToCartByProductId(newProduct.ProductId, newProduct.Quantity);
        cartInfoSection.ViewCart();

        cartPage.AssertProductName(newProduct.ProductName);
        cartPage.AssertProductQuantity(newProduct.Quantity.ToString());
        cartPage.AssertUnitPrice(newProduct.UnitPrice);
        cartPage.AssertTotalPrice(newProduct.ProductTotalPrice);
    }

    public static void AddMultipleProductsToCart(List<Product> newProductsList, string expectedSubTotal, string expectedEcoTax, string expectedVatTax, double expectedTotal)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");

        categoryPage.AddMultipleProductsToCart(newProductsList);
        cartInfoSection.ViewCart();

        cartPage.AssertOrderSubtotal(expectedSubTotal);
        cartPage.AssertOrderEcoTax(expectedEcoTax);
        cartPage.AssertOrderVatTax(expectedVatTax);
        cartPage.AssertGrandTotal(expectedTotal);
    }

    public static void IncreaseProductQuantity(Product newProduct, string newQuantity)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.ClickProductImage(newProduct);
        productDetailsPage.AddProductToCart();
        cartInfoSection.ViewCart();

        cartPage.IncreaseProductQuantity(newQuantity);

        cartPage.AssertProductQuantity(newQuantity);
    }

    public static void RemoveProductQuantity(Product newProduct)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(newProduct.ProductId, newProduct.Quantity);
        cartInfoSection.ViewCart();

        cartPage.RemoveAllProductsFromCart();

        cartPage.AssertShoppingCartEmptyTextDisplayed();
    }

    public static void ProceedToCheckout(List<Product> newProductsList)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.AddMultipleProductsToCart(newProductsList);
        cartInfoSection.ViewCart();

        cartPage.ProceedToCheckout();

        checkoutPage.AssertPageUrl();
    }
    
    public static void ApplyShippingRate(string expectedSubTotal, string expectedShippingRate, double expectedGrandTotal, Product newProduct, CartPageShippingInfo shippingInfo)
    {
        cartPage.RemoveAllProductsFromCart();
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(newProduct.ProductId, newProduct.Quantity);
        cartInfoSection.ViewCart();

        cartPage.ApplyShippingRate(shippingInfo);

        cartPage.AssertShippingEstimateHasBeenApplied();
        cartPage.AssertFlatShippingRate(expectedShippingRate);
    }
    
    public static void FillInGiftCertificateAndAddItToCart(UserData userInfo, GiftCertificateInfo giftCertificateInfo, GiftCertificateTheme giftTheme)
    {
        mainPage.ClickMyAccountLoginLink();
        loginPage.LoginUser(userInfo.Email, userInfo.Password);
        cartPage.RemoveAllProductsFromCart();
        mainMenuSection.OpenMyVoucherPage();

        myVoucherPage.FillInGiftCertificate(giftCertificateInfo, giftTheme);
        myVoucherPage.AssertGiftCertificateFormCompletedSuccessfully("/index.php?route=account/voucher/success");
        voucherSuccessPage.ClickContinueButton();
        
        cartPage.AssertGiftCertificateAddedToCart();
    }
    
    public static void DisplayWarningWhenGiftInputFieldEmptyAndApplyGiftCertificateButtonClicked(Product newProduct, string expectedWarning)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(newProduct.ProductId, newProduct.Quantity);
        cartInfoSection.ViewCart();

        cartPage.ApplyGiftSertificate(string.Empty);
        
        cartPage.AssertWarningMessage(expectedWarning);
    }
    
    public static void DisplayWarningWhenCouponInputEmptyAndApplyCouponButtonClicked(Product newProduct, string expectedWarning)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(newProduct.ProductId, newProduct.Quantity);
        cartInfoSection.ViewCart();
    
        cartPage.ApplyGiftSertificate("Gift Certificate for Tony Tony");
    
        cartPage.AssertWarningMessage(expectedWarning);
    }

    public static void DisplayWarningWhenCouponInputFieldEmptyAndApplyCouponButtonClicked(Product newProduct, string expectedWarning)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(newProduct.ProductId, newProduct.Quantity);
        cartInfoSection.ViewCart();

        cartPage.ApplyCoupon(string.Empty);

        cartPage.AssertWarningMessage(expectedWarning);
    }
    
    public static void DisplayWarningWhenCouponInputNotValidAndApplyCouponButtonClicked(Product newProduct, string expectedWarning)
    {
        mainPage.SearchCategoryById(20);
        categoryPage.ShowProducts("100");
        categoryPage.AddProductToCartByProductId(newProduct.ProductId, newProduct.Quantity);
        cartInfoSection.ViewCart();
    
        cartPage.ApplyCoupon("abv");
      
        cartPage.AssertWarningMessage(expectedWarning);
    }
}

