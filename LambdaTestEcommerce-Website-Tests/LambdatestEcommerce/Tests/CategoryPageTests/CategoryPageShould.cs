﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.CategoryPageTests;

[TestFixture]
public class CategoryPageShould : BaseTest
{
    [SetUp]
    public void SetUp()
    {
        List<string> ShowValues = new List<string> { "15", "25", "50", "75", "100" };
        List<string> CategoriesList = new List<string> { "All Categories", "Desktops", "Laptops", "Components", "Tablets",
                                                          "Software", "Phones & PDAs", "Cameras", "MP3 Players" };
        var category = CategoriesList[2];

        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch(category);
        _categoryPage.ShowProducts(ShowValues[4]);
    }

    [Test]
    public void UserRedirectedToProductPage_When_ProductNameClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];

        _categoryPage.ClickProductName(product);

        _productDetailsPage.AssertProductName(product.ProductName);
    }

    [Test]
    public void UserRedirectedToProductPage_When_ProductImageClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[1];

        _categoryPage.ClickProductImage(product);

        _productDetailsPage.AssertProductPageUrl(product.ProductUrl);
    }

    [Test]
    public void OneItemAddedToCart_When_ProductAddToCartButtonClickedOnce()
    {
        var product = ProductFactory.DefaultProductsInStock()[1];

        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);

        _cartInfoSection.ViewCart();

        _cartPage.AssertProductName(product.ProductName);
    }

    [Test]
    // Bug EOS5D cannot be added to cart
    public void EOS5DAddedToCart_When_ProductAddToCartButtonClickedOnce()
    {
        var product = ProductFactory.Bug_CannonEOS5D();

        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);

        _cartInfoSection.ViewCart();

        _cartPage.AssertProductName(product.ProductName);
    }

    [Test]
    public void ProductInStockAddedMoreThanOnceToCart_When_ProductAddToCartButtonClickedMoreThanOnce()
    {
        var product = ProductFactory.DefaultProductInStockQuantityMoreThan1()[0];

        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);

        _cartInfoSection.ViewCart();

        _cartPage.AssertProductQuantity(product.Quantity.ToString());
    }


    [Test]
    public void NotInStockProductAddedToCart_When_ProductAddToCartButtonClickedOnce()
    {
        var product = ProductFactory.DefaultProductsOutOfStock()[0];

        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        _cartInfoSection.ViewCart();

        _cartPage.AssertProductName(product.ProductName);
    }

    [Test]
    public void ProductsFilteredByManufacturer_When_ManufacturerSelectedFromManufacturerFilter()
    {
        _categoryPage.FilterByManufacturer("Sony");

        _categoryPage.AssertProductsCount();//1
    }

    [Test]
    // This is a bug - SeeMore covers Nikon's name, thus Nikon cannot be selected
    public void UserUnableToSelectManufacturer_When_SeeMoreClicked()
    {
        _categoryPage.FilterByManufacturer("Nikon");

        _categoryPage.AssertProductsCount();//should be 2
    }

    [Test]
    //There is a bug - HTC cannot be selected as SeeMore element is covering it
    public void UserUnableToSelectHTCAsSeeMoreElementCoversIt()
    {
        _categoryPage.FilterByManufacturer("Sony");
        _categoryPage.FilterByManufacturer("HTC");
        
        _categoryPage.AssertProductsCount();
    }

    [Test]
    public void UserFilteredProductsByTwoManufacturers_When_TwoManufacturersClicked()
    {
        _categoryPage.SelectManufacturer("Sony");
        _categoryPage.SelectManufacturer("Apple");

        _categoryPage.AssertProductsCount();//43
    }

    [Test]
    public void BorderColorOfSelectedManufacturerImageIsNotTransparent_When_ManufacturerImageSelected()
    {
        _categoryPage.SelectManufacturer("Sony");

        _categoryPage.AssertManufacturerImageBorderColorWhenElementSelected("Sony");
    }

    [Test]
    public void BorderOfNotSelectedManufacturerImageIsTransparent()
    {
        _categoryPage.SelectManufacturer("Sony");
        _categoryPage.DeselectManufacturer("Sony");

        _categoryPage.AssertManufacturerImageBorderColorWhenElementNotSelected("Sony");
    }

    [Test]
    public void UserClearedAllFilters_When_ClearAllXClicked()
    {
        var manufacturer1 = "Sony";
        var manufacturer2 = "Apple";

        _categoryPage.ClickListView();
        _categoryPage.SelectManufacturer(manufacturer1);
        _categoryPage.SelectManufacturer(manufacturer2);

        _categoryPage.ClearAllFilters();

        _categoryPage.AssertManufacturerImageBorderColorWhenElementNotSelected(manufacturer1);
        _categoryPage.AssertManufacturerImageBorderColorWhenElementNotSelected(manufacturer2);
    }

    [Test]
    public void UserMovedPriceSliderToTheRightSuccessfully()
    {
        _categoryPage.MovePriceSlider(105, 0);
        _categoryPage.AssertPriceSliderOffset("left: 50.3155%;");

        _categoryPage.MovePriceSlider(-101, 0);
        _categoryPage.AssertPriceSliderOffset("left: 1.89274%;");
    }

    [Test]
    public void UserSearchedForProductByName_When_ProductNameEnteredInSearchInputField_And_EnterKeyClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[1];

        _categoryPage.UseSearchFilter(product.ProductName);

        _categoryPage.AssertAllProductsDisplayedHaveTheSameName(product.ProductName);
        _categoryPage.AssertProductsCount();
    }
}