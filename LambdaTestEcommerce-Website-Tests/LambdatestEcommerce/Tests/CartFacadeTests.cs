﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Facades;
using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests;
public class CartFacadeTests : BaseTest
{
    [Test]
    public void ProductAddedToCart_When_ProductAddToCartButtonClicked()
    {
        var productsInStockList = ProductFactory.DefaultProductsInStock();
        CartFacade.AddProductToCart(productsInStockList[1]);
    }

    // for loggedIn users prices are different
    [Test]
    public void MultipleProductsAddedToCart_When_AddToCartButtonClicked()
    {
        var productsInStockList = ProductFactory.DefaultProductsInStock();
        CartFacade.AddMultipleProductsToCart(productsInStockList, "$320.00", "$4.00", "$64.00", 388);
    }

    [Test]
    public void IncreasedProductQuantity_When_QuantityIncreasedByHand_And_UpdateButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[1];
        CartFacade.IncreaseProductQuantity(product, "2");
    }

    [Test]
    public void ProductQuantityRemoved_When_RemoveButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[1];
        CartFacade.RemoveProductQuantity(product);
    }

    [Test]
    public void ProceedToCheckout_When_CheckoutButtonClicked()
    {
        var productsInStockList = ProductFactory.DefaultProductsInStock();
        CartFacade.ProceedToCheckout(productsInStockList);
    }

    [Test]
    public void ShippingRateApplied_When_CountryRegionAndPostCodeSelected_GetQuoteButtonClicked_FlatShippingRateSelected_And_ApplyShippingClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        CartFacade.ApplyShippingRate("$200.00", "$5.00", 205.00, product, shippingInfo);
    }

    [Test]
    public void GiftCertificateAddedToCartByLoggedInUser()
    {
        var userInfo = UserInfoFactory.DefaultUserTony();
        var giftCertificateInfo = GiftCertificateFactory.BirthdayGiftCertificate();
        CartFacade.FillInGiftCertificateAndAddItToCart(userInfo, giftCertificateInfo, GiftCertificateTheme.Birthday);
    }

    [Test]
    public void WarningMessageDisplayed_When_GiftInputFieldIsLeftEmpty_And_ApplyGiftCertificateButtonIsClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        string warningMessage = "Warning: Please enter a gift certificate code!";
        CartFacade.DisplayWarningWhenGiftInputFieldEmptyAndApplyGiftCertificateButtonClicked(product, warningMessage);
    }

    [Test]
    public void WarningMessageDisplayed_When_GiftInputNotValid_And_ApplyGiftCertificateButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        string warningMessage = "Warning: Gift Certificate is either invalid or the balance has been used up!";
        CartFacade.DisplayWarningWhenCouponInputEmptyAndApplyCouponButtonClicked(product, warningMessage);
    }

    [Test]
    public void WarningMessageDisplayed_When_CouponInputEmpty_And_ApplyCouponButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        string warningMessage = "Warning: Please enter a coupon code!";
        CartFacade.DisplayWarningWhenCouponInputFieldEmptyAndApplyCouponButtonClicked(product, warningMessage);
    }

    [Test]
    public void WarningMessageDisplayed_When_CouponInputNotValid_And_ApplyCouponButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        string warningMessage = "Warning: Coupon is either invalid, expired or reached its usage limit!";
        CartFacade.DisplayWarningWhenCouponInputNotValidAndApplyCouponButtonClicked(product, warningMessage);
    }
}
