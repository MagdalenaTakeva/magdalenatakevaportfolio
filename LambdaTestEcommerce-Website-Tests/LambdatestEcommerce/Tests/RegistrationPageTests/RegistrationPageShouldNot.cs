﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.RegistrationPageTests;

[TestFixture]
public class RegistrationPageShouldNot : BaseTest
{
    [Test]
    public void UserUnableToRegister_When_ExistingEmailProvided()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());


        _registrationPage.ProvideEmail(UserInfoFactory.DefaultUserTony().Email);
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertWarningMessage("Warning: E-Mail Address is already registered!");
    }

    [Test]
    public void UserUnableToRegister_When_AgreeToPrivacyPolicyCheckBoxNotChecked()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ClickContinueButton();

        _registrationPage.AssertWarningMessage("Warning: You must agree to the Privacy Policy!");
    }

    [Test]
    [Combinatorial]
    public void UserUnableToRegister_When_FirstNameNotValid(
       [Values("", "FirstFirstFirstFirstFirstFirstFir")] string firstNameInvalidInput)
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvideFirstName(firstNameInvalidInput);
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertErrorMessageByLabel("First Name", "First Name must be between 1 and 32 characters!");
    }

    [Test]
    [Combinatorial]
    public void UserUnableToRegister_When_LastNameNotValid(
      [Values("", "FirstFirstFirstFirstFirstFirstFir")] string lastNameInvalidInput)
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvideLastName(lastNameInvalidInput);
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertErrorMessageByLabel("Last Name", "Last Name must be between 1 and 32 characters!");
    }
   
    //bug - user is able to register with a password of more than 20 chars
    [Test]
    [Combinatorial]
    public void UserUnableToRegister_When_PasswordNotValid(
     [Values("", "T56", "123456789123456789123")] string passwordInvalidInput)
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvidePassword(passwordInvalidInput);
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertErrorMessageByLabel("Password", "Password must be between 4 and 20 characters!");
    }

    [Test]
    public void UserUnableToRegister_When_PasswordAndConfirmPasswordDoNotMatch()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvidePassword(UserInfoFactory.DefaultUserTony().Password);
        _registrationPage.ProvidePasswordConfirm(string.Empty);
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertErrorMessageByLabel("Password Confirm", "Password confirmation does not match password!");
    }

    [Test]
    public void UserUnableToRegister_When_TelephoneNotValid()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvideTelephone(string.Empty);
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertTelephoneWarningMessageDisplayedTogetherWithHelperMessage("Telephone", "Telephone must be between 3 and 32 characters!", "Enter valid phone number with country code!");
    }

    [Test]
    public void UserUnableToRegister_When_EmailBlank()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvideEmail(string.Empty);
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertErrorMessageByLabel("E-Mail","E-Mail Address does not appear to be valid!");
    }

    [Test]
    [Combinatorial]
    public void UserUnableToRegister_When_EmailHasNoAtSign(
        [Values("maggie", "maggie.com", "...")] string emailInvalidInput)
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvideEmail(emailInvalidInput);
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertEmailHasNoAtSign(emailInvalidInput);
    }

    [Test]
    public void UserUnableToRegister_When_EmailMissesTextPartBeforeAtSign()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvideEmail("@");
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertEmailMissesPartBeforeAtSign("@");
    }

    [Test]
    public void UserUnableToRegister_When_EmailMissesTextPartAfterAtSign()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvideEmail("maggie@");
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertEmailMissesPartAfterAtSign("maggie@");
    }

    [Test]
    public void UserUnableToRegister_When_DotSignInEmailIsAtTheWrongPosition()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());

        _registrationPage.ProvideEmail("maggie@.com");
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertDotSignInEmailIsAtTheWrongPosition();
    }
}