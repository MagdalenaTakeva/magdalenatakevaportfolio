﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.RegistrationPageTests;

[TestFixture]
public class RegistrationPageShould : BaseTest
{
    [Test]
    public void UserSuccessfullyRegistered_When_ValidCredentialsProvided()
    {
        _registrationPage.RegisterNewUser(UserInfoFactory.CreateUser());
        _registrationPage.CheckPrivacyPolicyCheckbox();
        _registrationPage.ClickContinueButton();

        _registrationPage.AssertSuccessfulRegistrationUrl();
    }

    [Test]
    public void AllRequiredInputFieldsDisplayed_When_UserRedirectedToRegistrationPage()
    {
        _registrationPage.GoTo();

        _registrationPage.AssertInputFieldDisplayed("First Name");
        _registrationPage.AssertInputFieldDisplayed("Last Name");
        _registrationPage.AssertInputFieldDisplayed("E-Mail");
        _registrationPage.AssertInputFieldDisplayed("Telephone");
        _registrationPage.AssertInputFieldDisplayed("Password");
        _registrationPage.AssertInputFieldDisplayed("Password Confirm");
    }

    [Test]
    public void LinkToLoginPageDisplayed_When_UserRedirectedToRegistrationPage()
    {
        _registrationPage.GoTo();

        _registrationPage.AssertLinkToLoginPageDisplayed();
    }

    [Test]
    public void UserRedirectedToLoginPage_When_LoginLinkClicked()
    {
        _registrationPage.GoTo();

        _registrationPage.GoToLoginPage();

        _myAccountPage.AssertPageUrl("/index.php?route=account/login");
    }

    [Test]
    public void UserRedirectedToPrivacyPolicyDialogBox_When_PrivacyPolicyLinkClicked()
    {
        _registrationPage.GoTo();

        _registrationPage.GoToPrivacyPolicyPage();

        _registrationPage.AssertPrivacyPolicyDialogBoxHeaderDisplayed();
    }

    [Test]
    public void UserRedirectedToLoginPage_When_RightColumnLoginLinkClicked()
    {
        _registrationPage.GoTo();

        _registrationPage.ClickRightColumnLinkByName("Login");

        _myAccountPage.AssertPageUrl("/index.php?route=account/login");
    }

    [Test]
    public void UserRedirectedToForgottenPasswordPage_When_RightColumnForgottenPasswordLinkClicked()
    {
        _registrationPage.GoTo();

        _registrationPage.ClickRightColumnLinkByName("Forgotten Password");

        _myAccountPage.AssertPageUrl("/index.php?route=account/forgotten");
    }
}