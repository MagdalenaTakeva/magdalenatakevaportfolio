﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.CompareProductsSectionTests
{
    public class CompareProductsSectionTests : BaseTest
    {
        [SetUp]
        public void TestSetUp()
        {
            _mainPage.GoTo();
        }

        [Test]
        public void LoggedInUserRedirectedToProductsComparePage_When_ProductsCompareIconClicked()
        {
            var user = UserInfoFactory.DefaultUserTony();

            _mainMenuSection.LoginFromMyAccountMenu();
            _loginPage.LoginUser(user.Email, user.Password);

            _compareProductsSection.OpenCompareProductsPage();

            _compareProductsPage.AssertPageUrl();
        }

        [Test]
        public void NoProductsChosenMessageDisplayed_When_NewUserClickedOnCompareProductsIcon_Without_ProductsBeingAddedForComparison()
        {
            _compareProductsSection.OpenCompareProductsPage();

            _compareProductsPage.AssertPageTitle();

            _compareProductsPage.AssertUserNotChosenAnyProducts();
        }

        [Test]
        public void UserRedirectedToHomePage_When_ContinueButtonClicked()
        {
            _compareProductsSection.OpenCompareProductsPage();

            _compareProductsPage.ClickContinueButton();

            _mainPage.AssertPageUrl("/index.php?route=common/home");
        }

        [Test]
        public void UserAddedMultipleProductsToCompareProductsPage_When_CompareButtonClicked_And_CompareProductsIconClicked()
        {
            var category = "Desktops";
            var productlist = ProductFactory.DefaultProductsInStock();


            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ClickListView();
            _categoryPage.ShowProducts("100");

            _categoryPage.AddMultipleProductsToCompare(productlist);
            _compareProductsSection.OpenCompareProductsPage();

            _compareProductsPage.AssertPageUrl();
            _compareProductsPage.AssertNumberOfProductsAsExpected(3);
        }

        [Test]
        public void UserAddedMultipleProductsToCartFromCompareProductsPage_When_AllAddToCartButtonsClickedOneBYOne()
        {
            var category = "Desktops";
            var productList = ProductFactory.DefaultProductToCompareList();


            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ClickListView();
            _categoryPage.ShowProducts("100");
            _categoryPage.AddMultipleProductsToCompare(productList);
            _compareProductsSection.CloseCompareProductNotification();
            _compareProductsSection.OpenCompareProductsPage();
            _compareProductsPage.AssertNumberOfProductsAsExpected(2);

            _compareProductsPage.AddAllProductsToCart();
            _cartInfoSection.CloseProductNotification();
            _cartInfoSection.ViewCart();

            _cartPage.AssertNumberOfProductsAsExpected(2);
        }

        [Test]
        // Bug with Canon EOS 5D - cannot Add It To Cart From CompareProductsPage.
        // Instead QuickView appears where required select dropdown is not working - no options to selecty
        // Thus test cannot continue
        public void UserAddedCanonEOS5DToCartFromCompareProductsPage_When_AddToCartButtonClicked()
        {
            var category = "Desktops";
            var product = ProductFactory.Bug_CannonEOS5D();


            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ClickListView();
            _categoryPage.ShowProducts("100");
            _categoryPage.AddProductToCompareList(product.ProductId);
            _compareProductsSection.CloseCompareProductNotification();
            _compareProductsSection.OpenCompareProductsPage();
            _compareProductsPage.AssertNumberOfProductsAsExpected(2);

            _compareProductsPage.AddProductToCart(product.ProductId);
            _cartInfoSection.CloseProductNotification();
            _cartInfoSection.ViewCart();

            _cartPage.AssertNumberOfProductsAsExpected(1);
        }

        [Test]
        public void UserRemovedProduct2FromCompareProductsPage_When_RemoveButtonClicked()
        {
            var category = "Desktops";
            var product1 = ProductFactory.DefaultProductsInStock()[0];
            var product2 = ProductFactory.DefaultProductsInStock()[2];

            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ClickListView();
            _categoryPage.ShowProducts("100");
            _categoryPage.AddProductToCompareList(product1.ProductId);
            _compareProductsSection.CloseCompareProductNotification();
            _categoryPage.AddProductToCompareList(product2.ProductId);
            _compareProductsSection.CloseCompareProductNotification();
            _compareProductsSection.OpenCompareProductsPage();
            _compareProductsPage.AssertNumberOfProductsAsExpected(2);

            _compareProductsPage.RemoveProduct(product2.ProductId);

            _compareProductsPage.AssertPageUrl();
            _compareProductsPage.AssertNumberOfProductsAsExpected(1);
            _compareProductsPage.AssertSuccessMessageDisplayed();
        }

        [Test]
        public void UserRemovedAllProductsFromCompareProductsPage_When_ProductsRemoveButtonClicked()
        {
            var category = "Desktops";
            var productList = ProductFactory.DefaultProductToCompareList();

            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ClickListView();
            _categoryPage.ShowProducts("100");
            _categoryPage.AddMultipleProductsToCompare(productList);
            _compareProductsSection.OpenCompareProductsPage();
            _compareProductsPage.AssertNumberOfProductsAsExpected(2);

            // _compareProductsPage.RemoveMultipleProductsFromProductsComparePage(productList);
            _compareProductsPage.RemoveAllProductsFromCompareProductsPage();

            _compareProductsPage.AssertPageUrl();
            _compareProductsPage.AssertNumberOfProductsAsExpected(0);
            _compareProductsPage.AssertSuccessMessageDisplayed();
        }

        [Test]

        public void UserAddedTwoItemFromTopProductsCarouselToProductComparePage_When_ProductDisplayedInCarousel_And_CompareThisProductButtonClicked()
        {
            var productsToCompare = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product1Id = productsToCompare[6].ProductId;
            var product2Id = productsToCompare[0].ProductId;
            
            _mainPage.AddItemsInTopProductsModuleToCompareList(productsToCompare, product1Id, product2Id);
            _compareProductsSection.OpenCompareProductsPage();

            _compareProductsPage.AssertNumberOfProductsAsExpected(2);
        }
    }
}
