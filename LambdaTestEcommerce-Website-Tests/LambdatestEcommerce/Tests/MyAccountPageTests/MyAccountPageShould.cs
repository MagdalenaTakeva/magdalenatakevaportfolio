﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.MyAccountTests;

[TestFixture]
public class MyAccountPageShould : BaseTest
{
    [SetUp]
    public void LoginUser()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());
    }

    [Test]
    public void ExistingUserRedirectedToEditAccountPage_When_EditYourAccountInformationLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Edit your account information");

        _myAccountPage.AssertPageUrl("/index.php?route=account/edit");
    }

    [Test]
    public void ExistingUserRedirectedToChangePasswordPage_When_ChangeYourPasswordLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Change your password");

        _myAccountPage.AssertPageUrl("/index.php?route=account/password");
    }

    [Test]
    public void ExistingUserRedirectedToAddressBookPage_When_ModfyYourAddressBookEntriesLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Modify your address book entries");

        _myAccountPage.AssertPageUrl("/index.php?route=account/address");
    }

    [Test]
    public void ExistingUserRedirectedToWishListPage_When_ModfyYourWishListLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Modify your wish list");

        _myAccountPage.AssertPageUrl("/index.php?route=account/wishlist");
    }

    [Test]
    public void ExistingUserRedirectedToNewsletterSubscribtionPage_When_SubscribeUnsubscribeToNewsletterLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Subscribe / unsubscribe to newsletter");

        _myAccountPage.AssertPageUrl("/index.php?route=account/newsletter");
    }

    [Test]
    public void ExistingUserRedirectedToOrderHistoryPage_When_ViewYourOrderHistoryLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("View your order history");

        _myAccountPage.AssertPageUrl("/index.php?route=account/order");
    }

    [Test]
    public void ExistingUserRedirectedToDownloadsPage_When_DownloadsLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Downloads");

        _myAccountPage.AssertPageUrl("/index.php?route=account/download");
    }

    [Test]
    public void ExistingUserRedirectedToRewardPage_When_YourRewardPointsLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Your Reward Points");

        _myAccountPage.AssertPageUrl("/index.php?route=account/reward");
    }

    [Test]
    public void ExistingUserRedirectedToReturnsPage_When_ViewYourReturnRequestsLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("View your return requests");

        _myAccountPage.AssertPageUrl("/index.php?route=account/return");
    }

    [Test]
    public void ExistingUserRedirectedToTransactionPage_When_YourTransactionsLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Your Transactions");

        _myAccountPage.AssertPageUrl("/index.php?route=account/transaction");
    }

    [Test]
    public void ExistingUserRedirectedToRecurringPage_When_RecurringPaymentsLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Recurring payments");

        _myAccountPage.AssertPageUrl("/index.php?route=account/recurring");
    }

    [Test]
    public void ExistingUserRedirectedToAffiliateAddPage_WhenRegisterForAnAffiliateAccountLinkClicked()
    {
        _myAccountPage.ClickOnLinkByLabel("Register for an affiliate account");

        _myAccountPage.AssertPageUrl("/index.php?route=account/affiliate/add");
 
    }

    [TearDown]
    public void GoBackToMyAccountPage()
    {
        _breadcrumbSection.OpenBreadcrumbItem("Account");
    }
}