﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.CartPageTests;

[TestFixture]
public class CartPageShouldNot : BaseTest
{
    [Test]
    public void WarningMessageDisplayed_When_GiftInputEmpty_And_ApplyGiftCertificateButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        _cartInfoSection.ViewCart();

        _cartPage.ApplyGiftSertificate(string.Empty);

        _cartPage.AssertWarningMessage($"Warning: Please enter a gift certificate code!");
    }

    [Test]
    public void WarningMessageDisplayed_When_GiftInputNotValid_And_ApplyGiftCertificateButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        _cartInfoSection.ViewCart();

        _cartPage.ApplyGiftSertificate("Gift Certificate for Tony Tony");

        _cartPage.AssertWarningMessage($"Warning: Gift Certificate is either invalid or the balance has been used up!");
    }

    [Test]
    public void WarningMessageDisplayed_When_CouponInputEmpty_And_ApplyCouponButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        _cartInfoSection.ViewCart();

        _cartPage.ApplyCoupon(string.Empty);

        _cartPage.AssertWarningMessage("Warning: Please enter a coupon code!");
    }

    [Test]
    public void WarningMessageDisplayed_When_CouponInputNotValid_And_ApplyCouponButtonClicked()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        _cartInfoSection.ViewCart();

        _cartPage.ApplyCoupon("abv");
      
        _cartPage.AssertWarningMessage("Warning: Coupon is either invalid, expired or reached its usage limit!");
    }
}