﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.CartPageTests;

[TestFixture]
public class CartPageLoggedInUserTests :  BaseTest
{
    [Test]

    // Product unit price is not the same for logged in user and new user

    public void ProductUnitPriceStaysTheSame_For_LoggedIn_And_NotLoggedInUser()
    {
        //a bug
        Models.Product product = ProductFactory.DefaultProductsInStock()[1];
        var userInfo = UserInfoFactory.DefaultUserTony();
        string category = "Desktops";
        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch(category);
        _categoryPage.ShowProducts("100");
        _categoryPage.ClickProductName(product);
        _productDetailsPage.AssertProductPrice(product.UnitPrice);
        _mainPage.GoTo();
        _mainPage.ClickMyAccountLoginLink();

        _loginPage.LoginUser(userInfo);
        _cartPage.RemoveAllProductsFromCart();
        _searchSection.SelectCategoryToSearch(category);
        _categoryPage.ShowProducts("100");
        _categoryPage.ClickProductName(product);
        _productDetailsPage.AssertProductPrice(product.UnitPrice);
    }
}
