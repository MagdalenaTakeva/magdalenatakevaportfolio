﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.CartPageTests;

[TestFixture]
public class CartPageShould : BaseTest
{
    [Test]    
    public void SameProductAddedToCart2Times_When_AddToCartButtonClicked2Times()
    {
        var product = ProductFactory.DefaultProductsInStock()[1];
        int quantity = 2;
        _cartPage.RemoveAllProductsFromCart();
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");

        _categoryPage.AddProductToCartByProductId(product.ProductId, quantity);
        _cartInfoSection.ViewCart();

        _cartPage.AssertProductName(product.ProductName);
        _cartPage.AssertProductQuantity((quantity.ToString()));
        _cartPage.AssertUnitPrice(product.UnitPrice);
        _cartPage.AssertTotalPrice(product.ProductTotalPrice * quantity);
    }

    [Test]
    public void ProductQuantityIncreased_And_ProductTotalUpdated_When_QuantityIncreasedByHand_And_UpdateButtonClicked()
    {
        var productsInStockList = ProductFactory.DefaultProductsInStock();
        var quantity = 2;
        _cartPage.RemoveAllProductsFromCart();
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(productsInStockList[0].ProductId, productsInStockList[0].Quantity);
        _cartInfoSection.ViewCart();
        _cartPage.AssertTotalPrice(productsInStockList[0].ProductTotalPrice * 1);

        _cartPage.IncreaseProductQuantity(quantity.ToString());

        _cartPage.AssertProductQuantity(quantity.ToString());
        _cartPage.AssertTotalPrice(productsInStockList[0].ProductTotalPrice * quantity);
    }

    [Test]
    public void AllProductsRemovedFromCart_When_EachProductRemoveButtonClicked()
    {
        var productsInStockList = ProductFactory.DefaultProductsInStock();
        _cartPage.RemoveAllProductsFromCart();
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddMultipleProductsToCart(productsInStockList);
        _cartInfoSection.ViewCart();

        _cartPage.RemoveAllProductsFromCart();

        _cartPage.AssertShoppingCartEmptyTextDisplayed();
    }

    [Test]
    public void UserRedirectedToCheckout_When_CheckoutButtonClicked()
    {
        var productsInStockList = ProductFactory.DefaultProductsInStock();
        _cartPage.RemoveAllProductsFromCart();
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(productsInStockList[0].ProductId, productsInStockList[0].Quantity);
        _cartInfoSection.ViewCart();

        _cartPage.ProceedToCheckout();

        _checkoutPage.AssertPageUrl();
    }

    [Test]
    public void UserRedirectedToMainPage_When_ContinueShoppingButtonClicked()
    {
        var productsInStockList = ProductFactory.DefaultProductsInStock();
        _cartPage.RemoveAllProductsFromCart();
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(productsInStockList[0].ProductId, productsInStockList[0].Quantity);
        _cartInfoSection.ViewCart();

        _cartPage.PressContinueShoppingButton();

        _mainPage.AssertPageUrl("/index.php?route=common/home");
    }

    [Test]
    public void UserRedirectedToMainPage_When_AllProductsRemovedFromCart_And_ContinueButtonClicked()
    {
        var productsInStockList = ProductFactory.DefaultProductsInStock();
        _cartPage.RemoveAllProductsFromCart();
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddMultipleProductsToCart(productsInStockList);
        _cartInfoSection.ViewCart();

        _cartPage.RemoveAllProductsFromCart();
        _cartPage.PressContinueButton();

        _mainPage.AssertPageUrl("/index.php?route=common/home");
    }


    [Test]
    public void ShippingRateApplied_When_CountryRegionAndPostCodeSelected_GetQuoteButtonClicked_FlatShippingRateSelected_And_ApplyShippingClicked()
    {
        //the test will not pass with iPod Nano - ProductFactory.DefaultProductsInStock()[2];
        // There is a bug for iPod Nano - no option to apply shipping rate
       // var product = ProductFactory.DefaultProductsInStock()[1];

        //the test will pass - ProductFactory.DefaultProductsInStock()[1];
        var product = ProductFactory.DefaultProductsInStock()[0];
        string flatShippingRate = "$5.00";
        var cartShippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();

        _cartPage.RemoveAllProductsFromCart();
        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        _cartInfoSection.ViewCart();
        
        _cartPage.ApplyShippingRate(cartShippingInfo);

        _cartPage.AssertAlertSuccessMessage("Success: Your shipping estimate has been applied!");
        _cartPage.AssertFlatShippingRate(flatShippingRate);
    }

    [Test]
    public void LoggedInUserAddedGiftCertificateToCart_When_GiftVoucherFilledIn_And_ContinueButtonClickedAtVoucherSuccessPage()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony().Email, UserInfoFactory.DefaultUserTony().Password);
        _cartPage.RemoveAllProductsFromCart();
        _mainMenuSection.OpenMyVoucherPage();

        _myVoucherPage.FillInGiftCertificate(GiftCertificateFactory.BirthdayGiftCertificate(),GiftCertificateTheme.Birthday);
        _myVoucherPage.AssertGiftCertificateFormCompletedSuccessfully("/index.php?route=account/voucher/success");
        _voucherSuccessPage.ClickContinueButton();
        
        _cartPage.AssertGiftCertificateAddedToCart();
    }
}
