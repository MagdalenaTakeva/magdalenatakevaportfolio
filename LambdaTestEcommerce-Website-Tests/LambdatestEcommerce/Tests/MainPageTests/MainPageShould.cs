﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.MainPageTests;

[TestFixture]
public class MainPageShould : BaseTest
{
    [Test]

    public void UserAddedTwoItemsFromTopProductsCarouselToProductComparePage_When_ProductDisplayedInCarousel_And_CompareThisProductButtonClicked()
    {
        var productsToCompare = CompareProductInfoFactory.ListTopProductsModuleProducts();
        var product1Id = productsToCompare[6].ProductId;
        var product2Id = productsToCompare[0].ProductId;
        
        _mainPage.GoTo();
        _mainPage.AddItemsInTopProductsModuleToCompareList(productsToCompare, product1Id, product2Id);
        _compareProductsSection.OpenCompareProductsPage();

        _compareProductsPage.AssertNumberOfProductsAsExpected(2);
    }

    [Test]
    public void UserAddedTwoItemsFromTopProductsCarouselToWhishList_When_ProductDisplayedInCarousel_And_AddToWishListButtonClicked()
    {
        var productsToCompare = CompareProductInfoFactory.ListTopProductsModuleProducts();
        var product1Id = productsToCompare[6].ProductId;
        var product2Id = productsToCompare[0].ProductId;
        var user = UserInfoFactory.DefaultUserTony();

        _mainPage.ClickMyAccountLoginLink();
        _loginPage.LoginUser(user.Email, user.Password);  
        _mainMenuSection.OpenHomePage();
        _wishlistSection.OpenWishlist();
        _wishlistSection.DeleteAllProductsInWishList();
        _wishlistSection.PressContinueButton();
        _mainMenuSection.OpenHomePage();

        _mainPage.AddItemsInTopProductsModuleToWishList(productsToCompare, product1Id, product2Id);
        _wishlistSection.OpenWishlist();

        _wishlistSection.AssertNumberOfProductsAsExpected(2);
    }

    [Test]
    public void UserRedirectedToDesktopsCategoryPage_When_DesktopsSelectedFromAllCategoriesDropdown_And_SearchButtonClicked2()
    {
        _mainPage.SearchCategoryById(20);

        _categoryPage.AssertCategoryPageUrlByCategoryId(20);
    }

    [Test]
    public void UserRedirectedToLaptopsCategoryPage_When_LaptopsSelectedFromAllCategoriesDropdown_And_SearchButtonClicked()
    {
        _mainPage.SearchCategoryById(18);

        _categoryPage.AssertCategoryPageUrlByCategoryId(18);
    }

    [Test]
    public void UserRedirectedToComponentsCategoryPage_When_ComponentsSelectedFromAllCategoriesDropdown_And_SearchButtonClicked()
    {
        _mainPage.SearchCategoryById(25);

        _categoryPage.AssertCategoryPageUrlByCategoryId(25);
    }

    [Test]
    public void UserRedirectedToTabletsCategoryPage_When_TabletsSelectedFromAllCategoriesDropdown_And_SearchButtonClicked()
    {
        _mainPage.SearchCategoryById(57);

        _categoryPage.AssertCategoryPageUrlByCategoryId(57);
    }

    [Test]
    public void UserRedirectedToSoftwareCategoryPage_When_SoftwareSelectedFromAllCategoriesDropdown_And_SearchButtonClicked()
    {
        _mainPage.SearchCategoryById(17);

        _categoryPage.AssertCategoryPageUrlByCategoryId(17);
    }

    [Test]
    public void UserRedirectedToPhonesPDAsCategoryPage_When_PhonesPDAsSelectedFromAllCategoriesDropdown_And_SearchButtonClicked()
    {
        _mainPage.SearchCategoryById(24);

        _categoryPage.AssertCategoryPageUrlByCategoryId(24);
    }

    [Test]
    public void UserRedirectedToCamerasCategoryPage_When_CamerasSelectedFromAllCategoriesDropdown_And_SearchButtonClicked()
    {
        _mainPage.SearchCategoryById(33);
        
        _categoryPage.AssertCategoryPageUrlByCategoryId(33);
    }

    [Test]
    public void UserRedirectedToMP3PlayerCategoryPage_When_MP3PlayerSelectedFromAllCategoriesDropdown_And_SearchButtonClicked()
    {
        _mainPage.SearchCategoryById(34);
     
        _categoryPage.AssertCategoryPageUrlByCategoryId(34);
    }

    [Test]
    public void UserRedirectedToAccountLoginPage_When_MyAccountLoginLinkClicked()
    {
        _mainPage.ClickMyAccountLoginLink();

        _myAccountPage.AssertPageUrl("/index.php?route=account/login");
        _myAccountPage.AssertPageTitle("Account Login");
    }

    [Test]
    public void UserRedirectedToRegistrationPage_When_MyAccountRegisterLinkClicked()
    {
        _mainPage.ClickMyAccountRegisterLink();

        _myAccountPage.AssertPageUrl("/index.php?route=account/register");
        _myAccountPage.AssertPageTitle("Register Account");
    }
}