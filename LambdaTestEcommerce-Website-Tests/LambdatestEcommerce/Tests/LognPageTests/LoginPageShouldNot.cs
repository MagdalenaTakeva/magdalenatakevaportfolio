﻿namespace LambdatestEcommerce.Tests.LognPageTests;

[TestFixture]
public class LoginPageShouldNot : BaseTest
{
    [Test]
    [Combinatorial]
    public void UserUnableToLogIn_With_InvalidEmail_And_ValidPassword_When_LoginButtonClicked(
        [Values("veronika.com", "veronika")] string invalidEmail)
    {

        _loginPage.LoginUser(invalidEmail, "Veronika123");
        
        _loginPage.AssertWarningMessage("Warning: No match for E-Mail Address and/or Password.");
    }

    [Test]
    public void UserUnableToLogIn_With_InvalidEmail_And_InvalidPassword_When_LoginButtonClicked_1()
    {
        _loginPage.LoginUser("veronika", "Veronika");

        _loginPage.AssertWarningMessage("Warning: No match for E-Mail Address and/or Password.");
    }

    [Test]
    public void UserUnableToLogIn_With_ValidEmail_And_InvalidPassword_When_LoginButtonClicked()
    {
        _loginPage.LoginUser("veronika@abv.bg", "Veronika");

        _loginPage.AssertWarningMessage("Warning: No match for E-Mail Address and/or Password.");
    }

    [Test]
    public void UserUnableToLogIn_With_InvalidEmailAndPassword_When_LoginButtonClicked()
    {
        _loginPage.LoginUser("veronika@", "Veronika");

        _loginPage.AssertWarningMessage("Warning: No match for E-Mail Address and/or Password.");
    }

    [Test]
    public void UserUnableToLogIn_When_PasswordFieldIsLeftBlank()
    {
        _loginPage.LoginUser("veronika@abv.bg", string.Empty);

        _loginPage.AssertWarningMessage("Warning: No match for E-Mail Address and/or Password.");
    }

    [Test]
    public void UserUnableToLogIn_When_PasswordAndEmailFieldsAreEmpty()
    {
        _loginPage.LoginUser(string.Empty, string.Empty);

        _loginPage.AssertWarningMessage("Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour.");
    }
}