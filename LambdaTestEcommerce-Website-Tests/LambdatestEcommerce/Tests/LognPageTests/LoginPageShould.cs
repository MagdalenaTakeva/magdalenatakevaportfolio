﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.LognPageTests;

[TestFixture]
public class LoginPageShould : BaseTest
{
    [SetUp]
    public void SetUp()
    {
        _loginPage.GoTo();
    }

    [Test]
    public void EmailField_PasswordField_And_LogInButtonDisplayedOnLoginPage()
    {
        _loginPage.AssertEmailInputFieldIsDisplayed();
        _loginPage.AssertPasswordInputFieldIsDisplayed();
        _loginPage.AssertLoginButtonIsDisplayed();
    }

    [Test]
    public void NewUserRedirectedToRegistrationPage_When_ContinueButtonClicked()
    {
        _loginPage.ClickContinueButton();

        _registrationPage.AssertPageUrl();
    }

    [Test]
    public void NewUserRedirectedToRegistrationPage_When_SideBarRegisterLinkClicked()
    {
        _loginPage.ClickRightColumnLinkByName("Register");

        _registrationPage.AssertPageUrl();
    }

    [Test]
    public void NewUserRedirectedToForgottenPasswordPage_When_ForgottenPasswordLinkClicked()
    {
        _loginPage.ClickForgottenPasswordLink();

        _forgottenPasswordPage.AssertPageUrl();
    }

    [Test]
    public void NewUserRedirectedToForgottenPasswordPage_When_SideBarForgottenPasswordLinkClicked()
    {
        _loginPage.ClickRightColumnLinkByName("Forgotten Password");

        _forgottenPasswordPage.AssertPageUrl();
    }

    [Test]
    [Combinatorial]
    public void NewUserRedirectedToLoginPage_When_SideBarLinksClicked(
        [Values("My Account", "Address Book", "Wish List", "Order History",
        "Downloads", "Recurring payments", "Reward Points", "Returns", "Transactions", "Newsletter")] string linkName)
    {
        _loginPage.ClickRightColumnLinkByName(linkName);

        _loginPage.AssertPageUrl();
    }

    [Test]
    public void NewUserSuccessfullyLoggedIn_When_ValidEmailAnd_ValidPasswordProvided_And_LogInButtonClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.AssertPageUrl("/index.php?route=account/account");
    }

    [Test]
    public void LoggedInUserRedirectedToMyAccountPage_When_SideBarMyAccountLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("My Account");

        _myAccountPage.AssertPageUrl("/index.php?route=account/account");
    }

    [Test]
    public void LoggedInUserRedirectedToEditAccountPage_When_SideBarEditAccountLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Edit Account");

        _myAccountPage.AssertPageUrl("/index.php?route=account/edit");
    }

    [Test]
    public void LoggedInUserRedirectedToPasswordPage_When_SideBarPasswordLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Password");

        _myAccountPage.AssertPageUrl("/index.php?route=account/password");
    }

    [Test]
    public void LoggedInUserRedirectedToAddressBookPage_When_SideBarAddressBookLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Address Book");

        _myAccountPage.AssertPageUrl("/index.php?route=account/address");
    }

    [Test]
    public void LoggedInUserRedirectedToWishListPage_When_SideBarWishListLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Wish List");

        _myAccountPage.AssertPageUrl("/index.php?route=account/wishlist");
    }

    [Test]
    public void LoggedInUserRedirectedToNotificationPage_When_SideBarNotificationLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Notification");

        _myAccountPage.AssertPageUrl("/index.php?route=extension/maza/account/notification/product");
    }

    [Test]
    public void LoggedInUserRedirectedToOrderHistoryPage_When_SideBarOrderHistoryLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Order History");

        _myAccountPage.AssertPageUrl("/index.php?route=account/order");
    }

    [Test]
    public void LoggedInUserRedirectedToDownloadsPage_When_SideBarDownloadsLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Downloads");

        _myAccountPage.AssertPageUrl("/index.php?route=account/download");
    }

    [Test]
    public void LoggedInUserRedirectedToRecurringPaymentsPage_When_SideBarRecurringPaymentsLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Recurring payments");

        _myAccountPage.AssertPageUrl("/index.php?route=account/recurring");
    }

    [Test]
    public void LoggedInUserRedirectedToRewardPointsPage_When_SideBarRewardPointsLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Reward Points");

        _myAccountPage.AssertPageUrl("/index.php?route=account/reward");
    }

    [Test]
    public void LoggedInUserRedirectedToReturnsPage_When_SideBarReturnsLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Returns");

        _myAccountPage.AssertPageUrl("/index.php?route=account/return");
    }

    [Test]
    public void LoggedInUserRedirectedToTransactionsPage_When_SideBarTransactionsLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Transactions");

        _myAccountPage.AssertPageUrl("/index.php?route=account/transaction");
    }

    [Test]
    public void LoggedInUserRedirectedToNewsletterPage_When_SideBarNewsletterLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Newsletter");

        _myAccountPage.AssertPageUrl("/index.php?route=account/newsletter");
    }

    [Test]
    public void LoggedInUserLoggedOut_When_SideBarLogoutLinkClicked()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());

        _myAccountPage.ClickRightColumnLinkByName("Logout");

        _myAccountPage.AssertPageUrl("/index.php?route=account/logout");
    }
}