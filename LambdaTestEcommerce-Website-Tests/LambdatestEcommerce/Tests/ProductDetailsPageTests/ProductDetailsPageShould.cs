﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.ProductDetailsPageTests;

[TestFixture]
public class ProductDetailsPageShould : BaseTest
{

    [Test]
    public void ProductAddedToCart_When_EnabledAddToCartButtonIsClicked()
    {
        Models.Product product = ProductFactory.DefaultProductsInStock()[1];
        string productQuantity = "1";

        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch("Desktops");
        _categoryPage.ClickProductImage(product);
        _productDetailsPage.AssertProductDetails(product.ProductName, product.ProductBrand, product.ProductTotalPrice);
        _productDetailsPage.AssertProductInStockAndAddToCartAndBuyNowButtonsEnabled();
        
        _productDetailsPage.AddProductToCart();
        _cartPage.GoTo();

        _cartPage.AssertProductName(product.ProductName);
        _cartPage.AssertProductQuantity(productQuantity);
        _cartPage.AssertGrandTotal(product.ProductTotalPrice);
    }

    [Test]
    public void ProductQuantityIncreased_And_ProductAddedToCart_When_IncreaseQuantityButton_And_EnabledAddToCartButtonClicked()
    {
        Models.Product product = ProductFactory.DefaultProductsInStock()[1];
        string productQuantity = "2";

        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch("Desktops");
        _categoryPage.ClickProductImage(product);
        _productDetailsPage.AssertProductDetails(product.ProductName, product.ProductBrand, product.ProductTotalPrice);
        _productDetailsPage.AssertProductInStockAndAddToCartAndBuyNowButtonsEnabled();
        
        _productDetailsPage.IncreaseProductQuantity(2);
        _productDetailsPage.AddProductToCart();
        _cartPage.GoTo();

        _cartPage.AssertProductName(product.ProductName);
        _cartPage.AssertProductQuantity(productQuantity);
        _cartPage.AssertGrandTotal(product.ProductTotalPrice* Double.Parse(productQuantity));
    }

    [Test]
    public void ProductQuantityDecreased_And_ProductAddedToCart_When_DecreaseQuantityButtonClicked_And_EnabledAddToCartButtonClicked()
    {
        Models.Product product = ProductFactory.DefaultProductsInStock()[1];
        string productQuantity = "1";
        int increaseQuantityTo = 3;
        int decreaseQuantityTo = 1;

        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch("Desktops");
        _categoryPage.ClickProductImage(product);
        _productDetailsPage.AssertProductDetails(product.ProductName, product.ProductBrand, product.ProductTotalPrice);
        _productDetailsPage.AssertProductInStockAndAddToCartAndBuyNowButtonsEnabled();
        
        _productDetailsPage.IncreaseProductQuantity(increaseQuantityTo);
        _productDetailsPage.DecreaseProductQuantity(increaseQuantityTo,decreaseQuantityTo);
        _productDetailsPage.AddProductToCart();
        _cartPage.GoTo();

        _cartPage.AssertProductName(product.ProductName);
        _cartPage.AssertProductQuantity(productQuantity);
        _cartPage.AssertGrandTotal(product.ProductTotalPrice);
    }

    [Test]
    public void UserRedirectedToCheckout_When_BuyNowButtonClicked()
    {
        Models.Product product = ProductFactory.DefaultProductsInStock()[1];
        string productQuantity = "1";

        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch("Desktops");
        _categoryPage.ClickProductImage(product);
        _productDetailsPage.AssertProductDetails(product.ProductName, product.ProductBrand, product.ProductTotalPrice);
        _productDetailsPage.AssertProductInStockAndAddToCartAndBuyNowButtonsEnabled();
        
        _productDetailsPage.BuyNowProduct();

        _checkoutPage.AssertProductQuantity(productQuantity);
        _checkoutPage.AssertProductName(product.ProductName);
    }
}