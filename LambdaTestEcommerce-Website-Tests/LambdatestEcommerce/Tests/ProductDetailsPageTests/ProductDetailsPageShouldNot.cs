﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.ProductDetailsPageTests;

[TestFixture]
public class ProductDetailsPageShouldNot : BaseTest
{
    [Test]
    public void ProductNotAddedToCart_When_DisabledAddToCartButtonIsClicked()
    {
        Models.Product _product = ProductFactory.DefaultProductsOutOfStock()[0];
        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch("Desktops");
        _categoryPage.ClickProductImage(_product);

        _productDetailsPage.AssertAddToCartButtonDisabled();
        _productDetailsPage.ClickAddToCartButtonThatShouldBeDisabled();

        _productDetailsPage.AssertProductPageUrl(_product.ProductUrl);
    }

    [Test]
    public void ProductNotBought_When_DisabledBuyNowButtonIsClicked()
    {
        Models.Product _product = ProductFactory.DefaultProductsOutOfStock()[0];
         
        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch("Desktops");
        _categoryPage.ClickProductImage(_product);

        _productDetailsPage.AssertbuyNowButtonDisabled();
        _productDetailsPage.ClickBuyNowButtonThatShouldBeDisabled();

        _productDetailsPage.AssertProductPageUrl(_product.ProductUrl);
    }
}
