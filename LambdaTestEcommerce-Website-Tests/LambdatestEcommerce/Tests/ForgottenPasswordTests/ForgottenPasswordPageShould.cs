﻿namespace LambdatestEcommerce.Tests.ForgottenPasswordTests;

[TestFixture]
public class ForgottenPasswordPageShould : BaseTest
{
    [SetUp]
    public void UserRedirectedToForgottenPasswordPage_When_ForgottenPasswordLinkClicked()
    {
        _loginPage.GoTo();
        _loginPage.ClickForgottenPasswordLink();
    }

    [Test]
    public void WarningMessageDisplayed_When_EmailNotIncludedInRecords()
    {
        var emailNotInRecords = "magdalena@abv.bg";

        _forgottenPasswordPage.SendEmail(emailNotInRecords);

        _forgottenPasswordPage.AssertWarningMessage();
    }

    [Test]
    public void SuccessMessageDisplayed_When_CorrectEmailProvided_And_ContinueButtonClicked()
    {
        var emailInRecords = "magdalena.takeva@gmail.com";

        _forgottenPasswordPage.SendEmail(emailInRecords);
    
        _forgottenPasswordPage.AssertSuccessMessage();
    }

    [Test]
    public void UserRedirectedBackToLogInPage_When_BackButtonClicked()
    {
        _forgottenPasswordPage.GoBack();

        _loginPage.AssertPageUrl();
    }
}
