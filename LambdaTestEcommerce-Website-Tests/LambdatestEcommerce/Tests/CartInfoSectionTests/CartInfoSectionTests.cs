﻿using LambdatestEcommerce.Factories;
using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Tests.CartInfoSectionTests
{
    public class CartInfoSectionTests : BaseTest
    {
        [SetUp]
        public void TestSetUp()
        {
            _mainPage.GoTo();
        }

        [Test]
        public void NewUserOpenedCart_When_CartIconClicked()
        {
            _cartInfoSection.OpenCart();

            _cartInfoSection.AssertCartTitle();
        }

        [Test]
        public void NewUserRedirectedToCartPage_When_CartIconOpened_And_EditButtonClicked()
        {
            _cartInfoSection.OpenCart();

            _cartInfoSection.EditCartFromCartIcon();

            _cartPage.AssertPageUrl();
        }

        [Test]
        public void NewUserRedirectedToCheckoutPage_When_CartIconOpened_And_CheckoutButtonClicked()
        {
            var category = "Desktops";
            var product = ProductFactory.DefaultProductsInStock()[0];

            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ShowProducts("100");
            _categoryPage.ClickProductImage(product);
            _productDetailsPage.AddProductToCart();
            _cartInfoSection.CloseProductNotification();
            _cartInfoSection.OpenCart();

            _cartInfoSection.ProceedToCheckout();

            _checkoutPage.AssertPageUrl();
        }

        [Test]
        public void UserRedirectedToCartPageToEditProductAmount_When_ProductAddedToCart_CartIconClicked_And_EditButtonClicked()
        {
            var category = "Desktops";
            var product = ProductFactory.DefaultProductsInStock()[0];

            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ShowProducts("100");
            _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
            _cartInfoSection.CloseProductNotification();

            _cartInfoSection.OpenCart();
            _cartInfoSection.EditCartFromCartIcon();

            _cartPage.AssertPageUrl();
        }

        [Test]
        public void CartCurrentAmountIsZero_When_CartIsEmpty()
        {
            _cartInfoSection.AssertCartCurrentAmount("0");
        }

        [Test]
        public void CartCurrentAmountIncreased_When_ProductQuantityIncreased_And_ProductAddedToCart()
        {
            var category = "Desktops";
            var product = ProductFactory.DefaultProductsInStock()[0];

            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ShowProducts("100");
            _categoryPage.ClickProductImage(product);
            _cartInfoSection.AssertCartCurrentAmount("0");

            _productDetailsPage.IncreaseProductQuantity(2);
            _productDetailsPage.AddProductToCart();

            _cartInfoSection.AssertCartCurrentAmount("2");
        }

        [Test]
        public void CartCurrentAmountShowHowManyProductsAddedToCart_When_MultipleProductsAddedToCart()
        {
            List<Product> productList = ProductFactory.DefaultProductsInStock();
            var category = "Desktops";

            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ShowProducts("100");

            _categoryPage.AddMultipleProductsToCart(productList);
            _cartInfoSection.CloseProductNotification();

            _cartInfoSection.AssertCartCurrentAmount(_cartInfoSection.GetCurrentAmount());
        }

        [Test]
        public void ProductTaxDetailsAndChargesAreDisplayed_When_CartOpenedFromCartIcon()
        {
            var productList = ProductFactory.DefaultProductsInStock();
            string category = "Desktops";

            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ShowProducts("100");
            _categoryPage.AddMultipleProductsToCart(productList);
            _cartInfoSection.ViewCart();

            _cartInfoSection.OpenCart();

            _cartInfoSection.AssertOrderSubtotalDisplayed();

            _cartInfoSection.AssertOrderSubtotal("$320.00");
            _cartInfoSection.AssertOrderEcoTax("$4.00");
            _cartInfoSection.AssertOrderVatTax("$64.00");
            _cartInfoSection.AssertOrderTotal("$388.00");
        }
    }
}
