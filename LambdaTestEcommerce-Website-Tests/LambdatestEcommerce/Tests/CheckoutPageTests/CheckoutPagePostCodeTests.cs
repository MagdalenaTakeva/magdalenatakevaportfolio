﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.CheckoutPageTests;

[TestFixture]
public class CheckoutPagePostCodeTests : BaseTest
{
    [SetUp]
    public void SetUp()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        var quantity = 2;

        _mainPage.SearchCategoryById(20);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product.ProductId, quantity);
        _cartInfoSection.ViewCart();
        _cartPage.ProceedToCheckout();
    }

   [Test]
    [Combinatorial]
    public void PostCodeErrorMessageDisplayed_When_PostCodeInputNotValid_CountryAndRegionInputNeverBeenFilled_And_ContinueButtonClicked(
      [Values(" ", "a", "1", "1)(*&^%$#@?><", "aaaaaaaaaaa", "12345678912")] string postcodeInvalidInput)
    {
        _checkoutPage.ProvidePaymentPostCode(postcodeInvalidInput);
    }
    
    [TearDown]

    public void AfterTestSetup()
    {
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();

        _checkoutPage.AssertWarningMessageByLabel("Post Code", "Postcode must be between 2 and 10 characters!");
    }
}
