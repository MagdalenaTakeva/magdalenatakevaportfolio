﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.CheckoutPageTests;

[TestFixture]
public class CheckoutPageShouldNot : BaseTest
{
    [SetUp]
    public void SetUpForNewUser()
    {
        var product = ProductFactory.DefaultProductInStockQuantityMoreThan1();
        var category = "Desktops";

        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch(category);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product[0].ProductId, product[0].Quantity);
        _cartInfoSection.ViewCart();
        _cartPage.ProceedToCheckout();

    }

   [Test]
    public void WarningMessageDisplayed_When_RegisterAccountRadioButtonSelected_BillingInfoFilled_TermsAndConditionsNotChecked_And_ContinueButtonClicked()
    {
        var userType = UserTypes.NewUser;
        var userInfo = UserInfoFactory.CreateUser();
        var addressType = AddressTypes2.NewBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;

        _checkoutPage.FillInClientInfo(userInfo, userType,
                                                    addressType,
                                                    billingAddress,
                                                    shippingAddress,
                                                    billingAndShippingAddressesAreTheSame);

        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertAlertWarningMessage("Warning: You must agree to the Terms & Conditions!");
     }

    [Test]
    public void WarningMessageDisplayed_When_LogInButtonClicked_BillingInfoFilled_TermsAndConditionsNotSelected_And_ContinueButtonClicked()
    {
        var userType = UserTypes.LoggedOutUser;
        var userInfo = UserInfoFactory.DefaultUserTony();
        var addressType = AddressTypes2.ExistingBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;

        _checkoutPage.FillInClientInfo(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);

        _checkoutPage.PressContinueButton();

        _checkoutPage.AssertAlertWarningMessage("Warning: You must agree to the Terms & Conditions!");
    }

    [Test]
    public void WarningMessageDisplayed_When_GuestCheckoutRadioButtonSelected_BillingInfoFilled_TermsAndConditionsNotChecked_And_ContinueButtonClicked()
    {
        var userType = UserTypes.GuestUser;
        var userInfo = UserInfoFactory.CreateUser();
        var addressType = AddressTypes2.NewBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;

        _checkoutPage.FillInClientInfo(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);

        _checkoutPage.PressContinueButton();

        _checkoutPage.AssertAlertWarningMessage("Warning: You must agree to the Terms & Conditions!");
    }

    [Test]
    public void WarningMessageDisplayed_When_RegisterAccountRadioButtonSelected_BillingInfoFilled_PrivacyPolicyCheckboxNotChecked_And_ContinueButtonClicked()
    {
        var userType = UserTypes.NewUser;
        var userInfo = UserInfoFactory.CreateUser();
        var addressType = AddressTypes2.NewBillingAddress;
        var billingAddress = BillingAddressInfoFactory.CreateBillingAddress();
        var shippingAddress = ShippingAddressInfoFactory.CreateShippingAddress();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.NotTheSame;

        _checkoutPage.FillInClientInfo(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);

        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertAlertWarningMessage("Warning: You must agree to the Privacy Policy!");
    }

    [Test]
    [Combinatorial]
    public void FirstNameErrorMessageDisplayed_When_FirstNameInputNotValid_And_ContinueButtonClicked(
          [Values(" ", "abcdefghigabcdefghigabcdefghigabc")] string firstNameInvalidInput)
    {
        _checkoutPage.SelectRegisterAccountRadioButton();

        _checkoutPage.ProvidePaymentFirstName(firstNameInvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("First Name", "First Name must be between 1 and 32 characters!");
    }

    [Test]
    [Combinatorial]
    public void LastNameErrorMessageDisplayed_When_LastNameInputNotValid_And_ContinueButtonClicked(
         [Values(" ", "abcdefghigabcdefghigabcdefghigabc")] string lastNameInvalidInput)
    {
        _checkoutPage.SelectRegisterAccountRadioButton();

        _checkoutPage.ProvidePaymentLastName(lastNameInvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("Last Name", "Last Name must be between 1 and 32 characters!");
    }

    [Test]
    [Combinatorial]
    public void EmailErrorMessageDisplayed_When_EmailInputNotValid_And_ContinueButtonClicked(
         [Values(" ","maggie", "maggie@", "maggie.com", "maggie@.com","@", "...")] string emailInvalidInput)
    {
        _checkoutPage.SelectRegisterAccountRadioButton();

        _checkoutPage.ProvidePaymentEmail(emailInvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("E-Mail", "E-Mail address does not appear to be valid!");
    }

    [Test]
    [Combinatorial]
    public void TelephoneErrorMessageDisplayed_When_TelephoneInputNotValid_And_ContinueButtonClicked(
         [Values(" ", "abcdefghigabcdefghigabcdefghigabc", "123456789123456789123456789123456", "12", "aa")] string telephoneInvalidInput)
    {
        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.ProvidePaymentTelephone(telephoneInvalidInput);
        _checkoutPage.AssertTelephoneHelperMessage();
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("Telephone", "Telephone must be between 3 and 32 characters!");
    }
    
    [Test]
    [Combinatorial]
    public void PasswordErrorMessageDisplayed_When_PasswordInputNotValid_And_ContinueButtonClicked(
         [Values(" ", "abc", "12")] string passwordInvalidInput)
    {
        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.ProvidePaymentPassword(passwordInvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("Password", "Password must be between 4 and 20 characters!");
    }

    //bug - when password is more than 21chars - no error message appears
    [Combinatorial]
    public void PasswordErrorMessageDisplayed_When_PasswordLengthIsMoreThan20Characters(
         [Values("123456789123456789123", "abcdefghiabcdefghiabc")] string passwordInvalidInput)
    {
        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.ProvidePaymentPassword(passwordInvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();

        _checkoutPage.AssertWarningMessageByLabel("Password", "Password must be between 4 and 20 characters!");
    }

    [Test]
    public void PasswordConfirmErrorMessageDisplayed_When_PasswordConfirmInputFieldLeftBlank()
    { 
        var password = UserInfoFactory.CreateUser().Password;

        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.ProvidePaymentPassword(password);
        _checkoutPage.ProvidePaymentConfirmPassword(string.Empty);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("Password Confirm", "Password confirmation does not match password!");
    }

    [Test]
    public void PasswordConfirmErrorMessageDisplayed_When_PasswordConfirmDoesNotMatchPassword()
    {
        var password1 = UserInfoFactory.CreateUser().Password;
        var password2 = UserInfoFactory.CreateUser().Password;

        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.ProvidePaymentPassword(password1);
        _checkoutPage.ProvidePaymentConfirmPassword(password2);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();

        _checkoutPage.AssertWarningMessageByLabel("Password Confirm", "Password confirmation does not match password!");
    }

    [Test]
    [Combinatorial]
    public void Address1ErrorMessageDisplayed_When_Address1InputNotValid_And_ContinueButtonClicked(
        [Values(" ", "aa", "j5n93i8uca0695rpmhxn8kzeuizffpc14smj420j8x2svnkenn3bjh81mrrlj6lr4kd6ax7x9bncbxvmlfo0st7033660mbopzspapqfbmfbq5j19ei3pmhetkbl37xjx")] string address1InvalidInput)
    {
        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.ProvidePaymentAddress1(address1InvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("Address 1", "Address 1 must be between 3 and 128 characters!");
    }

    [Test]
    [Combinatorial]
    public void CityErrorMessageDisplayed_When_CityInputNotValid_And_ContinueButtonClicked(
       [Values(" ", "a", "j5n93i8uca0695rpmhxn8kzeuizffpc14smj420j8x2svnkenn3bjh81mrrlj6lr4kd6ax7x9bncbxvmlfo0st7033660mbopzspapqfbmfbq5j19ei3pmhetkbl37xjx")] string cityInvalidInput)
    {
        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.ProvidePaymentCity(cityInvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("City", "City must be between 2 and 128 characters!");
    }

    [Test]
    [Combinatorial]
    public void CountryErrorMessageDisplayed_When_CountryInputNotValid_And_ContinueButtonClicked()
    {
        var countryInvalidInput = " --- Please Select --- ";

        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.SelectPaymentCountry(countryInvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();

        _checkoutPage.AssertWarningMessageByLabel("Country", "Please select a country!");

    }

    [Test]
    public void RegionErrorMessageDisplayed_When_RegionInputNotValid_And_ContinueButtonClicked()
    {
        var regionInvalidInput = " --- Please Select --- ";

        _checkoutPage.SelectRegisterAccountRadioButton();
        _checkoutPage.SelectPaymentRegion(regionInvalidInput);
        _checkoutPage.CheckPrivacyPolicyCheckbox();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    
        _checkoutPage.AssertWarningMessageByLabel("Region / State", "Please select a region / state!");
    }
}
