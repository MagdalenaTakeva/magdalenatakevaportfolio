﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.WishListSectionTests
{
    public class WishListSectionTests : BaseTest
    {
        [SetUp]
        public void TestSetUp()
        {
            _mainPage.GoTo();
        }

        [Test]
        public void NewUserRedirectedToLoginPage_When_HeartIconClicked()
        {
            _wishlistSection.OpenWishlist();

            _loginPage.AssertPageUrl();
        }

        [Test]
        public void LoggedInUserRedirectedFromMyAccountToWishlistPage_When_HeartIconClicked()
        {
            var user = UserInfoFactory.DefaultUserTony();

            _mainMenuSection.LoginFromMyAccountMenu();
            _loginPage.LoginUser(user.Email, user.Password);

            _wishlistSection.OpenWishlist();

            _wishlistSection.AssertPageUrl();
            _wishlistSection.AssertPageTitle();
        }

        [Test]

        public void LoggedInUserRedirectedToWishListPage_When_ProductAddedToWishlistFromCategoryPage_And_HeartIconClicked()
        {
            var category = "Desktops";
            var user = UserInfoFactory.DefaultUserTony();
            var product = ProductFactory.DefaultProductToAddOrRemoveFromWishList()[0];
            var wishList = WishListInfoFactory.DefaultProductAddedToWishList();

            _mainMenuSection.LoginFromMyAccountMenu();
            _loginPage.LoginUser(user.Email, user.Password);
            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ShowProducts("100");
            _categoryPage.ClickListView();

            _categoryPage.AddProductToWishList(product.ProductId);
            _wishlistSection.CloseProductNotification();
            _wishlistSection.OpenWishlist();

            _wishlistSection.AssertDetailsOfProductAddedToWishlist(wishList);
        }

        [Test]
        public void UserRedirectedToWishListPage_When_ProductSuccessfullyAddedToWishlistFromCategoryPage_And_WishListButtonClicked()
        {
            var category = "Desktops";
            var user = UserInfoFactory.DefaultUserTony();
            var product = ProductFactory.DefaultProductToAddOrRemoveFromWishList()[0];
            var wishList = WishListInfoFactory.DefaultProductAddedToWishList();
            _mainMenuSection.LoginFromMyAccountMenu();
            _loginPage.LoginUser(user.Email, user.Password);
            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ShowProducts("100");
            _categoryPage.ClickListView();

            _categoryPage.AddProductToWishList(product.ProductId);
            _wishlistSection.ViewProductsInWishList();

            _wishlistSection.AssertDetailsOfProductAddedToWishlist(wishList);
        }


       [Test]
        public void LoggedInUserAddedProductFromWishListToCart_When_AddTOCartButtonClicked()
        {
            var category = "Desktops";
            var user = UserInfoFactory.DefaultUserTony();
            var product = ProductFactory.DefaultProductToAddOrRemoveFromWishList()[0];
            var wishList = WishListInfoFactory.DefaultProductAddedToWishList();

            _mainMenuSection.LoginFromMyAccountMenu();
            _loginPage.LoginUser(user.Email, user.Password);
            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ClickListView();
            _categoryPage.ShowProducts("100");
            _categoryPage.AddProductToWishList(product.ProductId);
            _wishlistSection.ViewProductsInWishList();

            _wishlistSection.ClickAddToCartButton(product);
            _cartInfoSection.ViewCart();

            _cartPage.AssertPageUrl();
            _cartPage.AssertProductName(product.ProductName);
        }

        [Test]
        public void LoggedInUserRemovedProductFromWishList_When_RemoveButtonClicked()
        {
            var category = "Desktops";
            var user = UserInfoFactory.DefaultUserTony();
            var product = ProductFactory.DefaultProductToAddOrRemoveFromWishList()[0];
            var wishList = WishListInfoFactory.DefaultProductAddedToWishList();

            _mainMenuSection.LoginFromMyAccountMenu();
            _loginPage.LoginUser(user.Email, user.Password);
            _searchSection.SelectCategoryToSearch(category);
            _categoryPage.ClickListView();
            _categoryPage.ShowProducts("100");
            _categoryPage.AddProductToWishList(product.ProductId);
            _wishlistSection.ViewProductsInWishList();

            _wishlistSection.ClickRemoveButton();

            _wishlistSection.AssertPageTitle();
            _wishlistSection.AssertWishListModifiedSuccessfully();

        }

        [TearDown]
        public void TearDown()
        {
            _wishlistSection.OpenWishlist();
            _wishlistSection.DeleteAllProductsInWishList();
            _cartInfoSection.OpenCart();
            _cartInfoSection.EditCartFromCartIcon();
            _cartPage.RemoveAllProductsFromCart();
        }
    }
}
