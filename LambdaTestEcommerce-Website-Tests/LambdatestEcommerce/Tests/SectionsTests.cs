﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests;
internal class SectionsTests : BaseTest
{
    [SetUp]
    public void TestSetUp()
    {
        _mainPage.GoTo();
    }

    //Main Menu Section Tests
    //AssertPageUrl will be updated once Classes created for other pages: Special offers, Modules, Designs, Widgets, etc
    [Test]
    public void NewUserRedirectedToHomePage_When_HomeLinkClicked()
    {
        _mainMenuSection.OpenHomePage();

        _mainMenuSection.AssertPageUrl();
    }

    [Test]
    public void NewUserRedirectedToSpecialOffersPage_When_SpecialLinkClicked()
    {
        _mainMenuSection.OpenSpecialOffersPage();

        _mainMenuSection.AssertPageUrl("/index.php?route=product/special");
    }

    [Test]
    public void NewUserRedirectedToModulesPage_When_AddOnsLinkHovered_And_ModulesLinkClicked()
    {
        _mainMenuSection.OpenModulesPageFromAddOnsMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=extension/maza/page&page_id=10");
    }

    [Test]
    public void NewUserRedirectedToDesignsPage_When_AddOnsLinkHovered_And_DesignsLinkClicked()
    {
        _mainMenuSection.OpenDesignsPageFromAddOnsMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=extension/maza/page&page_id=11");
    }

    [Test]
    public void NewUserRedirectedToWidgetsPage_When_AddOnsLinkHovered_And_WidgetsLinkClicked()
    {
        _mainMenuSection.OpenWidgetsPageFromAddOnsMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=extension/maza/page&page_id=9");
    }

    [Test]
    public void NewUserRedirectedToBlogPage_When_BlogLinkClicked()
    {
        _mainMenuSection.OpenBlogPage();

        _mainMenuSection.AssertPageUrl("/index.php?route=extension/maza/blog/home");
    }

    [Test]
    public void NewUserUserRedirectedToLoginPage_When_MyAccountLinkClicked()
    {
        _mainMenuSection.OpenMyAccountPage();

        _mainMenuSection.AssertPageUrl("/index.php?route=account/login");
    }

    [Test]
    public void NewUserUserRedirectedToLoginPage_When_MyAccountLinkHovered_AndLoginLinkClicked()
    {
        _mainMenuSection.LoginFromMyAccountMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=account/login");
    }

    [Test]
    public void LoggedInUserRedirectedToMyAccountPage_When_MyAccountLinkHovered_And_DashboardLinkClicked()
    {
        Models.UserData user = UserInfoFactory.DefaultUserTony();

        _mainMenuSection.LoginFromMyAccountMenu();
        _loginPage.LoginUser(user.Email, user.Password);

        _mainMenuSection.OpenDashboardFromMyAccountMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=account/account");
    }

    [Test]
    public void LoggedInUserRedirectedToOrderHistoryPage_When_MyAccountLinkHovered_And_MyOrderLinkClicked()
    {
        Models.UserData user = UserInfoFactory.DefaultUserTony();

        _mainMenuSection.LoginFromMyAccountMenu();
        _loginPage.LoginUser(user.Email, user.Password);

        _mainMenuSection.OpenOrderHistoryPageFromMyAccountMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=account/order");
    }

    [Test]
    public void LoggedInUserRedirectedToProductReturnsPage_When_MyAccountLinkHovered_And_ReturnLinkClicked()
    {
        Models.UserData user = UserInfoFactory.DefaultUserTony();

        _mainMenuSection.LoginFromMyAccountMenu();
        _loginPage.LoginUser(user.Email, user.Password);

        _mainMenuSection.OpenProductReturnsPageFromMyAccountMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=account/return/add");
    }

    [Test]
    public void LoggedInUserRedirectedToOrderTrackingPage_When_MyAccountLinkHovered_And_TrackingLinkClicked()
    {
        Models.UserData user = UserInfoFactory.DefaultUserTony();

        _mainMenuSection.LoginFromMyAccountMenu();
        _loginPage.LoginUser(user.Email, user.Password);

        _mainMenuSection.OpenTrackingPageFromMyAccountMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=information/tracking");//bug - user is redirected but page content is: "The page you requested cannot be found."
    }


    [Test]
    public void LoggedInUserRedirectedToMyVoucherPage_When_MyAccountLinkHovered_And_MyVoucherLinkClicked()
    {
        Models.UserData user = UserInfoFactory.DefaultUserTony();

        _mainMenuSection.LoginFromMyAccountMenu();
        _loginPage.LoginUser(user.Email, user.Password);

        _mainMenuSection.OpenMyVoucherPage();

        _mainMenuSection.AssertPageUrl("/index.php?route=account/voucher");
    }

    [Test]
    public void LoggedInUserLoggedout_When_MyAccountLinkHovered_And_LogoutLinkClicked()
    {
        Models.UserData user = UserInfoFactory.DefaultUserTony();

        _mainMenuSection.LoginFromMyAccountMenu();
        _loginPage.LoginUser(user.Email, user.Password);

        _mainMenuSection.LogoutFromMyAccountMenu();

        _mainMenuSection.AssertPageUrl("/index.php?route=account/logout");
    }

    // Breadcrumbs section tests
    [Test]
    public void BreadcrumbItemSearchOpened_When_BreadcrumbSearchLinkClicked()
    {
        Models.Product product = ProductFactory.DefaultProductsInStock()[0];
        string category = "Desktops";
        string breadcrumbItem = "Search";

        _searchSection.SelectCategoryToSearch(category);
        _categoryPage.ClickProductImage(product);
       
        _breadcrumbSection.OpenBreadcrumbItem(breadcrumbItem);
        
        _breadcrumbSection.AssertPageUrl("/index.php?route=product/search&search=&category_id=20");
    }

    [Test]
    public void BreadcrumbItemCategoryOpened_When_BreadcrumbCategoryLinkClicked()
    {
        Models.Product product = ProductFactory.DefaultProductsInStock()[0];
        string category = "Desktops";

        _searchSection.SelectCategoryToSearch(category);
        _categoryPage.ClickProductImage(product);

        _breadcrumbSection.OpenBreadcrumbItem(category);
        
        _breadcrumbSection.AssertPageUrl("/index.php?route=product/category&path=20");
    }

    // Search Section tests
    [Test]
    public void UserSuccessfullySearchedForProduct_When_ProductEnteredInSearchSection_And_SearchButtonClicked()
    {
        Models.Product product = ProductFactory.DefaultProductsInStock()[0];
        _searchSection.SearchForItem(product.ProductName);
       
        _searchSection.AssertPageUrl("/index.php?route=product%2Fsearch&search=Samsung+SyncMaster+941BW");
    }

    [Test]
    public void NewUserRedirectedToTopCategoriesPage_When_ShopByCategoryLinkClicked()
    {
        _shopByCategorySection.OpenTopCategories();

       _shopByCategorySection.AssertTopCategoriesTitle();
    }
}