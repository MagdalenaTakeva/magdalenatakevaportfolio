﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.QuickViewSectionTests
{
    public class QuickViewSectionTests : BaseTest
    {

        [Test]
        public void UserQuickViewItemInTopProductsModule_When_QuickViewButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();
            
            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);

            _mainPage.AssertProductName(product.ProductName);
            _mainPage.AssertProductAvailability(product.ProductAvailability);
        }

        [Test]
        public void UserAddedProductToCart_When_QuickViewButtonClicked_And_AddToCartButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();

            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);
            _quickViewSection.AddProductToCart();
            _cartInfoSection.CloseProductNotification();
            _cartInfoSection.OpenCart();

            _cartInfoSection.AssertCartCurrentAmount("1");
        }

        [Test]
        public void UserAddedProductToCart_When_QuickViewButtonClicked_AddToCartButtonClicked_And_NotificationViewCartButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();

            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);
            _quickViewSection.AddProductToCart();
            _cartInfoSection.ViewCart();

            _cartPage.AssertProductQuantity("1");
            _cartPage.AssertProductName(product.ProductName);
        }

        [Test]
        public void UserRedirectedToCheckout_When_QuickViewButtonClicked_And_BuyNowButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();

            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);
            _quickViewSection.BuyNowProduct();

            _checkoutPage.AssertPageUrl();
            _checkoutPage.AssertProductName(product.ProductName);
        }

        [Test]
        public void UserUnableToAddToCartProduct_When_ProductOutOfStock()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[0];//Product with id=107 is Out of Stock

            _mainPage.GoTo();

            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);

            _quickViewSection.AssertAddToCartButtonDisabled();
        }

        [Test]
        public void UserUnableToBuyNowProduct_When_ProductOutOfStock()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[0];//Product with id=107 is Out of Stock

            _mainPage.GoTo();

            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);

            _quickViewSection.AssertBuyNowButtonDisabled();
        }

        [Test]
        public void UserIncreasedProductQuantityAndAddedProductToCart_When_PlusSignClicked_And_AddToCartButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();
            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);

            _quickViewSection.IncreaseProductQuantity(1, 4);
            _quickViewSection.AddProductToCart();
            _cartInfoSection.ViewCart();

            _cartPage.AssertProductQuantity(4);
        }

        [Test]

        public void UserDecreasedProduuctQuantity_When_MinusSignClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();
            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);

            _quickViewSection.IncreaseProductQuantity(1, 4);
            _quickViewSection.DecreaseProductQuantity(4, 2);

            _quickViewSection.AddProductToCart();
            _cartInfoSection.ViewCart();

            _cartPage.AssertProductQuantity(2);
        }

        [Test]
        public void LoggedInUserAddedProductToWishList_When_QuickViewButtonClicked_And_AddItemToWishListButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];
            var user = UserInfoFactory.DefaultUserTony();

            _loginPage.LoginUser(user.Email, user.Password);
            _wishlistSection.OpenWishlist();
            _wishlistSection.DeleteAllProductsInWishList();
            _wishlistSection.PressContinueButton();
            _mainMenuSection.OpenHomePage();

            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);
            _quickViewSection.AddProductToWishList();
            _wishlistSection.ViewProductsInWishList();

            _wishlistSection.AssertNumberOfProductsAsExpected(1);
        }

        [Test]
        public void ExistingUserUnableToAddItemToWishList_When_NotLoggedIn()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();

            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);
            _quickViewSection.AddProductToWishList();

            _quickViewSection.AssertNotificationLoginButtonDisplayed();
            _quickViewSection.AssertNotificationRegisterButtonDisplayed();
            _wishlistSection.AssertNumberOfProductsAsExpected(0);
        }

        [Test]
        public void ExistingUserRedirectedToLoginPage_When_TryToAddItemToWishListFromQuickView_And_NotificationLoginButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();
            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);

            _quickViewSection.AddProductToWishList();
            _quickViewSection.ClickNotificationLoginButton();

            _loginPage.AssertPageUrl();
        }

        [Test]
        public void ExistingUserRedirectedToRegisterPage_When_TryToAddItemToWishLiistFromQuickView_And_NotificationRegisterButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product = productList[2];

            _mainPage.GoTo();
            _mainPage.QuickViewItemInTopProductsModule(productList, product.ProductId);

            _quickViewSection.AddProductToWishList();
            _quickViewSection.ClickNotificationRegisterButton();

            _registrationPage.AssertPageUrl();
        }

        [Test]
        public void UserAddedItemToCompareList_When_QuickViewButtonClicked_And_AddThisProductToCompareButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product1 = productList[2];
            
            _mainPage.GoTo();
            _mainPage.QuickViewItemInTopProductsModule(productList, product1.ProductId);

            _quickViewSection.AddProductToCompareList();
            _compareProductsSection.CloseCompareProductNotification();
            _compareProductsSection.OpenCompareProductsPage();

            _compareProductsPage.AssertNumberOfProductsAsExpected(1);
        }

        [Test]
        public void UserAddedItemToCompareList_When_QuickViewButtonClicked_AddThisProductToCompareButtonClicked_And_NotificationProductCompareButtonClicked()
        {
            var productList = CompareProductInfoFactory.ListTopProductsModuleProducts();
            var product1 = productList[2];

            _mainPage.GoTo();
            _mainPage.QuickViewItemInTopProductsModule(productList, product1.ProductId);

            _quickViewSection.AddProductToCompareList();
            _compareProductsSection.ClickNotificationProductCompareButton();

            _compareProductsPage.AssertNumberOfProductsAsExpected(1);
        }
    }
}
