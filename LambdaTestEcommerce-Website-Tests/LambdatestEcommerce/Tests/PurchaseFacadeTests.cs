﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Factories;
using LambdatestEcommerce.Models;
using static LambdatestEcommerce.Facades.PurchaseFacade;

namespace LambdatestEcommerce.Tests;
public class PurchaseFacadeTests : BaseTest
{
    [Test]
    public void UserPurchasedProduct_When_ListViewSelected_ProductAddedToCart_ShippingRateApplied_DifferentBillingAndShippingAddressesProvided_CheckoutButtonClicked_And_OrderConfirmed()
    {
        var userType = UserTypes.NewUser;
        var userInfo = UserInfoFactory.CreateUser();
        var addressType = AddressTypes2.NewBillingAddress;
        var billingAddress = BillingAddressInfoFactory.CreateBillingAddress();
        var shippingAddress = ShippingAddressInfoFactory.CreateShippingAddress();
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var productList = ProductFactory.DefaultProductInStock();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.NotTheSame;
        AddProductToCartApplyShippingRateCheckoutAndConfirmOrder(productList,
                                        userInfo,
                                        userType,
                                        addressType,
                                        billingAddress,
                                        shippingAddress,
                                        billingAndShippingAddressesAreTheSame,
                                        shippingInfo);
    }

    [Test]
    public void UserPurchasedProduct_When_GridViewSelected_ProductAddedToCart_ShippingRateApplied_DifferentBillingAndShippingAddressesProvided_CheckoutButtonClicked_And_OrderConfirmed()
    {
        var userType = UserTypes.NewUser;
        var userInfo = UserInfoFactory.CreateUser();
        var addressType = AddressTypes2.NewBillingAddress;
        var billingAddress = BillingAddressInfoFactory.CreateBillingAddress();
        var shippingAddress = ShippingAddressInfoFactory.CreateShippingAddress();
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var productList = ProductFactory.DefaultProductInStock();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.NotTheSame;
        var shippingMethod = ShippingMethod.Applied;

        SelectGridViewAddProductToCartApplyShippingRateCheckoutAndConfirmOrder(productList,
                                                                                userInfo,
                                                                                userType,
                                                                                addressType,
                                                                                billingAddress,
                                                                                shippingAddress,
                                                                                billingAndShippingAddressesAreTheSame,
                                                                                shippingInfo,
                                                                                shippingMethod);
    }

    [Test]
    public void PurchasedProducts_When_NewUser_WithSameBillingAndShippingAddresses_And_AppliedShippingRate()
    {
        var userType = UserTypes.NewUser;
        var userInfo = UserInfoFactory.CreateUser();
        var addressType = AddressTypes2.NewBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;
        var productList = ProductFactory.DefaultProductsInStock();

        AddProductToCartApplyShippingRateCheckoutAndConfirmOrder(productList,
                                                                       userInfo,
                                                                       userType,
                                                                       addressType,
                                                                       billingAddress,
                                                                       shippingAddress,
                                                                       billingAndShippingAddressesAreTheSame,
                                                                       shippingInfo);
    }

    [Test]
    public void PurchasedProduct_When_GuestUser_WithSameBillingAndShippingAddresses_And_AppliedShippingRate()
    {
        var userType = UserTypes.GuestUser;
        var userInfo = UserInfoFactory.CreateUser();
        var addressType = AddressTypes2.NewBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;
        var productList = ProductFactory.DefaultProductInStock();

        AddProductToCartApplyShippingRateCheckoutAndConfirmOrder(productList,
                                                                    userInfo,
                                                                    userType,
                                                                    addressType,
                                                                    billingAddress,
                                                                    shippingAddress,
                                                                    billingAndShippingAddressesAreTheSame,
                                                                    shippingInfo);
    }

    //if the logged out user have saved items in his cart, once he logs in at Checkout, those will be added to the product he is currently buying
    //this test is running when there are no saved products in cart

    [Test]
    public void PurchasedProduct_When_LoggedOutUser_WithSameBillingAndShippingAddresses_And_AppliedShippingRate()
    {
        var userType = UserTypes.LoggedOutUser;
        var userInfo = UserInfoFactory.DefaultUserVeronika();
        var addressType = AddressTypes2.ExistingBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;
        var productList = ProductFactory.DefaultProductInStock();

        AddProductToCartApplyShippingRateCheckoutAndConfirmOrder(productList,
                                                                     userInfo,
                                                                     userType,
                                                                     addressType,
                                                                     billingAddress,
                                                                     shippingAddress,
                                                                     billingAndShippingAddressesAreTheSame,
                                                                     shippingInfo);
    }

    [Test]
    //There is a bug - no option to apply flat shipping rate for iPod Nano
    public void UserPurchasedProduct_When_UserLoggedIn_AddedProductToCart_CheckedOut_And_ConfirmedOrder()
    {
        var userType = UserTypes.LoggedInUser;
        var userInfo = UserInfoFactory.DefaultUserTony();
        var addressType = AddressTypes2.ExistingBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;
        var productList = ProductFactory.DefaultProductInStock();

        BuyProductAsLoggedInUser(productList,
                                       userInfo,
                                       userType,
                                       addressType,
                                       billingAddress,
                                       shippingAddress,
                                       billingAndShippingAddressesAreTheSame,
                                       shippingInfo);
    }

    [Test]
    public void StoredProductInCartAddedToNewProductAddedToCart_When_UserLoggedInAtCheckout()
    {
        var userType = UserTypes.LoggedOutUser;
        var userInfo = UserInfoFactory.DefaultUserTony();
        var addressType = AddressTypes2.ExistingBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;
        var product = ProductFactory.DefaultProductsInStock()[0];

        AddStoredItemInCartToNewItemInCartWhenUserLogsInAtCheckout(product,
                                                               userInfo,
                                                               userType,
                                                               addressType,
                                                               billingAddress,
                                                               shippingAddress,
                                                               billingAndShippingAddressesAreTheSame,
                                                               shippingInfo);
    }


    [Test]
    public void NewUserPurchasedMultipleProducts_When_ProductsAddedToCart_SameBillingAndShippingAddressesProvided_CheckoutClicked_OrderConfirmed()
    {
        var userType = UserTypes.NewUser;
        var userInfo = UserInfoFactory.CreateUser();
        var addressType = AddressTypes2.NewBillingAddress;
        List<Product> productList = ProductFactory.DefaultProductsInStock();
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;

        AddMultipleProductsToCartApplyShippingRateCheckoutAndConfirmOrder(userType,
                                                                           userInfo,
                                                                           addressType,
                                                                           productList,
                                                                           billingAddress,
                                                                           shippingAddress,
                                                                           billingAndShippingAddressesAreTheSame,
                                                                           shippingInfo);
    }

    [Test]
    public void LoggedInUserBuysGiftCertificateAndOneProduct_When_UserLoggedIn_GiftCertificatePurchased_ProductAddedToCart_CheckoutCompleted_OrderConfirmed_MyaccountLinkClicked_OrderHistoryLinkClicked()
    {
        var userInfo = UserInfoFactory.DefaultUserTony();
        var giftCertificateInfo = GiftCertificateFactory.BirthdayGiftCertificate();
        var giftCertificateTheme = GiftCertificateTheme.Birthday;
        var product = ProductFactory.DefaultProductsInStock()[0];
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;

        LoggedInUserBuysGiftCertificateAndOneProduct(userInfo,
                                                      giftCertificateInfo,
                                                      giftCertificateTheme,
                                                      product,
                                                      shippingInfo,
                                                      billingAndShippingAddressesAreTheSame,
                                                      "$201.00", "$5.00", 206.00);
    }

    [Test]
    public void UserPurchasedItems_When_UserLoggedIn_AddedAdditionalItemsToItemsStoredInShoppingCart_CompletedCheckout_And_ConfirmedOrder()
    {
        var userInfo = UserInfoFactory.DefaultUserTony();
        var userType = UserTypes.LoggedInUser;
        var addressType = AddressTypes2.ExistingBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;
        var shippingInfo = CartPageShippingInfoFactory.DefaultShippingInfo();
        var product = ProductFactory.DefaultProductsInStock()[0];

        BuyProductsByLoggingInAndAddingAdditionalItemsToAlreadyStoredItemsInCart(product,
                                                                                 userInfo,
                                                                                 userType,
                                                                                 addressType,
                                                                                 billingAddress,
                                                                                 shippingAddress,
                                                                                 billingAndShippingAddressesAreTheSame,
                                                                                 shippingInfo);
    }
}