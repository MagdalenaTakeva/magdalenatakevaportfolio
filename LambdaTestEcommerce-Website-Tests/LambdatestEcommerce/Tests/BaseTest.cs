﻿using LambdatestEcommerce.Facades;
using LambdatestEcommerce.Pages.AccountDownloads;
using LambdatestEcommerce.Pages.Cart;
using LambdatestEcommerce.Pages.Category;
using LambdatestEcommerce.Pages.Checkout;
using LambdatestEcommerce.Pages.CompareProductsPage;
using LambdatestEcommerce.Pages.ConfirmPage;
using LambdatestEcommerce.Pages.ContactsPage;
using LambdatestEcommerce.Pages.ForgottenPasswordPage;
using LambdatestEcommerce.Pages.Login;
using LambdatestEcommerce.Pages.LogoutPage;
using LambdatestEcommerce.Pages.Main;
using LambdatestEcommerce.Pages.MyAccount;
using LambdatestEcommerce.Pages.MyVoucherPage;
using LambdatestEcommerce.Pages.ProductDetails;
using LambdatestEcommerce.Pages.Registration;
using LambdatestEcommerce.Pages.SuccessPage;
using LambdatestEcommerce.Pages.VoucherSuccessPage;
using LambdatestEcommerce.Sections.BreadcrumbSection;
using LambdatestEcommerce.Sections.CartInfoSection;
using LambdatestEcommerce.Sections.CompareProductsSection;
using LambdatestEcommerce.Sections.MainMenuSection;
using LambdatestEcommerce.Sections.QuickViewSection;
using LambdatestEcommerce.Sections.SearchSection;
using LambdatestEcommerce.Sections.ShopByCategorySection;
using LambdatestEcommerce.Sections.WishListSection;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OrderHistoryPage = LambdatestEcommerce.Pages.OrderHistoryPage.OrderHistoryPage;

namespace LambdatestEcommerce.Tests;
public class BaseTest
{
    protected static IWebDriver _driver;
    protected static MainPage _mainPage;
    protected static RegistrationPage _registrationPage;
    protected static MyAccountPage _myAccountPage;
    protected static LoginPage _loginPage;
    protected static LogoutPage _logoutPage;
    protected static ForgottenPasswordPage _forgottenPasswordPage;
    protected static CategoryPage _categoryPage;
    protected static CartPage _cartPage;
    protected static CheckoutPage _checkoutPage;
    protected static ConfirmPage _confirmPage;
    protected static CompareProductsSection _compareProductsSection;
    protected static SuccessPage _successPage;
    protected static OrderHistoryPage _orderHistoryPage;
    protected static MyVoucherPage _myVoucherPage;
    protected static VoucherSuccessPage _voucherSuccessPage;
    protected static ContactsPage _contactsPage;
    protected static ProductDetailsPage _productDetailsPage;
    protected static PurchaseFacade _purchaseFacade;
    protected static CartFacade _cartFacade;
    protected static MainMenuSection _mainMenuSection;
    protected static BreadcrumbSection _breadcrumbSection;
    protected static SearchSection _searchSection;
    protected static ShopByCategorySection _shopByCategorySection;
    protected static CartInfoSection _cartInfoSection;
    protected static WishlistSecion _wishlistSection;
    protected static AccountDownloadsPage _accountDownloadsPage;
    protected static CompareProductsPage _compareProductsPage;
    protected static QuickViewSection _quickViewSection;


    protected Actions actions;

    [SetUp]
    public void ClassInit()
    {
        //new DriverManager().SetUpDriver(new ChromeConfig(), VersionResolveStrategy.MatchingBrowser);
        _driver = new ChromeDriver(@"C:\Temp\WebDrivers\Chrome_driver\chromedriver-win64");
        _mainPage = new MainPage(_driver);
        _registrationPage = new RegistrationPage(_driver);
        _loginPage = new LoginPage(_driver);
        _logoutPage = new LogoutPage(_driver);
        _forgottenPasswordPage = new ForgottenPasswordPage(_driver);
        _myAccountPage = new MyAccountPage(_driver);
        _categoryPage = new CategoryPage(_driver);
        _cartPage = new CartPage(_driver);
        _checkoutPage = new CheckoutPage(_driver);
        _confirmPage = new ConfirmPage(_driver);
        _compareProductsSection = new CompareProductsSection(_driver);
        _successPage = new SuccessPage(_driver);
        _orderHistoryPage = new OrderHistoryPage(_driver);
        _myVoucherPage = new MyVoucherPage(_driver);
        _voucherSuccessPage = new VoucherSuccessPage(_driver);
        _contactsPage = new ContactsPage(_driver);
        _productDetailsPage = new ProductDetailsPage(_driver);
        _purchaseFacade = new PurchaseFacade(_driver);
        _cartFacade = new CartFacade(_driver);
        _mainMenuSection = new MainMenuSection(_driver);
        _breadcrumbSection = new BreadcrumbSection(_driver);
        _searchSection = new SearchSection(_driver);
        _shopByCategorySection = new ShopByCategorySection(_driver);
        _cartInfoSection = new CartInfoSection(_driver);
        _wishlistSection = new WishlistSecion(_driver);
        _accountDownloadsPage = new AccountDownloadsPage(_driver);
        _compareProductsPage = new CompareProductsPage(_driver);
        _quickViewSection = new QuickViewSection(_driver);
        actions = new Actions(_driver);
    }

    
    [TearDown]
    public void TestCleanup()
    {
        _driver.Quit();
    }
}
