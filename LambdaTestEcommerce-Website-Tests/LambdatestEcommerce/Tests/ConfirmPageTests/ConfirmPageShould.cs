﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.ConfirmPageTests;

[TestFixture]
public class ConfirmPageShould : BaseTest
{
    [SetUp]
    public void SetUp()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        var userInfo = UserInfoFactory.DefaultUserTony();
        var category = "Desktops";

        _loginPage.LoginUser(userInfo.Email, userInfo.Password);
        _cartPage.RemoveAllProductsFromCart();
        _searchSection.SelectCategoryToSearch(category);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        _cartInfoSection.ViewCart();
        _cartPage.ProceedToCheckout();
        _checkoutPage.CheckTermsAndConditionsCheckbox();
        _checkoutPage.PressContinueButton();
    }

    [Test]
    public void LoggedInUserRedirectedToSuccessPage_When_ConfirmOrderButtonClicked()
    {
        _confirmPage.ConfirmOrder();
        _successPage.AssertPageUrl();
    }

    [Test]
    public void PaymentAndShippingAddressesTheSame_When_LoggedInUserSelectedShippingAndBillingAddressesToBeTheSame()
    {
        var paymentAddress = "Tony Marinov Planet fgfg Sofia 1000 Sofia - town,Bulgaria";
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;
        _confirmPage.AssertPaymentAddress(paymentAddress);
        _confirmPage.AssertPaymentAndShippingAddressesAreTheSame(billingAndShippingAddressesAreTheSame);
    }

    [Test]
    public void ShippingMethodDisplayed_When_LoggedInUserRedirectedToConfirmOrderPage()
    {         
        _confirmPage.AssertShippingMethodIsFlatShippingRate();
    }

    [Test]
    public void LoggedInUserRedirectedBackToCheckoutPage_When_EditButtonClicked()
    {         
        _confirmPage.ClickEditButton();

        _checkoutPage.AssertPageUrl();
    }
}
