﻿using LambdatestEcommerce.Enums;
using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.SuccessPageTests;

[TestFixture]
public class SuccessPageShould : BaseTest
{
    [SetUp]
    public void SetUp()
    {
        var product = ProductFactory.DefaultProductsInStock()[0];
        var userInfo = UserInfoFactory.DefaultUserTony();
        var userType = UserTypes.LoggedOutUser;
        var addressType = AddressTypes2.ExistingBillingAddress;
        var billingAddress = BillingAddressInfoFactory.DefaultBillinngAddress();
        var shippingAddress = ShippingAddressInfoFactory.DefaultShippingAddress();
        var billingAndShippingAddressesAreTheSame = BillingAndShippingAddresses.TheSame;
        string category = "Desktops";

        _mainPage.GoTo();
        _searchSection.SelectCategoryToSearch(category);
        _categoryPage.ShowProducts("100");
        _categoryPage.AddProductToCartByProductId(product.ProductId, product.Quantity);
        _cartInfoSection.ViewCart();
        _cartPage.ProceedToCheckout();
        _checkoutPage.FillInCheckoutForm(userInfo, userType, addressType, billingAddress, shippingAddress, billingAndShippingAddressesAreTheSame);
        _confirmPage.ConfirmOrder();
    }

    [Test]
    public void ExistingUserRedirectedToHisOrderHistoryPage_When_HistoryLinkClicked()
    {
        _successPage.ClickOrderHistoryLink();

        _orderHistoryPage.AssertPageUrl();
    }

    [Test]
    public void ExistingUserRedirectedToHisAccountPage_When_MyAccountLinkClicked()
    {
        _successPage.ClickMyAccountLink();

        _myAccountPage.AssertPageUrl();
    }

    [Test]
    public void ExistingUserRedirectedToHisAccountDownloadsPage_When_DownloadsLinkClicked()
    {
        _successPage.ClickDownloadsLink();

        _accountDownloadsPage.AssertPageUrl();
    }

    [Test]
    public void ExistingUserRedirectedToContactsPage_When_StoreOwnerLinkClicked()
    {
        _successPage.ClickStoreOwnerLink();

        _contactsPage.AssertPageUrl();
    }

    [Test]
    public void ExistingUserRedirectedToHomePage_When_ContinueButtonClicked()
    {
        _successPage.PressContinueButton();

        _mainPage.AssertPageUrl("/index.php?route=common/home");
    }
}
