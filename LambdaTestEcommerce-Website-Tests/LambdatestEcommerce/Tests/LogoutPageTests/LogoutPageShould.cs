﻿using LambdatestEcommerce.Factories;

namespace LambdatestEcommerce.Tests.LogoutPageTests;

[TestFixture]
public class LogoutPageShould : BaseTest
{

    [SetUp]
    public void UserLoggedIn()
    {
        _loginPage.LoginUser(UserInfoFactory.DefaultUserTony());
    }

    [Test]
    public void LogoutPageAppeared_When_UserSuccessfullyLoggedOff_When_SideBarLogoutLinkClicked()
    {
        _loginPage.ClickRightColumnLinkByName("Logout");

        _logoutPage.AssertPageUrl();
        _logoutPage.AssertPageTitle();
        _logoutPage.AssertLogoutPageText();
    }
}
