namespace LambdatestEcommerce.Enums;

public enum BillingAndShippingAddresses
{
    TheSame,
    NotTheSame
}