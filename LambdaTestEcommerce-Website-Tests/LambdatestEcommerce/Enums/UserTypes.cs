﻿namespace LambdatestEcommerce.Enums;

public enum UserTypes
{
    NewUser,
    GuestUser,
    LoggedOutUser,
    LoggedInUser
}