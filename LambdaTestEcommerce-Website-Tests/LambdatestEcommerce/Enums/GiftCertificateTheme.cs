namespace LambdatestEcommerce.Enums;

public enum GiftCertificateTheme
{
    Birthday,
    Christmas,
    General
}