namespace LambdatestEcommerce.Enums;

public enum AddressTypes2
{
    NewBillingAddress,
    ExistingBillingAddress,
    NewShippingAddress,
    ExistingShippingAddress
}