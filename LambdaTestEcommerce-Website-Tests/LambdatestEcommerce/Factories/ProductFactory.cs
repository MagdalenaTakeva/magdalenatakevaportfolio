﻿using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Factories;

public class ProductFactory
{
    public static List<Product> DefaultProductsInStock()
    {
        List<Product> products = new List<Product>()
        {
             new Product()
            {
                ProductName = "Samsung SyncMaster 941BW",
                ProductCode = "Product 6",
                ProductBrand = "Canon",
                UnitPrice = 242.00,
                NewUnitPrice = 200.00,
                Quantity = 1,
                ProductTotalPrice = 242.00,
                NewProductTotalPrice = 200.00,
                ProductId = 66,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=66&search=&category_id=20",
                ProductCategoryId = 20,
            },
             new Product()
            {
                ProductName = "HTC Touch HD",
                ProductCode = "Product 1",
                ProductBrand = "HTC",
                UnitPrice = 146.00,
                NewUnitPrice = 120.00,
                Quantity = 1,
                ProductTotalPrice = 146.00,
                NewProductTotalPrice = 120.00,
                ProductId = 28,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=28&search=&category_id=20",
                ProductCategoryId = 20,
            },
          /*   new Product()
             {
                ProductName = "Canon EOS 5D",
                ProductCode = "Product 3",
                ProductBrand = "Canon",
                UnitPrice = 134.00,
                Quantity = 1,
                ProductTotalPrice = 134.00,
                ProductId = 30,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=30&search=&category_id=20",
                ProductCategoryId = 20,
             }*/
             new Product()
            {
                ProductName = "iPod Nano",
                ProductCode = "Product 9",
                ProductBrand = "Apple",
                UnitPrice = 122.00,
                NewUnitPrice = 100.00,
                Quantity = 1,
                ProductTotalPrice = 122.00,
                NewProductTotalPrice = 100.00,
                ProductId = 36,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=36&search=&category_id=20",
                ProductCategoryId = 20,
            },

        };
        return products;
    }

    public static List<Product> DefaultProductsOutOfStock()
    {
        List<Product> products = new List<Product>()
        {
        new Product()
            {
            ProductName = "Palm Treo Pro",
                ProductCode = "Product 2",
                ProductBrand = "Palm",
                UnitPrice = 337.99,
                Quantity = 1,
                ProductTotalPrice = 337.99,
                ProductId = 29,
                InStock = false,
                AddToCart = false,
                BuyNow = false,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=29&search=&category_id=20",
                ProductCategoryId = 20,
            },
        new Product()
            {

                ProductName = "iPod Touch",
                ProductCode = "Product 5",
                ProductBrand = "Apple",
                UnitPrice = 194.00,
                Quantity = 1,
                ProductTotalPrice = 194.00,
                ProductId = 32,
                InStock = false,
                AddToCart = false,
                BuyNow = false,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=32&search=&category_id=20",
                ProductCategoryId = 20,
            },
            new Product()
            {
                ProductName = "Nikon D300",
                ProductCode = "Product 4",
                ProductBrand = "Nikon",
                UnitPrice = 98.00,
                NewUnitPrice = 80.00,
                Quantity = 1,
                ProductTotalPrice = 98.00,
                NewProductTotalPrice = 80.00,
                ProductId = 31,
                InStock = false,
                AddToCart = false,
                BuyNow = false,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=31&search=&category_id=20",
                ProductCategoryId = 20,
            },
        };
        return products;
    }

    public static List<Product> DefaultProductInStock()
    {
        List<Product> products = new List<Product>()
        {
         new Product()
            {
                ProductName = "HTC Touch HD",
                ProductCode = "Product 1",
                ProductBrand = "HTC",
                UnitPrice = 146.00,
                NewUnitPrice = 120.00,
                Quantity = 1,
                ProductTotalPrice = 146.00,
                NewProductTotalPrice = 120.00,
                ProductId = 28,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=28&search=&category_id=20",
                ProductCategoryId = 20,
            }
        };
        return products;
    }

    public static List<Product> DefaultProductToAddOrRemoveFromWishList()
    {
        List<Product> products = new List<Product>()
        {
        new Product()
            {
                ProductName = "iPod Nano",
                ProductCode = "Product 9",
                ProductBrand = "Apple",
                UnitPrice = 122.00,
                NewUnitPrice = 100.00,
                Quantity = 1,
                ProductTotalPrice = 122.00,
                NewProductTotalPrice = 100.00,
                ProductId = 36,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=36&search=&category_id=20",
                ProductCategoryId = 20,
            }
        };
        return products;
    }

    public static List<Product> DefaultProductToCompareList()
    {
        List<Product> products = new List<Product>()
        {
       new Product()
            {
                ProductName = "HTC Touch HD",
                ProductCode = "Product 1",
                ProductBrand = "HTC",
                UnitPrice = 146.00,
                NewUnitPrice = 120.00,
                Quantity = 1,
                ProductTotalPrice = 146.00,
                NewProductTotalPrice = 120.00,
                ProductId = 28,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=28&search=&category_id=20",
                ProductCategoryId = 20,
            },
             new Product()
             {
                ProductName = "iPod Shuffle",
                ProductCode = "Product 3",
                ProductBrand = "Canon",
                UnitPrice = 182.00,
                NewUnitPrice = 150.00,
                Quantity = 1,
                ProductTotalPrice = 182.00,
                NewProductTotalPrice = 150.00,
                ProductId = 34,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=34&search=&category_id=20",
                ProductCategoryId = 20,
             }
        };
        return products;
    }

    public static Product Bug_CannonEOS5D()
    {
        var product = new Product()
        {
            ProductName = "Canon EOS 5D",
            ProductCode = "Product 3",
            ProductBrand = "Canon",
            UnitPrice = 134.00,
            Quantity = 1,
            ProductTotalPrice = 134.00,
            ProductId = 30,
            InStock = true,
            AddToCart = true,
            BuyNow = true,
            ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=30&search=&category_id=20",
            ProductCategoryId = 20,
        };

        return product;
    }

    public static Product Bug_iPodNano()
    {
        var product = new Product()
        {
            ProductName = "iPod Nano",
            ProductCode = "Product 9",
            ProductBrand = "Apple",
            UnitPrice = 122.00,
            NewUnitPrice = 100.00,
            Quantity = 1,
            ProductTotalPrice = 122.00,
            NewProductTotalPrice = 100.00,
            ProductId = 36,
            InStock = true,
            AddToCart = true,
            BuyNow = true,
            ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=36&search=&category_id=20",
            ProductCategoryId = 20,
        };

        return product;
    }

    public static List<Product> DefaultProductInStockQuantityMoreThan1()
    {
        List<Product> products = new List<Product>()
        {
        new Product()
            {
                ProductName = "Samsung SyncMaster 941BW",
                ProductCode = "Product 6",
                ProductBrand = "Canon",
                UnitPrice = 242.00,
                NewUnitPrice = 200.00,
                Quantity = 3,
                ProductTotalPrice = 242.00,
                NewProductTotalPrice = 200.00,
                ProductId = 66,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=66&search=&category_id=20",
                ProductCategoryId = 20,
            },
        new Product()
            {
                ProductName = "HTC Touch HD",
                ProductCode = "Product 1",
                ProductBrand = "HTC",
                UnitPrice = 120.00,
                NewUnitPrice = 100.00,
                Quantity = 1,
                ProductTotalPrice = 120.00,
                NewProductTotalPrice = 100.00,
                ProductId = 28,
                InStock = true,
                AddToCart = true,
                BuyNow = true,
                ProductUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=36&search=&category_id=20",
                ProductCategoryId = 20,
            }
        };
        return products;
    }
}
