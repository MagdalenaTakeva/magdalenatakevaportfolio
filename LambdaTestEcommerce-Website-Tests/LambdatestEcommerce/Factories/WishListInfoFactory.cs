﻿using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Factories
{
    internal class WishListInfoFactory
    {
        public static List<WishListInfo> DefaultWishList()
        {
            List<WishListInfo> wishListProducts = new List<WishListInfo>()
            {
                 new WishListInfo()
                {
                    ImageLink = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=29",
                    ProductName = "Palm Treo Pro",
                    Model = "Product 2",
                    Stock = "Out Of Stock",
                    UnitPrice = "$279.99"
                },
                new WishListInfo()
                {
                    ImageLink = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=33",
                    ProductName = "Samsung SyncMaster 941BW",
                    Model = "Product 6",
                    Stock = "In Stock",
                    UnitPrice = "$200.00"
                }
            };
            

            return wishListProducts;
        }

        public static List<WishListInfo> DefaultProductAddedToWishList()
        {
            List<WishListInfo> wishListProducts = new List<WishListInfo>()
            {
                 new WishListInfo()
                {
                    ImageLink = "https://ecommerce-playground.lambdatest.io/index.php?route=product/product&product_id=36",
                    ProductName = "iPod Nano",
                    Model = "Product 9",
                    Stock = "In Stock",
                    UnitPrice = "$100.00"
                }
            };


            return wishListProducts;
        }
    }
}

