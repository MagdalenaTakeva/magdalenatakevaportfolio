﻿using Bogus;
using Bogus.Extensions;
using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Factories
{
    public class ShippingAddressInfoFactory
    {
        public static ShippingAddressInfo CreateShippingAddress()
        {
            var tempShippingAddress = new Faker<ShippingAddressInfo>()


                .RuleFor(x => x.FirstName, f => f.Name.FirstName().ClampLength(1, 32))
                .RuleFor(x => x.LastName, f => f.Name.LastName().ClampLength(1, 32))
                .RuleFor(x => x.Company, f => "Space X")
                .RuleFor(x => x.Address1, f => f.Address.StreetAddress().ClampLength(1, 128))
                .RuleFor(x => x.Address2, f => f.Address.StreetAddress().ClampLength(1, 128))
                .RuleFor(x => x.City, f => f.Address.City().ClampLength(2, 128))
                .RuleFor(x => x.PostCode, f => f.Address.City().ClampLength(2, 10))
                .RuleFor(x => x.Country, f => "Bulgaria")
                .RuleFor(x => x.Region, f => "Sofia - town");

            var shippingAddress = tempShippingAddress.Generate();

            return shippingAddress;
        }

        public static ShippingAddressInfo DefaultShippingAddress()
        {
            var shippingAddress = new ShippingAddressInfo()
            {
                FirstName = "Veronika",
                LastName = "Ivanova",
                Company = "Space X",
                Address1 = "Pirinski prohod 20",
                Address2 = "Manastirski Livadi",
                City = "Sofia",
                PostCode = "1000",
                Country = "Bulgaria",
                Region = "Plovdiv",
            };

            return shippingAddress;
        }
    }
}
