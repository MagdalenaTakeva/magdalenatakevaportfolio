﻿using Bogus;
using Bogus.Extensions;
using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Factories;
public class UserInfoFactory
{
    Faker faker = new Faker();

    public static UserData DefaultUserVeronika()
    {
        var userInfo = new UserData()
        {
            FirstName = "Veronika",
            LastName = "Ivanova",
            Email = "veronika@abv.bg",
            Telephone = "1234",
            Password = "Veronika123",
            PasswordConfirm = "Veronika123",
        };

        return userInfo;
    }

    public static UserData DefaultUserTony()
    {
        var defaultUser = new UserData()
        {
            FirstName = "Tony",
            LastName = "Tony",
            Email = "Tony@abv.bg",
            Telephone = "1234568",
            Password = "Tony56",
            PasswordConfirm = "Tony56",
        };

        return defaultUser;
    }

    public static UserData CreateUser()
    {
        var tempUser = new Faker<UserData>()
            .RuleFor(x => x.FirstName, f => f.Name.FirstName().ClampLength(1, 32))
            .RuleFor(x => x.LastName, f => f.Name.LastName().ClampLength(1, 32))
            .RuleFor(x => x.Email, f => f.Internet.Email())
            .RuleFor(x => x.Telephone, f => f.Phone.PhoneNumber().ClampLength(3, 32))
            .RuleFor(x => x.Password, f => f.Internet.Password().ClampLength(4, 20));

        var user = tempUser.Generate();
        user.PasswordConfirm = user.Password;
        return user;
    }
}
