using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Factories;

public class CartPageShippingInfoFactory
{
    public static CartPageShippingInfo DefaultShippingInfo()
    {
        var cartShippingInfo = new CartPageShippingInfo()
        {
            Country = "Bulgaria",
            Region = "Sofia - town",
            PostCode = "1000",
        };

        return cartShippingInfo;
    }
}