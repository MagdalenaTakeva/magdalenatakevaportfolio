using Bogus;
using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Factories;

public class GiftCertificateFactory
{
    public static GiftCertificateInfo BirthdayGiftCertificate()
    {
        var giftCertificateInfo = new GiftCertificateInfo()
        {
            RecipientsName = "Veronika",
            RecipientsEmail = "veronika@abv.bg",
            YourName = "Tony Tony",
            YourEmail = "Tony@abv.bg",
            Amount = "1"
        };

        return giftCertificateInfo;
    }
}