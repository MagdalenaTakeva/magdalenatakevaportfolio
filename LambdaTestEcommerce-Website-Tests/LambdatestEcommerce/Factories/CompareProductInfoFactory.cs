﻿using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Factories
{
    public class CompareProductInfoFactory
    {
        public static List<CompareProductInfo> ListTopProductsModuleProducts()
        {
            List<CompareProductInfo> topProductsModuleProductsList = new List<CompareProductInfo>()
            {
                new CompareProductInfo()
                {
                    ProductId = 107,
                    ProductName = "iMac",
                    ProductAvailability = "Out Of Stock"
                },
                new CompareProductInfo()
                {
                    ProductId = 106,
                    ProductName = "iMac",
                    ProductAvailability = "Out Of Stock"
                },
                new CompareProductInfo()
                {
                    ProductId = 105,
                    ProductName = "iMac",
                    ProductAvailability = "In Stock"
                },
                new CompareProductInfo()
                {
                    ProductId = 104,
                    ProductName = "iMac",
                    ProductAvailability = "In Stock"
                },
                new CompareProductInfo()
                {
                    ProductId = 103,
                    ProductName = "HTC Touch HD",
                    ProductAvailability = "In Stock"
                },
                new CompareProductInfo()
                {
                    ProductId = 102,
                    ProductName = "HTC Touch HD",
                    ProductAvailability = "In Stock"
                },
                new CompareProductInfo()
                {
                    ProductId = 101,
                    ProductName = "HTC Touch HD",
                    ProductAvailability = "In Stock"
                },
                new CompareProductInfo()
                {
                    ProductId = 100,
                    ProductName = "HTC Touch HD",
                    ProductAvailability = "In Stock"
                },
                new CompareProductInfo()
                {
                    ProductId = 99,
                    ProductName = "HP LP3065",
                    ProductAvailability = "In Stock"
                }
            };
            return topProductsModuleProductsList;
        }

        public static List<CompareProductInfo> ListTopCollectionProducts()
        {
            List<CompareProductInfo> productList = new List<CompareProductInfo>()
        {
           new CompareProductInfo()
            {
                ProductId = 28,
                ProductName = "HTC Touch HD",
                ProductAvailability = "In Stock"
            },
             new CompareProductInfo()
            {
                ProductId = 29,
                ProductName = "Palm Treo Pro",
                ProductAvailability = "Out Of Stock"
            },
             new CompareProductInfo()
            {
                ProductId = 42,
                ProductName = "Apple Cinema 30\"",
                ProductAvailability = "In Stock"
            },
              new CompareProductInfo()
            {
                ProductId = 40,
                ProductName = "iPhone",
                ProductAvailability = "Out Of Stock"
            },
              new CompareProductInfo()
            {
                ProductId = 30,
                ProductName = "Canon EOS 5D",
                ProductAvailability = "In Stock"
            }

        };
            return productList;

        }
    }
}

