﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.ShopByCategorySection;
public partial class ShopByCategorySection : BaseSectionsPage
{
    public ShopByCategorySection(IWebDriver driver) : base(driver)
    {
    }

    public void OpenTopCategories()
    {
        SelectElement(ShopByCategoryLink);
    }
}
