﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.ShopByCategorySection;
public partial class ShopByCategorySection
{
    private IWebElement ShopByCategoryLink => WaitAndFindElement(By.Id("entry_217831"));
    private IWebElement TopCategoriesTitle => WaitAndFindElement(By.XPath("//h5[contains(text(),'Top categories')]"));

    public IWebElement GetTopCategoriesByLabel(string label)
    {
        return WaitAndFindElement(By.XPath($"//li[@class='nav-item']//span[contains(text(), '{label}')]"));
    }
}
