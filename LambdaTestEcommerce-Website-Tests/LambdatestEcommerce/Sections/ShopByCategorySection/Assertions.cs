﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.ShopByCategorySection;
public partial class ShopByCategorySection
{
    public void AssertTopCategoriesTitle()
    {
        Assert.AreEqual("Top categories", TopCategoriesTitle.Text.Trim());
    }
}
