﻿namespace LambdatestEcommerce.Sections.BreadcrumbSection;
public partial class BreadcrumbSection
{
    public void AssertPageUrl(string endPoint)
    {
        string expectedUrl = "https://ecommerce-playground.lambdatest.io" + endPoint;
        Assert.AreEqual(expectedUrl, Driver.Url);
    }
}