﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.BreadcrumbSection;

public partial class BreadcrumbSection
{
    private IWebElement Breadcrumb => WaitAndFindElement(By.ClassName("breadcrumb"));
}
