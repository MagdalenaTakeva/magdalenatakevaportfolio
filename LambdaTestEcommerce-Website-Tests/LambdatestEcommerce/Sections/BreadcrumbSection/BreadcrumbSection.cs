﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.BreadcrumbSection;
public partial class BreadcrumbSection : BaseSectionsPage
{
    public BreadcrumbSection(IWebDriver driver) : base(driver)
    {
    }

    public void OpenBreadcrumbItem(string itemToOpen)
    {
        Breadcrumb.FindElement(By.LinkText(itemToOpen)).Click();
    }
}