﻿namespace LambdatestEcommerce.Sections.QuickViewSection
{
    public partial class QuickViewSection
    {
        public void AssertProductName(string productName)
        {
            Assert.AreEqual(productName, ProductName.Text);
        }

        public void AssertProductAvailability(string availability)
        {
            Assert.AreEqual(availability, Availability.Text);
        }

        public void AssertNotificationLoginButtonDisplayed()
        {
            Assert.True(LoginButton.Displayed);
        }

        public void AssertNotificationRegisterButtonDisplayed()
        {
            Assert.True(RegisterButton.Displayed);
        }

        public void AssertBuyNowButtonDisabled()
        {
            Assert.IsFalse(BuyNowButton.Enabled);
        }

        public void AssertAddToCartButtonDisabled()
        {
            Assert.IsFalse(AddToCartButton.Enabled);
        }
    }
}
