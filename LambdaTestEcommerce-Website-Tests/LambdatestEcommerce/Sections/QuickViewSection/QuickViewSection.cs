﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.QuickViewSection
{
    public partial class QuickViewSection : BaseSectionsPage
    {
        public QuickViewSection(IWebDriver driver) : base(driver)
        {
        }

        public void AddProductToCart()
        {
            SelectElement(AddToCartButton);
        }

        public void BuyNowProduct()
        {
            SelectElement(BuyNowButton);
        }

        public void IncreaseProductQuantity(int startQty, int endQty)
        {
            for (int i = 1; i < endQty; i++)
            {
                SelectElement(IncreaseQuantity);
            }
        }

        public void DecreaseProductQuantity(int startQty, int endQty)
        {
            for (int i = startQty; i > endQty; i--)
            {
                SelectElement(DecreaseQuantity);
            }
        }

        public void AddProductToCompareList()
        {
            SelectElement(CompareThisProductButton);
        }

        public void AddProductToWishList()
        {
            SelectElement(AddToWishList);
        }

        public void CloseLoginOrRegisterNotification()
        {
            SelectElement(CloseLoginNotification);  
        }

        public void ClickNotificationLoginButton()
        {
            SelectElement(LoginButton);
        }

        public void ClickNotificationRegisterButton()
        {
            SelectElement(RegisterButton);
        }
    }
}
