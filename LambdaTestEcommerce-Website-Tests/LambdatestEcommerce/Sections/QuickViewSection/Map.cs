﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.QuickViewSection
{
    public partial class QuickViewSection
    {
        private IWebElement AddToCartButton => WaitAndFindElement(By.XPath("//div[@id='entry_212964']/button"));
        private IWebElement BuyNowButton => WaitAndFindElement(By.XPath("//div[@id='entry_212965']/button"));

        private IWebElement CompareThisProductButton => WaitAndFindElement(By.XPath("//div[@id='entry_212966']/button"));
        private IWebElement Quantity => WaitAndFindElement(By.XPath("//input[@name='quantity']"));
        private IWebElement IncreaseQuantity => WaitAndFindElement(By.XPath("//button[@aria-label='Increase quantity']"));
        private IWebElement DecreaseQuantity => WaitAndFindElement(By.XPath("//button[@aria-label='Decrease quantity']"));
        private IWebElement ProductName => WaitAndFindElement(By.TagName("h1"));
        private IWebElement Brand => WaitAndFindElement(By.XPath("//li[.//text()='Brand:']/a"));
        private IWebElement ProductCode => WaitAndFindElement(By.XPath("//span[.//text()='Product 14']"));
        private IWebElement Availability => WaitAndFindElement(By.XPath("//span[@class = 'badge badge-success']"));
        private IWebElement Price => WaitAndFindElement(By.XPath("//div[@class='price']/span"));
        private IWebElement AddToWishList => WaitAndFindElement(By.XPath("//div[@class='image-thumb d-flex']/button[@title='Add to Wish List']"));
        private IWebElement LargeImage => WaitAndFindElement(By.XPath("//div[@class='image-thumb d-flex']/a"));
        private IReadOnlyCollection<IWebElement> AdditionalImages => WaitAndFindElements(By.XPath("//div[@class='image-additional']/descendant::a"));
        private IWebElement VerticalPreviousSlide => WaitAndFindElement(By.XPath("//div[@aria-label='Previous slide'][1]"));
        private IWebElement HorizontalPreviousSlide => WaitAndFindElement(By.XPath("//div[@aria-label='Previous slide'][2]"));
        private IWebElement VerticalNextSlide => WaitAndFindElement(By.XPath("//div[@aria-label='Next slide'][1]"));
        private IWebElement HorizontalNextSlide => WaitAndFindElement(By.XPath("//div[@aria-label='Next slide'][2]"));
        private IWebElement Close => WaitAndFindElement(By.XPath("//button[@aria-label='close']"));

        // Login Notification

        private IWebElement CloseLoginNotification => WaitAndFindElement(By.XPath("//button[@aria-label = 'Close']/span"));
        private IWebElement LoginButton => WaitAndFindElement(By.XPath("//a[contains(@class, ' btn-danger')]"));
        private IWebElement RegisterButton => WaitAndFindElement(By.XPath("//div[@class='col']/a[contains(@class, ' btn-secondary')]"));
    }
}
