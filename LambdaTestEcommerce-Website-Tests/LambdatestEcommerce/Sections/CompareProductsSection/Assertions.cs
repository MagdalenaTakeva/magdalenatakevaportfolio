﻿namespace LambdatestEcommerce.Sections.CompareProductsSection;
public partial class CompareProductsSection

{
    public void AssertPageUrl()
    {
        string expectedUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=product/compare";
        Assert.AreEqual(expectedUrl, Driver.Url);
    }
}
