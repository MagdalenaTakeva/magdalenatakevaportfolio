﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.CompareProductsSection;
public partial class CompareProductsSection : BaseSectionsPage
{
    public CompareProductsSection(IWebDriver driver) : base(driver)
    {
    }

    public void OpenCompareProductsPage()
    {
        SelectElement(CompareProductsIcon); 
    }

    public void ClickNotificationProductCompareButton()
    {
        SelectElement(ProductCompareButton);
    }

    public void CloseCompareProductNotification()
    {
        SelectElement(CloseProductCompareNotification);
    }
}
