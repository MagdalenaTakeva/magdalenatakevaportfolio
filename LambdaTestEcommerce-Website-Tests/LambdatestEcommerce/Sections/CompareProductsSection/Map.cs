﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.CompareProductsSection;
public partial class CompareProductsSection
{
    private IWebElement CompareProductsIcon => WaitAndFindElement(By.Id("entry_217823"));

    private IWebElement ProductCompareButton => WaitAndFindElement(By.XPath("//a[text()[contains(.,'Product Compare')]]"));

    private IWebElement CloseProductCompareNotification => WaitAndFindElement(By.XPath("//button[@aria-label = 'Close']/span"));

    public IWebElement GetAddToCartButtonByProductId(int productId)
    {
        return WaitAndFindElement(By.XPath($"//button[contains(@class,'compare-{productId}')]"));
    }
}
