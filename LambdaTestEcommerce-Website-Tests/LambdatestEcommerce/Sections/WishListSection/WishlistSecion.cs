﻿using LambdatestEcommerce.Models;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.WishListSection;
public partial class WishlistSecion : BaseSectionsPage
{
    public WishlistSecion(IWebDriver driver) : base(driver)
    {
    }

    public void OpenWishlist()
    {
        SelectElement(HeartIcon);
    }

    public void AddProductToCart(Product product)
    {
        SelectElement(GetAddToCartButtonByProductId(product.ProductId));
    }

    public void ViewProductsInWishList()
    {
        SelectElement(ViewWishList);
    }

    public void CloseProductNotification()
    {
        SelectElement(CloseNotification);
    }

    public void ClickAddToCartButton(Product product)
    {
        if (product is null)
        {
            throw new ArgumentNullException(nameof(product));
        }

        SelectElement(GetAddToCartButtonByProductId(product.ProductId));
    }

    public void ClickRemoveButton()
    {
        SelectElement(RemoveButton);
    }

    public void DeleteAllProductsInWishList()
    {
        if (RemoveButtons.Count == 0) { return; }

        foreach (var removeButton in RemoveButtons)
        {
            SelectElement(removeButton);
        }
    }

    public void PressContinueButton()
    {
        SelectElement(ContinueButton);
    }
}
