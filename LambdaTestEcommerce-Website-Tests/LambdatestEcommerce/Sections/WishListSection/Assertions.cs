﻿using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Sections.WishListSection;
public partial class WishlistSecion
{
    public void AssertPageUrl()
    {
        var expectedUrl = base.Url + "/index.php?route=account/wishlist";
        Assert.AreEqual(expectedUrl, Driver.Url);
    }

    public void AssertPageTitle()
    {
        Assert.AreEqual("My Wish List", Driver.Title);
    }

    public void AssertDefaultWishListProductDetails(List<WishListInfo> wishList)
    {
        for (var i = 2; i <= TrCollection.Count; i++)
        {
            var productName = GetProductDetails(i, 2).Text;
            var productModel = GetProductDetails(i, 3).Text;
            var stock = GetProductDetails(i, 4).Text;
            var unitPrice = GetProductDetails(i, 5).Text.Trim();

            Assert.AreEqual(wishList[i-2].ProductName, productName);
            Assert.AreEqual(wishList[i-2].Model, productModel);
            Assert.AreEqual(wishList[i - 2].Stock, stock);
            Assert.AreEqual(wishList[i - 2].UnitPrice,unitPrice);

        }
    }

    public void AssertDetailsOfProductAddedToWishlist(List<WishListInfo> wishList)
    {
        for (var i = 2; i <= TrCollection.Count; i++)
        {
            var productName = GetProductDetails(i, 2).Text;
            var productModel = GetProductDetails(i, 3).Text;
            var stock = GetProductDetails(i, 4).Text;
            var unitPrice = GetProductDetails(i, 5).Text.Trim();

            Assert.AreEqual(wishList[i - 2].ProductName, productName);
            Assert.AreEqual(wishList[i - 2].Model, productModel);
            Assert.AreEqual(wishList[i - 2].Stock, stock);
            Assert.AreEqual(wishList[i - 2].UnitPrice, unitPrice);

        }
    }

    public void AssertWishListModifiedSuccessfully()
    {
        Assert.AreEqual("Success: You have modified your wish list!", AlertSuccessMessage().Text.Replace("\r\n×", ""));
    }

    public void AssertNumberOfProductsAsExpected(int expectedNumber)
    {
        int actualNumber = Products.Count;
        if (actualNumber == 0)
        {
            actualNumber = 0;
        }

        Assert.AreEqual(expectedNumber, actualNumber);
    }
}
