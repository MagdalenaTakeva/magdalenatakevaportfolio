﻿using LambdatestEcommerce.Models;
using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.WishListSection;
public partial class WishlistSecion
{
    private IWebElement HeartIcon => WaitAndFindElement(By.Id("entry_217824"));
    private IWebElement WishListPageTitle => WaitAndFindElement(By.TagName("h1"));
    private IWebElement ViewWishList => WaitAndFindElement(By.XPath("//a[contains(.,'Wish List')]"));
    private IWebElement CloseNotification => WaitAndFindElement(By.XPath("(//button[@data-dismiss = 'toast']//span)[1]"));
    public IWebElement RemoveButton => WaitAndFindElement(By.XPath("//*[@class ='btn btn-light btn-sm text-danger']"));
    public IWebElement AddToCartButton => WaitAndFindElement(By.XPath("//*[@class='btn btn-light btn-sm']"));
    public IWebElement NoResultsText => FindElement(By.XPath("//p[text() = 'No results!']"));
    public IWebElement ContinueButton => FindElement(By.XPath("//*[@class = 'btn btn-primary']"));

    private IReadOnlyCollection<IWebElement> Products => FindElements(By.XPath("//td[@class='text-center']/a[contains(@href, 'product/product')]"));



    public IWebElement GetProductByLabel(string productName)
    {
        return WaitAndFindElement(By.XPath($"//td[@class='text-left']/a [contains(text(), '{productName}')]"));
    }

    public IWebElement GetProductDetails(int row, int col)
    {
        return WaitAndFindElement(By.XPath($"//table[@class='table table-hover border']/descendant::tr[{row}]/td[{col}]"));
    }

    public IWebElement GetAddToCartButtonByProductId(int productId)
    {
        return WaitAndFindElement(By.XPath($"//button[contains(@onclick,\"cart.add('{productId}');\")]"));
    }

    

    public IWebElement TableResponsive => WaitAndFindElement(By.XPath("//table[@class= 'table table-hover border']"));
    public IReadOnlyList<IWebElement> TrCollection => TableResponsive.FindElements(By.TagName("tr"));
    public IReadOnlyList<IWebElement> TdCollection => TableResponsive.FindElements(By.TagName("td"));
    public IReadOnlyCollection<IWebElement> RemoveButtons => FindElements(By.XPath("//*[@class='btn btn-light btn-sm text-danger']"));
    public IReadOnlyCollection<IWebElement> AddToCartButtons => FindElements(By.XPath("//*[@class='btn btn-light btn-sm']"));

    public IWebElement AlertSuccessMessage()
    {
        return WaitAndFindElement(By.XPath($"//div[contains(@class, 'alert-success')]"));
    }
}
