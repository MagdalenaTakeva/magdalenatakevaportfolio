﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.CartInfoSection;
public partial class CartInfoSection : BaseSectionsPage
{
    public CartInfoSection(IWebDriver driver) : base(driver)
    {
    }

public string GetCurrentAmount()
    {
        return CartAmount.Text;
    }

    public void OpenCart()
    {
        ScrollToElement(CartIcon);
        WaitForElementToBeClickable(CartIcon);
        CartIcon.Click();
        WaitForAjax();
    }

    public void EditCartFromCartIcon()
    {
        ScrollToElement(EditCartButton);
        WaitForElementToBeClickable(EditCartButton);
        EditCartButton.Click();
    }

    public void ProceedToCheckout()
    {
        ScrollToElement(CheckoutButton);
        WaitForElementToBeClickable(CheckoutButton);
        CheckoutButton.Click();
    }

    public void ViewCart()
    {
        WaitForElementToBeClickable(ViewCartButton);
        ViewCartButton.Click();
    }

    public void CloseProductNotification()
    {
        WaitForElementToBeClickable(CloseNotification);
        CloseNotification.Click();
    }

    public void CloseCart()
    {
        WaitForElementToBeClickable(Close);
        Close.Click();
    }
}
