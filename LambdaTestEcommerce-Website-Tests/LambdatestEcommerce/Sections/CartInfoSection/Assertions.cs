﻿namespace LambdatestEcommerce.Sections.CartInfoSection;
public partial class CartInfoSection
{
    public void AssertCartCurrentAmount(string currentAmount)
    {
        Assert.AreEqual(currentAmount, CartAmount.Text);
    }

    public void AssertCartCurrentAmount(IDictionary<string, int> productNamesList)
    {
        Assert.AreEqual(productNamesList.Sum(x => x.Value).ToString(), CartAmount.Text);
    }

    public void AssertCartTitle()
    {
        Assert.AreEqual("Cart", CartTitle.Text);
    }

    public void AssertOrderSubtotalDisplayed()
    {
        Assert.True(SubTotal.Displayed);
    }

    public void AssertOrderEcoTaxDisplayed()
    {
        Assert.True(EcoTax.Displayed);
    }

    public void AssertOrderVatTaxDisplayed()
    {
        Assert.True(VatTax.Displayed);
    }

    public void AssertOrderTotalDisplayed()
    {
        Assert.True(Total.Displayed);
    }

    public void AssertOrderSubtotal(string expectedSubTotal)
    {
        Assert.AreEqual(expectedSubTotal, SubTotal.Text);
    }

    public void AssertOrderEcoTax(string expectedEcoTax)
    {
        Assert.AreEqual(expectedEcoTax, EcoTax.Text);
    }

    public void AssertOrderVatTax(string expectedVatTax)
    {
        Assert.AreEqual(expectedVatTax, VatTax.Text);
    }

    public void AssertOrderTotal(string expectedTotal)
    {
        Assert.AreEqual(expectedTotal, Total.Text);
    }
}
