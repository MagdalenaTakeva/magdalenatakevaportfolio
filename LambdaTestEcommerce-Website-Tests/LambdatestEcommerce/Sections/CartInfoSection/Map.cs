﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.CartInfoSection;

public partial class CartInfoSection
{
    private IWebElement CartIcon => WaitAndFindElement(By.XPath("//*[@id='entry_217825']/a"));
    private IWebElement CartTitle => WaitAndFindElement(By.XPath("//div[@id='cart-total-drawer']/h5"));
    private IWebElement CartAmount => WaitAndFindElement(By.XPath("//*[@id=\"entry_217825\"]//span[contains(@class, 'cart-item-total')]"));
    private IWebElement EditCartButton => WaitAndFindElement(By.XPath("//*[@id='entry_217850']/a"));
    private IWebElement CheckoutButton => WaitAndFindElement(By.XPath("//*[@id='entry_217851']/a"));
    private IWebElement Close => WaitAndFindElement(By.XPath("//h5[text()='Cart ']/a[@aria-label = 'close']"));
    private IWebElement ViewCartButton => WaitAndFindElement(By.XPath("//a[text()='View Cart ']"));
    private IWebElement CloseNotification => WaitAndFindElement(By.XPath("(//button[@data-dismiss = 'toast']//span)[1]"));

    private IWebElement SubTotal => WaitAndFindElement(By.XPath("//tr[1]/td/strong"));
    private IWebElement EcoTax => WaitAndFindElement(By.XPath("//tr[2]/td/strong"));
    private IWebElement VatTax => WaitAndFindElement(By.XPath("//tr[3]/td/strong"));
    private IWebElement Total => WaitAndFindElement(By.XPath("//tr[4]/td/strong"));

    public IWebElement GetProductDetails(int row, int col)
    {
        return WaitAndFindElement(By.XPath($"//table[@class = 'table']//tr[{row}]/td[{col}]"));
    }

    public IWebElement GetPurchaseCostAndTaxesByLabels(string label)
    {
        //label can be: 'Sub-Total:, 'Eco Tax (-2.00):', 'VatTax (20%):', 'Total:'
        return WaitAndFindElement(By.XPath($"(//div[@id = 'entry_217847']//td[text()='{label}']/parent::tr//strong)[1]"));
    }
}
