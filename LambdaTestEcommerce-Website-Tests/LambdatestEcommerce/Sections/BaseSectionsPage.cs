﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Interactions;

namespace LambdatestEcommerce.Sections;
public class BaseSectionsPage
{
    private const int WAIT_FOR_ELEMENT_TIMEOUT = 30;
    Actions actions;

    public BaseSectionsPage(IWebDriver driver)
    {
        Driver = driver;
        WebDriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(WAIT_FOR_ELEMENT_TIMEOUT));
        actions = new Actions(driver);
    }

    protected virtual string Url => "https://ecommerce-playground.lambdatest.io";

    protected IWebDriver Driver { get; set; }
    protected WebDriverWait WebDriverWait { get; set; }

    protected IWebElement WaitAndFindElement(By locator)
    {
        return WebDriverWait.Until(ExpectedConditions.ElementExists(locator));
    }

    protected IReadOnlyCollection<IWebElement> WaitAndFindElements(By locator)
    {

        return WebDriverWait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(locator));
    }

    protected IReadOnlyCollection<IWebElement> FindElements(By locator)
    {
        return Driver.FindElements(locator);
    }

    protected IWebElement FindElement(By locator)
    {
        return Driver.FindElement(locator);
    }

    protected void WaitForAjax()
    {
        var js = (IJavaScriptExecutor)Driver;
        WebDriverWait.Until(wd => js.ExecuteScript("return jQuery.active").ToString() == "0");
    }

    protected void WaitUntilPageLoadsCompletely()
    {
        var js = (IJavaScriptExecutor)Driver;
        WebDriverWait.Until(wd => js.ExecuteScript("return document.readyState").ToString() == "complete");
    }

    public void WaitForElementToBeClickable(IWebElement elementToClick)
    {
        WebDriverWait.Until(ExpectedConditions.ElementToBeClickable(elementToClick));
    }

    public void WaitForElementToBeVisible(By by)
    {
        WebDriverWait.Until(ExpectedConditions.ElementIsVisible(by));
    }

    public void HoverOverElement(IWebElement elementToBeHovered)
    {
        actions.MoveToElement(elementToBeHovered).Perform();
    }

    public void ScrollToElement(IWebElement elementToScrollTo)
    {
        ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", elementToScrollTo);
    }

    public void FocusControlElement(IWebElement elementToFocusOn)
    {
        ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].focus();", elementToFocusOn);
    }

    public void WaitForElementToExist(By by)
    {
        WebDriverWait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
    }

    public void SelectElement(IWebElement element)
    {
        ScrollToElement(element);
        WaitForElementToBeClickable(element);
        element.Click();
        WaitForAjax();
    }
}
