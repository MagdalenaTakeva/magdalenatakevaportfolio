﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.SearchSection;
public partial class SearchSection
{
    private IWebElement SearchField => WaitAndFindElement(By.XPath("//input[@aria-label='Search For Products']"));
    private IWebElement SearchButton => WaitAndFindElement(By.XPath("//button[text()='Search']"));
    public IWebElement AllCategoriesButton => WaitAndFindElement(By.XPath("//div[@id='entry_217822']//button[text()='All Categories']"));
    public IWebElement GetSearchCategeoryByName(string categoryName)
    {
        return Driver.FindElement(By.XPath($"//div[@id='entry_217822']//a[text()='{categoryName}']"));
    }
}