﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.SearchSection;
public partial class SearchSection : BaseSectionsPage
{
    public SearchSection(IWebDriver driver) : base(driver)
    {
    }

    public void SearchForItem(string searchText)
    {
        SearchField.SendKeys(searchText);
        ClickSearchButton();
    }

    public void SelectCategoryToSearch(string categoryName)
    {
        SelectElement(AllCategoriesButton);
        SelectElement(GetSearchCategeoryByName(categoryName));
        ClickSearchButton();
    }

    public void SelectCategoryAndProductToSearch(string categoryName, string productName)
    {
        SelectElement(AllCategoriesButton);
        SelectElement(GetSearchCategeoryByName(categoryName));
        SearchField.SendKeys(productName);
        ClickSearchButton();
    }

    public void ClickSearchButton()
    {
        SelectElement(SearchButton);
    }
}
