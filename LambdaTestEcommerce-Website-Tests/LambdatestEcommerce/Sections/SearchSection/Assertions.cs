﻿using LambdatestEcommerce.Models;

namespace LambdatestEcommerce.Sections.SearchSection;
public partial class SearchSection
{
    public void AssertPageUrl(string endPoint)
    {
        string expectedUrl = "https://ecommerce-playground.lambdatest.io" + endPoint;
        Assert.AreEqual(expectedUrl, Driver.Url);
    }

    public void AssertPageUrl(Product product)
    {
       
        Assert.AreEqual(product.ProductUrl, Driver.Url);
    }
}