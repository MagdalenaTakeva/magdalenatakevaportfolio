﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.MainMenuSection;
public partial class MainMenuSection : BaseSectionsPage
{

    public MainMenuSection(IWebDriver driver) : base(driver)
    {
    }

    public void OpenHomePage()
    {
        SelectElement(HomeLink);
    }

    public void OpenSpecialOffersPage()
    {
        SelectElement(SpecialLink);
    }

    public void OpenBlogPage()
    {
        SelectElement(BlogLink);
    }

    public void OpenMegaMenuPage()
    {
        SelectElement(MegaMenu);
    }

    public void OpenMyAccountPage()
    {
        SelectElement(MyAccount);
    }

    public void OpenMyVoucherPage()
    {
        HoverOverElement(MyAccount);
        WaitForElementToBeClickable(MyAccountMyVoucher);
        GetDropdownMenuOptionBylabel("My voucher").Click();
    }

    public void LoginFromMyAccountMenu()
    {
        HoverOverElement(MyAccount);
        WaitForElementToBeClickable(MyAccountLogin);
        GetDropdownMenuOptionBylabel("Login").Click();
    }

    public void LogoutFromMyAccountMenu()
    {
        HoverOverElement(MyAccount);
        WaitForElementToBeClickable(MyAccountLogout);
        GetDropdownMenuOptionBylabel("Logout").Click();
    }

    public void OpenTrackingPageFromMyAccountMenu()
    {
        HoverOverElement(MyAccount);
        WaitForElementToBeClickable(MyAccountTracking);
        GetDropdownMenuOptionBylabel("Tracking").Click();
    }

    public void OpenProductReturnsPageFromMyAccountMenu()
    {
        HoverOverElement(MyAccount);
        WaitForElementToBeClickable(MyAccountReturn);
        GetDropdownMenuOptionBylabel("Return").Click();
    }

    public void OpenOrderHistoryPageFromMyAccountMenu()
    {
        HoverOverElement(MyAccount);
        WaitForElementToBeClickable(MyAccountMyOrder);
        GetDropdownMenuOptionBylabel("My order").Click();
    }

    public void OpenDashboardFromMyAccountMenu()
    {
        HoverOverElement(MyAccount);
        WaitForElementToBeClickable(MyAccountDashboard);
        GetDropdownMenuOptionBylabel("Dashboard").Click();
    }

    public void OpenModulesPageFromAddOnsMenu()
    {
        HoverOverElement(AddOns);
        WaitForElementToBeClickable(AddOnsModules);
        GetDropdownMenuOptionBylabel("Modules").Click();
    }

    public void OpenDesignsPageFromAddOnsMenu()
    {
        HoverOverElement(AddOns);
        WaitForElementToBeClickable(AddOnsDesigns);
        GetDropdownMenuOptionBylabel("Designs").Click();
    }

    public void OpenWidgetsPageFromAddOnsMenu()
    {
        HoverOverElement(AddOns);
        WaitForElementToBeClickable(AddOnsWidgets);
        GetDropdownMenuOptionBylabel("Widgets").Click();
    }
}
