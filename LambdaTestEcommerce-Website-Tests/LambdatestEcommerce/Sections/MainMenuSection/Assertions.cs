﻿namespace LambdatestEcommerce.Sections.MainMenuSection;

public partial class MainMenuSection
{
    public void AssertPageUrl(string endPoint)
    {
        string expectedUrl = "https://ecommerce-playground.lambdatest.io" + endPoint;
        Assert.AreEqual(expectedUrl, Driver.Url);
    }

    public void AssertPageUrl()
    {
        string expectedUrl = "https://ecommerce-playground.lambdatest.io/index.php?route=common/home";
        Assert.AreEqual(expectedUrl, Driver.Url);
    }
}
