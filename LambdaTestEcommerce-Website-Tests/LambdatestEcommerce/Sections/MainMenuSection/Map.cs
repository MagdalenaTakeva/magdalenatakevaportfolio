﻿using OpenQA.Selenium;

namespace LambdatestEcommerce.Sections.MainMenuSection;

public partial class MainMenuSection
{
    public IWebElement HomeLink => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Home')]"));
    public IWebElement SpecialLink => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Special')]"));
    public IWebElement BlogLink => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Blog')]"));
    public IWebElement MegaMenu => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Mega Menu')]"));

    public IWebElement AddOns => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'AddOns')]"));
    public IWebElement AddOnsModules => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Modules')]"));
    public IWebElement AddOnsDesigns => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Designs')]"));
    public IWebElement AddOnsWidgets => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Widgets')]"));
    public IWebElement MyAccount => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'My account')]"));
    public IWebElement MyAccountMyVoucher => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'My voucher')]"));
    public IWebElement MyAccountLogout => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Logout')]"));
    public IWebElement MyAccountLogin => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Login')]"));
    public IWebElement MyAccountTracking => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Tracking')]"));
    public IWebElement MyAccountDashboard => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Dashboard')]"));
    public IWebElement MyAccountMyOrder => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'My order')]"));
    public IWebElement MyAccountReturn => WaitAndFindElement(By.XPath("//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), 'Return')]"));

    public IWebElement GetDropdownMenuOptionBylabel(string label)
    {
        return WaitAndFindElement(By.XPath($"//ul[contains(@class, 'horizontal')]/descendant::span[contains(text(), '{label}')]"));
    }
}
