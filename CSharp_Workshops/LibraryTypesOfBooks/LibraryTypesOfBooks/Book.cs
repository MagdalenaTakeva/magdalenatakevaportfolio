﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace LibraryTypesOfBooks
{
    public class Book
    {
        private string _title = string.Empty;
        private decimal _price = 0.01M;
        private string _author = string.Empty;
        private readonly Regex rxAuthor = new(@".+ [0-9].+");

        public Book(string title, string author, decimal price)
        {
            Title = title;
            Author = author;
            Price = price;
        }

       public string Title 
       { 
            get => _title; 
            set 
            { 
                if ( string.IsNullOrEmpty( value ) || value.Length < 3)
                {
                    throw new ArgumentException("Title not valid! Must be more than 3 characters!");
                }
                _title = value;
            } 
       }   

        public string Author
        {
            get => _author;
            set
            {
                if (rxAuthor.IsMatch(value)){
                    throw new ArgumentOutOfRangeException("Author not valid! Not allowed to start with number!");
                }

                if( string.IsNullOrEmpty( value ))
                {
                    throw new ArgumentException("Author not valid!");
                }
                _author = value;
            }
        }

        public virtual decimal Price
        {
            get => _price;
            set
            {
                if ( value <= 0M)
                {
                    throw new ArgumentOutOfRangeException("Price not valid! Cannot be zero or negative!");
                }
                _price = value;

            }
        }

        public override string ToString()
        {
            string result = $"Title = {Title}, Author = {Author}, " +
                $"Price = {Price.ToString("c02", CultureInfo.CreateSpecificCulture("en-US"))}";
            return result;
        }
    }
}
