﻿namespace LibraryTypesOfBooks
{
    public class GoldenEditionBook : Book
    {
        public GoldenEditionBook(string title, string author, decimal price) : base(title, author, price)
        {
        }

        public override decimal Price
        {
            get => base.Price;
            set
            {
                if (value <= 0M)
                {
                    throw new ArgumentException("Price not valid! Cannot be zero or negative!");
                }
                base.Price = value * 1.30M;
            }
        }
    }
}
