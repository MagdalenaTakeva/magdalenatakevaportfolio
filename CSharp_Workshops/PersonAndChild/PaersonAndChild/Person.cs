﻿namespace PersonAndChild
{
    public class Person: IEquatable<Person>, IComparable<Person>
    {
        private int _age;

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
            Id = Guid.NewGuid();
            
        }

        public string Name { get; set; }
        public virtual int Age { 
            get => _age; 
            set { _age = value >  0 ? value : throw new ArgumentException("The age cannot be " +
                "less than or equal to 0"); }
        }
        public Guid Id { get; set; }

        public int CompareTo(Person other)
        {
            return Age.CompareTo(other.Age);
        }

        public bool Equals(Person other)
        {
            return Id.Equals(other.Id);
        }

        public override string ToString()
        {
            return $"Model Name = {Name}, Age = {Age}";
        }
    }
}
