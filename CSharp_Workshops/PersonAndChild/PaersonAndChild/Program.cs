﻿
using PersonAndChild;

var person1 = new Person("George", 20);
var person2 = new Child("Simona", 14);
var person3 = new Person("Tom", 20);
var person4 = person1;

Console.WriteLine($"Are they the same person? - {person1.Equals(person2)}");//False
Console.WriteLine($"Are they the same age? - {person1.CompareTo(person3)}");//0 - Yes
Console.WriteLine($"Are they the same person? - {person1.Equals(person4)}");//True