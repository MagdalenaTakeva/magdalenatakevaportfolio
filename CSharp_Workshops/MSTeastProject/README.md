# MSTest Project
## Task
Write 3 classes: Student, Course and School
1. Student should have a name and a unique number (inside the entire school) should be between 10000 and 99999
2. Each course should contain a set of students
3. Students in course should be less than 30 and should be able to join and leave courses
4. Name cannot be empty, and the unique number
5. Create MSTest project and create tests for the Student, Course and School classes

## Solution Description
Student class implements the IEquatable<Student> interface, which allows for comparing of two Student instances for equality based on their Id.
The class also provides two static methods: CreateListOfStudents() and RandomValidNumberForStudentId(). CreateListOfStudents() generates a list of Student instances with a specified count, while RandomValidNumberForStudentId() returnsa random valid unique Id for a student.

Course class has 2 properties: Name and StudentsList. The Name ensures that it cannot be empty, while StudentsList ensures that a course cannot have more than 29 students. The class has methods to add and remove student and to check if a student is enrolled in a course ContainsStudent(). IEquatable<Course> interface is implemented so that we can compare two Course instances for equality based on their name. The class provides also a static method CreateListOfCourses() that generates a liist of Courrse instances with a specified count.

School class has 3 proprty names: name, a list of students and a list of courses. It makes sure that a name cannot be empty and that each student list has a unique Id.
The class provides several constructor overloads for different combinations of name, list of students and list of courses. The class has methods to add/ remove courses and add/remove students, and a method that ensures that students' list does not contain duplicate Ids, CheckStudentsListForSameId(). In addition, the class has one more method ContainsStudent() that checks if a student is already enrolled in the school.