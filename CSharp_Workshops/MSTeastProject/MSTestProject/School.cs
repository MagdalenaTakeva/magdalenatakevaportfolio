﻿using MSTestProject;

namespace MSTestProject
{
    public class School
    {
        //School class has 3 properties: name, a list of students and a list of courses (Courses)
        private string _name;
        private List<Student> _students = new List<Student> { };
        private readonly Exception invalidInput = new ArgumentOutOfRangeException("Invalid input");


        // Constructor overloads for different combinations of names, students and courses
        public School(string name) : this(name, new List<Course> { }, new List<Student> { })
        {

        }

        public School(string name, List<Course> coursesInSchool) : 
            this(name, coursesInSchool, new List<Student> { })
        {

        }

        public School(List<Student> studentsInSchool, string name) : 
            this(name, new List<Course> { }, studentsInSchool)
        {

        }
        public School(string name, List<Course> coursesInSchool, List<Student> studentsInSchool)
        {
            Name = name;
            Courses = coursesInSchool;
            Students = studentsInSchool;
        }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw invalidInput;
                }
                else
                {
                    _name = value;
                }
            }
        }

        // Ensures that each student has unique Id
        public List<Student> Students
        {
            get => _students;
            set
            {
                if (CheckStudentsListForSameId(value))
                {
                    throw invalidInput;
                }
                else
                {
                    _students = value;
                }
            }
        }

        public List<Course> Courses { get; set; } = new List<Course>() { };

        public void AddCourse(Course course)
        {
            Courses.Add(course);
        }

        public void RemoveCourse(Course course)
        {
            Courses.Remove(course);
        }

        public void AddStudent(Student student)
        {
            if (!ContainsStudent(student))
            {
                Students.Add(student);
            }
        }

        public void RemoveStudent(Student student)
        {
            if (ContainsStudent(student))
            {
                Students.Remove(student);
            }
        }

        private bool CheckStudentsListForSameId(List<Student> students)
        {
            bool flag = false;
            var ids = new HashSet<int>();
            foreach (var student in students)
            {
                if (ids.Contains(student.Id))
                {
                    flag = true;
                    break;
                }
                ids.Add(student.Id);
            }
            return flag;
        }

        public bool ContainsStudent(Student student)
        {
            bool flag = false;
            foreach (var st in Students)
            {
                if (st.Equals(student))
                {
                    flag |= true;
                    break;
                }
            }
            return flag;
        }
    }
}
