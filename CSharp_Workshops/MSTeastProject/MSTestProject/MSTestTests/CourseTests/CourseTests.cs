﻿using MSTestProject;

namespace MSTeastProject.MSTestTests.CourseTests
{
    [TestClass]
    public class CourseTests
    {
        Random random = new Random();
        int studentId;

        [TestInitialize]
        public void SetUp()
        {
            studentId = Student.RandomValidNumberForStudentId(random);
        }

        [TestMethod]
        public void CourseCreatedSuccessfully_When_InitializedWithNameOnly()
        {
            var course = new Course("Java Fundamentals");

            Assert.IsNotNull(course);
        }

        [TestMethod]
        public void CourseCreatedSuccessfully_When_InitializedWithNameAndStudentList()
        {
            var expectedStudents = Student.CreateListOfStudents(2, studentId);
            var course = new Course("JAVA", expectedStudents);

            Assert.AreEqual(2, course.StudentsList.Count);
            Assert.AreEqual("JAVA", course.Name);
        }

        [TestMethod]
        public void ExceptionThrown_When_TryCreateCourseWithEmptyName()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Course(String.Empty));
        }

        [TestMethod]
        public void CourseReturnedItsName_When_CallIt()
        {
            var course = new Course("JAVA");

            Assert.AreEqual("JAVA", course.Name);
        }

        [TestMethod]
        public void CourseNameChanged_When_CourseNewNameAssigned()
        {
            var course = new Course("JAVA");
            course.Name = "JAVA Basics";

            Assert.AreEqual("JAVA Basics", course.Name);
        }

        [TestMethod]
        public void ExceptionThrown_When_TrySetCourseNameToBeEmpty()
        {
            var course = new Course("JAVA");

            Assert.ThrowsException<ArgumentOutOfRangeException>(() => course.Name = string.Empty);
        }

        [TestMethod]
        public void StudentsListReturned_When_CallForIt()
        {
            var expectedStudentsList = Student.CreateListOfStudents(2, studentId);
            var course = new Course("JAVA", expectedStudentsList);

            var actualStudentsList = course.StudentsList;

            Assert.AreEqual(expectedStudentsList, actualStudentsList);
        }

        [TestMethod]
        public void StudentsAddedToCourse_When_AddStudent()
        {
            var course = new Course("JAVA");
            var firstStudent = new Student(studentId, "Peter");
            var secondStudent = new Student(studentId + 1, "George");

            course.AddStudent(firstStudent);
            course.AddStudent(secondStudent);

            Assert.AreEqual(2, course.StudentsList.Count);
            Assert.IsTrue(course.StudentsList.Contains(firstStudent));
            Assert.IsTrue(course.StudentsList.Contains(secondStudent));
        }

        [TestMethod]
        public void StudentNotAdded_When_TryAddSameStudentTwice()
        {
            var inputListStudents = Student.CreateListOfStudents(2, studentId);
            var course = new Course("JAVA", inputListStudents);
            var firstStudent = course.StudentsList[0];

            course.AddStudent(new Student(firstStudent.Id, firstStudent.Name));

            Assert.AreEqual(2, course.StudentsList.Count);
        }

        [TestMethod]
        public void StudenNotAdded_When_TryAddExistingStudent()
        {
            var course = new Course("JAVA", Student.CreateListOfStudents(3, studentId, "Marina"));
            var student = course.StudentsList[1];

            course.AddStudent(student);

            Assert.AreEqual(3, course.StudentsList.Count);
        }

        [TestMethod]
        public void StudentNotAddedToCourse_When_TryToAdd30thStudentToCourse()
        {
            var course = new Course("JAVA", Student.CreateListOfStudents(29, studentId));

            course.AddStudent(new Student(20000, "Tomi"));

            Assert.AreEqual(29, course.StudentsList.Count);
        }

        [TestMethod]
        public void StudentRemovedSuccessfully_When_RemoveStudentById()
        {
            var expectedStudents = Student.CreateListOfStudents(2, studentId, "Peter");
            var course = new Course("JAVA", expectedStudents);
            var studentToBeRemoved = new Student(studentId, "Peter");

            course.RemoveStudent(studentToBeRemoved);

            Assert.AreEqual(1, course.StudentsList.Count);
            Assert.IsFalse(course.StudentsList.Contains(studentToBeRemoved));
        }

        [TestMethod]
        public void NothingHappened_When_TryToRemoveUnexistingStudentById()
        {
            var expectedStudents = Student.CreateListOfStudents(2, studentId);
            var course = new Course("C# Basics", expectedStudents);
            var unexistingStudent = new Student(studentId + 10, "Peter");

            course.RemoveStudent(unexistingStudent);

            Assert.AreEqual(2, course.StudentsList.Count);
            Assert.IsFalse(course.StudentsList.Contains(unexistingStudent));
        }

        [TestMethod]
        public void ThrowException_When_TryCreateCourseWith30Students()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Course("JAVA", Student.CreateListOfStudents(30)));
        }


    }
}
