﻿using MSTestProject;

namespace MSTeastProject.MSTestTests.StudentTests
{
    [TestClass]
    public class StudentTests
    {
        Random random = new Random();
        int studentId;

        [TestInitialize]
        public void SetUp()
        {
            studentId = Student.RandomValidNumberForStudentId(random);
        }

        [DataRow(10001)]
        [DataRow(34000)]
        [DataRow(99998)]
        [DataTestMethod]
        public void StudentCreated_When_InitializeStudent(int id)
        {
            var student = new Student(id, "Marina");

            Assert.IsNotNull(student);
        }

        [DataRow(10002)]
        [DataRow(10003)]

        [DataTestMethod]
        public void NewlyCreatedStudent_Is_InstanceOfStudent(int id)
        {
            var student = new Student(id, "Marina2");

            Assert.IsInstanceOfType(student, typeof(Student));
        }


        [DataRow(-50000)]
        [DataRow(500)]
        [DataRow(9999)]
        [DataRow(100000)]
        [DataRow(500000)]
        [DataTestMethod]
        public void ExceptionThrown_When_TryCreateStudentWithInvalidId(int id)
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Student(id, "Marina"));
        }

        [TestMethod]
        public void StudentNameReturned_When_Called()
        {
            var student = new Student(studentId, "Marina");

            Assert.AreEqual("Marina", student.Name);
        }

        [TestMethod]
        public void StudentNameChanged_When_AssignNewValue()
        {
            var student = new Student(studentId, "Marina");
            var newName = "George";

            student.Name = newName;

            Assert.AreEqual(newName, student.Name);
        }

        [TestMethod]
        public void StudentIdReturned_When_Called()
        {
            var student = new Student(studentId, "Marina");
            Assert.AreEqual(studentId, student.Id);
        }

        [TestMethod]
        public  void ExceptionThrown_When_TryCreateStudentWithEmptyName()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Student(studentId, string.Empty));
        }

        [TestMethod]
        public void StudentsAreEqual_When_OnlyTheirIdsAreEqual()
        {
            var firstStudent = new Student(studentId, "Peter");
            var secondStudent = new Student(studentId, "George");

            Assert.AreEqual(firstStudent, secondStudent);
        }

        [TestMethod]
        public void StudentsAreNotEqual_When_TheirIdsDiffer()
        {
            var firstStudent = new Student(studentId, "Peter");
            var secondStudent = new Student(studentId + 1, "Peter");

            Assert.AreNotEqual(firstStudent, secondStudent);
        }
    }
}
