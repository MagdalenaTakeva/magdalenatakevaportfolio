﻿using MSTestProject;

namespace MSTeastProject.MSTestTests.SchoolTests
{
    [TestClass]
    public class SchoolTests
    {
        public const string SCHOOL_NAME = "Ivan Vazov";

        [TestMethod]
        public void SchoolCreated_When_InitializedWithName()
        {
            var school = new School(SCHOOL_NAME);

            Assert.AreEqual(SCHOOL_NAME, school.Name);
        }

        [TestMethod]
        public void SchoolCreated_When_InitializedWithSchoolNameAndListOfCourses()
        {
            var expectedCourses = Course.CreateListOfCourses(2);
            var school = new School(SCHOOL_NAME, expectedCourses);

            Assert.IsNotNull(school);
            Assert.AreEqual(expectedCourses, school.Courses);
        }

        [TestMethod]
        public void SchoolCreated_When_InitializedWithStudentsListAndSchoolName()
        {
            var expectedStudents = Student.CreateListOfStudents(2);
            var school = new School(expectedStudents, SCHOOL_NAME);

            Assert.IsNotNull(school);
            Assert.AreEqual(expectedStudents, school.Students);
        }

        [TestMethod]
        public void SchoolCreated_When_InitializedWithSchoolNameStudentsListAndCoursesList()
        {
            var expectedCourses = Course.CreateListOfCourses(2);
            var expectedStudents = Student.CreateListOfStudents(2);
            var school = new School( SCHOOL_NAME, expectedCourses, expectedStudents );

            Assert.IsNotNull(school);
            Assert.AreEqual(expectedCourses, school.Courses);
            Assert.AreEqual(expectedStudents, school.Students);
        }

        [TestMethod]
        public void SchoolNameReturned_When_CallForIt()
        {
            var school = new School(SCHOOL_NAME);

            Assert.AreEqual("Ivan Vazov", school.Name);
        }

        [TestMethod]
        public void SchoolNameChanged_When_NewNameValueAssigned()
        {
            var school = new School(SCHOOL_NAME);

            school.Name = "New name";

            Assert.AreEqual("New name", school.Name);
        }

        [TestMethod]
        public void CourseAddedToSchool_When_AddCourse()
        {
            var school = new School(SCHOOL_NAME, Course.CreateListOfCourses(2, "JAVA Fundamentals"), Student.CreateListOfStudents(2));
            var coursesToBeAdded = Course.CreateListOfCourses(2, "Advanced");

            for(var i = 0; i < coursesToBeAdded.Count; i++)
            {
                school.AddCourse(coursesToBeAdded[i]);
            }

            Assert.AreEqual(4, school.Courses.Count);
            Assert.IsTrue(coursesToBeAdded.All( c => school.Courses.Contains(c)));
        }

        [TestMethod]
        public void CourseRemovedSuccessfully_When_RemoveCourse()
        {
            var school = new School(SCHOOL_NAME, Course.CreateListOfCourses(2, "JAVA Fundamentals"), Student.CreateListOfStudents(2));
            var courseTobeRemoved = school.Courses[0];
            
            school.RemoveCourse(courseTobeRemoved);

            Assert.AreEqual(1, school.Courses.Count);
            Assert.AreEqual("JAVA Fundamentals N: 2", school.Courses[0].Name);
        }

        [TestMethod]
        public void StudentAddedToSchool_When_AddStudent()
        {
            var school = new School(SCHOOL_NAME, Course.CreateListOfCourses(2, "Fundamentals"));
            var newStudent = new Student(10003, "Peter");

            school.AddStudent(newStudent);

            Assert.AreEqual(1, school.Students.Count);
            Assert.IsTrue(school.Students.Contains(newStudent));
        }

        [TestMethod]
        public void NothingHappened_When_TryAddExistingStudent()
        {
            var school = new School(SCHOOL_NAME, Course.CreateListOfCourses(2, "Fundamentals"), Student.CreateListOfStudents(1));
            var existingStudent = school.Students[0];

            school.AddStudent(existingStudent);

            Assert.AreEqual(1, school.Students.Count);
            Assert.IsTrue(school.Students.Contains(existingStudent));

        }

        [TestMethod]
        public void StudentRemovedFromSchool_When_RemoveStudent()
        {
            var school = new School(SCHOOL_NAME, Course.CreateListOfCourses(2, "Fundamentals"), Student.CreateListOfStudents(2));
            var studentToBeRemoved = school.Students[0];

            school.RemoveStudent(studentToBeRemoved);

            Assert.AreEqual(1, school.Students.Count());
        }

        [TestMethod]
        public void NothingHappened_When_TryRemoveNonExistingStudent()
        {
            var school = new School(SCHOOL_NAME, Course.CreateListOfCourses(2, "Fundamentals"), Student.CreateListOfStudents(2));
            var notExistingStudent = new Student(23000, "Marina");
            school.RemoveStudent(notExistingStudent);

            Assert.AreEqual(2, school.Students.Count());
            Assert.IsFalse(school.Students.Contains(notExistingStudent));
        }
    }
}
