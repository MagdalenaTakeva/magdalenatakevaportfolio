﻿using MSTestProject;

namespace MSTestProject
{
    public class Course : IEquatable<Course>
    {
        private string _name;
        private List<Student> _studentsList = new List<Student> { };
        private readonly Exception invalidInput = new ArgumentOutOfRangeException("Invalid input");

        public Course(string name) : this(name, new List<Student> { })
        {

        }

        public Course(string name, List<Student> students)
        {
            Name = name;
            StudentsList = students;
        }

        public string Name
        {
            get => _name;
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _name = value;
                }
                else
                {
                    throw invalidInput;
                }
            }
        }

        public List<Student> StudentsList
        {
            get => _studentsList;
            private protected set
            {
                if (value.Count <= 29)
                {
                    _studentsList = value;
                }
                else
                {
                    throw invalidInput;
                }
            }
        }

        public void AddStudent(Student student)
        {
            if (!ContainsStudent(student) && this.StudentsList.Count < 29)
            {
                this.StudentsList.Add(student);
            }
        }

        public void RemoveStudent(Student student)
        {
            if (ContainsStudent(student))
            {
                this.StudentsList.Remove(student);
            }
        }

        public void RemoveStudent(int id)
        {
            if (ContainsStudent(id))
            {
                for (int i = 0; i < this.StudentsList.Count; i++)
                {
                    if (this.StudentsList[i].Id == id)
                    {
                        this.StudentsList.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        //Check if a student is enrolled in the course
        public bool ContainsStudent(Student student)
        {
            return ContainsStudent(student.Id);
        }

        public bool ContainsStudent(int id)
        {
            bool flag = false;
            foreach (Student student in StudentsList)
            {
                if (student.Id == id)
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

        public static List<Course> CreateListOfCourses(int count, string name = "Advanced")
        {
            var courses = new List<Course>();
            for(int i = 0; i < count; i++)
            {
                courses.Add(new Course($"{name} N: {i + 1}"));
            }
            return courses;
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Course);
        }

        //Allows to compare two course instances for equality based on their name
        public bool Equals(Course? obj)
        {
            return obj is Course course && Name == course.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }
    }
}
