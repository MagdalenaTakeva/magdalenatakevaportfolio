﻿namespace MSTestProject
{
    public class Student : IEquatable<Student>
    {
        private string _name;
        private int _id;
        private const int MinValue = 10000;
        private const int MaxValue = 99999;
        private readonly Exception invalidInput = new ArgumentOutOfRangeException("Invalid input");


        public Student(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Name
        {
            get => _name;

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw invalidInput;
                }
                else
                {
                    _name = value;
                }

            }
        }

        public int Id
        {
            get => _id;
            set
            {
                if (value >= MinValue && value <= MaxValue)
                {
                    _id = value;
                }
                else
                {
                    throw invalidInput;
                }
            }
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Student);
        }

        public bool Equals(Student? other)
        {
            return other != null && Id == other.Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name);
        }

        public static List<Student> CreateListOfStudents(int count, int startIndex = 10000, string name = "Petar")
        {
            var studentsList = new List<Student>();
            for (int i = 0; i < count; i++)
            {
                studentsList.Add(new Student(startIndex + i, $"{name} N: { i + 1 }"));
            }
            return studentsList;
        }

        public static int RandomValidNumberForStudentId(Random random)
        {
            return random.Next(20000, 90000);
        }


        public override string ToString()
        {
            return $"Name: {Name}, ID: {Id}";
        }
    }
}
