package com.magdalena.calculatorTest;

import org.testng.annotations.Test;

public class MultiplicationTest {
    @Test(groups = {"smoke"})
    public void verifyMultiplicationLogic1() {
        System.out.println("Inside multiplication logic 1");

    }

    @Test(groups = {"smoke.low"}, priority = 1)
    public void verifyMultiplicationLogic2() {
        System.out.println("Inside multiplication logic 2");
        System.out.println("Thread is: " + Thread.currentThread().getId());
    }

    @Test(groups = {"smoke.high"})
    public void verifyMultiplicationLogic3() {
        System.out.println("Inside multiplication logic 3");

    }

    @Test(groups = {"smoke.low"}, priority = 0)
    public void verifyMultiplicationLogic4() {
        System.out.println("Inside multiplication logic 2");
        System.out.println("Thread is: " + Thread.currentThread().getId());
    }
}
