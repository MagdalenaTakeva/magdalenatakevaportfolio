package com.magdalena.calculatorTest;

import org.testng.annotations.Test;

public class DifferenceTest {

    @Test(groups = {"smoke"})
    public void verifyDiffLogic1() {
        System.out.println("Inside difference logic 1");

    }

    @Test(groups = {"smoke.low"})
    public void verifyDiffLogic2() {
        System.out.println("Inside difference logic 2");

    }

    @Test(groups = {"smoke.high"})
    public void verifyDiffLogic3() {
        System.out.println("Inside difference logic 3");
        System.out.println("Thread is: " + Thread.currentThread().getId());
    }
}
