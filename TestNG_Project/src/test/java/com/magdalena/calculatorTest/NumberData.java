package com.magdalena.calculatorTest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NumberData {
    @DataProvider
    public static Object[][] getNumbers() {

        // 1. Get file path
        //System.getProperty("user.dir") will provide the TestNG project Directory
        String path = System.getProperty("user.dir") + File.separator +
                "src" + File.separator +
                "main" + File.separator +
                "resources" + File.separator +
                "numbers.csv";
        // Create ArrayList where we will store all data
        List<String> dataList = new ArrayList<>();
        // We need a 2D array as well
        Object[][] dataArray = null;
        int rowNum = 0;
        //2. Read CSV using FileReader
        try {
            FileReader reader = new FileReader(path);
            // use apache commons csv reader
            try {
                Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                        .setDelimiter(',')
                        .setHeader("number1", "number2")
                        .setSkipHeaderRecord(true).build().parse(reader);

                for (CSVRecord record : records) {
                    dataList.add(record.get("number1"));
                    dataList.add(record.get("number2"));
                    rowNum++;
                }

                //initialize data array
                dataArray = new Object[rowNum][2];
                // 4. Convert list into 2D array
                int index = 0;

                for (int i = 0; i < dataArray.length; i++) {
                    for (int j = 0; j < dataArray[i].length; j++) {
                        dataArray[i][j] = dataList.get(index++);
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new SkipException(e.getMessage());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new SkipException(e.getMessage());
        }
        return dataArray;
    }
}
