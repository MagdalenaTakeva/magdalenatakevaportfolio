package com.magdalena.calculatorTest;

import com.magdalena.calculator.Sum;
import org.testng.Assert;
import org.testng.annotations.*;

public class SumTest {

    //this method is responsible to read data from csv and pass this data to our test case
    @Test(dataProvider = "getNumbers", dataProviderClass = NumberData.class)
    public void sumTest(String num1, String num2) {
        int num_1 = Integer.parseInt(num1);
        int num_2 = Integer.parseInt(num2);
        int expectedResult = num_1 + num_2;
        //call sum logic
        Sum s = new Sum();
        int actualResult = s.getSum(num_1, num_2);
        Assert.assertEquals(actualResult, expectedResult);
        System.out.println("Thread is: " + Thread.currentThread().getId());
    }


    @Test
    @Parameters({"companyName"})
    public void parameterTest(String companyName) {
        System.out.println("Parameter name is: " + companyName);
        System.out.println("Thread is: " + Thread.currentThread().getId());
    }

    @Test(groups = {"smoke"})
    public void verifySumLogic10() {
        System.out.println("Inside sum logic 10");
        System.out.println("Thread is: " + Thread.currentThread().getId());
    }

    @Test(groups = {"smoke.high"})
    public void verifySumLogic11() {
        System.out.println("Inside sum logic 11");
        System.out.println("Thread is: " + Thread.currentThread().getId());
    }

    @Test(invocationCount = 15, threadPoolSize = 12)
    public void verifySumLogic12() {
        System.out.println("Inside sum logic 12");
        System.out.println("Thread is: " + Thread.currentThread().getId());

    }
}
