package com.magdalena.calculatorTest;

import com.magdalena.calculator.Division;
import com.magdalena.calculator.Sum;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DivTest {

    @Test(dataProvider = "getNumbers", dataProviderClass = NumberData.class)
    public void divTest(String num1, String num2){
        int num_1 = Integer.parseInt(num1);
        int num_2 = Integer.parseInt(num2);
        int expectedResult = num_1/num_2;
        Division division = new Division();
        int actualResult = division.getDivision(num_1,num_2);
        Assert.assertEquals(actualResult,expectedResult);
        System.out.println("Thread is: " + Thread.currentThread().getId());
    }
}
