package com.magdalena.calculatorTest;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.List;

public class TestMain {
    public static void main(String[] args) {
        /*
         * 1. Create TestNG object
         *  2. List of XMLSUite
         *  3. List of XMLTest
         *  4. List of XMLClass
         *  5. Provide the class we want to execute
         *  6. TestNG.run()*/

        TestNG testNG = new TestNG();

        List<XmlSuite> suiteList = new ArrayList<XmlSuite>();
        XmlSuite suite = new XmlSuite();
        suiteList.add(suite);

        List<XmlTest> testList = new ArrayList<XmlTest>();
        XmlTest testSet = new XmlTest(suite);
        testList.add(testSet);

        List<XmlClass> testClassList = new ArrayList<XmlClass>();
        XmlClass testClass = new XmlClass();
        XmlClass sumClass = new XmlClass();

        testClass.setClass(DifferenceTest.class);
        sumClass.setClass(SumTest.class);

        testClassList.add(sumClass);
        testClassList.add(testClass);


        testSet.setXmlClasses(testClassList);

        testNG.setXmlSuites(suiteList);
        testNG.run();
    }
}
