package com.magdalena.calculatorTest;

import com.magdalena.calculator.Sum;
import org.testng.Assert;
import org.testng.annotations.*;

@Ignore
public class Tests {
    /*
    If we have a requirement to execute specific code before and after a test
    * @BeforeSuite
    * @BeforeTest - provides flexibility to execute some methods before a test set
    * @BeforeClass - Will execute before all executions inside a test class
    * @BeforeMethod - will execute a code before a test
    * @Test
    * @AfterMethod - will execute a code after a test
    * @AfterClass - Will execute after all executions inside a test class
    * @AfterTest - provides flexibility to execute some methods after a test set
    * @AfterSuite
     */

    // In TestNG a test is represented by a test set and test method is represented by test case
    @BeforeSuite
    private void beforeSuite() {
        System.out.println("Inside of beforeSuite inside");
    }

    @AfterSuite
    private void afterSuite() {
        System.out.println("Inside of afterSuite inside");
    }

    @BeforeTest
    private void beforeTest() {
        System.out.println("Inside of beforeTest inside");
    }

    @BeforeClass
    private void setUp() {
        System.out.println("inside Set up inside");
    }

    @BeforeMethod
    private void someMethod() {
        System.out.println("inside BeforeMethod method");
    }

    @AfterMethod
    private void someMethod2() {
        System.out.println("inside AfterMethod method");
    }

    @Test(dependsOnMethods = {"verifySumLogic9"})
    public void verifySumLogic1() {
        System.out.println("Inside sum logic 1");
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void verifySumLogic2() {
        System.out.println(34 / 0);
    }

    @Test(timeOut = 40)
    public void verifySumLogic3() throws InterruptedException {
        Thread.sleep(50);
        System.out.println("Inside sum logic 3");
    }

    @Test(priority = 6)
    public void verifySumLogic4() {
        Sum s = new Sum();
        int number1 = 12;
        int number2 = 12;
        int expectedResult = number1 + number2 + 20;
        int actualResult = s.getSum(number1, number2);
        Assert.assertNotNull(actualResult);
        System.out.println("Inside sum logic 4");
    }

    @Test(priority = 5)
    public void verifySumLogic5() {
        System.out.println("Inside sum logic 5");
    }

    @Test(priority = 4)
    public void verifySumLogic6() {
        System.out.println("Inside sum logic 6");
    }

    @Test(priority = 3)
    public void verifySumLogic7() {
        System.out.println("Inside sum logic 7");
    }

    @Test(priority = 2)
    public void verifySumLogic8() {
        System.out.println("Inside sum logic 8");
    }

    @Test(priority = 1, description = "verifySumLogic1 depends on this test")
    public void verifySumLogic9() {
        System.out.println("Inside sum logic 9");
    }

    @Test(priority = 0)
    public void verifySumLogic10() {
        System.out.println("Inside sum logic 10");
    }


    @AfterClass
    private void tearDown() {
        System.out.println("Inside teardown inside");
    }

    @AfterTest
    private void afterTest() {
        System.out.println("Inside of afterTest inside");
    }
}
