package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;

import static com.telerikacademy.dealership.models.common.Validator.*;
import static com.telerikacademy.dealership.models.ModelsConstants.*;

import com.telerikacademy.dealership.models.common.Utils;

import static com.telerikacademy.dealership.models.common.enums.VehicleType.CAR;

public class CarImpl extends VehicleBase implements Car {

    private int seats;
    private final static String SEATS_FIELD = "Seats";
    //private static final String SEATS_ERROR_MESSAGE = "Seats should be between %d and %d characters long.";
    //look in DealershipFactoryImpl - use it to create proper constructor


    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, CAR);
        setSeats(seats);


    }

    @Override
    public int getSeats() {
        return this.seats;
    }

    private void setSeats(int seats) {
        ValidateIntRange(seats, MIN_SEATS, MAX_SEATS,
                String.format(NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, SEATS_FIELD, MIN_SEATS, MAX_SEATS));

        this.seats = seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %s", SEATS_FIELD, getSeats());
    }



}
