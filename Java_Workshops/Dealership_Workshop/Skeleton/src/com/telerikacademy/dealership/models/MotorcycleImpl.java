package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;
import static com.telerikacademy.dealership.models.common.Validator.*;
import static com.telerikacademy.dealership.models.ModelsConstants.*;


public class MotorcycleImpl extends VehicleBase implements Motorcycle {
    private String category;
    private final static String CATEGORY_FIELD = "Category";


    //look in DealershipFactoryImpl - use it to create proper constructor


    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    private void setCategory(String category) {

        ValidateIntRange(category.length(), MIN_CATEGORY_LENGTH, MAX_CATEGORY_LENGTH,
                String.format(NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, CATEGORY_FIELD, MIN_CATEGORY_LENGTH, MAX_CATEGORY_LENGTH));

        this.category = category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %s", CATEGORY_FIELD, getCategory());
    }



}
