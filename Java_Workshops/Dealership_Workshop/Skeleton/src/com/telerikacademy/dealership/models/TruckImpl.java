package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Truck;

import static com.telerikacademy.dealership.models.ModelsConstants.*;
import static com.telerikacademy.dealership.models.common.Validator.ValidateIntRange;

public class TruckImpl extends VehicleBase implements Truck {
     private int weightCapacity;
     private final static String WEIGHT_CAPACITY_FIELD = "Weight capacity";
    
    //look in DealershipFactoryImpl - use it to create proper constructor


    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    @Override
    public int getWeightCapacity() {
        return this.weightCapacity;
    }

    private void setWeightCapacity(int weightCapacity) {
        ValidateIntRange(weightCapacity, MIN_CAPACITY, MAX_CAPACITY,
                String.format(NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, WEIGHT_CAPACITY_FIELD, MIN_CAPACITY, MAX_CAPACITY));

        this.weightCapacity = weightCapacity;
    }


    @Override
    protected String printAdditionalInfo() {
        return String.format("%s: %dt",WEIGHT_CAPACITY_FIELD, getWeightCapacity());
    }

}
