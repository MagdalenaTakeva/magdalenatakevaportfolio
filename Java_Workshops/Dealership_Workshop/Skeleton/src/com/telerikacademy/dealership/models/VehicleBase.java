package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;

import static com.telerikacademy.dealership.models.common.Validator.*;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import static com.telerikacademy.dealership.models.ModelsConstants.*;


import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    //add fields
    private String make;
    private String model;
    private double price;

    private int wheels;
    private VehicleType vehicleType;
    private List<Comment> comments;


    // finish the constructor and validate input;
    // look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?


    public VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        comments = new ArrayList<>();
        this.vehicleType = vehicleType;
        this.wheels = vehicleType.getWheelsFromType();
    }


    @Override
    public String getMake() {
        return this.make;
    }

    private void setMake(String make) {
        ValidateNull(make, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, MAKE_FIELD));

        ValidateIntRange(make.length(), MIN_MAKE_LENGTH, MAX_MAKE_LENGTH,
                String.format(STRING_MUST_BE_BETWEEN_MIN_AND_MAX, MAKE_FIELD, MIN_MAKE_LENGTH, MAX_MAKE_LENGTH));

        this.make = make;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    @Override
    public void removeComment(Comment comment) {
        ValidateNull(comment, COMMENT_CANNOT_BE_NULL);
        comments.remove(comment);
    }

    private void setModel(String model) {

        ValidateNull(model, String.format(FIELD_CANNOT_BE_NULL, MODEL_FIELD));

        ValidateIntRange(model.length(), MIN_MODEL_LENGTH, MAX_MODEL_LENGTH,
                String.format(STRING_MUST_BE_BETWEEN_MIN_AND_MAX, MODEL_FIELD, MIN_MODEL_LENGTH, MAX_MODEL_LENGTH));

        this.model = model;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    private void setPrice(double price) {

        ValidateDecimalRange(price, MIN_PRICE, MAX_PRICE,
                String.format(NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, PRICE_FIELD, MIN_PRICE, MAX_PRICE));

        this.price = price;
    }

    @Override
    public int getWheels() {
        return this.wheels;
    }

    public VehicleType getType() {
        return this.vehicleType;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }


    @Override
    public void addComment(Comment comment) {
        ValidateNull(comment, COMMENT_CANNOT_BE_NULL);
        comments.add(comment);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("  %s: %s", MAKE_FIELD, getMake())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MODEL_FIELD, getModel())).append(System.lineSeparator());
        builder.append(String.format("  %s: %d", WHEELS_FIELD, getWheels())).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());


        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //todo replace this comment with explanation why this method is protected:
    //First - the method is made abstract which means that all subclasses of the VehicleBase should implement it
    //Second - the class access modifier (protected) means that the method can be used only by classes within this package
    // and by child classes of VehicleBase that are in another package
    protected abstract String printAdditionalInfo();

    private String printComments() {
        StringBuilder builder = new StringBuilder();

        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments) {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }

}
