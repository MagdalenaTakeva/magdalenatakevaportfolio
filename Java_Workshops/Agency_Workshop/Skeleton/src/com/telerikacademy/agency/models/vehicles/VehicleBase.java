package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    private static final String TYPE_CANT_BE_NULL = "Type cannot be null.";
    private static final double MIN_PRICE_PER_KM = 0.10;
    private static final double MAX_PRICE_PER_KM = 2.5;
    private static final int MIN_PASSENGER_CAPACITY = 1;
    private static final int MAX_PASSENGER_CAPACITY = 800;
    private static final String PRICE_PER_KM_ERROR =
            "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";
    public static final String PASSENGER_CAPACITY_ERROR =
            "A vehicle with less than 1 passengers or more than 800 passengers cannot exist";

    private double pricePerKilometer;
    private int passengerCapacity;
    private VehicleType type;


    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKm(pricePerKilometer);

        if (type == null)
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        this.type = type;
    }

    public VehicleBase(int passengerCapacity, double pricePerKilometer) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKm(pricePerKilometer);
    }

    private void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PASSENGER_CAPACITY || passengerCapacity > MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(PASSENGER_CAPACITY_ERROR);
        }

        this.passengerCapacity = passengerCapacity;

    }

    private void setPricePerKm(double pricePerKilometer) {
        if (pricePerKilometer < MIN_PRICE_PER_KM || pricePerKilometer > MAX_PRICE_PER_KM) {
            throw new IllegalArgumentException(PRICE_PER_KM_ERROR);
        }
        this.pricePerKilometer = pricePerKilometer;

    }

    @Override
    public String print() {
        return String.format("%s ---%n" +
                        "Passenger capacity: %d%n" +
                        "Price per kilometer: %.2f%n" +
                        "Vehicle type: %s%n",
                this.printClassName(),
                this.getPassengerCapacity(),
                this.getPricePerKilometer(),
                this.getType());
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    public String printClassName() {
        String className = getClass().getSimpleName();
        return className.substring(0, className.length() - 4);
    }


}
