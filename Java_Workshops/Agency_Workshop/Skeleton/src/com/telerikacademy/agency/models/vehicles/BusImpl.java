package com.telerikacademy.agency.models.vehicles;


import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private static final int MIN_BUS_PASSENGER_CAPACITY = 10;
    private static final int MAX_BUS_PASSENGER_CAPACITY = 50;
    private static final String BUS_PASSENGER_CAPACITY_ERROR =
            "A bus cannot have less than %d passengers or more than %d passengers.";


    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer);
        validatePassengerCapacity(passengerCapacity);
    }

    @Override
    public String print() {
        return String.format("%s%n", super.print());
    }

    @Override
    public String toString() {
        return print();
    }


    public void validatePassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_BUS_PASSENGER_CAPACITY || passengerCapacity > MAX_BUS_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(String.format(BUS_PASSENGER_CAPACITY_ERROR,
                    MIN_BUS_PASSENGER_CAPACITY, MAX_BUS_PASSENGER_CAPACITY));
        }
    }

    @Override
    public VehicleType getType() {
        return VehicleType.LAND;
    }
}
