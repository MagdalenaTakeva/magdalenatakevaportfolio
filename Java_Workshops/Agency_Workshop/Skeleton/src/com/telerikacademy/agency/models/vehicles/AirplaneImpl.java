package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer);
        validatePassengerCapacity(passengerCapacity);
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public String print() {
        return String.format("%s" +
                "Has free food: %s%n",super.print(), this.hasFreeFood);
    }

    @Override
    public VehicleType getType() {
        return VehicleType.AIR;
    }


    public void validatePassengerCapacity(int passengerCapacity) {
        if(passengerCapacity < 0){
            throw new IllegalArgumentException("'PassengerCapacity' should be a number representing quantity.");
        }
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    @Override
    public String toString() {
        return print();
    }






}
