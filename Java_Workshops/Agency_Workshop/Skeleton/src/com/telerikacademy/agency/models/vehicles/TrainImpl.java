package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {
    private static final int MIN_TRAIN_PASSENGER_CAPACITY = 30;
    private static final int MAX_TRAIN_PASSENGER_CAPACITY = 150;
    private static final int MIN_CARTS = 1;
    private static final int MAX_CARTS = 15;

    private static final String TRAIN_PASSENGER_CAPACITY_ERROR =
            "A train cannot have less than %d passengers or more than %d passengers.";
    private static final String CART_ERROR =
            "A train cannot have less than %d cart or more than %d carts.";
    private int carts;


    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer);
        validatePassengerCapacity(passengerCapacity);
        setCarts(carts);

    }

    @Override
    public String print() {
        return String.format("%s" +
                "Carts amount: %d%n", super.print(), getCarts());
    }

    @Override
    public VehicleType getType() {
        return VehicleType.LAND;
    }

    @Override
    public String toString() {
        return print();
    }


    public void validatePassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_TRAIN_PASSENGER_CAPACITY || passengerCapacity > MAX_TRAIN_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(String.format(TRAIN_PASSENGER_CAPACITY_ERROR,
                    MIN_TRAIN_PASSENGER_CAPACITY, MAX_TRAIN_PASSENGER_CAPACITY));
        }
    }

    @Override
    public int getCarts() {
        return carts;
    }

    private void setCarts(int carts) {
        if (carts < MIN_CARTS || carts > MAX_CARTS) {
            throw new IllegalArgumentException(String.format(CART_ERROR, MIN_CARTS, MAX_CARTS));
        }
        this.carts = carts;
    }


}

