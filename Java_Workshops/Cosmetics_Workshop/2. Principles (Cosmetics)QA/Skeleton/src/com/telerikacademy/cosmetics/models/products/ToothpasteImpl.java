package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;


import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {
    private List<String> ingredients;


    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);

    }


    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(this.ingredients);

    }

    private void setIngredients(List<String> ingredients) {
        if (ingredients == null) {
            throw new IllegalArgumentException("Ingredients cannot be empty");

        }
        this.ingredients = new ArrayList<>(ingredients);
    }

    @Override
    public String print() {
        return String.format("%s#Ingredients: %s%n===", super.print(), getIngredients().toString());
    }
}
