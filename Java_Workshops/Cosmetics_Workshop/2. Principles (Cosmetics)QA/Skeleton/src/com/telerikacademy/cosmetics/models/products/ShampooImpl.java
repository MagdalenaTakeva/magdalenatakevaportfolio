package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;


public class ShampooImpl extends ProductBase implements Shampoo {

    private int milliliters;
    private UsageType usageType;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setUsage(usage);
    }


    @Override
    public int getMilliliters() {
        return this.milliliters;
    }

    private void setMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters are not negative number.");
        }
        this.milliliters = milliliters;
    }

    @Override
    public UsageType getUsage() {
        return this.usageType;
    }

    private void setUsage(UsageType usageType) {
        this.usageType = usageType;
    }


    @Override
    public String print() {
        return String.format("%s" +
                "#Milliliters: %d%n" +
                "#Usage: %s%n===", super.print(), getMilliliters(), getUsage());
    }


}
