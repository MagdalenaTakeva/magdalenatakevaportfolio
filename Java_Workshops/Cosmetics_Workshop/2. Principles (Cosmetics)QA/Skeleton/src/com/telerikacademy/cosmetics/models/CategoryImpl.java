package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;


import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    //use constants for validations values
    private static final int NAME_MIN_LENGTH = 2;
    private static final int NAME_MAX_LENGTH = 15;
    private static final String NAME_INVALID_MESSAGE = "Name should be between 2 and 15 symbols.";
    private static final String DELETE_PRODUCT_ERROR_MESSAGE = "Product not found in category.";
    
    private String name;
    private final List<Product> products;
    
    public CategoryImpl(String name) {
        //validate
        //initialize the collection
        setName(name);
        products = new ArrayList<>();

    }
    
    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        this.name = name;
    }
    
    public List<Product> getProducts() {
        //if we do not want the ArrayList to be changed from the outside we return a copy of the ArrayList
        //BECAUSE: if we return the same list actually we are returning the same reference, the same address
        //and whomever has the address can change the original even by mistake
        // todo why are we returning a copy? Replace this comment with explanation!
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        if(product==null){
            throw new IllegalArgumentException("Product cannot be null");
        }
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        //validate
        if (!products.contains(product)) {
            throw new IllegalArgumentException(DELETE_PRODUCT_ERROR_MESSAGE);
        }
        products.remove(product);
    }
    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {

        StringBuilder result = new StringBuilder();
        result.append(String.format("#Category: %s\n", name));

        if (products.isEmpty()) {
            result.append(" #No product in this category");
        }

        for (Product product : products) {
            result.append(product.print());
        }

        return result.toString();
    }
    
}
