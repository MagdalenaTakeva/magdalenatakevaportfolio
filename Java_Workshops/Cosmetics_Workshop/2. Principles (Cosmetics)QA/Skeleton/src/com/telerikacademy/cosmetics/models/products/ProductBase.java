package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;


public class ProductBase implements Product {

    //Class fields common for all subclasses - ShampooImpl, ToothpasteImpl, CreamImpl
    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MIN_BRAND_LENGTH = 2;
    private static final int MAX_BRAND_LENGTH = 10;
    public String name;
    public double price;
    public GenderType gender;
    public String brand;

    //Finish the class
    //What variables, what constants should you write here?
    //validate


    public ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setPrice(price);
        setGender(gender);
        setBrand(brand);
    }


    @Override
    public String getName() {
        return this.name;
    }

    private void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }

        if (name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(String.format("Name length should be between %s and %s characters",
                    MIN_NAME_LENGTH, MAX_NAME_LENGTH));
        }
        this.name = name;
    }


    @Override
    public String getBrand() {
        return this.brand;
    }

    private void setBrand(String brand) {
        if (brand.length() < MIN_BRAND_LENGTH || brand.length() > MAX_BRAND_LENGTH) {
            throw new IllegalArgumentException(String.format("Brand length should be between %s and %s characters",
                    MIN_BRAND_LENGTH, MAX_BRAND_LENGTH));
        }
        this.brand = brand;
    }


    @Override
    public double getPrice() {
        return this.price;
    }

    private void setPrice(double price) {
        if (price < 0.0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
        this.price = price;
    }


    @Override
    public GenderType getGender() {
        return this.gender;
    }


    private void setGender(GenderType gender) {
        this.gender = gender;
    }


    @Override
    public String print() {
        return String.format("#%s %s%n#Price: %.2f%n#Gender: %s%n", getName(), getBrand(), getPrice(), getGender());
    }


}
