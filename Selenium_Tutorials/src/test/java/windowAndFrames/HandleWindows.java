package windowAndFrames;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class HandleWindows extends BaseClass {

    @Test
    public void windows() {
        driver.get("https://www.lambdatest.com/selenium-playground/window-popup-modal-demo");
        System.out.println("Parent window:" + driver.getTitle());
        String windowParent = driver.getWindowHandle();
        System.out.println(windowParent);
        // Click the "Follow on Twitter" button
        driver.findElement(By.linkText("Follow On Twitter")).click();
        //We use a List to get all the valuues from the Set and then we can use get()
        Set<String> windowHandles = driver.getWindowHandles();
        List<String> windows = new ArrayList<>(windowHandles);
        // Tell the browser to switch to the child window, whicch is the second one o the two windows
        driver.switchTo().window(windows.get(1));
        String title = driver.getTitle();
        System.out.println("Child window:" + title);
        System.out.println(title);

        // Switch to parent window
        driver.switchTo().window(windows.get(0));
        System.out.println("Back to parent window:" + driver.getTitle());

        /*Possible Exceptions:
         * IndexOutOfBounds Exception
         * NoSuchWindow Exception - when target window is already closed and we want to get its title
         * */
    }
}
