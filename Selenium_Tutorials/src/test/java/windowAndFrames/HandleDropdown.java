package windowAndFrames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class HandleDropdown extends BaseClass {
    @Test
    public void selectSingleDropdown() {

        driver.get("https://lambdatest.com/selenium-playground/select-dropdown-demo");
        //-------------Single dropdown---------------------------------------------------------------//
        // To find the dropdown we have to find the select unique locator
        WebElement dayDropdown = driver.findElement(By.id("select-demo"));
        // To select any dropdown we have to get the select class from selenium
        // We are going to create a constructor for that where we are going to pass the WebElement
        // We are doing this because it might be possible that a single page can have multiple dropdowns
        // So to interact with particular dropdown we have to say which dropdown it is
        Select select = new Select(dayDropdown);
        select.selectByVisibleText("Friday");
        //To get the value we have selected we use getFirstSelectedOption()
        Assert.assertEquals(select.getFirstSelectedOption().getText(), "Friday");
        select.selectByValue("Monday");
        Assert.assertEquals(select.getFirstSelectedOption().getText(), "Monday");
        select.selectByIndex(4);
        Assert.assertEquals(select.getFirstSelectedOption().getText(), "Wednesday");
    }
    //-------------------------Multiple dropdown-----------------------------------------------------//

    @Test
    public void selectMultiDropdown() {
        driver.get("https://lambdatest.com/selenium-playground/select-dropdown-demo");
        WebElement multipleDropdown = driver.findElement(By.id("multi-select"));
        Assert.assertTrue(multipleDropdown.isDisplayed());
        Select sel = new Select(multipleDropdown);
        // assert dropdown is a multi-dropdown
        Assert.assertTrue(sel.isMultiple());

        List<WebElement> options = sel.getOptions();
        System.out.println("All available options are selected and listed");
        for (WebElement option : options) {
            System.out.println(option.getText());
        }

        sel.selectByIndex(3);
        sel.selectByValue("California");
        sel.selectByVisibleText("New Jersey");
        List<WebElement> selectedOptions = sel.getAllSelectedOptions();

        System.out.println("Out of all available options, only those that are selected are listed");
        for (WebElement country : selectedOptions) {
            System.out.println(country.getText());
        }
        sel.deselectAll();
    }
}


