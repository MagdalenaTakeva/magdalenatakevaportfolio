package windowAndFrames;


import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseClass {
    public RemoteWebDriver driver;
    public String Status = "failed";

    // For parallel testing
    @Parameters({"browsername", "testName"})//we will get this parameter from the xml filex
    @BeforeMethod
    public void setup(String browser, String testName) throws MalformedURLException {
        String username = System.getenv("LT_USERNAME") == null ? "magdalena.takeva" : System.getenv("LT_USERNAME");
        String authkey = System.getenv("LT_ACCESS_KEY") == null ? "VicBqkw0eoMBuzjzlyZIeSFKgXgTX7a36cjTMRlMlCW89Co0FO" : System.getenv("LT_ACCESS_KEY");
        String hub = "@hub.lambdatest.com/wd/hub";

        DesiredCapabilities caps = new DesiredCapabilities();
        // Configure your capabilities here
        caps.setCapability("platform", "Windows 10");
        caps.setCapability("browserName", browser);
        caps.setCapability("version", "107.0");
        caps.setCapability("resolution", "2560x1440");
        caps.setCapability("build", "TestNG With Java");
        caps.setCapability("name", testName);
        caps.setCapability("plugin", "git-testng");

        String[] Tags = new String[]{"Feature", "Magicleap", "Severe"};
        caps.setCapability("tags", Tags);

        driver = new RemoteWebDriver(new URL("https://" + username + ":" + authkey + hub), caps);
    }

    @Test
    public void basicTest() throws InterruptedException {
        String spanText;
        System.out.println("Loading Url");

        driver.get("https://lambdatest.github.io/sample-todo-app/");

        System.out.println("Checking Box");
        driver.findElement(By.name("li1")).click();

        System.out.println("Checking Another Box");
        driver.findElement(By.name("li2")).click();

        System.out.println("Checking Box");
        driver.findElement(By.name("li3")).click();

        System.out.println("Checking Another Box");
        driver.findElement(By.name("li4")).click();

        System.out.println("Checking Another Box");
        driver.findElement(By.name("li5")).click();

        System.out.println("Input field placeholder");

        String placeholderText = driver.findElement(By.xpath("//form/input")).getAttribute("placeholder");
        Assert.assertEquals("Want to add more", placeholderText);

        // Let's also assert that the todo we added is present in the list.

        spanText = driver.findElement(By.xpath("//li[5]/child::span")).getText();
        Assert.assertEquals("Fifth Item", spanText);
        Status = "passed";
        Thread.sleep(150);
        System.out.println("TestFinished");
    }

    @AfterMethod
    public void tearDown() {
        driver.executeScript("lambda-status=" + Status);
        driver.quit();
    }
}
