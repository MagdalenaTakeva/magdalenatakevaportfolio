package windowAndFrames;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class HandleAlerts extends BaseClass {

    @Test
    public void JavaScriptPoopUpBoxes() {
        driver.get("https://www.lambdatest.com/selenium-playground/javascript-alert-box-demo");
        /* JavaScript Alert Box - When an alert box pops up, user have to click "OK" button to proceed.*/

        // Click the button to display an alert box:
        driver.findElement(By.xpath("//div[text()='Java Script Alert Box']/following-sibling::button")).click();
        //When there is an Alert, we cannot do anything unless we handle it by switching to it
        Alert alert = driver.switchTo().alert();
        //We have to use the getText() before handling the alert
        // as once we accept() or dismiss() the alert, it will disappear
        System.out.println(alert.getText());
        alert.accept();

        /* JavaScript Confirm Box - When a Confirm box pops up, user can click "OK" or "Cancel" to proceed.
        Box returns true, if the user clicks "OK" and box returns false if user clicks "Cancel"*/

        //Click the button to display a Confirm box
        driver.findElement(By.xpath("(//button[text()='Click Me'])[2]")).click();
        System.out.println(alert.getText());
        alert.dismiss(); // or alert.accept();

        /* JavaScript Prompt Box*/

        //Click the button to display a prompt box:
        driver.findElement(By.xpath("(//button[text()='Click Me'])[3]")).click();
        System.out.println(alert.getText());
        alert.sendKeys("Maggie");
        alert.accept();
    }
}
