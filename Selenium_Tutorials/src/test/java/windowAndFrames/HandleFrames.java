package windowAndFrames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class HandleFrames extends BaseClass {
//    WebDriver driver;
//
//    @BeforeMethod
//    public void setUp() {
//        WebDriverManager.chromedriver().setup();
//        driver = new ChromeDriver();
//    }

    @Test
    public void frames() {
        driver.get("https://letcode.in/frame");
        // Accept consent
        WebElement googleConsentBut =
                driver.findElement(By.xpath("//body/div/div[2]/div[1]/div[2]/div[2]/button[1]/p"));
        googleConsentBut.click();

        // frame
//        driver.switchTo().frame("firstFr");//in brackets frame id or name
//        driver.switchTo().frame(1);
        WebElement framaEle = driver.findElement(By.xpath("//iframe[@src='frameUI']"));
        driver.switchTo().frame(framaEle);
        // Then find the element
        driver.findElement(By.name("fname")).sendKeys("Maggie");
        driver.findElement(By.name("lname")).sendKeys("Takeva");
        //go to nested frame to access email element
        WebElement innerFrame = driver.findElement(By.cssSelector("iframe.has-background-white"));
        driver.switchTo().frame(innerFrame);
        driver.findElement(By.name("email")).sendKeys("maggie@abv.bg");
        //switch to parent frame from nested frame to acc ess fname element
        driver.switchTo().parentFrame();
        //clear whatever was previously written in the fname field
        driver.findElement(By.name("fname")).clear();
        driver.findElement(By.name("fname")).sendKeys("Marina");

        // Use defaultContent() to go from nested frame to main page
        driver.switchTo().defaultContent();//this way we will go to the main page
        driver.findElement(By.linkText("Log in")).click();

    }
//
//    @AfterMethod
//    public void tearDown() {
//        driver.quit();
//    }

}
