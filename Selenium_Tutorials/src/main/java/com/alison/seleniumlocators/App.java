package com.alison.seleniumlocators;

import org.openqa.selenium.WebElement;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        /*
         * 1. Ask user for input
         * 2. Validate user input
         * 3. Run example on user provided browser
         * */

        System.out.println("Please enter browser of your choice: chrome or firefox");
        Scanner scanner = new Scanner(System.in);
        String browser = scanner.nextLine();

        // example class
        ElementFindingExamples examples = new ElementFindingExamples();
        WebElementPart1 examples2 = new WebElementPart1();
        WebElementsPart2 example3 = new WebElementsPart2();
        WebTables example4 = new WebTables();
        WebDriverWaits webDriverWaits = new WebDriverWaits();
        JavaScriptExamples javaScriptExamples = new JavaScriptExamples();
        if (browser.equalsIgnoreCase("chrome") || browser.equalsIgnoreCase("firefox")
                || browser.equalsIgnoreCase("edge")) {
            //  code to be executed
            examples.findElementByName(browser);
            examples.findElementById(browser);
            examples.findElementByLinkedText(browser);
            examples.findElementByPartialLinkedText(browser);
            examples.findElementByXpath(browser);
            examples.findElementByCSS(browser);
            examples2.handleIframe(browser);
            examples2.handleCheckboxes(browser);
            examples2.handleRadioButtons(browser);
            examples2.handleAlert(browser);
            examples2.handleDropdown(browser);
            example3.openWebPageUsingNavigation(browser);
            example3.handleBackwardNavigation(browser);
            example3.handleForwardNavigation(browser);
            example3.navigateFacebook(browser);
            examples2.handleMultipleWindows(browser);
            example3.handlePageRefresh(browser);
            example4.getSingleElementFromWebTable(browser);
            webDriverWaits.fluentWait(browser);
            webDriverWaits.webDriverWait(browser);
            example4.getAllElementsFromWebTable(browser);
            javaScriptExamples.invokeAlert(browser);
            javaScriptExamples.sendTextToAnElement(browser);
            javaScriptExamples.refreshBrowser(browser);
            javaScriptExamples.getTitleOfWebPage(browser);
            javaScriptExamples.verticalScroll(browser);
        } else {
            System.out.println("Browser " + browser + " that you entered is not supported ");
        }
    }
}
