package com.alison.seleniumlocators;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.function.Function;

public class WebDriverWaits {
    private WebDriver driver;
    WebDriverWait wait;

    public void fluentWait(String browser) {
        driver = Browse.getUserBrowser(browser);

        driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_alert");
        WebElement acceptAll = driver.findElement(By.xpath("//*[text()='Accept all & visit the site']"));
        acceptAll.click();
        driver.switchTo().frame("iframeResult");

        //initialize the wait object by providing all configuration for it
        Wait wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofMillis(5000))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(NoSuchFieldException.class);

        // Then provide our custom until() condition
        // Function input type will be of WebDriver and output type will be of WebElement
        wait.until(new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                WebElement element = driver.findElement(By.tagName("button"));
                return element;
            }
        });

        driver.findElement(By.tagName("button")).click();

        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.accept();
    }

    public void webDriverWait(String browser) {
        driver = Browse.getUserBrowser(browser);

        driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_alert");
        WebElement acceptAll = driver.findElement(By.xpath("//*[text()='Accept all & visit the site']"));
        acceptAll.click();
        driver.switchTo().frame("iframeResult");
        // use WebDriverWait
        driver.findElement(By.tagName("button")).click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();

    }
}
