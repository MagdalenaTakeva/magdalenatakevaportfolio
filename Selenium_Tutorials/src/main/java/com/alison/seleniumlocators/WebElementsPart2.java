package com.alison.seleniumlocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebElementsPart2 {
    private WebDriver driver;
    WebDriverWait wait;

    public void openWebPageUsingNavigation(String browser) {
        driver = Browse.getUserBrowser(browser);
        // open facebook using navigation
        driver.navigate().to("https://www.facebook.com");
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
    }

    public void handleBackwardNavigation(String browser) {
        driver = Browse.getUserBrowser(browser);
        // open facebook using navigation
        driver.navigate().to("https://www.facebook.com");
        // accept cookies
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
        // open w3schools
        driver.navigate().to("https://www.w3schools.com/");
        // accept cookies
        WebElement acceptAll = driver.findElement(By.xpath("//*[text()='Accept all & visit the site']"));
        acceptAll.click();
        // backward navigation
        driver.navigate().back();
    }

    public void handleForwardNavigation(String browser) {
        driver = Browse.getUserBrowser(browser);
        // open facebook using navigation
        driver.navigate().to("https://www.facebook.com");
        // accept cookies
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
        // open w3schools
        driver.navigate().to("https://www.w3schools.com/");
        // accept cookies
        WebElement acceptAll = driver.findElement(By.xpath("//*[text()='Accept all & visit the site']"));
        acceptAll.click();
        // backward navigation - to Facebook
        driver.navigate().back();
        // forward navigation - to w3schools
        driver.navigate().forward();
    }

    public void navigateFacebook(String browser) {
        driver = Browse.getUserBrowser(browser);
        // open facebook using navigation
        driver.navigate().to("https://www.facebook.com");
        // accept cookies
        consentFacebook();
        // open the About page
        driver.findElement(By.linkText("About")).click();
        consentFacebook();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        //navigate back to login page
        driver.navigate().back();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // click on Login button
        driver.findElement(By.linkText("Log In")).click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // move back to About page
        driver.navigate().back();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // forward navigation - this will open the Login page again
        driver.navigate().forward();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // navigate backward - About page
        driver.navigate().back();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        //navigate forward - Login page
        driver.navigate().forward();
        // consentFacebook();
    }

    public void handlePageRefresh(String browser) {
        driver = Browse.getUserBrowser(browser);
        // open facebook using navigation
        driver.navigate().to("https://www.facebook.com");
        consentFacebook();
        // refresh page
        driver.navigate().refresh();
        consentFacebook();
    }

    public void consentFacebook() {
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
    }
}
