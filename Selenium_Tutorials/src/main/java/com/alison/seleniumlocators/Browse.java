package com.alison.seleniumlocators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;

import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.Capabilities;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.time.Duration;
import java.util.concurrent.TimeUnit;


public class Browse {
    private static WebDriver driver;

    public static WebDriver getUserBrowser(String browser) {

        if (driver == null) {
            //open browser
            if (browser.equalsIgnoreCase("chrome")) {
                System.setProperty("webdriver.chrome.driver", "C:\\Temp\\WebDrivers\\Chrome_driver\\chromedriver_win32\\chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--incognito");
                DesiredCapabilities cap = new DesiredCapabilities();
                driver = new ChromeDriver();
                cap.setCapability(ChromeOptions.CAPABILITY, options);
                options.merge(cap);
                driver = new ChromeDriver(options);

            } else if (browser.equalsIgnoreCase("edge")) {
                System.setProperty("webdriver.edge.driver", "C:\\Temp\\WebDrivers\\Edge_driver\\edgedriver_win64\\msedgedriver.exe");
                driver = new EdgeDriver();
            } else {
                System.out.println("Browser not supported: " + browser);
                //to apply timeout configuration on driver object
                // it will work throughout the life span of the webdriver object
                //we fo not have to provide again anywhere in our script this timeout
                driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(120));
                // every action taken by this driver object will wait 20 seconds by default
                driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
            }
            return driver;
        }
        return driver;
    }
}



