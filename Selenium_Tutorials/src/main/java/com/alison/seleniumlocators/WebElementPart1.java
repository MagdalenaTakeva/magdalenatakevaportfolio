package com.alison.seleniumlocators;

import org.openqa.selenium.*;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class WebElementPart1 {
    private WebDriver driver;
    WebDriverWait wait;

    public void handleIframe(String browserName) {
        driver = Browse.getUserBrowser(browserName);
        //open iframe page
        driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_iframe");
        clickAcceptAllButton();
        //switch to parent frame
        driver.switchTo().frame("iframeResult");
        //find child iframe
        WebElement childIFrame = driver.findElement(By.tagName("iframe"));
        driver.switchTo().frame(childIFrame);
        clickAcceptAllButton();
        // find HTML button
        driver.findElement(By.linkText("Learn HTML")).click();
        //switch to default page
        driver.switchTo().defaultContent();
    }

    public void handleCheckboxes(String browserName) {
        driver = Browse.getUserBrowser(browserName);
        //open iframe page
        driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_input_type_checkbox");
        clickAcceptAllButton();

        //find first checkbox, select it
        driver.switchTo().frame("iframeResult");

        driver.findElement(By.name("vehicle1")).click();

        //deselect checkbox
        driver.findElement(By.name("vehicle2")).click();
        driver.findElement(By.name("vehicle1")).click();
    }

    public void handleRadioButtons(String browserName) {
        driver = Browse.getUserBrowser(browserName);
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));

        //open radio button web page
        driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_input_type_radio");
        clickAcceptAllButton();
        // switch to parent iframe
        driver.switchTo().frame("iframeResult");
        driver.findElement(By.id("html")).click();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("css"))));
        driver.findElement(By.id("css")).click();

    }

    public void handleAlert(String browserName) {
        driver = Browse.getUserBrowser(browserName);
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));

        //open alert web page
        driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_alert");
        clickAcceptAllButton();
        // switch to the parent iframe
        driver.switchTo().frame("iframeResult");
        // find and click over the button
        driver.findElement(By.tagName("button")).click();
        // switch to the new alert
        Alert alert = driver.switchTo().alert();
        // print alert text
        System.out.println("Alert text is: " + alert.getText());
        // accept alert
        alert.accept();
    }

    public void handleDropdown(String browserName) {
        driver = Browse.getUserBrowser(browserName);
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));

        //open alert web page
        driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select");
        clickAcceptAllButton();
        // switch to the parent iframe
        driver.switchTo().frame("iframeResult");
        // find select element
        WebElement selectElement = driver.findElement(By.tagName("select"));
        Select dropdown = new Select(selectElement);
        dropdown.selectByVisibleText("Audi");
    }

    public void handleMultipleWindows(String browser) {
        driver = Browse.getUserBrowser(browser);
        // open w3schools.com
        driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_win_open");
        //print title of page
        System.out.println("Parent title is: " + driver.getTitle());
        clickAcceptAllButton();
        //switch to iframe
        driver.switchTo().frame("iframeResult");
        // capture window id
        String parentWindowId = driver.getWindowHandle();
        System.out.println("Parent window id is: " + parentWindowId);
        //Result is:
        // Parent window id is: CDwindow-2ABCB1DA4C01BE6CEFF4A5D373134A6B in Chrome
        //Parent window id is: CDwindow-D25B100A1F1B05440741473F261EBA71 in Edge
        // We need to save these ids in some variables so that we can use them when required

        // click over the button
        driver.findElement(By.tagName("button")).click();
        //get all windows ids
        Set<String> windIdsSet = driver.getWindowHandles();
        // convert this set to list - this way we can have different windows ids just by providing the index
        List<String> windList = new ArrayList<String>(windIdsSet);
        //switch to the child window
        driver.switchTo().window(windList.get(1));
        System.out.println("Child title is: " + driver.getTitle());
        // again switch to the parent page
        driver.switchTo().window(parentWindowId);
        System.out.println("Parent title is: " + driver.getTitle());
        //maximize the window
        driver.manage().window().maximize();
    }

    public void clickAcceptAllButton() {
        WebElement acceptAll = driver.findElement(By.xpath("//*[text()='Accept all & visit the site']"));
        acceptAll.click();
    }
}
