package com.alison.seleniumlocators;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class JavaScriptExamples {
    public void invokeAlert(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        driver.get("https://www.facebook.com");
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
        // create object of Javascript executer to be able to perform any javscript execution on this driver object
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        //execute alert() function
        executor.executeScript("alert('This alert is injected by WebDriver');");
    }

    public void sendTextToAnElement(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        driver.get("https://www.facebook.com");
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
        // create object of Javascript executer to be able to perform any javscript execution on this driver object
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        //enter email and password
        executor.executeScript("document.getElementById('email').value='magdalena@abv.bg';");
        // as getElementsByName() returns an array we have to provide the index of the array element
        executor.executeScript("document.getElementsByName('pass')[0].value='password';");
        //click the submit button
        executor.executeScript("document.getElementsByName('login')[0].click();");
    }

    public void refreshBrowser(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        driver.get("https://www.facebook.com");
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
        // create object of Javascript executer to be able to perform any javscript execution on this driver object
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        //refresh the page
        executor.executeScript("history.go(0)");
    }

    public void getTitleOfWebPage(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        driver.get("https://www.facebook.com");
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
        // create object of Javascript executer to be able to perform any javscript execution on this driver object
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        //get page title
        String pageTitle = executor.executeScript("return document.title").toString();
        System.out.println(pageTitle);
    }

    public void verticalScroll(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        driver.get("https://www.facebook.com");
        driver.manage().window().setSize(new Dimension(1024, 450));
        WebElement consent = driver.findElement(By.xpath("//*[text()='Allow essential and optional cookies']"));
        consent.click();
        // create object of Javascript executer to be able to perform any javscript execution on this driver object
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        //vertical scroll

        executor.executeScript("window.scrollBy(0,750);");

        executor.executeScript("window.scrollBy(0,-250);");

    }
}
