package com.alison.seleniumlocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class WebTables {
    public void getSingleElementFromWebTable(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        // open timeanddate web page
        driver.get("https://www.timeanddate.com/worldclock/");
        WebElement agreeButton = driver.findElement(By.xpath("//*[text()='AGREE']"));
        agreeButton.click();
        //get data of p79 element
        String elementText = driver.findElement(By.id("p79")).getText();
        System.out.println(elementText);
    }

    public void getAllElementsFromWebTable(String browser) {
        //first crreate a webdriver object
        WebDriver driver = Browse.getUserBrowser(browser);
        // open timeanddate web page
        driver.get("https://www.w3schools.com/html/tryit.asp?filename=tryhtml_table_intro");
        driver.findElement(By.xpath("//*[text()='Accept all & visit the site']")).click();

        // switch frame
        driver.switchTo().frame("iframeResult");
        // get table
        WebElement table = driver.findElement(By.tagName("table"));
        // get all rows
        List<WebElement> rowList = table.findElements(By.tagName("tr"));
        //run through each row and retrieve data
        for (WebElement row : rowList) {
            //get all data elements - columns
            List<WebElement> columnsList = row.findElements(By.tagName("td"));
            // run through each column and print data
            for (WebElement cols : columnsList) {
                System.out.print(cols.getText() + "\t");

            }
            System.out.println();
        }
    }
}
