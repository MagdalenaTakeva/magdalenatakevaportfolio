package com.alison.seleniumlocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ElementFindingExamples {
    public void findElementByName(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        // open Facebook
        driver.get("https://www.facebook.com/");
        WebElement consentButton = driver.findElement(By.xpath("(//button[@type='submit'])[3]"));
        consentButton.click();
        // find email WebElement
        WebElement email = driver.findElement(By.name("email"));
        email.sendKeys("autor@quaatso.com");
    }

    public void findElementById(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        // open Facebook
        driver.get("https://www.facebook.com/");
        WebElement consentButton = driver.findElement(By.xpath("(//button[@type='submit'])[3]"));
        consentButton.click();
        // find email WebElement
        WebElement password = driver.findElement(By.id("pass"));
        password.sendKeys("password");
    }

    public void findElementByLinkedText(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        // open Facebook
        driver.get("https://www.facebook.com/");
        WebElement consentButton = driver.findElement(By.xpath("(//button[@type='submit'])[3]"));
        consentButton.click();

        // find email WebElement
        WebElement password = driver.findElement(By.linkText("Forgotten account"));
        password.click();
    }

    public void findElementByPartialLinkedText(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        // open Facebook
        driver.get("https://www.facebook.com/");
        WebElement consentButton = driver.findElement(By.xpath("(//button[@type='submit'])[3]"));
        consentButton.click();
        // find email WebElement
        WebElement password = driver.findElement(By.partialLinkText("Fund"));
        password.click();
    }

    public void findElementByXpath(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        // open Facebook
        driver.get("https://www.facebook.com/");
        WebElement consentButton = driver.findElement(By.xpath("(//button[@type='submit'])[3]"));
        consentButton.click();
        // find email WebElement
        WebElement emailElement = driver.findElement(By.xpath("//*[@name='email']"));
        emailElement.sendKeys("author@quaatso.com");
    }

    public void findElementByCSS(String browser) {
        WebDriver driver = Browse.getUserBrowser(browser);
        // open Facebook
        driver.get("https://www.facebook.com/");
        WebElement consentButton = driver.findElement(By.xpath("(//button[@type='submit'])[3]"));
        consentButton.click();
        // find email WebElement
        WebElement password = driver.findElement(By.cssSelector("*[id='pass']"));
        password.sendKeys("password");
    }
}
