package wiremockexamples.extensions;

import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.matching.MatchResult;
import com.github.tomakehurst.wiremock.matching.RequestMatcherExtension;

public class BodyLengthMatcher extends RequestMatcherExtension {

    @Override
    public String getName(){
        return "body-too-long";
    }
    @Override
    public MatchResult match(Request request, Parameters parameters) {

        // Get the value of the maxLength matcher parameter
        int maxLength = parameters.getInt("maxLength");

        // Compare the request body length to the maxLength
        //parameter value and return the result as a MatchResult
        return MatchResult.of(request.getBody().length > maxLength);
    }
}
