package wiremockexamples;

import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.RegisterExtension;
import wiremockexamples.extensions.BodyLengthMatcher;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static io.restassured.RestAssured.given;

public class Chapter14Test {
    private RequestSpecification requestSpec;

    /*
   Custom request matchers
    _Add custom request matching logic to WireMock
    _Can be combined with existing standard matchers
    _Done by extending RequestMatcherExtension class
     */

    @RegisterExtension
    static WireMockExtension wireMock = WireMockExtension.newInstance().
            options(wireMockConfig().
                    port(9876).
                    extensions(new BodyLengthMatcher())).
            build();


    @BeforeEach
    public void createRequestSpec(){
        requestSpec = new RequestSpecBuilder().
                setBaseUri("http://localhost").
                setPort(9876).
                build();
    }

    public void stubForBodyLengthMatcher(){
        wireMock.stubFor(requestMatching("body-too-long", Parameters.one("maxLength", 20))
                .willReturn(aResponse().withStatus(422)));
    }
}
