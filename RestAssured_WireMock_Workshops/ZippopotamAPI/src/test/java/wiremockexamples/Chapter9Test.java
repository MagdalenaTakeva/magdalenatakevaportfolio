package wiremockexamples;

import com.github.tomakehurst.wiremock.stubbing.Scenario;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static io.restassured.RestAssured.given;

public class Chapter9Test extends BaseWireMockTest{
    /*
    Stateful mocks in WireMock
        _Supported through the concept of a Scenario
        _Essentially a finite state machine (FSM)
        _ States and state transitions
        _Combination of current state and incoming request determines the response being sent
        _ Before now, it was only the incoming request
     */

    public void setupStubExercise301() {

        /************************************************
         * Create a stub that exerts the following behavior:
         * - The scenario is called 'Loan processing'
         * - 1. A first GET to /loan/12345 returns HTTP 404
         * - 2. A POST to /requestLoan with body 'Loan ID: 12345' returns HTTP 201
         * 		and causes a transition to state 'LOAN_GRANTED'
         * - 3. A second GET (when in state 'LOAN_GRANTED') to /loan/12345
         *      returns HTTP 200 and body 'Loan ID: 12345'
         ************************************************/

        stubFor(get(urlEqualTo("/loan/12345"))
                .inScenario("Loan processing")
                .whenScenarioStateIs(Scenario.STARTED)
                .willReturn(aResponse()
                        .withStatus(404)
                ));

        stubFor(post(urlEqualTo("/requestLoan"))
                .inScenario("Loan processing")
                .whenScenarioStateIs(Scenario.STARTED)
                .withRequestBody(equalTo("Loan ID: 12345"))
                .willReturn(aResponse()
                        .withStatus(201))
                        .willSetStateTo("LOAN_GRANTED")
        );

        stubFor(get(urlEqualTo("/loan/12345"))
                .inScenario("Loan processing")
                .whenScenarioStateIs("LOAN_GRANTED")
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("Loan ID: 12345")
                ));

    }

    @Test
    public void testExercise301() {

        /***
         * Use this test to test the Java implementation of exercise 301
         */

        setupStubExercise301();

        given().
                spec(requestSpec).
                when().
                get("/loan/12345").
                then().
                assertThat().
                statusCode(404);

        given().
                spec(requestSpec).
                and().
                body("Loan ID: 12345").
                when().
                post("/requestLoan").
                then().
                assertThat().
                statusCode(201);

        given().
                spec(requestSpec).
                when().
                get("/loan/12345").
                then().
                assertThat().
                statusCode(200).
                and().
                body(org.hamcrest.Matchers.equalTo("Loan ID: 12345"));
    }
}
