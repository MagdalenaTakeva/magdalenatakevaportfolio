package wiremockexamples;

import dataentities.Location;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class Chapter6Test extends BaseWireMockTest{

    // De-serializing an API Response to a Java Object
    //demonstrates how to transform an API response, which again can be in XML or in JSON format
    // to an instance of an actual Java object. This is a process which is known as deserialization.
    @Test
    public void requestUsZipCode90210_checkPlaceNameInResponseBody_expectBeverlyHills(){

        ////tell REST Assured to convert the response to an instance of the Location Java object using the as method,
        // which takes a parameter the type that you want to convert the JSON or XML API response to
        Location location =
                given().
                        spec(requestSpec).
                        when().
                        get("http://api.zippopotam.us/us/90210").
                        as(Location.class);

        Assert.assertEquals(
                "Beverly Hills",
                location.getPlaces().get(0).getPlaceName()
        );
    }

    //Serializing a Java object to a JSON Request Body
    //if we have an instance of a Java object, we want to be able to automatically convert
    // that to an XML or JSON request body so we can send data to an API instead of just retrieving
    // data from an API and then converting it into a Java object, and this process is known as _serialization.
    @Test
    public void sendLvZipCode1050_checkStatusCode_expect200() {

        Location location = new Location();
        location.setCountry("Netherlands");

        given().
                spec(requestSpec).
                contentType(ContentType.JSON).
                body(location).
                log().body().
                when().
                post("/lv/1050").
                then().
                assertThat().
                statusCode(200);
    }

}
