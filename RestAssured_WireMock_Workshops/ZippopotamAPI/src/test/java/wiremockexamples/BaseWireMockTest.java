package wiremockexamples;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.junit.BeforeClass;
import org.junit.Rule;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class BaseWireMockTest {
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(9876));

    protected static RequestSpecification requestSpec;

    @BeforeClass
    public static void createRequestSpec() {

        requestSpec = new RequestSpecBuilder().
                setBaseUri("http://localhost:9876").
                build();

    }
}
