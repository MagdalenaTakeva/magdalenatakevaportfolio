package wiremockexamples;

import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import models.LoanDetails;
import models.LoanRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static io.restassured.RestAssured.given;

/*
With @WireMockTest(httpPort = 9876) the WireMock server will be started before the first
test method in the test class and stopped after the last test method has completed.
 */
@WireMockTest(httpPort = 9876)
public class Chapter11Test {
    /*
    Verifying incoming requests
        _Apart from returning responses, we can verify that incoming requests have certain properties
        _ Fail a test if these verifications aren’t met
        _We can do this with WireMock in a way very similar to mocking frameworks for unit tests(e.g., Mockito for Java)
     */

    private RequestSpecification requestSpec;

    @BeforeEach
    public void createRequestSpec() {

        requestSpec = new RequestSpecBuilder().
                setBaseUri("http://localhost").
                setPort(9876).
                build();
    }

    public void setupStubServiceUnavailable() {

        stubFor(post(urlEqualTo("/requestLoan"))
                .willReturn(aResponse()
                        .withStatus(503)
                        .withStatusMessage("Loan processor service unavailable")
                ));
    }

    @Test
    public void testExercise501() {

        setupStubServiceUnavailable();

        given().
                spec(requestSpec).
                when().
                post("/requestLoan").
                then().
                assertThat().
                statusCode(503);

        /**
         * Add a verification to this test that verifies that exactly one HTTP POST
         * has been submitted to the /requestLoan endpoint
         */

        verify(exactly(1), postRequestedFor(urlEqualTo("/requestLoan")));

    }

    @ParameterizedTest(name = "Request a loan for ${0}")
    @CsvSource({
            "1000",
            "1500",
            "2000"
    })
    public void testExercise502(String loanAmount) {

        setupStubServiceUnavailable();

        LoanDetails loanDetails = new LoanDetails(Integer.parseInt(loanAmount), 100, "pending");
        LoanRequest loanRequest = new LoanRequest(12212, loanDetails);

        given().
                spec(requestSpec).
                and().
                contentType(ContentType.JSON).
                and().
                body(loanRequest).
                log().all().
                when().
                post("/requestLoan").
                then().
                assertThat().
                statusCode(503);

        /**
         * Add a verification to this test that verifies after each test that
         * - exactly 1 HTTP POST has been submitted to the /requestLoan endpoint
         * - the request contained a Content-Type header with a value containing 'application/json'
         * - the request body contained a field loanDetails.amount with a value equal to the
         *   loanAmount variable supplied to the test
         *   (hint: see setupStubExercise205() in WireMockAnswers2Test.java)
         */

        verify(exactly(1), postRequestedFor(
                urlEqualTo("/requestLoan"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(matchingJsonPath(
                        String.format("$.loanDetails[?(@.amount == '%s')]", loanAmount))));

    }


}
