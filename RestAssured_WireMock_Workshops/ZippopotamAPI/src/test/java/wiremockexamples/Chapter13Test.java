package wiremockexamples;

import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import wiremockexamples.extensions.HttpDeleteFilter;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class Chapter13Test {
    private RequestSpecification requestSpec;

    /*
    An extension can be registered using:
        - its class name (“com.example.HttpDeleteFilter”)
        - the class (HttpDeleteFilter.class)
        - an instance (new HttpDeleteFilter())
     */
    @RegisterExtension
    static WireMockExtension wireMock = WireMockExtension.newInstance().
            options(wireMockConfig().
                    port(9876).
                    extensions(new HttpDeleteFilter())).
            build();

   @BeforeEach
    public void createRequestSpec() {

        requestSpec = new RequestSpecBuilder().
                setBaseUri("http://localhost").
                setPort(9876).
                build();
    }

    public void stubForHttpDeleteFilter() {

        wireMock.stubFor(delete(urlEqualTo("/delete-me"))
                .willReturn(aResponse()
                        .withStatus(403)
                        .withBody("HTTP DELETE is not allowed!")
                ));
    }

    @Test
    public void callWireMockWithDeleteMethod_checkStatusCodeEquals403_checkErrorsTitleInResponseBody_expectHttpDeleteIsNotAllowed() {

        stubForHttpDeleteFilter();

        /***
         * Use this test to test implementation of the request filter
         * As HTTP method used is DELETE, Response status returned is HTTP 403 Forbidden and request is stopped being processed
         * Errors[0].title in response body should equal "HTTP DELETE is not allowed!"
         */

        given().
                spec(requestSpec).
                when().
                delete("/delete-me").
                then().
                assertThat().
                log().all().
                statusCode(403).
                and().
                body("errors[0].title", equalTo("HTTP DELETE is not allowed!"));
    }
}
