package bookingApi;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class BDDStylePostRequest extends BaseTest {

    @Test
    public void passPayloadToPostRequestBDDStyle_expectStatusCode200_BodyTokenNotNull_bodyTokenLength15_bodyTokenIsAlphanumericValue(){

        String jsonString = "{\n" +
                "    \"username\" : \"admin\",\n" +
                "    \"password\" : \"password123\"\n" +
                "}";

        given().
                spec(requestSpec).
                body(jsonString).
        when().
                post("/auth").
        then().
                assertThat().
                log().body().
                statusCode(200).
                body("token", notNullValue()).
                body("token.length()", is(15)).
                body("token", matchesRegex("^[a-z0-9]+$"));
    }

}
