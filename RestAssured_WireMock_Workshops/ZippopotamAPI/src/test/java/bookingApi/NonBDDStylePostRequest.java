package bookingApi;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

public class NonBDDStylePostRequest {
    String jsonString = "{\n" +
            "    \"username\" : \"admin\",\n" +
            "    \"password\" : \"password123\"\n" +
            "}";

    // Create a request specification
    RequestSpecification request = RestAssured.given();

    @Test
    public void passPayloadToPostRequestNoNBDDStyle_expectStatusCode200_BodyTokenNotNull_bodyTokenLength15_bodyTokenIsAlphanumericValue(){
        request.contentType(ContentType.JSON);
        request.baseUri("https://restful-booker.herokuapp.com/auth");
        request.body(jsonString);

        // Calling POST method on URI. After hitting we get Response
        Response response = request.post();

        // Printing Response as string
        System.out.println(response.asString());

        // Get Validatable response to perform validation
        ValidatableResponse validatableResponse = response.then();

        // Validate status code as 200
        validatableResponse.statusCode(200);

        // Validate token field is null
        validatableResponse.body("token", Matchers.notNullValue());

        // Validate token length is 15
        validatableResponse.body("token.length()", Matchers.is(15));

        // Validate token is an alphanumeric value
        validatableResponse.body("token", Matchers.matchesRegex("^[a-z0-9]+$"));
    }

}
