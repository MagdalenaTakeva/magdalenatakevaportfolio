package bookingApi;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class BDDStylePutRequest extends BaseTest{

    @Test
    public void updateRequestBody_expectStatusCode200_expectFirstNameAmod_lastNameMahajan(){
        String jsonString1 = "{\n" +
                "    \"username\" : \"admin\",\n" +
                "    \"password\" : \"password123\"\n" +
                "}";

        String jsonString2 = "{\n" +
                "    \"firstname\" : \"Maggie\",\n" +
                "    \"lastname\" : \"Takeva\",\n" +
                "    \"totalprice\" : 111,\n" +
                "    \"depositpaid\" : true,\n" +
                "    \"bookingdates\" : {\n" +
                "        \"checkin\" : \"2018-01-01\",\n" +
                "        \"checkout\" : \"2019-01-01\"\n" +
                "    },\n" +
                "    \"additionalneeds\" : \"Breakfast\"\n" +
                "}";

        // First generate token and store it in variable for late use with PUT request
        String token = given().spec(requestSpec).body(jsonString1).when().post("/auth").then().log().all().extract().path("token");

        given().
                spec(requestSpec).
                cookie("token", token).
                body(jsonString2).
                when().
                put("/booking/1").
                then().
                assertThat().
                log().all().
                statusCode(200).
                body("firstname", Matchers.equalTo("Maggie")).
                body("lastname", Matchers.equalTo("Takeva"));


    }

}
