package examples;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import io.restassured.http.ContentType;
import org.junit.Test;

public class Chapter1Test extends BaseTest {
    @Test
    public void requestUsZipCode90210_checkPlaceNameInResponseBody_expectBeverlyHills(){
        given().
                when().
                    spec(requestSpec).
                    get("us/90210").
                then().
                    assertThat().
                    body("places[0].'place name'", equalTo("Beverly Hills"));
    }

    @Test
    public void requestUsZipCode90210_checkListOfPlaceNamesInResponseBody_expectContainsBeverlyHills(){
        given().
            when().
                spec(requestSpec).
                get("us/90210").
            then().
                assertThat().
                body("places.'place name'", hasItem("Beverly Hills"));
    }

    @Test
    public void requestUsZipCode90210_checkStatusCode_expectHTTP200(){
        given().
            when().
                spec(requestSpec).
                get("us/90210").
            then().
                 assertThat().
                 statusCode(200);
    }

    @Test
    public  void requestUsZipCode90210_checkContentType_expectContentTypeJSON(){
        given().
             when().
                spec(requestSpec).
                get("us/90210").
             then().
                assertThat().
                contentType(ContentType.JSON);
    }

    @Test
    public void requestUsZipCode_logRequestAndResponseDetails(){
        given().
                when().
                    log().all().
                    spec(requestSpec).
                    get("us/90210").
                then().
                     log().body();
    }

    @Test
    public void requestUsZipCode90210_checkStateNameInResponseBody_expectCalifornia(){
        given().
                when().
                    spec(requestSpec).
                    get("us/90210").
                then().
                    assertThat().
                    body("places[0].state", equalTo("California"));
    }

    @Test
    public void requestUsZipCode90210_checkNumberOfPlaceNamesInResponseBody_expectOne(){
        given().
                when().
                    spec(requestSpec).
                    get("us/90210").
                then().
                    assertThat().
                    body("places.'place name'", hasSize(1));
    }
}
