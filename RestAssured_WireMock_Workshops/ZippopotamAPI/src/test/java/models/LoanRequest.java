package models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor @AllArgsConstructor
public class LoanRequest {
    private int customerId;
    private LoanDetails loanDetails;
}
